# KeenType

KeenType is a modernized version of a Java-based implementation of the
[New Typesetting System](https://ctan.org/tex-archive/systems/nts),
which was heavily based on Donald E. Knuth's original TeX.

This implementation provides a 100% pure Java library that can produce
scalable vector graphics of mathematical expressions. All required
fonts, font metrics, glyph mappings, and plain TeX files are included.

# Requirements

Building software requires:

* JDK 19
* Gradle 7.6

# Build

After cloning this repository, build the application as follows:

    gradle clean jar

The compiled artifact is `app/build/libs/keentype.jar`.

# Application

Run the application using any of the following commands:

    java -jar app/build/libs/keentype.jar docs/examples/sample.tex
    java -jar app/build/libs/keentype.jar docs/examples/equations.tex

Open the resulting SVG file in an SVG editor to see the results.

# Library

To use KeenType as a library:

1. Build the project.
1. Copy `keentype-lib.jar` into your application.

Typeset an equation to SVG as follows:

``` java
final var str = "$\\sigma=\\sqrt{\\sum\\limits_{i=1}^{k} p_i(x_i-\\mu)^2}$";
final var typesetter = new KeenType();
final var svg = typesetter.toSvg( str );
```

The `svg` string contains an SVG document.

Note that instantiating `KeenType` will load and parse Knuth's `plain.tex`
into memory. This is an expensive operation that will significantly reduce
performance if executed in a critical loop. Preferably, the instance is
reused until no longer needed. For example:

``` java
final var typesetter = new KeenType();

for( int i = 0; i < 1000; i++ ) {
  System.out.println( typesetter.toSvg( equation[ i ] ) );
}
```

Be sure to wrap TeX-based equations in `$` or `$$` prior to typesetting.

# Rendering

The [EchoSVG](https://github.com/css4j/echosvg) library is a modern fork of
Apache Batik that can rasterize SVG documents.

# Output

The following image shows various equations and mathematics being typeset in
the Java-based desktop Markdown editor, [KeenWrite](https://keenwrite.com):

![KeenType screeshot](docs/images/keentype-01.png)

# Organization

The code is organized as follows:

- app -- The main application launched when running the JAR file.
- docs -- Information about changes and source code history.
- dvi -- Typesets into the device independent format.
- fonts -- Unused fonts.
- lib -- Library for converting equations to renderable SVG documents.
- log -- Logs information to the console.
- svg -- Typesets into the scalable vector format.
- tex -- Implements the typesetting system (fails the [TRIPTEST](http://mirrors.ctan.org/info/knuth-pdf/tex/tripman.pdf)).

# License

See [LICENSE](LICENSE.txt) for details.

# Changes

See [CHANGELOG.md](docs/CHANGELOG.md) for details about the modifications
from the original implementation.


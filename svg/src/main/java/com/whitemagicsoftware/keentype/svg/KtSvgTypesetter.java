/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.svg;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.render.KtCoordinateStack;
import com.whitemagicsoftware.keentype.render.KtPendingRule;
import com.whitemagicsoftware.keentype.render.KtTurtle;
import com.whitemagicsoftware.keentype.tex.KtFileFormat;
import com.whitemagicsoftware.keentype.tex.KtFontInfo;
import com.whitemagicsoftware.keentype.tex.KtFontInformator;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.whitemagicsoftware.keentype.base.KtDimen.ZERO;
import static com.whitemagicsoftware.keentype.tex.KtFileFormat.SVG_EXT;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Responsible for typesetting characters into an SVG diagram.
 */
public final class KtSvgTypesetter implements KtTypesetter {
  private KtFileName mFileName;
  private OutputStream mOutput;

  private KtTurtle mTurtle;
  private KtCoordinateStack mStack;

  private KtFontMetric mCurrentFontMetric = KtFontMetric.NULL;
  private KtFontInfo mCurrentFontInfo = KtFontInfo.NULL;
  private KtPendingRule mPendingRule = KtPendingRule.NULL;

  private KtDimen mPageWidth;
  private KtDimen mPageHeight;

  private double mScale = 1.0;

  private final KtFontInformator mFontInf;
  private final Map<KtFontMetric, KtFontInfo> mFontMetrics =
    new HashMap<>( 64 );
  private final boolean mFullPage;

  private final KtSvgFormatWriter mWriter = new KtSvgFormatWriter();

  /**
   * Creates a new {@link KtTypesetter} capable of converting TeX macros into
   * an SVG document. The document will be created with the minimum width and
   * height that fully encompasses the TeX expression.
   *
   * @param fontInf Initial font information to use for drawing primitives.
   */
  public KtSvgTypesetter( final KtFontInformator fontInf ) {
    this( fontInf, false );
  }

  /**
   * Creates a new {@link KtTypesetter} capable of converting TeX macros into
   * an SVG document.
   *
   * @param fontInf  Initial font information to use for drawing primitives.
   * @param fullPage Set to {@code true} to use the page dimensions provided
   *                 when starting the page; use {@code false} to use the
   *                 natural dimensions derived from the extents of the
   *                 drawing primitive boundary.
   */
  public KtSvgTypesetter(
    final KtFontInformator fontInf,
    final boolean fullPage ) {
    assert fontInf != null;

    mFontInf = fontInf;
    mFullPage = fullPage;
  }

  @Override
  public void set( final char ch, final KtFontMetric metric ) {
    mTurtle.sync();

    if( !metric.equals( mCurrentFontMetric ) ) {
      final var info = mFontMetrics.computeIfAbsent(
        metric, k -> mFontInf.getInfo( metric )
      );

      mTurtle.font( info );

      mCurrentFontMetric = metric;
      mCurrentFontInfo = info;
    }

    final var w = mCurrentFontInfo.getCharWidth( ch );

    if( w != KtDimen.NULL ) {
      mTurtle.draw( ch );
      mTurtle.advance( w );
    }
  }

  public void set( final KtCharCode code, final KtFontMetric metric ) {
    set( code.toChar(), metric );
  }

  @Override
  public void setRule( final KtDimen h, final KtDimen w ) {
    if( !h.lessThan( 0 ) && !w.lessThan( 0 ) ) {
      mTurtle.sync();
      mPendingRule = new KtPendingRule( mTurtle, w, h );
    }
  }

  @Override
  public void setSpecial( final byte[] spec ) {
    mTurtle.sync();
  }

  @Override
  public void moveLeft( final KtDimen x ) {
    mTurtle.left( x );
    tryToSetPendRule();
  }

  @Override
  public void moveRight( final KtDimen x ) {
    mTurtle.right( x );
    tryToSetPendRule();
  }

  @Override
  public void moveUp( final KtDimen y ) {
    mTurtle.up( y );
  }

  @Override
  public void moveDown( final KtDimen y ) {
    mTurtle.down( y );
  }

  @Override
  public KtMark mark() {
    return mTurtle.mark();
  }

  @Override
  public void syncHoriz() {
    putPendRule();
    mTurtle.syncHorizontal();
  }

  @Override
  public void syncVert() {
    putPendRule();
    mTurtle.syncVertical();
  }

  @Override
  public void push() {
    putPendRule();
    mStack = mTurtle.stack( mStack );
  }

  @Override
  public void pop() {
    putPendRule();

    if( mStack != KtCoordinateStack.NULL ) {
      mTurtle.moveSvgTo( mStack.x(), mStack.y() );
      mStack = mStack.next();
    }
  }

  @Override
  public void startPage(
    final KtDimen yOffset,
    final KtDimen xOffset,
    final KtDimen height,
    final KtDimen width,
    final int[] paragraphs ) {
    assert yOffset != null;
    assert xOffset != null;
    assert height != null;
    assert width != null;

    mPageWidth = width.plus( xOffset );
    mPageHeight = height.plus( yOffset );

    mCurrentFontMetric = KtFontMetric.NULL;
    mCurrentFontInfo = KtFontInfo.NULL;

    mTurtle = new KtTurtle( ZERO, ZERO, xOffset, yOffset, mWriter );
    mStack = KtCoordinateStack.NULL;
  }

  @Override
  public int pageCount() {
    return 1;
  }

  @Override
  public void endPage() { }

  @Override
  public void startDocument( final OutputStream os ) {
    mOutput = os;
    mWriter.reset();
  }

  @Override
  public void close() {
    final var svg = mFullPage
      ? mWriter.export( mPageWidth.toDouble(), mPageHeight.toDouble() )
      : mWriter.export( mScale );

    try {
      mOutput.write( svg.getBytes( UTF_8 ) );
    } catch( final IOException e ) {
      throw new UncheckedIOException( e );
    } finally {
      try {
        mOutput.close();
      } catch( final IOException ignored ) { }
    }
  }

  private void putPendRule() {
    if( mPendingRule != KtPendingRule.NULL ) {
      mPendingRule.put();
      mPendingRule = KtPendingRule.NULL;
    }
  }

  private void tryToSetPendRule() {
    if( mPendingRule != KtPendingRule.NULL && mPendingRule.canBeSet() ) {
      mPendingRule.set();
      mPendingRule = KtPendingRule.NULL;
    }
  }

  @Override
  public void setScale( final double scale ) {
    assert scale > 0;

    mScale = scale;
  }

  @Override
  public void setFileName( final KtFileName fileName ) {
    assert fileName != null;
    mFileName = addExtension( fileName );
  }

  @Override
  public Optional<KtFileName> getFileName() {
    return Optional.ofNullable( mFileName );
  }

  @Override
  public KtFileFormat getExtension() {
    return SVG_EXT;
  }
}

/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.svg;

import com.whitemagicsoftware.keentype.render.KtAbstractCanvas;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Responsible for building a Document Object Model version of a TeX formula
 * in SVG XML format. This is an optimization that avoids first creating an
 * SVG string followed by a DOM.
 */
public final class KtDomFormatWriter extends KtAbstractCanvas<Document> {

  private static final Document sDocument;

  static {
    try {
      final var dbf = DocumentBuilderFactory.newInstance();
      final var db = dbf.newDocumentBuilder();
      sDocument = db.newDocument();
    } catch( final ParserConfigurationException e ) {
      throw new RuntimeException( e );
    }
  }

  @Override
  public void drawString( final String s, final double x, final double y ) {
  }

  @Override
  public void drawLine( final double x1, final double y1,
                        final double x2, final double y2,
                        final double thickness ) {
  }

  @Override
  public Document export() {
    return sDocument;
  }

  public Document export( final double scale ) {
    return sDocument;
  }

  @Override
  public Document export( final double w, final double h, final double scale ) {
    return sDocument;
  }
}

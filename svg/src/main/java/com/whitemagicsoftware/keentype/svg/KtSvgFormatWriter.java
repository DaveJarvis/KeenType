/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.svg;

import com.whitemagicsoftware.keentype.render.KtCanvas;
import com.whitemagicsoftware.keentype.render.KtFastDouble;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Path2D;

import static com.whitemagicsoftware.keentype.render.KtFontReader.DEFAULT_FONT;

/**
 * Responsible for building an SVG version of a TeX formula. Both Batik and
 * JFreeSVG can accomplish the same thing, but they are general-purpose
 * solutions for a greater problem set.
 * <p>
 * This class is not thread-safe and can be {@link #reset()} for performance.
 * </p>
 */
public final class KtSvgFormatWriter implements KtCanvas<String> {
  /**
   * Precision when computing final canvas dimensions.
   */
  private static final int PRECISION = 4;

  /**
   * Initial SVG string buffer size.
   */
  private static final int BUFFER_SIZE = 65536;
  private static final String HEADER =
    "<svg xmlns='http://www.w3.org/2000/svg' version='1.1' ";

  private static final FontRenderContext RENDER_CONTEXT =
    new FontRenderContext( null, true, true );

  /**
   * Font used when drawing glyph paths.
   */
  private Font mFont = DEFAULT_FONT;

  /**
   * Initialized with a capacity of {@link #BUFFER_SIZE} to minimize the
   * number of memory re-allocations. This is the primary document modified
   * while the clients call various drawing primitives.
   */
  private final StringBuilder mSvg = new StringBuilder( BUFFER_SIZE );

  /**
   * The final document is populated upon calling either {@link #export()} or
   * {@link #export(double, double)}.
   */
  private final StringBuilder mDoc = new StringBuilder( BUFFER_SIZE );

  /**
   * Filled when drawing paths, not thread-safe.
   */
  private final float[] mCoords = new float[ 6 ];

  /**
   * Set to {@code true} by calling {@link #export(double, double)}. Set back to
   * {@code false} by calling {@link #reset()}.
   */
  private boolean mClosed;

  /**
   * Maximum canvas width extent, updated while drawing.
   */
  private double mW;

  /**
   * Maximum canvas height extent, updated while drawing.
   *
   * @see #export()
   */
  private double mH;

  /**
   * Creates a new instance for typesetting equations.
   *
   * @see #export()
   */
  public KtSvgFormatWriter() { }

  /**
   * Changes the {@link Font} used when drawing glyphs.
   *
   * @param font The new text {@link Font}.
   */
  @Override
  public void setFont( final Font font ) {
    assert font != null;
    mFont = font;
  }

  /**
   * Adds a string to the vector graphic.
   *
   * @param s The string to place on the graphics.
   * @param x The upper-left X coordinate.
   * @param y The upper-left Y coordinate.
   */
  @Override
  public void drawString( final String s, final double x, final double y ) {
    assert s != null;

    final var glyphs = mFont.createGlyphVector( RENDER_CONTEXT, s );
    final var path = (Path2D) glyphs.getOutline( (float) x, (float) y );

    mSvg.append( "<g><path " );

    if( path.getWindingRule() == 0 ) {
      mSvg.append( "fill-rule='evenodd' " );
    }

    mSvg.append( "d='" );

    final var iterator = path.getPathIterator( null );

    while( !iterator.isDone() ) {
      switch( iterator.currentSegment( mCoords ) ) {
        case 0 -> mSvg.append( 'M' )
                      .append( toGeometryPrecision( mCoords[ 0 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 1 ] ) );
        case 1 -> mSvg.append( 'L' )
                      .append( toGeometryPrecision( mCoords[ 0 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 1 ] ) );
        case 2 -> mSvg.append( 'Q' )
                      .append( toGeometryPrecision( mCoords[ 0 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 1 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 2 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 3 ] ) );
        case 3 -> mSvg.append( 'C' )
                      .append( toGeometryPrecision( mCoords[ 0 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 1 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 2 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 3 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 4 ] ) )
                      .append( ' ' )
                      .append( toGeometryPrecision( mCoords[ 5 ] ) );
        case 4 -> mSvg.append( 'Z' );
      }

      iterator.next();
    }

    mSvg.append( "'/></g>" );

    final var bounds = path.getBounds();

    updateExtents(
      bounds.getX() + bounds.getWidth(),
      bounds.getY() + bounds.getHeight()
    );
  }

  public void drawLine(
    final double x1, final double y1,
    final double x2, final double y2,
    final double strokeWidth ) {
    mSvg.append( "<g><line " )
        .append( "stroke='#000000' " )
        .append( "stroke-linecap='round' " )
        .append( "stroke-width='" )
        .append( strokeWidth )
        .append( "' x1='" ).append( toGeometryPrecision( x1 ) )
        .append( "' y1='" ).append( toGeometryPrecision( y1 ) )
        .append( "' x2='" ).append( toGeometryPrecision( x2 ) )
        .append( "' y2='" ).append( toGeometryPrecision( y2 ) )
        .append( "'/></g>" );
    updateExtents( x2, y2 );
  }

  @Override
  public String export( final double scale ) {
    return export( mW + 1, mH + 1, scale );
  }

  /**
   * Calling this method is idempotent until a call to {@link #reset()} is
   * made. No checks are made to ensure  that the document is in a valid
   * state prior to writing the closing {@code svg} element.
   * <p>
   * We avoid overriding {@link #toString()} for this purpose because it can
   * interfere with debugging.
   * </p>
   *
   * @param w The final document width.
   * @param h The final document height.
   * @return An SVG string representing the primitives called while drawing.
   */
  @Override
  public String export( final double w, final double h, final double scale ) {
    if( mClosed ) {
      return mDoc.toString();
    }

    mClosed = true;

    final var tScale = KtFastDouble.toString( scale, PRECISION );

    return mDoc.append( HEADER )
               .append( "width='" )
               .append( KtFastDouble.toString( w * scale, PRECISION ) )
               .append( "' height='" )
               .append( KtFastDouble.toString( h * scale, PRECISION ) )
               .append( "'><g transform='scale(" )
               .append( tScale )
               .append( ',' )
               .append( tScale )
               .append( ")'>" )
               .append( mSvg )
               .append( "</g></svg>" )
               .toString();
  }

  /**
   * Resets the "canvas" to start writing after the {@link #HEADER} text. This
   * resets the idempotency of {@link #export(double, double)}.
   */
  public void reset() {
    mSvg.setLength( 0 );
    mDoc.setLength( 0 );
    mW = 0;
    mH = 0;
    mClosed = false;
  }

  /**
   * When drawing primitives are called, track the maximum width and height
   * so that a minimal page boundary can be formed around the SVG document.
   * If either the given width or height exceed the current maximum extents,
   * then extents are embiggened to enclose the whole diagram.
   *
   * @param w The width boundary to compare against the running maximum.
   * @param h The height boundary to compare against the running maximum.
   */
  private void updateExtents( final double w, final double h ) {
    if( w > mW ) {
      mW = w;
    }

    if( h > mH ) {
      mH = h;
    }
  }
}

/**
 * This package contains instructions for transforming TeX drawing primitives
 * into the Scalable Vector Graphic (SVG) file format.
 */
package com.whitemagicsoftware.keentype.svg;

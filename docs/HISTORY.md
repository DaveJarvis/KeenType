# History

The New Typesetting system (NTS) version 1.00-beta started in 1992. The
inaugural meeting was held on September 23-25, 1993 during the autumn
meeting of DANTE e.V. at Kaiserslautern (Germany). The following people
participated:

- Peter Breitenlohner
- Joachim Lammarsch
- Mariusz Olko
- Bernd Raichle
- Rainer M. Sch�pf
- Friedhelm Sowa
- Joachim Schrod
- Philip Taylor
- Jiri Zlatuska

In 2001, the program became complete enough to be released as a beta version.

## Sponsors

The project would not have been possible without the sponsorship of several
local TeX user groups and one anonymous individual sponsor. The sponsoring
groups were:

- [CSTG](http://www.cstug.cz)
- [DANTE e.V.](http://www.dante.de)
- [GUST](http://www.gust.org.pl)
- [GUTenberg](http://www.gutenberg.eu.org)
- [NTG](http://www.ntg.nl)
- [TUG USA](http://www.tug.org)

## NTS Team

Although all of the NTS code was initially written by one person, a team
provided useful suggestions, feedback, and public relations. At the time
of the first official release, the members of the team were:

- Peter Breitenlohner
- Hans Hagen (Project Leader)
- Taco Hoekwater
- Jerzy Ludwichowski
- Karel Skoupy (Implementor)
- Philip Taylor (Technical Director)
- Jiri Zlatuska (Managing Director)

The project was hosted by the faculty of informatics of Masaryk University
represented by Ludek Matyska. DANTE e.V. arranged the financial aspects and
hosted meetings. DANTE e.V. is represented in the team by Volker RW Schaa.

Many people participated in email discussions, made suggestions, provided
input and insights, and gave feedback and error reports. We note the
motivating contributions of:

- Simon Pepping
- Fabrice Popineau
- Bernd Raichle
- Petr Sojka

And a very special thanks to
[Donald E. Knuth](https://www-cs-faculty.stanford.edu/~knuth/) for
[TeX](https://en.wikipedia.org/wiki/TeX).


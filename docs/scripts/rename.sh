#!/usr/bin/env bash

# This script can be used to bulk rename Java source files, along with all
# references, throughout the code base en masse. This is required to honour
# the license agreement that states any modifications must be produced under
# a different file name.

# Source: https://news.ycombinator.com/item?id=34495867

# Move the script to the root level before running.

javas=$(find . -regex '.*\.java$')
sed -i -E "$(printf 's/\\<(%s)\\>/Kt\\1/g;' $(grep -hrPo '\b(class|interface|record|enum) (?!Kt)(?!List\b)(?!Entry\b)\K[A-Z]\w+'))" $(echo $javas); 
perl-rename 's;\b(?!Kt)(\w+[.]java)$;Kt$1;' $(echo $javas)


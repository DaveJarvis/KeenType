// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.log;

import com.whitemagicsoftware.keentype.bus.KtBus;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.command.KtInteractionPrim;
import com.whitemagicsoftware.keentype.events.*;
import com.whitemagicsoftware.keentype.io.*;
import com.whitemagicsoftware.keentype.tex.*;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.whitemagicsoftware.keentype.tex.KtFileFormat.LOG_EXT;
import static com.whitemagicsoftware.keentype.tex.KtTeXCharMapper.KtTeXFileName;
import static com.whitemagicsoftware.keentype.tex.KtTeXIOHandler.openWriter;
import static java.util.Locale.ENGLISH;

/**
 * Clients can instantiate this class to establish TeX-like behaviour of
 * opening the log file to contain typesetting status information.
 */
@SuppressWarnings( "unused" )
public class KtLogEventHandler {
  {
    KtBus.register( this );
  }

  private final KtConfig mConfig;
  private final KtInputLine mFirstLine;
  private final KtTeXCharMapper mMapper;

  private KtLineOutput mLogFile = KtLineOutput.NULL;
  private KtFileName mLogName = KtFileName.NULL;

  public KtLogEventHandler(
    final KtConfig config,
    final KtInputLine firstLine,
    final KtTeXCharMapper mapper
  ) {
    assert config != null;
    assert firstLine != null;
    assert mapper != null;

    mConfig = config;
    mFirstLine = firstLine;
    mMapper = mapper;
  }

  @Subscribe
  public void handle( final KtOpenLogEvent event ) {
    KtFileName name = new KtTeXFileName( event.getJobName() );
    String ext = LOG_EXT.toString();
    name.append( '.' );
    name.append( ext );

    Writer wr;

    for( ; ; ) {
      try {
        wr = openWriter( name, LOG_EXT );
        break;
      } catch( final IOException e ) {
        // XXX enable term [535]
        cantOpen( name );
        name = promptFileName();
        name.addDefaultExt( ext );
      }
    }
    final KtWriterLineOutput out = new KtWriterLineOutput(
      wr, mMapper, false, KtTeXConfig.MAX_PRINT_LINE
    );
    out.addRaw( KtTeXConfig.BANNER );
    DateFormat fmt = new SimpleDateFormat( "d MMM yyyy HH:mm", ENGLISH );
    Calendar cal = mConfig.date();
    cal.setLenient( true );
    fmt.setCalendar( cal );
    out.add( "  " + fmt.format( cal.getTime() ).toUpperCase() );
    out.startLine();
    out.add( "**" );
    final KtLog log = new KtStandardLog( out, mMapper );
    mFirstLine.addContext( log, log );
    out.endLine();
    mLogName = name;
    mLogFile = out;
    KtCommand.setLogFile( mLogFile );
  }

  @Subscribe
  public void handle( final KtCloseLogEvent event ) {
    if( mLogFile != KtLineOutput.NULL ) {
      mLogFile.endLine();
      mLogFile.close();
    }

    mLogName = KtFileName.NULL;
    mLogFile = KtLineOutput.NULL;
  }

  @Subscribe
  public void handle( final KtTypesettingBeganEvent event ) { }

  @Subscribe
  public void handle( final KtTypesettingEndedEvent event ) {
    final var mTypesetter = event.getTypesetter();
    final var fileName = mTypesetter.getFileName();
    final var pages = mTypesetter.pageCount();

    getLog()
      .startLine()
      .add( "Output written on " )
      .add( fileName.orElseGet( () -> new KtTeXFileName( "null" ) ) )
      .add( " (" )
      .add( pages )
      .add( ' ' )
      .add( pages == 1 ? "page" : "pages" )
      .add( ".)" );
  }

  public KtFileName getLogName() {
    return mLogName;
  }

  private void cantOpen( final KtFileName name ) {
    getLog().startLine()
            .add( "! I can't write on file `" )
            .add( name )
            .add( "'." );
  }

  private KtFileName promptFileName() {
    getLog().startLine()
            .add( "Please type another transcript file name" );

    if( !KtInteractionPrim.isInteractive() ) {
      fatalError();
    }

    KtInputLine line = KtCommand.promptInput( ": " );
    KtFileName name = new KtTeXFileName();
    KtCharCode code;
    line.skipSpaces();

    do {
      code = line.getNextRawCode();
    }
    while( code != KtInputLine.EOL && name.accept( code ) > 0 );

    return name;
  }

  private void fatalError() {
    KtInteractionPrim.setScroll();
    KtTeXError err = KtTeXErrorPool.get( "MissingFile" );

    if( KtCommand.isFileLogActive() ) {
      KtErrorEvent.publish( err, null, true, false );
    }
    else {
      err.addText( getLog().startLine().add( "! " ), null );
    }

    throw new KtFatalError( "MissingFile" );
  }

  private KtLog getLog() {
    return KtCommand.normLog;
  }
}

// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.log;

import com.whitemagicsoftware.keentype.bus.KtBus;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.tex.*;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.events.KtErrorEvent;
import com.whitemagicsoftware.keentype.events.KtResetErrorCountEvent;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtInputLine;
import com.whitemagicsoftware.keentype.tex.*;
import org.greenrobot.eventbus.Subscribe;

/**
 * Clients can instantiate this class to establish TeX-like behaviour of
 * prompting for input when errors are encountered.
 */
@SuppressWarnings( "unused" )
public class KtErrorEventHandler {
  {
    KtBus.register( this );
  }

  private final KtTeXTokenMaker mMaker;
  private final KtContextDisplay mContextDisplay;

  private int mErrorCount;

  public KtErrorEventHandler(
    final KtTeXTokenMaker maker,
    final KtContextDisplay contextDisplay ) {
    assert maker != null;
    assert contextDisplay != null;

    mMaker = maker;
    mContextDisplay = contextDisplay;
  }

  @Subscribe
  public void handle( final KtResetErrorCountEvent event ) {
    mErrorCount = 0;
  }

  /**
   * Handle processing error messages during typesetting.
   *
   * @param event The event published when a problem is encountered.
   */
  @Subscribe
  public void handle( final KtErrorEvent event ) {
    final boolean counting = event.isCounting();
    final boolean delAllowed = event.isDelAllowed();

    final var log = KtCommand.normLog;

    if( counting ) {
      log.startLine().add( "! " );
      event.addText( log );
    }

    log.add( '.' );
    KtCommand.getTokStack().show( mContextDisplay );

    if( KtInteractionPrim.isErrStopping() ) {
      int currHelp = 0;

      for( ; ; ) {
        log.endLine();
        KtCommand.getTokStack().cleanFinishedInserts();
        KtInputLine line = KtCommand.promptInput( "? " );
        KtCharCode cmd = line.getNextRawCode();
        if( cmd == KtInputLine.EOL ) { break; }
        char c = cmd.toChar();
        KtFilePos pos = KtCommand.getTokStack().filePos();

        switch( Character.toUpperCase( c ) ) {
          case 'S' -> {
            setInteraction( "scrollmode" );
            return;
          }
          case 'R' -> {
            setInteraction( "nonstopmode" );
            return;
          }
          case 'Q' -> {
            setInteraction( "batchmode" );
            return;
          }
          case 'I' -> {
            String desc;
            if( line.empty() ) {
              line = KtCommand.promptInput( "insert>" );
              desc = "<insert>  ";
            }
            else {
              line = line.pureRest();
              desc = "<insert>   ";
            }
            final var tokenizer = new KtInputLineTokenizer( line, mMaker, desc );
            tokenizer.setMidLine();
            KtCommand.getTokStack().push( tokenizer );
            return;
          }
          case 'E' -> {
            if( pos != KtFilePos.NULL ) {
              KtInteractionPrim.setScroll();
              throw new KtEditException( pos );
            }
          }
          default -> {
            if( delAllowed && Character.isDigit( c ) ) {
              int count = Character.digit( c, 10 );
              cmd = line.getNextRawCode();
              if( cmd != KtInputLine.EOL ) {
                c = cmd.toChar();
                if( Character.isDigit( c ) ) {
                  count = count * 10 + Character.digit( c, 10 );
                }
              }
              while( count-- > 0 ) { KtCommand.nextRawToken(); }
              KtCommand.getTokStack().show( mContextDisplay );
              currHelp = 2;
              continue;
            }
          }
          case 'H' -> {
            switch( currHelp ) {
              case 0 -> event.addHelp( log );
              case 1 -> KtTeXErrorPool.addHelpAfterHelp( log );
              case 2 -> KtTeXErrorPool.addHelpAfterDel( log );
            }
            currHelp = 1;
            continue;
          }
          case 'X' -> {
            KtInteractionPrim.setScroll();
            throw new KtFatalError( "User stop" );
          }
          // break;
        }
        log.add( "Type <return> to proceed, " )
           .add( "S to scroll future error messages," )
           .endLine()
           .add( "R to run without stopping, " )
           .add( "Q to run quietly," )
           .endLine()
           .add( "I to insert something, " );
        if( pos != KtFilePos.NULL ) { log.add( "E to edit your file," ); }
        if( delAllowed ) {
          log.startLine()
             .add( "1 or ... or 9 to ignore " )
             .add( "the next 1 to 9 tokens of input," );
        }
        log.startLine().add( "H for help, X to quit." );
      }
    }
    else {
      if( counting && ++mErrorCount >= KtTeXConfig.MAX_ERROR_COUNT ) {
        log.startLine()
           .add( "(That makes " + mErrorCount + " errors; please try again.)" );
        throw new KtFatalError( "Too many errors" );
      }

      event.addDesc( KtCommand.fileLog );
      KtCommand.fileLog.endLine();
      log.endLine();
    }
  }

  private void setInteraction( final String mode ) {
    mErrorCount = 0;
    KtInteractionPrim.set( mode );
    KtCommand.normLog.add( "OK, entering " ).add( mode ).add( "..." ).endLine();
  }
}

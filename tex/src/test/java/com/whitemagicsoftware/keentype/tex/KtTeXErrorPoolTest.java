/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.tex;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class KtTeXErrorPoolTest {

  /**
   * Tests that the number of errors that the typesetting system knows about
   * is equal to the number of identifiers defined in the pool.
   */
  @Test
  void test_Pool_IdentifiedErrors_AllAccounted() {
    final var expected = 85;

    final var ids = KtTeXErrorPool.idents();
    int actual;

    for( actual = 0; ids.hasNext(); ids.next() ) {
      actual++;
    }

    assertEquals( expected, actual );
  }
}

/**
 * This package contains a partial implementation of Donald E. Knuth's
 * TeX typesetting macro language. The implementation was developed by
 * <a href="https://www.dante.de/">DANTE e.V.</a>, updated by
 * <a href="https://jpfennell.com/">James Fennell</a>, the revised
 * by <a href="https://whitemagicsoftware.com/">White Magic Software, Ltd.</a>
 * <p>
 * This implementation fails the
 * <a href="http://mirrors.ctan.org/info/knuth-pdf/tex/tripman.pdf">TRIPTEST</a>.
 * </p>
 */

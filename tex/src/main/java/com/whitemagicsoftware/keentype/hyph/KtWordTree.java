// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.hyph.WordTree
// $Id: KtWordTree.java,v 1.1.1.1 2000/06/13 12:40:09 ksk Exp $
package com.whitemagicsoftware.keentype.hyph;

import java.io.Serializable;
import java.util.Objects;

public class KtWordTree implements KtWordMap {

  public static final KtWordTree NULL = null;
  public static final Entry NULL_ENTRY = null;

  private static class Entry implements Serializable {

    final char code;
    Object value;
    Entry below;
    Entry next;

    public Entry(char code, Entry next) {
      this.code = code;
      this.value = null;
      this.below = NULL_ENTRY;
      this.next = next;
    }
  }

  private final Entry root = new Entry('\000', NULL_ENTRY);
  private char maxCode = 0;

  private static boolean sameObjects(Object x, Object y) {
    return Objects.equals( x, y );
  }

  private Entry find(Entry from, char code) {
    for (Entry ent = from.below; ent != NULL_ENTRY; ent = ent.next)
      if (code == ent.code) return ent;
      else if (code < ent.code) break;
    return NULL_ENTRY;
  }

  private Entry get(Entry from, char code) {
    Entry ent;
    boolean first = true;
    for (ent = from.below; ent != NULL_ENTRY; ent = from.next) {
      if (code == ent.code) return ent;
      else if (code < ent.code) break;
      first = false;
      from = ent;
    }
    ent = new Entry(code, ent);
    if (first) from.below = ent;
    else from.next = ent;
    if (maxCode < code) maxCode = code;
    return ent;
  }

  private Entry find(Entry from, String word) {
    for (int i = 0; i < word.length() && from != NULL_ENTRY; i++) from = find(from, word.charAt(i));
    return from;
  }

  private Entry get(Entry from, String word) {
    for (int i = 0; i < word.length(); i++) from = get(from, word.charAt(i));
    return from;
  }

  public Object get(String word) {
    Entry ent = find(root, word);
    return ent != NULL_ENTRY ? ent.value : null;
  }

  public Object put(String word, Object value) {
    Entry ent = get(root, word);
    Object old = ent.value;
    ent.value = value;
    return old;
  }

  public class KtSeeker implements KtWordMap.KtSeeker {

    private Entry curr = root;

    public void reset() {
      curr = root;
    }

    public boolean isValid() {
      return curr != NULL_ENTRY;
    }

    public void seek(char code) {
      if (curr != NULL_ENTRY) curr = find(curr, code);
    }

    public Object get() {
      return curr != NULL_ENTRY ? curr.value : null;
    }
  }

  public KtWordMap.KtSeeker seeker() {
    return new KtSeeker();
  }

  public static final KtPack NULL_PACK = null;

  private record KtPack(
    char code,
    Object value,
    KtWordTree.KtPack below,
    KtWordTree.KtPack next ) {

    @Override
    public boolean equals( Object obj ) {
      if( obj == this ) { return true; }
      if( !(obj instanceof KtPack that) ) { return false; }
      return code == that.code
        && sameObjects( value, that.value )
        && below == that.below
        && next == that.next;
    }

    @Override
    public int hashCode() {
      int h = 191 * code;
      if( value != null ) { h += 313 * value.hashCode(); }
      if( below != NULL_PACK ) { h += 1009 * below.hashCode(); }
      if( next != NULL_PACK ) { h += 1571 * next.hashCode(); }
      return h;
    }
  }
}

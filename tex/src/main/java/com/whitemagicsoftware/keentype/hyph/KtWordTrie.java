// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.hyph.WordTrie
// $Id: KtWordTrie.java,v 1.1.1.1 2000/10/06 08:31:34 ksk Exp $
package com.whitemagicsoftware.keentype.hyph;

import java.io.Serializable;

public class KtWordTrie implements KtWordMap {

  public static final KtWordTrie NULL = null;
  public static final Entry NULL_ENTRY = null;

  private static final Entry[] EMPTY_TABLE = {new Entry('\000', 0, null)};
  public static final KtWordTrie EMPTY = new KtWordTrie( EMPTY_TABLE );

  static class Entry implements Serializable {

    final char code;
    final int link;
    Object value;

    public Entry(char code, int link, Object value) {
      this.code = code;
      this.link = link;
      this.value = value;
    }
  }

  private final Entry[] table;

  public KtWordTrie(Entry[] table ) {
    this.table = table;
  }

  public Object get(String word) {
    int size = word.length();
    int curr = 0;
    for (int i = 0; i < size; i++) {
      char code = word.charAt(i);
      curr = table[curr].link;
      if (curr == 0) return null;
      curr += code;
      if (curr <= 0 || curr >= table.length) return null;
      Entry ent = table[curr];
      if (ent == NULL_ENTRY || ent.code != code) return null;
    }
    return table[curr].value;
  }

  public Object put(String word, Object value) {
    throw new UnsupportedOperationException("packed");
  }

  public class KtSeeker implements KtWordMap.KtSeeker {

    private int curr = 0;

    public void reset() {
      curr = 0;
    }

    public boolean isValid() {
      return curr >= 0;
    }

    public void seek(char code) {
      if (curr >= 0) {
        curr = table[curr].link;
        if (curr != 0) {
          curr += code;
          if (curr > 0 && curr < table.length) {
            Entry ent = table[curr];
            if (ent != NULL_ENTRY && ent.code == code) return;
          }
        }
        curr = -1;
      }
    }

    public Object get() {
      return curr > 0 ? table[curr].value : null;
    }
  }

  public KtWordMap.KtSeeker seeker() {
    return new KtSeeker();
  }
}

/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.events;

import com.whitemagicsoftware.keentype.bus.KtBus;

/**
 * Responsible for notifying listeners that their error count, if any, is to
 * be reset back to zero.
 */
public class KtResetErrorCountEvent {

  /**
   * Event carries no data.
   */
  private KtResetErrorCountEvent() { }

  /**
   * Notify listeners of a request to reset their error count. This allows
   * the system to reset the number of errors encountered while typesetting.
   */
  @SuppressWarnings( "InstantiationOfUtilityClass" )
  public static void publish() {
    KtBus.post( new KtResetErrorCountEvent() );
  }
}

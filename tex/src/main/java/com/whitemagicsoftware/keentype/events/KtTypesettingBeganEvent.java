/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.events;

import com.whitemagicsoftware.keentype.bus.KtBus;

/**
 * Denotes that type setting of a document has started.
 */
public class KtTypesettingBeganEvent {
  private KtTypesettingBeganEvent() { }

  @SuppressWarnings( "InstantiationOfUtilityClass" )
  public static void publish() {
    KtBus.post( new KtTypesettingBeganEvent() );
  }
}

package com.whitemagicsoftware.keentype.events;

import com.whitemagicsoftware.keentype.bus.KtBus;
import com.whitemagicsoftware.keentype.io.KtName;

/**
 * Wrapper for logging-related information.
 */
public class KtOpenLogEvent {

  private final KtName mJobName;

  private KtOpenLogEvent( final KtName jobName ) {
    assert jobName != null;

    mJobName = jobName;
  }

  public KtName getJobName() {
    return mJobName;
  }

  public static void publish( final KtName jobName ) {
    KtBus.post( new KtOpenLogEvent( jobName ) );
  }
}

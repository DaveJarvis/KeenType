/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.events;

import com.whitemagicsoftware.keentype.bus.KtBus;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.tex.KtTeXError;

/**
 * Wrapper for error-related information.
 */
public final class KtErrorEvent {

  private final KtTeXError mError;
  private final KtLoggable[] mParams;
  private final boolean mDelAllowed;
  private final boolean mCounting;

  private KtErrorEvent( final KtTeXError error ) {
    this( error, new KtLoggable[ 0 ], false, false );
  }

  private KtErrorEvent(
    final KtTeXError error,
    final KtLoggable[] params,
    final boolean counting,
    final boolean delAllowed ) {
    assert error != null;

    mError = error;
    mParams = params;
    mCounting = counting;
    mDelAllowed = delAllowed;
  }

  public boolean isCounting() {
    return mCounting;
  }

  public boolean isDelAllowed() {
    return mDelAllowed;
  }

  public void addText( final KtLog log ) {
    mError.addText( log, mParams );
  }

  public void addHelp( final KtLog log ) {
    mError.addHelp( log, mParams );
  }

  public void addDesc( final KtLog log ) {
    mError.addDesc( log, mParams );
  }

  public static void publish( final KtTeXError error ) {
    publish( error, null, false );
  }

  public static void publish(
    final KtTeXError error,
    final KtLoggable[] params,
    boolean delAllowed ) {
    publish( error, params, true, delAllowed );
  }

  public static void publish(
    final KtTeXError err,
    final KtLoggable[] params,
    final boolean counting,
    final boolean delAllowed ) {
    KtBus.post( new KtErrorEvent( err, params, counting, delAllowed ) );
  }
}

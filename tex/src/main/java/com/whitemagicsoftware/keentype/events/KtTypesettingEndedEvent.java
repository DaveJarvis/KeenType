/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.events;

import com.whitemagicsoftware.keentype.bus.KtBus;
import com.whitemagicsoftware.keentype.node.KtTypesetter;

/**
 * Denotes that typesetting of a document has finished.
 */
public class KtTypesettingEndedEvent {
  private final KtTypesetter mTypesetter;

  private KtTypesettingEndedEvent( final KtTypesetter typeSetter ) {
    assert typeSetter != null;
    mTypesetter = typeSetter;
  }

  public KtTypesetter getTypesetter() {
    return mTypesetter;
  }

  public static void publish( final KtTypesetter typeSetter ) {
    KtBus.post( new KtTypesettingEndedEvent( typeSetter ) );
  }
}

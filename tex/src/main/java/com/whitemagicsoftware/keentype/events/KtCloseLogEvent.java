/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.events;

import com.whitemagicsoftware.keentype.bus.KtBus;

/**
 * Wrapper for logging-related information.
 */
public class KtCloseLogEvent {

  private KtCloseLogEvent() { }

  @SuppressWarnings( "InstantiationOfUtilityClass" )
  public static void publish() {
    KtBus.post( new KtCloseLogEvent() );
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.builder.NullBuilder
// $Id: KtNullBuilder.java,v 1.1.1.1 2001/03/20 11:21:34 ksk Exp $
package com.whitemagicsoftware.keentype.builder;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtLeaders;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;

public class KtNullBuilder extends KtBuilder {

  public void addNode(KtNode node) {}

  public void addNodes(KtNodeEnum nodes) {}

  public void addKern(KtDimen kern) {}

  public void addSkip(KtGlue skip) {}

  public void addNamedSkip(KtGlue skip, String name) {}

  public void addRule(KtBoxSizes sizes) {}

  public void addLeaders(KtGlue skip, KtLeaders lead) {}

  public void addLeadRule(KtGlue skip, KtBoxSizes sizes, String desc) {}

  public boolean isEmpty() {
    return true;
  }

  public KtNode lastNode() {
    return KtNode.NULL;
  }

  public void removeLastNode() {}

  public KtNode lastSpecialNode() {
    return KtNode.NULL;
  }

  public int getPrevGraf() {
    return 0;
  }

  public void setPrevGraf(int pg) {}

  public String modeName() {
    return "no";
  }
}

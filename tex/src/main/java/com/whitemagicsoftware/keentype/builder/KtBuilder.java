// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.builder;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.node.*;

public abstract class KtBuilder implements KtLoggable {

  public static final KtBuilder NULL = null;

  public boolean isHorizontal() {
    return false;
  }

  public boolean isVertical() {
    return false;
  }

  public boolean isMath() {
    return false;
  }

  public boolean isInner() {
    return false;
  }

  public boolean isCharAllowed() {
    return false;
  }

  public boolean wantsMigrations() {
    return false;
  }

  public boolean willBeBroken() {
    return false;
  }

  public boolean forbidsThirdPartOfDiscretionary() {
    return false;
  }

  public abstract void addNode(KtNode node);

  public abstract void addNodes( KtNodeEnum nodes);

  public abstract void addKern(KtDimen kern);

  public abstract void addSkip(KtGlue skip);

  public abstract void addNamedSkip(KtGlue skip, String name);

  public abstract void addRule( KtBoxSizes sizes);

  public abstract void addLeaders(KtGlue skip, KtLeaders lead);

  public abstract void addLeadRule(KtGlue skip, KtBoxSizes sizes, String desc);

  public void addSkip(KtGlue skip, String name) {
    if (name != null) addNamedSkip(skip, name);
    else addSkip(skip);
  }

  public void addPenalty(KtNum pen) {
    addNode(new KtPenaltyNode( pen));
  }

  public void addBox(KtNode box) {
    addNode(box);
  }

  public boolean unBox(KtBox box) {
    return false;
  }

  public KtBoxLeaders.KtMover getBoxLeadMover() {
    return KtBoxLeaders.NULL_MOVER;
  }

  public int getStartLine() {
    return 0;
  }

  /* if the spaceFactor is supported it is always > 0 */
  public int getSpaceFactor() {
    return 0;
  }

  public void setSpaceFactor(int sf) {}

  public void resetSpaceFactor() {}

  public void adjustSpaceFactor(int sf) {
    throw new RuntimeException("char not allowed");
  }

  /* if the prevDepth is supported it is always != KtDimen.NULL */
  public KtDimen getPrevDepth() {
    return KtDimen.NULL;
  }

  public void setPrevDepth(KtDimen pd) {}

  public void buildPage() {}

  /* if the getParagraph is supported it is always != KtNodeList.NULL */
  public KtNodeList getParagraph() {
    return KtNodeList.NULL;
  }

  public boolean needsParSkip() {
    return false;
  }

  public boolean canTakeLastNode() {
    return true;
  }

  public boolean canTakeLastBox() {
    return canTakeLastNode();
  }

  /* if supported it is always != KtLanguage.NULL */
  public KtLanguage getInitLang() {
    return KtLanguage.NULL;
  }

  public KtLanguage getCurrLang() {
    return KtLanguage.NULL;
  }

  public void setCurrLang(KtLanguage lang) {}

  public abstract boolean isEmpty();

  public abstract KtNode lastNode();

  public abstract void removeLastNode();

  public abstract KtNode lastSpecialNode();

  /* TeXtp[218] */
  public void show(KtLog log, int depth, int breadth) {
    log.startLine().add("### ");
    addOn(log);
    log.add(" entered at line ").add(getStartLine());
    specialShow(log, depth, breadth);
  }

  protected void specialShow(KtLog log, int depth, int breadth) {}

  public void addOn(KtLog log) {
    log.add(modeName()).add(" mode");
  }

  public abstract String modeName();

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * The KtBuilder stack
   */

  private static KtBuilder topBuilder = NULL;

  protected KtBuilder enclosing = NULL;

  private final Object mMutex = new Object();

  public static KtBuilder top() {
    return topBuilder;
  }

  public static void push( final KtBuilder b ) {
    synchronized( b.mMutex ) {
      if( b.enclosing == NULL ) {
        b.enclosing = topBuilder;
        topBuilder = b;
      }
      else {
        throw new RuntimeException( "builder already pushed" );
      }
    }
  }

  public static KtBuilder pop() {
    final KtBuilder b = topBuilder;

    if( b != NULL ) {
      synchronized( b.mMutex ) {
        topBuilder = b.enclosing;
        b.enclosing = NULL;
      }
    }

    return b;
  }

  public static void showStack(KtLog log, int depth, int breadth) {
    for (KtBuilder b = topBuilder; b != NULL; b = b.enclosing) b.show(log, depth, breadth);
  }

  public int getPrevGraf() {
    return enclosing != NULL ? enclosing.getPrevGraf() : 0;
  }

  public void setPrevGraf(int pg) {
    if (enclosing != NULL) enclosing.setPrevGraf(pg);
  }

  public int nearestValidSpaceFactor() {
    return enclosing != NULL ? enclosing.getSpaceFactor() : 0;
  }

  public KtDimen nearestValidPrevDepth() {
    return enclosing != NULL ? enclosing.getPrevDepth() : KtDimen.NULL;
  }
}

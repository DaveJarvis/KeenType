// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.builder.HorizBuilder
// $Id: KtHorizBuilder.java,v 1.1.1.1 2000/08/08 04:35:55 ksk Exp $
package com.whitemagicsoftware.keentype.builder;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxLeaders;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtHKernNode;
import com.whitemagicsoftware.keentype.node.KtHLeadRuleNode;
import com.whitemagicsoftware.keentype.node.KtHLeadersNode;
import com.whitemagicsoftware.keentype.node.KtHSkipNode;
import com.whitemagicsoftware.keentype.node.KtLeaders;
import com.whitemagicsoftware.keentype.node.KtNamedHSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtRuleNode;

public abstract class KtHorizBuilder extends KtListBuilder {

  public static final int NORMAL_SPACE_FACTOR = 1000;

  private int spaceFactor = NORMAL_SPACE_FACTOR;

  protected KtHorizBuilder(int line, KtNodeList list) {
    super(line, list);
  }

  protected KtHorizBuilder(int line) {
    super(line);
  }

  public boolean isHorizontal() {
    return true;
  }

  public boolean isCharAllowed() {
    return true;
  }

  public void addKern(KtDimen kern) {
    addNode(new KtHKernNode(kern));
  }

  public void addSkip(KtGlue skip) {
    addNode(new KtHSkipNode(skip));
  }

  public void addNamedSkip(KtGlue skip, String name) {
    addNode(new KtNamedHSkipNode(skip, name));
  }

  /* TeXtp[1056] */
  public void addRule(KtBoxSizes sizes) {
    addNode(new KtRuleNode(sizes));
    resetSpaceFactor();
  }

  public void addLeaders(KtGlue skip, KtLeaders lead) {
    addNode(new KtHLeadersNode(skip, lead));
  }

  public void addLeadRule(KtGlue skip, KtBoxSizes sizes, String desc) {
    addNode(new KtHLeadRuleNode(skip, sizes, desc));
  }

  public KtBoxLeaders.KtMover getBoxLeadMover() {
    return KtHLeadersNode.BOX_MOVER;
  }

  public void addBox(KtNode box) {
    super.addBox(box);
    resetSpaceFactor();
  }

  protected KtNodeEnum unBoxList(KtBox box) {
    return box.getHorizList();
  }

  /* TeXtp[1034] */
  public void adjustSpaceFactor(int sf) {
    if (sf > 0)
      spaceFactor =
          sf <= NORMAL_SPACE_FACTOR || spaceFactor >= NORMAL_SPACE_FACTOR
              ? sf
              : NORMAL_SPACE_FACTOR;
  }

  public int getSpaceFactor() {
    return spaceFactor;
  }

  public int nearestValidSpaceFactor() {
    return spaceFactor;
  }

  public void setSpaceFactor(int sf) {
    if (sf > 0) spaceFactor = sf;
  }

  public void resetSpaceFactor() {
    spaceFactor = NORMAL_SPACE_FACTOR;
  }

  /* TeXtp[219] */
  protected void specialShow(KtLog log) {
    log.startLine().add("spacefactor ").add(getSpaceFactor());
  }
}

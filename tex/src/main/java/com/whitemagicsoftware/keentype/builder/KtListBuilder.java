// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.builder.ListBuilder
// $Id: KtListBuilder.java,v 1.1.1.1 2000/04/12 10:43:14 ksk Exp $
package com.whitemagicsoftware.keentype.builder;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public abstract class KtListBuilder extends KtBuilder {

  protected KtNodeList list;
  protected final int startLine;

  protected KtListBuilder(int startLine, KtNodeList list) {
    this.startLine = startLine;
    this.list = list;
  }

  protected KtListBuilder(int line) {
    this(line, new KtNodeList());
  }

  public void addNode(KtNode node) {
    list.append(node);
  }

  public void addNodes(KtNodeEnum nodes) {
    list.append(nodes);
  }

  public boolean isEmpty() {
    return list.isEmpty();
  }

  public KtNode lastNode() {
    return list.lastNode();
  }

  public void removeLastNode() {
    list.removeLastNode();
  }

  public KtNode lastSpecialNode() {
    return list.lastSpecialNode();
  }

  public KtNodeList getList() {
    return list;
  }

  public int getStartLine() {
    return startLine;
  }

  protected void specialShow(KtLog log, int depth, int breadth) {
    KtCntxLog.addItems(log, list.nodes(), depth, breadth);
    // XXX maybe there should be log.endLine()
    // XXX check everything which originates from show_box()
    specialShow(log);
  }

  protected abstract void specialShow(KtLog log);

  public boolean unBox(KtBox box) {
    KtNodeEnum nodes = unBoxList(box);
    if (nodes != KtNodeEnum.NULL) {
      addNodes(nodes);
      return true;
    }
    return false;
  }

  protected abstract KtNodeEnum unBoxList(KtBox box);
}

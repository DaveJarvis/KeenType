// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.builder.VertBuilder
// $Id: KtVertBuilder.java,v 1.1.1.1 2000/08/08 04:44:04 ksk Exp $
package com.whitemagicsoftware.keentype.builder;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxLeaders;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtLeaders;
import com.whitemagicsoftware.keentype.node.KtNamedVSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtRuleNode;
import com.whitemagicsoftware.keentype.node.KtVKernNode;
import com.whitemagicsoftware.keentype.node.KtVLeadRuleNode;
import com.whitemagicsoftware.keentype.node.KtVLeadersNode;
import com.whitemagicsoftware.keentype.node.KtVSkipNode;

public abstract class KtVertBuilder extends KtListBuilder {

  public static final KtDimen IGNORE_DEPTH = KtDimen.valueOf(-1000);

  private int prevGraf = 0;
  private KtDimen prevDepth = IGNORE_DEPTH;

  protected KtVertBuilder(int line, KtNodeList list) {
    super(line, list);
  }

  protected KtVertBuilder(int line) {
    super(line);
  }

  public boolean isVertical() {
    return true;
  }

  public boolean wantsMigrations() {
    return true;
  }

  public void addKern(KtDimen kern) {
    addNode(new KtVKernNode(kern));
  }

  public void addSkip(KtGlue skip) {
    addNode(new KtVSkipNode(skip));
  }

  public void addNamedSkip(KtGlue skip, String name) {
    addNode(new KtNamedVSkipNode(skip, name));
  }

  /* TeXtp[1056] */
  public void addRule(KtBoxSizes sizes) {
    addNode(new KtRuleNode(sizes));
    uncheckedSetPrevDepth(IGNORE_DEPTH);
  }

  public void addLeaders(KtGlue skip, KtLeaders lead) {
    addNode(new KtVLeadersNode(skip, lead));
  }

  public void addLeadRule(KtGlue skip, KtBoxSizes sizes, String desc) {
    addNode(new KtVLeadRuleNode(skip, sizes, desc));
  }

  public KtBoxLeaders.KtMover getBoxLeadMover() {
    return KtVLeadersNode.BOX_MOVER;
  }

  /* TeXtp[679] */
  public void addBox(KtNode box) {
    super.addBox(box);
    uncheckedSetPrevDepth(box.getDepth());
  }

  protected KtNodeEnum unBoxList(KtBox box) {
    return box.getVertList();
  }

  public int getPrevGraf() {
    return prevGraf;
  }

  public void setPrevGraf(int pg) {
    prevGraf = pg;
  }

  public KtDimen getPrevDepth() {
    return prevDepth;
  }

  protected void uncheckedSetPrevDepth(KtDimen pd) {
    prevDepth = pd;
  }

  public KtDimen nearestValidPrevDepth() {
    return prevDepth;
  }

  public void setPrevDepth(KtDimen pd) {
    if (pd != KtDimen.NULL) prevDepth = pd;
  }

  public boolean needsParSkip() {
    return !list.isEmpty();
  }

  /* TeXtp[219] */
  protected void specialShow(KtLog log) {
    log.startLine().add("prevdepth ");
    if (getPrevDepth().moreThan(IGNORE_DEPTH)) log.add(getPrevDepth().toString());
    else log.add("ignored");
    if (prevGraf != 0)
      log.add(", prevgraf ").add(prevGraf).add( prevGraf == 1 ? " line" : " lines");
  }
}

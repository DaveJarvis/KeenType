// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.builder.ParBuilder
// $Id: KtParBuilder.java,v 1.1.1.1 2001/03/20 11:21:47 ksk Exp $
package com.whitemagicsoftware.keentype.builder;

import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtLanguage;
import com.whitemagicsoftware.keentype.node.KtLanguageNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public class KtParBuilder extends KtHorizBuilder {

  private final KtLanguage initLang;
  private KtLanguage currLang;

  public KtParBuilder(int line, KtLanguage initLang) {
    super(line);
    this.initLang = initLang;
    currLang = initLang;
  }

  public String modeName() {
    return "horizontal";
  }

  public boolean willBeBroken() {
    return true;
  }

  public KtNodeList getParagraph() {
    return list;
  }

  public KtLanguage getInitLang() {
    return initLang;
  }

  public KtLanguage getCurrLang() {
    return currLang;
  }

  public void setCurrLang(KtLanguage lang) {
    currLang = lang;
    addNode(new KtLanguageNode(lang));
  }

  /* TeXtp[218] */
  protected void specialShow(KtLog log, int depth, int breadth) {
    if (!initLang.isCommon()) log.add(" (").add(initLang).add(')');
    super.specialShow(log, depth, breadth);
  }

  /* TeXtp[219] */
  protected void specialShow(KtLog log) {
    super.specialShow(log);
    if (!currLang.isZero()) {
      log.add(", current language ");
      currLang.addNumberOn(log);
    }
  }
}

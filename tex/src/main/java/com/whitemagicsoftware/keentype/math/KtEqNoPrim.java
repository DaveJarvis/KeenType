// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.EqNoPrim
// $Id: KtEqNoPrim.java,v 1.1.1.1 2001/03/13 05:28:04 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtClosing;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtGroup;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;

public class KtEqNoPrim extends KtAdaptMathPrim {

  private final KtTokenList.KtInserter every;
  private final boolean left;

  public KtEqNoPrim(String name, KtCommand command, KtTokenList.KtInserter every, boolean left) {
    super(name, command);
    this.every = every;
    this.left = left;
  }

  protected class KtDisplayedClosing extends KtClosing {

    /* TeXtp[1142] */
    public void exec(KtGroup grp, KtToken src) {
      pushLevel(new KtEqNoGroup(left));
      KtMathPrim.getMathConfig().initFormula();
      every.insertToks();
    }
  }

  public KtClosing makeDisplayedClosing() {
    return new KtDisplayedClosing();
  }
}

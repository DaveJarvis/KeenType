// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathChoicePrim
// $Id: KtMathChoicePrim.java,v 1.1.1.1 2000/03/20 21:16:46 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtChoiceNoad;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.noad.KtNoadList;

public class KtMathChoicePrim extends KtMathPrim {

  public KtMathChoicePrim(String name) {
    super(name);
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* STRANGE
   * Empty KtNoad is added and then removed only for the case when
   * \showlists appear inside the group.
   */
  /* TeXtp[1172] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          KtNoadList[] choices = new KtNoadList[KtNoad.NUMBER_OF_STYLES];
          for (int i = 0; i < choices.length; i++) choices[i] = KtNoadList.EMPTY;
          bld.addNoad(new KtChoiceNoad(choices));
          pushLevel(new KtMathChoiceGroup(choices));
        }
      };
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathShiftToken
// $Id: KtMathShiftToken.java,v 1.1.1.1 2000/04/16 10:46:47 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtCharToken;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtMathShiftToken extends KtCharToken {

  public static final KtCharCode CODE = makeCharCode('$');
  public static final KtMathShiftToken TOKEN = new KtMathShiftToken(CODE);

  public static final KtMaker MAKER =
      new KtMaker() {
        public KtToken make(KtCharCode code) {
          return new KtMathShiftToken(code);
        }
      };

  public KtMathShiftToken(KtCharCode code) {
    super(code);
  }

  public boolean match(KtCharToken tok) {
    return tok instanceof KtMathShiftToken && tok.match( code);
  }

  public KtMaker getMaker() {
    return MAKER;
  }

  public String toString() {
    return "<MathShift: " + code + '>';
  }

  private static KtCommand command;

  public static void setCommand(KtCommand cmd) {
    command = cmd;
  }

  public KtCommand meaning() {
    return new KtMeaning() {

      public boolean isMathShift() {
        return true;
      }

      public void exec(KtToken src) {
        command.exec(src);
      }

      protected String description() {
        return "math shift character";
      }
    };
  }
}

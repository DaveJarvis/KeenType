// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.FormulaGroup
// $Id: KtFormulaGroup.java,v 1.1.1.1 2001/03/13 05:05:39 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.noad.KtConvStyle;
import com.whitemagicsoftware.keentype.noad.KtConversion;
import com.whitemagicsoftware.keentype.noad.KtDelimiter;
import com.whitemagicsoftware.keentype.noad.KtMathOffNode;
import com.whitemagicsoftware.keentype.noad.KtMathOnNode;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNamedHSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtTreatNode;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

public class KtFormulaGroup extends KtSimpleGroup {

  protected /* final */ KtMathBuilder builder;

  protected KtFormulaGroup(KtMathBuilder builder) {
    this.builder = builder;
  }

  public KtFormulaGroup(KtFormulaBuilder builder) {
    this((KtMathBuilder) builder);
  }

  public KtFormulaGroup() {
    this(new KtFormulaBuilder(currLineNumber()));
  }

  public void start() {
    KtBuilder.push(builder);
  }

  public static final int DIMP_MATH_SURROUND = newDimParam();

  /* TeXtp[1194,1196] */
  public void stop() {
    KtBuilder.pop();
    KtDimen surr = getConfig().getDimParam(DIMP_MATH_SURROUND);
    KtBuilder bld = KtTypoCommand.getBld();
    bld.addNode(new KtMathOnNode(surr));
    if (necessaryParamsDefined())
      bld.addNodes(
          KtConversion.madeOf(
                  builder.getList().noads(), new KtFormulaStyle(KtNoad.TEXT_STYLE, bld.willBeBroken()))
              .nodes());
    bld.addNode(new KtMathOffNode(surr));
    bld.resetSpaceFactor();
  }

  public KtToken expectedToken() {
    return KtMathShiftToken.TOKEN;
  }

  /* TeXtp[1197] */
  public static boolean expectAnotherMathShift() {
    KtToken tok = nextExpToken();
    if (!meaningOf(tok).isMathShift()) {
      backToken(tok);
      error("BadFormulaEnd");
      return false;
    }
    return true;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  public static final int INTP_DELIMITER_FACTOR = newIntParam();
  public static final int DIMP_NULL_DELIMITER_SPACE = newDimParam();
  public static final int DIMP_DELIMITER_SHORTFALL = newDimParam();
  public static final int DIMP_SCRIPT_SPACE = newDimParam();

  public static final byte TEXT_SIZE = 0;
  public static final byte SCRIPT_SIZE = 1;
  public static final byte SCRIPT_SCRIPT_SIZE = 2;
  public static final byte NUMBER_OF_FONT_SIZES = 3;

  private static boolean isScriptSize(byte size) {
    return size >= SCRIPT_SIZE;
  }

  /* TeXtp[1195] */
  public static boolean necessaryParamsDefined() {
    if (!necessaryParamsDefined(KtConvStyle.SYMBOL_FAMILY, KtFontMetric.ALL_MATH_SYMBOL_DIMEN_PARAMS)) {
      error("InsufSymFonts");
      return false;
    }
    if (!necessaryParamsDefined(
        KtConvStyle.EXTENSION_FAMILY, KtFontMetric.ALL_MATH_EXTENSION_DIMEN_PARAMS)) {
      error("InsufExtFonts");
      return false;
    }
    return true;
  }

  public static boolean necessaryParamsDefined(byte fam, int[] idxs) {
    for (byte s = TEXT_SIZE; s < NUMBER_OF_FONT_SIZES; s++)
      if (!KtMathPrim.getMathConfig().familyFont(s, fam).definesDimenParams(idxs)) return false;
    return true;
  }

  protected static class KtFormulaStyle extends KtConvStyle {

    protected byte fontSize;
    protected final boolean penalties;

    protected KtFormulaStyle(byte style, boolean cramped, boolean penalties) {
      super(style, cramped);
      this.penalties = penalties;
    }

    public KtFormulaStyle(byte style, boolean penalties) {
      this(style, false, penalties);
    }

    protected void setupFontSize() {
      switch( style ) {
        case KtNoad.SCRIPT_SCRIPT_STYLE -> fontSize = SCRIPT_SCRIPT_SIZE;
        case KtNoad.SCRIPT_STYLE -> fontSize = SCRIPT_SIZE;
        default -> fontSize = TEXT_SIZE;
      }
    }

    protected KtFontMetric fetchFontMetric(byte fam) {
      return KtMathPrim.getMathConfig().familyFont(fontSize, fam);
    }

    /* TeXtp[723] */
    private void metricError(byte fam, KtCharCode code) {
      error("UndefFamily", esc(KtMathPrim.getMathConfig().familyName(fontSize, fam)), num(fam), code);
    }

    /* TeXtp[722] */
    public KtNode fetchCharNode(byte fam, KtCharCode code) {
      KtFontMetric metric = fetchFontMetric(fam);
      if (metric.isNull()) {
        metricError(fam, code);
        return KtNode.NULL;
      }
      KtNode node = metric.getCharNode(code);
      if (node == KtNode.NULL) KtMathPrim.charWarning(metric, code);
      return node;
    }

    public KtNode fetchLargerNode(byte fam, KtCharCode code) {
      KtFontMetric metric = fetchFontMetric(fam);
      if (metric.isNull()) {
        metricError(fam, code);
        return KtNode.NULL;
      }
      KtNode node = metric.getLargerNode(code);
      if (node == KtNode.NULL) KtMathPrim.charWarning(metric, code);
      return node;
    }

    public KtBox fetchFittingWidthBox(byte fam, KtCharCode code, KtDimen desired) {
      KtFontMetric metric = fetchFontMetric(fam);
      if (metric.isNull()) {
        metricError(fam, code);
        return KtBox.NULL;
      }
      KtBox box = metric.getFittingWidthBox(code, desired);
      if (box == KtBox.NULL) KtMathPrim.charWarning(metric, code);
      return box;
    }

    /* STRANGE
     * why is there no single char warning?
     */
    /* TeXtp[706,707] */
    public KtNode fetchSufficientNode(KtDelimiter delimiter, KtDimen desired) {
      KtDimen maxSize = KtDimen.ZERO;
      KtNode maxNode = KtNode.NULL;
      boolean large = false;
      byte fam = delimiter.getSmallFam();
      KtCharCode code = delimiter.getSmallCode();
      ProbingBothSizes:
      for (; ; ) {
        if (code != KtCharCode.NULL)
          for (byte s = fontSize; s >= TEXT_SIZE; s--) {
            KtFontMetric metric = KtMathPrim.getMathConfig().familyFont(s, fam);
            KtNode node = metric.getSufficientNode(code, desired);
            if (node != KtNode.NULL) {
              KtDimen size = node.getHeight().plus(node.getDepth());
              if (size.moreThan(maxSize)) {
                maxSize = size;
                maxNode = node;
                if (!size.lessThan(desired)) break ProbingBothSizes;
              }
            }
          }
        if (large) break;
        large = true;
        fam = delimiter.getLargeFam();
        code = delimiter.getLargeCode();
      }
      return maxNode != KtNode.NULL
          ? maxNode
          : new KtHBoxNode(
              new KtBoxSizes(
                  KtDimen.ZERO,
                  getConfig().getDimParam(DIMP_NULL_DELIMITER_SPACE),
                  KtDimen.ZERO,
                  KtDimen.ZERO),
              KtGlueSetting.NATURAL,
              KtNodeList.EMPTY);
    }

    public KtDimen getXHeight(byte fam) {
      return fetchFontMetric(fam).getDimenParam(KtFontMetric.DIMEN_PARAM_X_HEIGHT);
    }

    /* TeXtp[741] */
    public KtDimen skewAmount(byte fam, KtCharCode code) {
      KtFontMetric metric = fetchFontMetric(fam);
      KtNum skewCharNum = metric.getNumParam(KtFontMetric.NUM_PARAM_SKEW_CHAR);
      if (skewCharNum != KtNum.NULL) {
        KtCharCode skewCode = KtToken.makeCharCode(skewCharNum.intVal());
        if (skewCode != KtCharCode.NULL) {
          KtDimen skew = metric.getKernBetween(code, skewCode);
          if (skew != KtDimen.NULL) return skew;
        }
      }
      return KtDimen.ZERO;
    }

    /* STRANGE
     * why .over(x).times(y) instead of times(y, x) ?
     */
    /* TeXtp[762] */
    public KtDimen delimiterSize(KtDimen height, KtDimen depth) {
      KtDimen max = height.max(depth);
      KtDimen size = max.over(500).times(getConfig().getIntParam(INTP_DELIMITER_FACTOR));
      max = max.times(2).minus(getConfig().getDimParam(DIMP_DELIMITER_SHORTFALL));
      return max.max(size);
    }

    public KtDimen scriptSpace() {
      return getConfig().getDimParam(DIMP_SCRIPT_SPACE);
    }

    public KtNode getSpacing(byte left, byte right) {
      KtMathPrim.KtMathSpacer spacer = KtMathPrim.getMathConfig().mathSpacing(left, right);
      KtGlue space = spacer.getGlue(fontSize);
      return space.isZero() ? KtNode.NULL : new KtNamedHSkipNode( muToPt( space), spacer.getName());
    }

    public KtNum getPenalty(byte left, byte right) {
      return penalties ? KtMathPrim.getMathConfig().mathPenalty( left, right) : KtNum.NULL;
    }

    public boolean isScript() {
      return isScriptSize(fontSize);
    }

    public KtConvStyle makeNew(byte style) {
      return new KtFormulaStyle(style, false, penalties);
    }

    protected KtConvStyle deriveNew(byte style, boolean cramped) {
      return new KtFormulaStyle(style, cramped, false);
    }

    public KtMathWordBuilder getWordBuilder(byte fam, KtTreatNode proc) {
      KtFontMetric metric = fetchFontMetric(fam);
      return metric.isNull() ? KtMathWordBuilder.NULL : metric.getMathWordBuilder( proc);
    }

    public boolean forcedItalCorr(byte fam) {
      return fetchFontMetric(fam).getDimenParam(KtFontMetric.DIMEN_PARAM_SPACE).isZero();
    }
  }
}

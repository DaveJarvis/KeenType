// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.SuperScriptPrim
// $Id: KtSuperScriptPrim.java,v 1.1.1.1 2000/03/03 19:05:59 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.noad.KtSuperScriptNoad;

public class KtSuperScriptPrim extends KtScriptPrim {

  public KtSuperScriptPrim(String name) {
    super(name);
  }

  protected boolean conflicting(KtNoad noad) {
    return noad.alreadySuperScripted();
  }

  protected KtNoad scripted(KtNoad noad, KtField script) {
    return new KtSuperScriptNoad(noad, script);
  }

  protected String errorIdent() {
    return "DoubleSuperscript";
  }
}

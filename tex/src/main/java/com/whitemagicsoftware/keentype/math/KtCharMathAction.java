// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.CharMathAction
// $Id: KtCharMathAction.java,v 1.1.1.1 2000/03/02 09:28:15 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtCharMathAction extends KtMathAction {

  private final KtCommand cmd;

  public KtCharMathAction(KtCommand cmd) {
    this.cmd = cmd;
  }

  /* TeXtp[1154] */
  public void exec(KtMathBuilder bld, KtToken src) {
    KtCharCode code = cmd.charCodeToAdd();
    if (code != KtCharCode.NULL) KtMathPrim.setMathChar(bld, code);
  }
}

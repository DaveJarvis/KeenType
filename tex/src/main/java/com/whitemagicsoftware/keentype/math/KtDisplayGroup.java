// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.DisplayGroup
// $Id: KtDisplayGroup.java,v 1.1.1.1 2001/03/22 12:11:04 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtParBuilder;
import com.whitemagicsoftware.keentype.noad.KtConvStyle;
import com.whitemagicsoftware.keentype.noad.KtConversion;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtChrKernNode;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHorizIterator;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtSizesEvaluator;
import com.whitemagicsoftware.keentype.node.KtVShiftNode;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

public class KtDisplayGroup extends KtFormulaGroup {

  public KtDisplayGroup(KtDisplayBuilder builder) {
    super( builder );
  }

  public KtDisplayGroup() {
    super(new KtDisplayBuilder(currLineNumber()));
  }

  public static final int INTP_PRE_DISPLAY_PENALTY = newIntParam();
  public static final int INTP_POST_DISPLAY_PENALTY = newIntParam();
  public static final int DIMP_PRE_DISPLAY_SIZE = newDimParam();
  public static final int DIMP_DISPLAY_WIDTH = newDimParam();
  public static final int DIMP_DISPLAY_INDENT = newDimParam();
  public static final int GLUEP_ABOVE_DISPLAY_SKIP = newGlueParam();
  public static final int GLUEP_BELOW_DISPLAY_SKIP = newGlueParam();
  public static final int GLUEP_ABOVE_DISPLAY_SHORT_SKIP = newGlueParam();
  public static final int GLUEP_BELOW_DISPLAY_SHORT_SKIP = newGlueParam();

  /* TeXtp[1194,1199] */
  public void stop() {
    KtBuilder.pop();
    KtBuilder bld = KtTypoCommand.getBld();
    KtConfig cfg = getConfig();
    boolean success = necessaryParamsDefined();
    KtNode eqNoBox = ((KtDisplayBuilder) builder).getEqNoBox();
    boolean eqNoLeft = ((KtDisplayBuilder) builder).getEqNoLeft();
    if (eqNoBox == KtNode.NULL) {
      eqNoLeft = false;
      expectAnotherMathShift();
    }
    KtNodeList list = KtNodeList.EMPTY;
    KtDimen eqNoWidth = KtDimen.ZERO;
    KtDimen eqNoAdd = KtDimen.ZERO;
    boolean sideBySide = false;
    if (success) {
      KtConvStyle style = new KtFormulaStyle(KtNoad.DISPLAY_STYLE, false);
      list = KtConversion.madeOf(builder.getList().noads(), style);
      if (eqNoBox != KtNode.NULL) {
        eqNoWidth = eqNoBox.getWidth().plus(eqNoBox.getLeftX());
        if (!eqNoWidth.isZero()) {
          eqNoAdd = style.getDimPar(KtConvStyle.DP_MATH_QUAD).plus(eqNoWidth);
          sideBySide = true;
        }
      }
    }
    KtNodeEnum mig =
        bld.wantsMigrations() ? list.extractedMigrations().nodes() : KtNodeList.EMPTY_ENUM;
    KtDimen lastVis = cfg.getDimParam(DIMP_PRE_DISPLAY_SIZE);
    KtDimen width = cfg.getDimParam(DIMP_DISPLAY_WIDTH);
    KtDimen indent = cfg.getDimParam(DIMP_DISPLAY_INDENT);
    KtBoolPar parBroken = new KtBoolPar();
    KtHBoxNode box = packer.packHBox(list, eqNoAdd, width, parBroken);
    sideBySide = sideBySide && !parBroken.get();
    KtDimen boxWidth = box.getWidth().plus(box.getLeftX());
    KtDimen delta = width.minus(boxWidth).halved();
    if (sideBySide && delta.lessThan(eqNoWidth.times(2)))
      delta =
          !list.isEmpty() && list.nodeAt(0).isSkip()
              ? KtDimen.ZERO
              : width.minus(boxWidth).minus(eqNoWidth).halved();
    bld.addPenalty(KtNum.valueOf(cfg.getIntParam(INTP_PRE_DISPLAY_PENALTY)));
    KtNode kernBetween =
        sideBySide
            ? new KtChrKernNode(width.minus(boxWidth).minus(eqNoWidth).minus(delta))
            : KtNode.NULL;
    int belowSkipParam;
    if (eqNoLeft) {
      belowSkipParam = GLUEP_BELOW_DISPLAY_SKIP;
      if (sideBySide) {
        int aboveSkipParam = GLUEP_ABOVE_DISPLAY_SKIP;
        bld.addSkip(cfg.getGlueParam(aboveSkipParam), cfg.getGlueName(aboveSkipParam));
        KtNodeList together = new KtNodeList(3);
        together.append(eqNoBox).append(kernBetween).append(box);
        box = KtHBoxNode.packedOf(together);
        appendBox(bld, box, indent);
      } else {
        appendBox(bld, eqNoBox, indent);
        bld.addPenalty(KtNum.valueOf(KtNode.INF_PENALTY));
        appendBox(bld, box, indent.plus(delta));
      }
    } else {
      boolean biggerSkips = !delta.plus(indent).moreThan(lastVis);
      belowSkipParam = biggerSkips ? GLUEP_BELOW_DISPLAY_SKIP : GLUEP_BELOW_DISPLAY_SHORT_SKIP;
      int aboveSkipParam =
          biggerSkips ? GLUEP_ABOVE_DISPLAY_SKIP : GLUEP_ABOVE_DISPLAY_SHORT_SKIP;
      bld.addSkip(cfg.getGlueParam(aboveSkipParam), cfg.getGlueName(aboveSkipParam));
      if (sideBySide) {
        KtNodeList together = new KtNodeList(3);
        together.append(box).append(kernBetween).append(eqNoBox);
        box = KtHBoxNode.packedOf(together);
        appendBox(bld, box, indent.plus(delta));
      } else {
        appendBox(bld, box, indent.plus(delta));
        if (eqNoBox != KtNode.NULL) {
          bld.addPenalty(KtNum.valueOf(KtNode.INF_PENALTY));
          appendBox(bld, eqNoBox, indent.plus(width).minus(eqNoWidth));
          belowSkipParam = -1;
        }
      }
    }
    bld.addNodes(mig);
    bld.addPenalty(KtNum.valueOf(cfg.getIntParam(INTP_POST_DISPLAY_PENALTY)));
    if (belowSkipParam != -1)
      bld.addSkip(cfg.getGlueParam(belowSkipParam), cfg.getGlueName(belowSkipParam));
  }

  private static void appendBox(KtBuilder bld, KtNode node, KtDimen shift) {
    KtTypoCommand.appendBox(bld, KtVShiftNode.shiftingRight(node, shift.plus(node.getLeftX())), false);
  }

  public void close() {
    resumeAfterDisplay();
  }

  /* TeXtp[1200] */
  public static void resumeAfterDisplay() {
    KtBuilder bld = KtBuilder.top();
    bld.setPrevGraf(bld.getPrevGraf() + 3);
    KtBuilder par = new KtParBuilder(currLineNumber(), KtTypoCommand.getTypoConfig().getLanguage());
    KtBuilder.push(par);
    skipOptExpSpacer();
    bld.buildPage();
  }

  private static final KtDisplayPacker packer = new KtDisplayPacker();

  public static class KtDisplayPacker extends KtTypoCommand.KtHBoxPacker {

    public KtHBoxNode packHBox(KtNodeList list, KtDimen additional, KtDimen desired, KtBoolPar broken) {
      KtBoolPar.set(broken, false);
      KtSizesEvaluator pack = new KtSizesEvaluator();
      KtHorizIterator.summarize(list.nodes(), pack);
      KtDimen height = pack.getWidth();
      KtDimen width = pack.getBody().plus(pack.getDepth());
      KtDimen depth = pack.getLeftX();
      KtDimen leftX = pack.getHeight();
      desired = desired.minus(leftX);
      KtDimen attempt = width.min(desired.minus(additional));
      pack.evaluate(attempt.minus(width), list.isEmpty());
      if (pack.getReport() == KtSizesEvaluator.OVERFULL && !additional.isZero()) {
        KtBoolPar.set(broken, true);
        attempt = width.min(desired);
        pack.evaluate(attempt.minus(width), list.isEmpty());
      }
      KtBoxSizes sizes = new KtBoxSizes(height, attempt, depth, leftX);
      KtHBoxNode hbox = new KtHBoxNode(sizes, pack.getSetting(), list);
      if (pack.getReport() == KtSizesEvaluator.OVERFULL) addOverfullRule(list, pack.getOverfull());
      if (check(pack)) reportBox(hbox);
      return hbox;
    }
  }
}

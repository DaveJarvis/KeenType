// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.LeftPrim
// $Id: KtLeftPrim.java,v 1.1.1.1 2000/08/05 16:20:27 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtClosing;
import com.whitemagicsoftware.keentype.command.KtFrozenToken;
import com.whitemagicsoftware.keentype.command.KtGroup;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtInnerNoad;
import com.whitemagicsoftware.keentype.noad.KtLeftNoad;
import com.whitemagicsoftware.keentype.noad.KtNoadList;
import com.whitemagicsoftware.keentype.noad.KtNoadListField;
import com.whitemagicsoftware.keentype.noad.KtRightNoad;
import com.whitemagicsoftware.keentype.noad.KtTreatNoadList;

public class KtLeftPrim extends KtMathPrim {

  public /* final */ KtToken rightTok;

  public KtLeftPrim(String name, KtPrimitive right) {
    super(name);
    rightTok = new KtFrozenToken(right);
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1191,1184] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          final KtLeftNoad left = new KtLeftNoad(scanDelimiter());
          pushLevel(
              new KtLeftGroup(
                  rightTok,
                  new KtTreatNoadList() {
                    public void execute(KtNoadList list) {
                      KtRightNoad right = new KtRightNoad(scanDelimiter());
                      KtNoadList whole = new KtNoadList(left);
                      whole.append(list).append(right);
                      bld.addNoad(new KtInnerNoad(new KtNoadListField(whole)));
                    }
                  },
                  left));
        }
      };

  /* TeXtp[1192] */
  public static final KtClosing EXTRA_RIGHT =
      new KtClosing() {
        public void exec(KtGroup grp, KtToken src) {
          KtMathPrim.scanDelimiter();
          error("ExtraRight", src);
        }
      };
}

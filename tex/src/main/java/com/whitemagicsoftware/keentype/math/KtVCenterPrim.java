// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.VCenterPrim
// $Id: KtVCenterPrim.java,v 1.1.1.1 2000/08/03 14:27:44 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.noad.KtNodeField;
import com.whitemagicsoftware.keentype.noad.KtVCenterNoad;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtTreatBox;

public class KtVCenterPrim extends KtMathPrim {

  private final KtTokenList.KtInserter every;

  public KtVCenterPrim(String name, KtTokenList.KtInserter every) {
    super(name);
    this.every = every;
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1167] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          makeBoxValue(
              new KtTreatBox() {
                public void execute(KtBox box, KtNodeEnum mig) {
                  bld.addNoad(new KtVCenterNoad(new KtNodeField(box)));
                }
              });
        }
      };

  /* TeXtp[645] */
  public void makeBoxValue(KtTreatBox proc) {
    KtDimen size = KtDimen.ZERO;
    boolean exactly = false;
    if (scanKeyword("to")) {
      size = scanDimen();
      exactly = true;
    } else if (scanKeyword("spread")) size = scanDimen();
    pushLevel(new KtVCenterGroup(size, exactly, proc));
    scanLeftBrace();
    every.insertToks();
  }
}

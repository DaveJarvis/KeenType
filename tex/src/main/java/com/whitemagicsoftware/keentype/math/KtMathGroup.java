// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathGroup
// $Id: KtMathGroup.java,v 1.1.1.1 2001/03/09 11:06:16 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtNoadList;
import com.whitemagicsoftware.keentype.noad.KtNoadListField;
import com.whitemagicsoftware.keentype.noad.KtTreatField;
import com.whitemagicsoftware.keentype.noad.KtVoidField;

public class KtMathGroup extends KtSimpleGroup {

  protected final KtTreatField proc;
  protected final KtFormulaBuilder builder;

  public KtMathGroup(KtTreatField proc, KtFormulaBuilder builder) {
    this.proc = proc;
    this.builder = builder;
  }

  public KtMathGroup(KtTreatField proc) {
    this(proc, new KtFormulaBuilder(currLineNumber()));
  }

  public void start() {
    KtBuilder.push(builder);
  }

  /* TeXtp[1186] */
  public void close() {
    KtBuilder.pop();
    KtNoadList list = builder.getList();
    KtField field = KtField.NULL;
    if (list.isEmpty()) field = KtVoidField.FIELD;
    else {
      if (list.length() == 1) field = list.noadAt(0).ordinaryField();
      if (field == KtField.NULL) field = new KtNoadListField(list);
    }
    proc.execute(field);
  }
}

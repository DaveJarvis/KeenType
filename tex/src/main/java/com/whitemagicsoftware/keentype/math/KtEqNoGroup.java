// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.EqNoGroup
// $Id: KtEqNoGroup.java,v 1.1.1.1 2000/10/18 10:31:00 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.noad.KtConversion;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

public class KtEqNoGroup extends KtFormulaGroup {

  private final boolean left;

  public KtEqNoGroup(KtFormulaBuilder builder, boolean left) {
    super(builder);
    this.left = left;
  }

  public KtEqNoGroup(boolean left) {
    this(new KtFormulaBuilder(currLineNumber()), left);
  }

  /* TeXtp[1194] */
  public void stop() {
    KtBuilder.pop();
    KtMathBuilder bld = (KtMathBuilder) KtTypoCommand.getBld();
    KtConfig cfg = getConfig();
    boolean success = necessaryParamsDefined();
    expectAnotherMathShift();
    KtNodeList list =
        success
            ? KtConversion.madeOf(builder.getList().noads(), new KtFormulaStyle(KtNoad.TEXT_STYLE, false))
            : KtNodeList.EMPTY;
    KtHBoxNode box = KtHBoxNode.packedOf(list);
    bld.setEqNo(box, left);
  }

  public void close() {
    popLevel();
  }
}

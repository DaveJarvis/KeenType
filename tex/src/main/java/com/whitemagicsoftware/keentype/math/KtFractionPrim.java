// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.FractionPrim
// $Id: KtFractionPrim.java,v 1.1.1.1 2000/03/09 16:56:16 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtDelimiter;

public class KtFractionPrim extends KtMathPrim {

  public static final byte ZERO_THICKNESS = 0;
  public static final byte DEFAULT_THICKNESS = 1;
  public static final byte EXPLICIT_THICKNESS = 2;

  private final boolean withDelims;
  private final byte thickMode;

  public KtFractionPrim(String name, boolean withDelims, byte thickMode) {
    super(name);
    this.withDelims = withDelims;
    this.thickMode = thickMode;
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1181] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          KtDelimiter left = KtDelimiter.NULL;
          KtDelimiter right = KtDelimiter.NULL;
          KtDimen thickness;
          if (withDelims) {
            left = scanDelimiter();
            right = scanDelimiter();
          }
          thickness = switch( thickMode ) {
            case EXPLICIT_THICKNESS -> scanDimen();
            case ZERO_THICKNESS -> KtDimen.ZERO;
            default -> KtDimen.NULL;
          };
          if (bld.alreadyFractioned()) error("AmbiguousFraction");
          else if (withDelims) bld.fractione(thickness, left, right);
          else bld.fractione(thickness);
        }
      };
}

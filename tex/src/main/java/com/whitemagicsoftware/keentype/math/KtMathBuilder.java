// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathBuilder
// $Id: KtMathBuilder.java,v 1.1.1.1 2001/03/22 22:31:41 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.noad.KtDelFractionNoad;
import com.whitemagicsoftware.keentype.noad.KtDelimiter;
import com.whitemagicsoftware.keentype.noad.KtEmptyField;
import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtFractionNoad;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.noad.KtNoadList;
import com.whitemagicsoftware.keentype.noad.KtNoadListField;
import com.whitemagicsoftware.keentype.noad.KtNodeField;
import com.whitemagicsoftware.keentype.noad.KtOrdNoad;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxLeaders;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtHKernNode;
import com.whitemagicsoftware.keentype.node.KtHLeadRuleNode;
import com.whitemagicsoftware.keentype.node.KtHLeadersNode;
import com.whitemagicsoftware.keentype.node.KtHSkipNode;
import com.whitemagicsoftware.keentype.node.KtLeaders;
import com.whitemagicsoftware.keentype.node.KtNamedHSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtRuleNode;

public abstract class KtMathBuilder extends KtBuilder {

  protected KtNoadList list;
  protected final int startLine;
  protected final KtNoad leftDelim;

  protected KtMathBuilder(int startLine, KtNoadList list, KtNoad leftDelim) {
    this.startLine = startLine;
    this.list = list;
    this.leftDelim = leftDelim;
  }

  protected KtMathBuilder(int startLine, KtNoadList list) {
    this(startLine, list, KtNoad.NULL);
  }

  protected KtMathBuilder(int startLine, KtNoad leftNoad) {
    this(startLine, new KtNoadList(), leftNoad);
  }

  protected KtMathBuilder(int startLine) {
    this(startLine, new KtNoadList());
  }

  public void addNoad(KtNoad noad) {
    list.append(noad);
  }

  public KtNoad lastNoad() {
    return list.lastNoad();
  }

  public void removeLastNoad() {
    list.removeLastNoad();
  }

  public void replaceLastNoad(KtNoad noad) {
    list.replaceLastNoad(noad);
  }

  public void addNode(KtNode node) {
    list.append(node);
  }

  public void addNodes(KtNodeEnum nodes) {
    list.append(nodes);
  }

  public KtNode lastNode() {
    return list.lastNode();
  }

  public void removeLastNode() {
    list.removeLastNode();
  }

  public KtNode lastSpecialNode() {
    return list.lastSpecialNode();
  }

  public int getStartLine() {
    return startLine;
  }

  public boolean unBox(KtBox box) {
    return false;
  }

  /* ****************************************************************** */

  public boolean isMath() {
    return true;
  }

  public boolean isCharAllowed() {
    return true;
  }

  public boolean forbidsThirdPartOfDiscretionary() {
    return true;
  }

  public void addKern(KtDimen kern) {
    addNode(new KtHKernNode(kern));
  }

  public void addSkip(KtGlue skip) {
    addNode(new KtHSkipNode(skip));
  }

  public void addNamedSkip(KtGlue skip, String name) {
    addNode(new KtNamedHSkipNode(skip, name));
  }

  /* TeXtp[1056] */
  public void addRule(KtBoxSizes sizes) {
    addNode(new KtRuleNode(sizes));
  }

  /* STRANGE
   * \leaders<box>\hskip #pt and \leaders<box>\mskip #mu
   * silently produce the same result.
   * Isn't it a bug?
   */
  public void addLeaders(KtGlue skip, KtLeaders lead) {
    addNode(new KtHLeadersNode(skip, lead));
  }

  public void addLeadRule(KtGlue skip, KtBoxSizes sizes, String desc) {
    addNode(new KtHLeadRuleNode(skip, sizes, desc));
  }

  public KtBoxLeaders.KtMover getBoxLeadMover() {
    return KtHLeadersNode.BOX_MOVER;
  }

  /* TeXtp[1076] */
  public void addBox(KtNode box) {
    list.append(new KtOrdNoad(new KtNodeField(box)));
  }

  public boolean canTakeLastBox() {
    return false;
  }

  /* ****************************************************************** */

  private KtNoadList numerator = KtNoadList.NULL;
  private KtDimen thickness;
  private KtDelimiter fractLeft = KtDelimiter.NULL;
  private KtDelimiter fractRight = KtDelimiter.NULL;

  private KtFractionNoad makeFraction(KtNoadList num, KtField den) {
    return fractLeft != KtDelimiter.NULL
        ? new KtDelFractionNoad(new KtNoadListField(num), den, thickness, fractLeft, fractRight)
        : new KtFractionNoad(new KtNoadListField(num), den, thickness);
  }

  public boolean alreadyFractioned() {
    return numerator != KtNoadList.NULL;
  }

  public boolean isEmpty() {
    return list.isEmpty() && !alreadyFractioned();
  }
  // XXX check in TeXtp that it's correct

  /* TeXtp[1181] */
  public void fractione(KtDimen thickness) {
    numerator = getList();
    list = new KtNoadList();
    this.thickness = thickness;
  }

  /* TeXtp[1181] */
  public void fractione(KtDimen thickness, KtDelimiter left, KtDelimiter right) {
    fractione(thickness);
    fractLeft = left;
    fractRight = right;
  }

  /* TeXtp[1185] */
  public KtNoadList getList() {
    return numerator == KtNoadList.NULL
        ? list
        : new KtNoadList(makeFraction(numerator, new KtNoadListField(list)));
  }

  /* STRANGE
   * isn't this faking of left delimiter crazy?
   */
  /* TeXtp[219] */
  protected void specialShow(KtLog log, int depth, int breadth) {
    KtCntxLog.addItems(
      log, (alreadyFractioned() ? list : fakeLeftDelim( list)).noads(), depth, breadth);
    log.endLine();
    if (alreadyFractioned()) {
      log.add("this will be denominator of:");
      KtCntxLog.addItem(
          log, makeFraction(fakeLeftDelim(numerator), KtEmptyField.FIELD), depth, breadth);
    }
  }

  private KtNoadList fakeLeftDelim(KtNoadList list) {
    if (leftDelim == KtNoad.NULL) return list;
    KtNoadList faked = new KtNoadList(list.length() + 1);
    return faked.append(leftDelim).append(list);
  }

  public void setEqNo(KtNode node, boolean left) {}
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathBinPrim
// $Id: KtMathBinPrim.java,v 1.1.1.1 2000/02/29 14:17:35 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.noad.KtBinNoad;
import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtNoad;

public class KtMathBinPrim extends KtMathCompPrim {

  public KtMathBinPrim(String name) {
    super(name);
  }

  public KtNoad makeNoad(KtField field) {
    return new KtBinNoad(field);
  }
}

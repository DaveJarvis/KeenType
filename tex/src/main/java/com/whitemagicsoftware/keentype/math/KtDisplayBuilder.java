// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.DisplayBuilder
// $Id: KtDisplayBuilder.java,v 1.1.1.1 2001/03/20 11:21:57 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.node.KtNode;

public class KtDisplayBuilder extends KtMathBuilder {

  public KtDisplayBuilder(int line) {
    super(line);
  }

  public boolean isInner() {
    return false;
  }

  public String modeName() {
    return "display math";
  }

  private KtNode eqNoBox = KtNode.NULL;
  private boolean eqNoLeft = false;

  public void setEqNo(KtNode node, boolean left) {
    eqNoBox = node;
    eqNoLeft = left;
  }

  public KtNode getEqNoBox() {
    return eqNoBox;
  }

  public boolean getEqNoLeft() {
    return eqNoLeft;
  }
}

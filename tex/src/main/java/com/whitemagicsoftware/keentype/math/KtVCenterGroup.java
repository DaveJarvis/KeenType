// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.VCenterGroup
// $Id: KtVCenterGroup.java,v 1.1.1.1 2000/03/19 23:19:56 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtTreatBox;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;
import com.whitemagicsoftware.keentype.typo.KtVertGroup;

public class KtVCenterGroup extends KtVertGroup {

  protected KtDimen size;
  protected boolean exactly;
  private final KtTreatBox proc;

  public KtVCenterGroup(KtDimen size, boolean exactly, KtTreatBox proc) {
    this.size = size;
    this.exactly = exactly;
    this.proc = proc;
  }

  public void close() {
    super.close();
    proc.execute(makeBox(builder.getList()), KtNodeList.EMPTY_ENUM);
  }

  protected KtBox makeBox(KtNodeList list) {
    return KtTypoCommand.packVBox(list, size, exactly);
  }
}

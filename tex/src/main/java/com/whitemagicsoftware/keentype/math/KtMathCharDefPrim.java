// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathCharDefPrim
// $Id: KtMathCharDefPrim.java,v 1.1.1.1 2001/03/22 15:52:06 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtAssignPrim;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.command.KtRelax;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtMathCharDefPrim extends KtAssignPrim {

  private final KtPrimitive prim;

  public KtMathCharDefPrim(String name, KtPrimitive prim) {
    super(name);
    this.prim = prim;
  }

  protected void assign(KtToken src, boolean glob) {
    KtToken tok = definableToken();
    tok.define(KtRelax.getRelax(), glob);
    skipOptEquals();
    int code = KtMathPrim.scanMathCharCode(); // XXX int
    tok.define(new KtMathGiven(code, prim.getName()), glob);

    // KtCharCode	code = KtToken.makeCharCode(scanMathCharCode());
    // if (code != KtCharCode.NULL)
    // tok.define(new KtMathGiven(code, prim.getName()), glob);
    // else throw new RuntimeException("no mathchar number scanned");
  }
}

class KtMathGiven extends KtCommand implements KtNum.KtProvider {

  private final int code; // XXX int
  private final String name;

  public KtMathGiven(int code, String name) {
    this.code = code;
    this.name = name;
  }

  public void exec(KtToken src) {
    KtMathPrim.handleMathCode(code, src);
  }

  public boolean hasMathCodeValue() {
    return true;
  }

  public int getMathCodeValue() {
    return code;
  }

  public boolean sameAs(KtCommand cmd) {
    return cmd instanceof KtMathGiven && code == ((KtMathGiven) cmd).code;
  }

  public void addOn(KtLog log) {
    log.addEsc(name).add('"').add(Integer.toHexString(code).toUpperCase());
  }

  public boolean hasNumValue() {
    return true;
  }

  public KtNum getNumValue() {
    return KtNum.valueOf(code);
  }

  public final String toString() {
    return "[mathchar given: " + code + ']';
  }
}

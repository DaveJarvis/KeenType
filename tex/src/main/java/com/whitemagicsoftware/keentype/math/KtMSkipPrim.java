// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MSkipPrim
// $Id: KtMSkipPrim.java,v 1.1.1.1 2000/03/02 14:10:35 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtMSkipNoad;

public class KtMSkipPrim extends KtMathPrim {

  private final KtGlue skip;

  public KtMSkipPrim(String name, KtGlue skip) {
    super(name);
    this.skip = skip;
  }

  public KtMSkipPrim(String name) {
    super(name);
    this.skip = KtGlue.NULL;
  }

  public KtGlue getSkip() {
    return skip != KtGlue.NULL ? skip : scanMuGlue();
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1057,1060] */
  public final KtMathAction NORMAL =
      new KtMathAction() {

        public void exec(KtMathBuilder bld, KtToken src) {
          bld.addNoad(new KtMSkipNoad(getSkip()));
        }

        public KtGlue getSkipForLeaders(KtBuilder bld) {
          return getSkip();
        }
      };
}

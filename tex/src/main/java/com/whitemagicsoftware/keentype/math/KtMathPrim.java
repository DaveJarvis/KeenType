// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathPrim
// $Id: KtMathPrim.java,v 1.1.1.1 2001/03/09 11:06:32 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtActiveCharToken;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.noad.KtCharField;
import com.whitemagicsoftware.keentype.noad.KtDelimiter;
import com.whitemagicsoftware.keentype.noad.KtEmptyField;
import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.noad.KtOrdNoad;
import com.whitemagicsoftware.keentype.noad.KtTreatField;
import com.whitemagicsoftware.keentype.node.KtChrKernNode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.typo.KtAction;
import com.whitemagicsoftware.keentype.typo.KtBuilderPrim;
import com.whitemagicsoftware.keentype.typo.KtCharHandler;

public abstract class KtMathPrim extends KtBuilderPrim {

  public KtMathPrim(String name) {
    super(name);
  }

  public abstract KtMathAction mathAction();

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public interface KtMathSpacer {
    KtGlue getGlue(byte fontSize);

    String getName();
  }

  public interface KtConfig {
    void initFormula();

    void initDisplay(KtDimen preSize, KtDimen width, KtDimen indent);

    KtNoad noadForCode(int code);

    KtCharField fieldForCode(int code);

    KtNoad noadForDelCode(int code);

    KtCharField fieldForDelCode(int code);

    KtDelimiter delimiterForDelCode(int code);

    KtFontMetric familyFont(byte size, byte fam);

    String familyName(byte size, byte fam);

    KtMathSpacer mathSpacing(byte left, byte right);

    KtNum mathPenalty(byte left, byte right);
  }

  private static KtConfig config;

  public static void setMathConfig(KtConfig conf) {
    config = conf;
  }

  public static KtConfig getMathConfig() {
    return config;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static final int INTP_MAX_MATH_CODE = newIntParam();
  public static final int INTP_MAX_DEL_CODE = newIntParam();

  /* TeXtp[436] */
  public static int scanMathCharCode() {
    return KtPrim.scanAnyCode(INTP_MAX_MATH_CODE, "BadMathCharCode");
  }

  /* TeXtp[437] */
  public static int scanDelimiterCode() {
    return KtPrim.scanAnyCode(INTP_MAX_DEL_CODE, "BadDelimiterCode");
  }

  /* TeXtp[1155] */
  public static void setMathChar(KtMathBuilder bld, KtCharCode code) {
    KtNoad noad = getMathConfig().noadForCode(code.mathCode());
    if (noad != KtNoad.NULL) bld.addNoad(noad);
    else backActiveChar(code);
  }

  public static void setMathChar(KtMathBuilder bld, int code) {
    bld.addNoad(getMathConfig().noadForCode(code));
  }

  public static void handleMathCode(int code, KtToken src) {
    KtBuilder bld = getBld();
    KtCharHandler hnd = getCharHandler(bld.getClass());
    if (hnd == KtCharHandler.NULL) error("CantUseIn", str("character"), bld);
    else if (hnd instanceof KtMathCharHandler) ((KtMathCharHandler) hnd).handleMath(bld, code, src);
    else insertDollar(src);
  }

  public static class KtMathCharHandler implements KtCharHandler {

    public void handle(KtBuilder bld, KtCharCode code, KtToken src) {
      setMathChar((KtMathBuilder) bld, code);
    }

    public void handleSpace(KtBuilder bld, KtToken src) {}

    public void handleMath(KtBuilder bld, int code, KtToken src) {
      setMathChar((KtMathBuilder) bld, code);
    }
  }

  public static final KtAction DOLLAR =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          insertDollar(src);
        }
      };

  public static final KtAction ZERO_KERN =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          bld.addNode(new KtChrKernNode(KtDimen.ZERO));
        }
      };

  /* STRANGE
   * Empty KtOrdNoad is added and then removed only for the case when
   * \showlists appear inside the group.
   */
  public static final KtMathAction LEFT_BRACE =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          bld.addNoad(new KtOrdNoad(KtEmptyField.FIELD));
          pushLevel(
              new KtMathGroup(
                  new KtTreatField() {
                    public void execute(KtField field) {
                      bld.replaceLastNoad(makeOrdNoad(field));
                    }
                  }));
        }
      };

  /* TeXtp[1186] */
  public static KtNoad makeOrdNoad(KtField field) {
    KtNoad noad = field.ordinaryNoad();
    return noad != KtNoad.NULL ? noad : new KtOrdNoad( field);
  }

  /* TeXtp[1047] */
  public static void insertDollar(KtToken src) {
    backToken(src);
    insertToken(KtMathShiftToken.TOKEN);
    error("MissingDollar");
  }

  /* TeXtp[1151] */
  public static void scanField(KtTreatField proc) {
    KtField field = KtField.NULL;
    boolean again;
    do {
      again = false;
      KtToken tok = nextNonRelax();
      KtCommand cmd = meaningOf(tok);
      if (cmd.hasMathCodeValue()) field = getMathConfig().fieldForCode(cmd.getMathCodeValue());
      else if (cmd.hasDelCodeValue())
        field = getMathConfig().fieldForDelCode(cmd.getDelCodeValue());
      else {
        KtCharCode code = cmd.charCodeToAdd();
        if (code == KtCharCode.NULL) backToken(tok);
        else {
          field = getMathConfig().fieldForCode(code.mathCode());
          if (field == KtField.NULL) {
            backActiveChar(code);
            again = true;
          }
        }
      }
    } while (again);
    if (field != KtField.NULL) proc.execute(field);
    else {
      scanLeftBrace();
      pushLevel(new KtMathGroup(proc));
    }
  }

  /* TeXtp[1152] */
  private static void backActiveChar(KtCharCode code) {
    backToken(new KtActiveCharToken(code));
    /*
    KtBoolPar		exp = new KtBoolPar(true);
    KtToken		tok = new KtActiveCharToken(code);
    for (;;) {
        KtCommand	cmd = meaningOf(tok);
        if (cmd.expandable() && exp.get()) cmd.doExpansion(tok);
        else { backToken(tok); break; }
        tok = nextRawToken(exp);
    }
       */
  }

  /* STRANGE
   * cmd.charCode() is tested only to exclude \chargiven and \char; Why?
   */
  /* TeXtp[1160] */
  public static KtDelimiter scanDelimiter() {
    KtDelimiter del = KtDelimiter.NULL;
    KtToken tok = nextNonRelax();
    KtCommand cmd = meaningOf(tok);
    if (cmd.hasDelCodeValue()) del = getMathConfig().delimiterForDelCode(cmd.getDelCodeValue());
    KtCharCode code = cmd.charCodeToAdd();
    if (code != KtCharCode.NULL && cmd.charCode() != KtCharCode.NULL)
      del = getMathConfig().delimiterForDelCode(code.delCode());
    if (del != KtDelimiter.NULL) return del;
    backToken(tok);
    error("MissingDelim");
    return KtDelimiter.VOID;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MKernPrim
// $Id: KtMKernPrim.java,v 1.1.1.1 2000/03/02 14:31:21 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtMKernNoad;

public class KtMKernPrim extends KtMathPrim {

  private final KtDimen kern;

  public KtMKernPrim(String name, KtDimen kern) {
    super(name);
    this.kern = kern;
  }

  public KtMKernPrim(String name) {
    super(name);
    this.kern = KtDimen.NULL;
  }

  public KtDimen getKern() {
    return kern != KtDimen.NULL ? kern : scanMuDimen();
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1057,1061] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(KtMathBuilder bld, KtToken src) {
          bld.addNoad(new KtMKernNoad(getKern()));
        }
      };
}

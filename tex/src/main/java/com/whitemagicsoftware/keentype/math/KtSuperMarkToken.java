// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.KtSuperMaroken
// $Id: KtSuperMarkToken.java,v 1.1.1.1 2000/03/03 18:23:17 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtCharToken;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtSuperMarkToken extends KtCharToken {

  public static final KtCharCode CODE = makeCharCode('^');
  public static final KtSuperMarkToken TOKEN = new KtSuperMarkToken(CODE);

  public static final KtMaker MAKER =
      new KtMaker() {
        public KtToken make(KtCharCode code) {
          return new KtSuperMarkToken(code);
        }
      };

  public KtSuperMarkToken(KtCharCode code) {
    super(code);
  }

  public boolean match(KtCharToken tok) {
    return tok instanceof KtSuperMarkToken && tok.match( code);
  }

  public KtMaker getMaker() {
    return MAKER;
  }

  public String toString() {
    return "<SuperScript: " + code + '>';
  }

  private static KtCommand command;

  public static void setCommand(KtCommand cmd) {
    command = cmd;
  }

  public KtCommand meaning() {
    return new KtMeaning() {

      public void exec(KtToken src) {
        command.exec(src);
      }

      protected String description() {
        return "superscript character";
      }
    };
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathChoiceGroup
// $Id: KtMathChoiceGroup.java,v 1.1.1.1 2000/03/05 09:20:08 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.noad.KtNoadList;

public class KtMathChoiceGroup extends KtSimpleGroup {

  private final KtNoadList[] choices;
  private KtMathBuilder builder;
  private int index = 0;

  public KtMathChoiceGroup(KtNoadList[] choices) {
    this.choices = choices;
  }

  public void start() {
    builder = new KtFormulaBuilder(currLineNumber());
    KtBuilder.push(builder);
    scanLeftBrace();
  }

  /* TeXtp[1174] */
  public void close() {
    KtBuilder.pop();
    choices[index] = builder.getList();
    if (++index < choices.length) pushLevel(this);
  }
}

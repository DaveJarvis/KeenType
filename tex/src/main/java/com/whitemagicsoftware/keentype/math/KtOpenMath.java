// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.OpenMath
// $Id: KtOpenMath.java,v 1.1.1.1 2001/03/12 21:48:59 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtLinesShape;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.typo.KtAction;
import com.whitemagicsoftware.keentype.typo.KtBuilderCommand;
import com.whitemagicsoftware.keentype.typo.KtParagraph;

public class KtOpenMath extends KtBuilderCommand {

  private final KtTokenList.KtInserter everyMath;
  private final KtTokenList.KtInserter everyDisplay;

  public KtOpenMath(KtTokenList.KtInserter everyMath, KtTokenList.KtInserter everyDisplay) {
    this.everyMath = everyMath;
    this.everyDisplay = everyDisplay;
  }

  /* TeXtp[1138] */
  public final KtAction OPEN_MATH =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          KtToken tok = nextRawToken();
          if (meaningOf(tok).isMathShift()) startDisplay();
          else {
            backToken(tok);
            startFormula();
          }
        }
      };

  /* STRANGE
   * Why is next token read when it is not needed at all?
   */
  /* TeXtp[1138] */
  public final KtAction OPEN_FORMULA =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          backToken(nextRawToken());
          startFormula();
        }
      };

  public static final int INTP_DISPLAY_WIDOW_PENALTY = newIntParam();
  private static final KtDimen NO_WIDTH = KtDimen.MAX_VALUE.negative();

  protected void startDisplay() {
    KtDimen.KtPar lastVisibleWidth = new KtDimen.KtPar(NO_WIDTH);
    KtBuilder parBld = getBld();
    KtNodeList list = parBld.getParagraph();
    if (list != KtNodeList.NULL)
      KtParagraph.lineBreak(
          list,
          parBld.getStartLine(),
          getConfig().getIntParam(INTP_DISPLAY_WIDOW_PENALTY),
          parBld.getInitLang(),
          lastVisibleWidth);
    KtDimen preSize = lastVisibleWidth.get();
    if (preSize == KtDimen.NULL) preSize = KtDimen.MAX_VALUE;
    else if (preSize != NO_WIDTH)
      preSize =
          preSize.plus(getCurrFontMetric().getDimenParam(KtFontMetric.DIMEN_PARAM_QUAD).times(2));
    KtLinesShape shape = getTypoConfig().linesShape();
    KtBuilder old = getBld();
    int lineNo = old.getPrevGraf() + 1;
    pushLevel(new KtDisplayGroup());
    KtMathPrim.getMathConfig().initDisplay(preSize, shape.getWidth(lineNo), shape.getIndent(lineNo));
    everyDisplay.insertToks();
    old.buildPage(); // XXX is this order unavoidable?
  }

  protected void startFormula() {
    pushLevel(new KtFormulaGroup());
    KtMathPrim.getMathConfig().initFormula();
    everyMath.insertToks();
  }

  public void addOn(KtLog log) {
    log.add("internal open math");
  }
}

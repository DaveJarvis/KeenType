// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.FamilyPrim
// $Id: KtFamilyPrim.java,v 1.1.1.1 2001/02/01 16:20:20 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtAssignPrim;
import com.whitemagicsoftware.keentype.command.KtCtrlSeqToken;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.typo.KtNullFontMetric;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

public class KtFamilyPrim extends KtAssignPrim implements KtTokenList.KtProvider {

  protected final KtNumKind tabKind = new KtNumKind();

  protected final KtFontMetric defVal;

  public KtFamilyPrim(String name, KtFontMetric defVal) {
    super(name);
    this.defVal = defVal;
  }

  public KtFamilyPrim(String name) {
    super(name);
    this.defVal = KtNullFontMetric.METRIC;
  }

  protected final void set(int idx, KtFontMetric val, boolean glob) {
    if (glob) getEqt().gput(tabKind, idx, val);
    else getEqt().put(tabKind, idx, val);
  }

  public final KtFontMetric get(int idx) {
    KtFontMetric val = (KtFontMetric) getEqt().get(tabKind, idx);
    return val != KtFontMetric.NULL ? val : defVal;
  }

  public void addEqValueOn(int idx, KtLog log) {
    get(idx).getIdent().addEscapedOn(log);
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  /* TeXtp[1234] */
  protected void assign(KtToken src, boolean glob) {
    int idx = scanFamilyCode();
    skipOptEquals();
    set(idx, KtTypoCommand.scanFontMetric(), glob);
  }

  public boolean hasFontTokenValue() {
    return true;
  }

  public boolean hasFontMetricValue() {
    return true;
  }

  /* TeXtp[415,465] */
  public KtToken getFontTokenValue() {
    return new KtCtrlSeqToken(get(scanFamilyCode()).getIdent());
  }

  /* TeXtp[577] */
  public KtFontMetric getFontMetricValue() {
    return get(scanFamilyCode());
  }

  public final void init(int idx, KtFontMetric val) {
    set(idx, val, true);
  }

  public static int scanFamilyCode() {
    return scanFileCode();
  }
}

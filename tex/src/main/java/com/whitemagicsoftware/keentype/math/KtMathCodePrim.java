// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathCodePrim
// $Id: KtMathCodePrim.java,v 1.1.1.1 2000/02/18 10:08:23 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtDefCodePrim;

// XXX defVal from KtDefCodePrim is not used. Make common ancestor for them!

public class KtMathCodePrim extends KtDefCodePrim {

  /**
   * Creates a new |KtMathCodePrim| with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the |KtMathCodePrim|
   */
  public KtMathCodePrim(String name, int maxVal) {
    super(name, 0, maxVal);
  }

  public final KtNum get(int idx) {
    KtNum val = (KtNum) getEqt().get(tabKind, idx);
    return val != KtNum.NULL ? val : KtNum.valueOf( idx);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.LimitsPrim
// $Id: KtLimitsPrim.java,v 1.1.1.1 2000/03/09 16:57:37 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtNoad;

public class KtLimitsPrim extends KtMathPrim {

  private final byte limits;

  public KtLimitsPrim(String name, byte limits) {
    super(name);
    this.limits = limits;
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1159] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          KtNoad noad = bld.lastNoad();
          if (noad != KtNoad.NULL && noad.acceptsLimits())
            bld.replaceLastNoad(noad.withLimits(limits));
          else error("MisplacedLimits");
        }
      };
}

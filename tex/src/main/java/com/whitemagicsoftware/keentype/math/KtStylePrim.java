// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.StylePrim
// $Id: KtStylePrim.java,v 1.1.1.1 2000/03/09 16:58:09 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtStyleNoad;

public class KtStylePrim extends KtMathPrim {

  private final byte style;

  public KtStylePrim(String name, byte style) {
    super(name);
    this.style = style;
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1171] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(KtMathBuilder bld, KtToken src) {
          bld.addNoad(new KtStyleNoad(style));
        }
      };
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.LeftGroup
// $Id: KtLeftGroup.java,v 1.1.1.1 2001/03/22 16:13:36 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtOtherToken;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.noad.KtTreatNoadList;

public class KtLeftGroup extends KtSimpleGroup {

  private final KtToken expected;
  private final KtTreatNoadList proc;
  protected final KtFormulaBuilder builder;

  public KtLeftGroup(KtToken expected, KtTreatNoadList proc, KtFormulaBuilder builder) {
    this.expected = expected;
    this.proc = proc;
    this.builder = builder;
  }

  public KtLeftGroup(KtToken expected, KtTreatNoadList proc, KtNoad leftDelim) {
    this.expected = expected;
    this.proc = proc;
    builder = new KtFormulaBuilder(currLineNumber(), leftDelim);
  }

  public void start() {
    KtBuilder.push(builder);
  }

  /* TeXtp[1191] */
  public void stop() {
    KtBuilder.pop();
    proc.execute(builder.getList());
  }

  public KtToken expectedToken() {
    return expected;
  }

  /* TeXtp[1065] */
  public void reject(KtToken tok) {
    backToken(tok);
    KtTokenList.KtBuffer buf = new KtTokenList.KtBuffer(2);
    buf.append(expected); // XXX make ins on demand and keep it
    buf.append(new KtOtherToken(KtToken.makeCharCode('.')));
    KtTokenList ins = buf.toTokenList();
    insertList(ins);
    error("MissingInserted", ins);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.ScriptPrim
// $Id: KtScriptPrim.java,v 1.1.1.1 2000/03/03 19:22:59 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtEmptyField;
import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtNoad;
import com.whitemagicsoftware.keentype.noad.KtOrdNoad;
import com.whitemagicsoftware.keentype.noad.KtTreatField;

public abstract class KtScriptPrim extends KtMathPrim {

  public KtScriptPrim(String name) {
    super(name);
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* TeXtp[1176] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          KtNoad last = bld.lastNoad();
          boolean scriptable = false;
          if (last != KtNoad.NULL && last.isScriptable()) {
            scriptable = true;
            if (conflicting(last)) {
              error(errorIdent());
              scriptable = false;
            }
          }
          final KtNoad body;
          if (scriptable) body = last;
          else {
            body = new KtOrdNoad(KtEmptyField.FIELD);
            bld.addNoad(body);
          }
          scanField(
              new KtTreatField() {
                public void execute(KtField field) {
                  bld.replaceLastNoad(scripted(body, field));
                }
              });
        }
      };

  protected abstract boolean conflicting(KtNoad noad);

  protected abstract KtNoad scripted(KtNoad noad, KtField script);

  protected abstract String errorIdent();
}

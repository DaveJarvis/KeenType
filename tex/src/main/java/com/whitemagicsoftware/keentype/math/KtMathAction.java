// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathAction
// $Id: KtMathAction.java,v 1.1.1.1 2000/03/02 08:48:32 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.typo.KtAction;

public abstract class KtMathAction extends KtAction {

  public static final KtMathAction NULL = null;

  public final void exec(KtBuilder bld, KtToken src) {
    exec((KtMathBuilder) bld, src);
  }

  public abstract void exec(KtMathBuilder bld, KtToken src);
}

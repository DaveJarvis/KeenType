// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.math.MathAccentPrim
// $Id: KtMathAccentPrim.java,v 1.1.1.1 2000/03/02 15:00:13 ksk Exp $
package com.whitemagicsoftware.keentype.math;

import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.noad.KtAccentNoad;
import com.whitemagicsoftware.keentype.noad.KtCharField;
import com.whitemagicsoftware.keentype.noad.KtEmptyField;
import com.whitemagicsoftware.keentype.noad.KtField;
import com.whitemagicsoftware.keentype.noad.KtTreatField;

public class KtMathAccentPrim extends KtMathPrim {

  public KtMathAccentPrim(String name) {
    super(name);
  }

  public KtMathAction mathAction() {
    return NORMAL;
  }

  /* STRANGE
   * Empty KtNoad is added and then removed only for the case when
   * \showlists appear inside the group.
   */
  /* TeXtp[1165] */
  public final KtMathAction NORMAL =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          final KtCharField accent = getMathConfig().fieldForCode(scanMathCharCode());
          bld.addNoad(new KtAccentNoad(accent, KtEmptyField.FIELD));
          scanField(
              new KtTreatField() {
                public void execute(KtField field) {
                  bld.replaceLastNoad(new KtAccentNoad(accent, field));
                }
              });
        }
      };

  public final KtMathAction FORCE_MATH_ACCENT =
      new KtMathAction() {
        public void exec(final KtMathBuilder bld, KtToken src) {
          error("AccentInMathMode", esc(getName()));
          NORMAL.exec(bld, src);
        }
      };
}

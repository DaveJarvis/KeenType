// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.io;

/**
 * Responsible for printing to the terminal and log file.
 */
public class KtStandardLog implements KtLog {

  public interface KtEscape {
    KtCharCode getEscape();
  }

  protected final KtLineOutput mOut;
  protected final KtEscape mEsc;

  public KtStandardLog( final KtLineOutput out, final KtEscape esc ) {
    mOut = out;
    mEsc = esc;
  }

  public final KtLineOutput getOutput() {
    return mOut;
  }

  public final KtLog add( char ch ) {
    mOut.add( ch );
    return this;
  }

  public final KtLog add( char ch, int count ) {
    while( count-- > 0 ) { mOut.add( ch ); }
    return this;
  }

  public final KtLog add( String str ) {
    mOut.add( str );
    return this;
  }

  public final KtLog add( KtCharCode code ) {
    mOut.add( code );
    return this;
  }

  public final KtLog endLine() {
    mOut.endLine();
    return this;
  }

  public final KtLog startLine() {
    mOut.startLine();
    return this;
  }

  public final KtLog flush() {
    mOut.flush();
    return this;
  }

  public final void close() {
    mOut.close();
  }

  public final KtLog add( boolean val ) {
    return add( String.valueOf( val ) );
  }

  public final KtLog add( int num ) {
    return add( String.valueOf( num ) );
  }

  public final KtLog addEsc() {
    KtCharCode escape = mEsc.getEscape();
    return escape != KtCharCode.NULL ? add( escape ) : this;
  }

  public final KtLog addEsc( String str ) {
    addEsc();
    return add( str );
  }

  public final KtLog add( KtLoggable x ) {
    x.addOn( this );
    return this;
  }

  public KtLog add( KtLoggable[] array ) {
    return add( array, 0, array.length );
  }

  public KtLog add( KtLoggable[] array, int offset, int length ) {
    int i = 0;
    while( i < length ) { add( array[ offset + i++ ] ); }
    return this;
  }

  public final KtLog resetCount() {
    mOut.resetCount();
    return this;
  }

  public final int getCount() {
    return mOut.getCount();
  }

  public final KtLog voidCounter() {
    return new KtStandardLog( mOut.voidCounter(), mEsc );
  }

  public final KtLog sepRoom( int count ) {
    if( !mOut.stillFits( count ) ) { mOut.endLine(); }
    else if( !mOut.isStartLine() ) { mOut.add( ' ' ); }
    return this;
  }
}

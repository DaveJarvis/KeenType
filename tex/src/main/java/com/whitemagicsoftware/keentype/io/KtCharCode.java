// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.io;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtNum;

public interface KtCharCode extends Serializable, KtLoggable {

  /** The reference to non existent internal character code */
  KtCharCode NULL = null;

  /** Symbolic constant for non ascii character code */
  char NO_CHAR = 0xffff;

  interface KtCodeWriter {
    void writeCode(KtCharCode code);
  }

  interface KtCharWriter {
    void writeChar(char chr);
  }

  interface KtMaker {
    KtCharCode make(char chr);

    KtCharCode make(int num);

    boolean isNewLine(char chr);

    void writeExpCodes(char chr, KtCodeWriter out);

    void writeExpChars(char chr, KtCharWriter out);
  }

  char toChar();

  int numValue();

  char toCanonicalLetter();

  boolean match(KtCharCode x);

  boolean match(char c);

  boolean match(int num);

  boolean match(KtNum num);

  KtCharCode toLowerCase();

  KtCharCode toUpperCase();

  int spaceFactor();

  int mathCode();

  int delCode();

  boolean isLetter();

  boolean isEscape();

  boolean isEndLine();

  boolean isNewLine();

  boolean startsExpand();

  boolean startsFileExt();

  void writeExpCodes(KtCodeWriter out);

  void writeExpChars(KtCharWriter out);

  void writeRawChars(KtCharWriter out);
}

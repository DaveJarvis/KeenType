// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.NullLog
// $Id: KtNullLog.java,v 1.1.1.1 1999/06/09 10:08:22 ksk Exp $
package com.whitemagicsoftware.keentype.io;

public class KtNullLog implements KtLog {

  public static final KtNullLog LOG = new KtNullLog();

  public final KtLog add(char ch) {
    return this;
  }

  public final KtLog add(char ch, int count) {
    return this;
  }

  public final KtLog add(String str) {
    return this;
  }

  public final KtLog add(KtCharCode x) {
    return this;
  }

  public final KtLog endLine() {
    return this;
  }

  public final KtLog startLine() {
    return this;
  }

  public final KtLog flush() {
    return this;
  }

  public final void close() {}

  public final KtLog add(boolean val) {
    return this;
  }

  public final KtLog add(int num) {
    return this;
  }

  public final KtLog addEsc() {
    return this;
  }

  public final KtLog addEsc(String str) {
    return this;
  }

  public final KtLog add(KtLoggable x) {
    return this;
  }

  public final KtLog add(KtLoggable[] array) {
    return this;
  }

  public final KtLog add(KtLoggable[] array, int offset, int length) {
    return this;
  }

  public final KtLog resetCount() {
    return this;
  }

  public final int getCount() {
    return 0;
  }

  public final KtLog voidCounter() {
    return LOG;
  }

  public final KtLog sepRoom(int count) {
    return this;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.BufferLineOutput
// $Id: KtBufferLineOutput.java,v 1.1.1.1 2001/02/03 13:13:46 ksk Exp $
package com.whitemagicsoftware.keentype.io;

public final class KtBufferLineOutput extends KtBaseLineOutput
    implements KtLoggable, KtCharCode.KtCodeWriter {

  private final KtCharCode[] buffer;
  private final int maxCount;
  private int pos;
  private final KtCharCode.KtMaker maker;

  public KtBufferLineOutput(int size, int maxCount, KtCharCode.KtMaker maker) {
    buffer = new KtCharCode[size];
    this.maxCount = maxCount;
    this.maker = maker;
    pos = 0;
  }

  public void add(KtCharCode code) {
    code.writeExpCodes(this);
  }

  public void add(char chr) {
    maker.writeExpCodes(chr, this);
  }

  public void writeCode(KtCharCode code) {
    if (maxCount <= 0 || pos < maxCount) buffer[pos++ % buffer.length] = code;
    charCount++;
  }

  public void reset() {
    for (int i = 0; i < buffer.length; buffer[i++] = KtCharCode.NULL)
      ;
    pos = 0;
    resetCount();
  }

  public int size() {
    return pos <= buffer.length ? pos : buffer.length;
  }

  public void addOn(KtLog log, int idx) {
    log.add(codeAt(idx));
  }

  public void addOn(KtLog log, int beg, int end) {
    while (beg < end) log.add(codeAt(beg++));
  }

  public void addOn(KtLog log) {
    addOn(log, 0, size());
  }

  private KtCharCode codeAt(int idx) {
    if (idx >= 0) {
      if (pos <= buffer.length) {
        if (idx < pos) return buffer[idx];
      } else if (idx < buffer.length) return buffer[(pos + idx) % buffer.length];
    }
    throw new ArrayIndexOutOfBoundsException("KtBufferLineOutput: index out of range: " + idx);
  }
}

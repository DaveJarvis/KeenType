// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.LineInput
package com.whitemagicsoftware.keentype.io;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;

/**
 * |KtLineInput| reads the input line by line and performs the conversions from
 * input character codes to their internal representation.
 */
public class KtLineInput {

  /**
   * Value returned by |readLine| when the input is finished.
   */
  public static final KtInputLine EOF = KtInputLine.NULL;

  /**
   * Underlying reader
   */
  private final LineNumberReader mReader;

  /**
   * Input character mapper
   */
  private final KtInputLine.KtMapper mMapper;

  /**
   * Creates |KtLineInput| with given |LineNumberReader| and |KtInputLine.KtMapper|.
   *
   * @param reader the LineNumberReader to read from.
   * @param mapper the mapper for character conversions etc.
   */
  public KtLineInput(
    final LineNumberReader reader,
    final KtInputLine.KtMapper mapper ) {
    assert reader != null;
    assert mapper != null;

    this.mReader = reader;
    this.mMapper = mapper;
  }

  /**
   * Creates |KtLineInput| with given |Reader| and |KtInputLine.KtMapper|.
   *
   * @param reader the Reader to read from.
   * @param mapper the mapper for character conversions etc.
   */
  public KtLineInput( final Reader reader, final KtInputLine.KtMapper mapper ) {
    this( new LineNumberReader( reader ), mapper );
  }

  /**
   * Releases the system resources associated with the |KtLineInput|.
   */
  public void close() {
    try {
      mReader.close();
    } catch( final IOException ignored ) { }
  }

  /**
   * Reads one line from the input, remove trailing characters which should
   * be ignored (usually spaces) and appends end line character if defined.
   *
   * @return an object representing the current input line or |EOF| if the
   * input is finished.
   */
  public KtInputLine readLine() {
    String str;

    try {
      str = mReader.readLine();
    } catch( final IOException ignored ) {
      return EOF;
    }

    return str == null ? EOF : new KtInputLine( str, mMapper );
  }

  public int getLineNumber() {
    return mReader.getLineNumber();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.Log
// $Id: KtLog.java,v 1.1.1.1 1999/06/09 10:07:58 ksk Exp $
package com.whitemagicsoftware.keentype.io;

public interface KtLog {

  KtLog NULL = null;

  KtLog add(char ch);

  KtLog add(char ch, int count);

  KtLog add(String str);

  KtLog add(KtCharCode x);

  KtLog endLine();

  KtLog startLine();

  KtLog flush();

  void close();

  KtLog add(boolean val);

  KtLog add(int num);

  KtLog addEsc();

  KtLog addEsc(String str);

  KtLog add(KtLoggable x);

  KtLog add(KtLoggable[] array);

  KtLog add(KtLoggable[] array, int offset, int length);

  KtLog resetCount();

  int getCount();

  KtLog voidCounter();

  KtLog sepRoom(int count);
}

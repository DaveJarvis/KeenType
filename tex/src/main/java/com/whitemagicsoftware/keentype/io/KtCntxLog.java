// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.CntxLog
// $Id: KtCntxLog.java,v 1.1.1.1 2000/05/02 06:02:30 ksk Exp $
package com.whitemagicsoftware.keentype.io;

public final class KtCntxLog {

  private final int maxDepth;
  private final int maxCount;
  private final String prefix;

  private KtCntxLog(int md, int mc, String p) {
    maxDepth = md;
    maxCount = mc;
    prefix = p;
  }

  private KtCntxLog(int md, int mc) {
    maxDepth = md;
    maxCount = mc;
    prefix = "";
  }

  private int count = 0;

  /* STRANGE
   * this method is public (and count is not a local variable)
   * only for third part of discretionary
   */
  /* TeXtp[182] */
  public void addItems(KtLog log, KtCntxLoggableEnum items) {
    if (prefix.length() <= maxDepth) {
      while (count <= maxCount && items.hasMoreContextLoggables()) {
        log.endLine().add(prefix);
        if (count++ == maxCount) log.add("etc.");
        else items.nextContextLoggable().addOn(log, this);
      }
    } else if (count <= maxCount && items.hasMoreContextLoggables()) log.add(" []");
  }

  private void addItem(KtLog log, KtCntxLoggable item) {
    if (prefix.length() <= maxDepth) {
      if (count <= maxCount) {
        log.endLine().add(prefix);
        if (count++ == maxCount) log.add("etc.");
        else item.addOn(log, this);
      }
    } else if (count <= maxCount) log.add(" []");
  }

  private KtCntxLog descendant(char p) {
    return new KtCntxLog(maxDepth, maxCount, prefix + p);
  }

  public void addOn(KtLog log, KtCntxLoggableEnum items, char p) {
    descendant(p).addItems(log, items);
  }

  public void addOn(KtLog log, KtCntxLoggable item, char p) {
    descendant(p).addItem(log, item);
  }

  public void addOn(KtLog log, KtCntxLoggableEnum items) {
    addOn(log, items, '.');
  }

  public void addOn(KtLog log, KtCntxLoggable item) {
    addOn(log, item, '.');
  }

  public static void addItems(KtLog log, KtCntxLoggableEnum items, int md, int mc) {
    new KtCntxLog(md, mc).addItems( log, items);
  }

  public static void addItem(KtLog log, KtCntxLoggable item, int md, int mc) {
    new KtCntxLog(md, mc).addItem( log, item);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.StringLineOutput
// $Id: KtStringLineOutput.java,v 1.1.1.1 2001/02/06 12:31:47 ksk Exp $
package com.whitemagicsoftware.keentype.io;

public final class KtStringLineOutput extends KtBaseLineOutput implements KtCharCode.KtCharWriter {

  private final StringBuffer buf;

  public KtStringLineOutput(StringBuffer buf) {
    this.buf = buf;
  }

  public KtStringLineOutput() {
    buf = new StringBuffer();
  }

  public StringBuffer getBuffer() {
    return buf;
  }

  public String toString() {
    return buf.toString();
  }

  public void add(KtCharCode code) {
    code.writeRawChars(this);
  }

  public void add(char chr) {
    buf.append(chr);
  }

  public void writeChar(char chr) {
    buf.append(chr);
  }
}

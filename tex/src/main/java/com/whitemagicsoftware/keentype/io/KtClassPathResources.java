/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved. */
package com.whitemagicsoftware.keentype.io;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Responsible for finding a resource anywhere on the class path.
 */
public class KtClassPathResources {
  private static final Map<URI, String> RESOURCES = new HashMap<>( 256 );
  public static final String GLOB_FONTS = "**.{ttf,otf,tfm}";
  public static final String GLOB_TEX = "**.{tex}";

  static {
    try {
      KtResourceWalker.walk( "fonts", GLOB_FONTS, KtClassPathResources::accept );
      KtResourceWalker.walk( "base", GLOB_TEX, KtClassPathResources::accept );
      KtResourceWalker.walk( "ext", GLOB_TEX, KtClassPathResources::accept );
    } catch( final URISyntaxException | IOException e ) {
      throw new RuntimeException( e );
    }
  }

  /**
   * Gets the first resource matching the given regular expression.
   *
   * @param regex Regular expression to match as part of a file name.
   * @return The first resource that matches the given regular expression.
   */
  public static Optional<URI> getResource( final Pattern regex ) {
    for( final var resource : RESOURCES.entrySet() ) {
      final var matcher = regex.matcher( resource.getValue() );

      if( matcher.matches() ) {
        return Optional.of( resource.getKey() );
      }
    }

    return Optional.empty();
  }

  private static void accept( final Path path ) {
    RESOURCES.put( path.toUri(), path.toAbsolutePath().toString() );
  }
}

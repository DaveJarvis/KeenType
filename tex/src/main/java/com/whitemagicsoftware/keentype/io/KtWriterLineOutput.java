// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.WriterLineOutput
// $Id: KtWriterLineOutput.java,v 1.1.1.1 2001/02/03 13:22:24 ksk Exp $
package com.whitemagicsoftware.keentype.io;

import java.io.IOException;
import java.io.Writer;

public final class KtWriterLineOutput extends KtBaseLineOutput implements KtCharCode.KtCharWriter {

  private final Writer out;
  private final KtCharCode.KtMaker maker;
  private final boolean worryRoom;
  private final int maxLineChars;
  private int numLineChars = 0;
  private final String lineSeparator;
  private boolean trouble = false;

  public KtWriterLineOutput(Writer out, KtCharCode.KtMaker maker, boolean worryRoom, int maxLineChars) {
    this.out = out;
    this.maker = maker;
    this.worryRoom = worryRoom && maxLineChars > 0;
    this.maxLineChars = maxLineChars;
    lineSeparator = System.getProperty("line.separator");
  }

  public KtWriterLineOutput(Writer out, KtCharCode.KtMaker maker) {
    this(out, maker, false, 0);
  }

  public void add( KtCharCode code) {
    if (code.isNewLine()) endLine();
    else code.writeExpChars(this);
  }

  public void add( char chr) {
    if (maker.isNewLine(chr)) endLine();
    else maker.writeExpChars(chr, this);
  }

  public void writeChar( char chr) {
    try {
      out.write(chr);
      charCount++;
      numLineChars++;
      if (maxLineChars > 0 && numLineChars >= maxLineChars) endLine();
    } catch (IOException x) {
      trouble = true;
    }
  }

  public void addRaw( String str) {
    try {
      out.write(str);
    } catch (IOException x) {
      trouble = true;
    }
  }

  public void endLine() {
    try {
      out.write(lineSeparator);
      out.flush();
    } catch (IOException x) {
      trouble = true;
    }
    numLineChars = 0;
  }

  public void flush() {
    try {
      out.flush();
    } catch (IOException x) {
      trouble = true;
    }
  }

  public void close() {
    try {
      out.close();
    } catch (IOException x) {
      trouble = true;
    }
  }

  public void setStartLine() {
    numLineChars = 0;
  }

  public boolean isStartLine() {
    return numLineChars == 0;
  }

  public boolean stillFits( int count) {
    return !(worryRoom && numLineChars + count > maxLineChars);
  }
}

package com.whitemagicsoftware.keentype.io;

import com.whitemagicsoftware.keentype.command.KtToken;

/**
 * Represents reaching the end of a stream.
 */
public class KtEofToken extends KtToken {
  /**
   * EOF tokens must be unique.
   *
   * @param ignored Unused.
   * @return {@code false}, always.
   */
  @Override
  public boolean match( final KtToken ignored ) {
    return false;
  }

  /**
   * Writes a representation of this token to the given logger.
   *
   * @param log Where to write the logging information about this token.
   */
  @Override
  public void addOn( final KtLog log ) {
    log.add( "<EOF>" );
  }
}

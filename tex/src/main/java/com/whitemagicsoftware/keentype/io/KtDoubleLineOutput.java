// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.io.DoubleLineOutput
// $Id: KtDoubleLineOutput.java,v 1.1.1.1 1999/04/19 07:37:15 ksk Exp $
package com.whitemagicsoftware.keentype.io;

public final class KtDoubleLineOutput extends KtLineOutput {

  KtLineOutput first;
  KtLineOutput second;

  public KtDoubleLineOutput(KtLineOutput first, KtLineOutput second) {
    this.first = first;
    this.second = second;
  }

  public void add( char ch) {
    first.add(ch);
    second.add(ch);
  }

  public void add( String str) {
    first.add(str);
    second.add(str);
  }

  public void add( KtCharCode code) {
    first.add(code);
    second.add(code);
  }

  public void endLine() {
    first.endLine();
    second.endLine();
  }

  public void flush() {
    first.flush();
    second.flush();
  }

  public void close() {
    first.close();
    second.close();
  }

  public void setStartLine() {
    first.setStartLine();
    second.setStartLine();
  }

  public boolean isStartLine() {
    return first.isStartLine() && second.isStartLine();
  }

  public boolean stillFits( int count) {
    return first.stillFits( count) && second.stillFits( count);
  }

  public void resetCount() {
    first.resetCount();
    second.resetCount();
  }

  public int getCount() {
    int x = first.getCount();
    int y = second.getCount();
    return x > y ? x : y;
  }

  public KtLineOutput voidCounter() {
    return new KtDoubleLineOutput(first.voidCounter(), second.voidCounter());
  }
}

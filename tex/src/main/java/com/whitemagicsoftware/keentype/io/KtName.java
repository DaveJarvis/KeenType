// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.io;

import java.io.Serializable;
import java.util.Vector;

/**
 * This class represents a name consisting of internal character codes. It is
 * used for control sequence names, names of primitive commands, etc.
 */
public class KtName implements Serializable, KtLoggable {

  public static final KtName NULL = null;
  public static final KtName EMPTY = new KtName();

  /** |KtBuffer| is used for incremental building of |KtName|. */
  public static class KtBuffer implements Serializable {

    /** Internal representation of internal character string */
    private final Vector<KtCharCode> data;

    /** Makes a |KtBuffer| with default initial size. */
    public KtBuffer() {
      data = new Vector<>();
    }

    /**
     * Makes a |KtBuffer| with given initial size.
     *
     * @param size the initial size.
     */
    public KtBuffer(int size) {
      data = new Vector<>( size );
    }

    /**
     * Appends one internal character code on the end of the internal character string.
     *
     * @param code the internal character code to be appended.
     * @return the |KtBuffer| for subsequent appends.
     */
    public KtBuffer append(KtCharCode code) {
      data.addElement(code);
      return this;
    }

    /**
     * Appends sequence of internal character codes on the end of the internal character string.
     *
     * @param codes the array of internal character codes to be appended.
     * @param offset the starting offset of the sequence.
     * @param count the number of codes to be appended.
     * @return the |KtBuffer| for subsequent appends.
     */
    public KtBuffer append(KtCharCode[] codes, int offset, int count) {
      data.ensureCapacity(data.size() + count);
      while (count-- > 0) data.addElement(codes[offset++]);
      return this;
    }

    /**
     * Appends sequence of internal character codes on the end of the internal character string.
     *
     * @param codes the array of internal character codes to be appended.
     * @return the |KtBuffer| for subsequent appends.
     */
    public KtBuffer append(KtCharCode[] codes) {
      return append(codes, 0, codes.length);
    }

    /**
     * Appends internal character string on the end of the internal character string.
     *
     * @param name the internal character string to be appended.
     * @return the |KtBuffer| for subsequent appends.
     */
    public KtBuffer append(KtName name) {
      return append(name.codes);
    }

    public void clear() {
      data.clear();
    }

    public int length() {
      return data.size();
    }

    public KtCharCode codeAt(int index) {
      return data.elementAt(index);
    }

    public void getCodes(int beg, int end, KtCharCode[] dst, int offset) {
      while (beg < end) dst[offset++] = codeAt(beg++);
    }

    /**
     * Gives a hash code for an internal character string.
     *
     * @return the hash code for this object.
     */
    @Override
    public int hashCode() {
      int h = 0;

      if( length() > 0 ) {
        for( int i = length() - 1; i >= 0; i-- ) {
          h = ((h << 5) - h) + codeAt( i ).hashCode();
        }
      }

      return h;
    }

    /**
     * Compares this internal character string to the specified object. The
     * result is |true| if and only if the argument is not |null| and is the
     * |KtName| object representing the same internal character string.
     *
     * @param o the object to compare this internal character string against.
     * @return |true| if the argument is equal, |false| otherwise.
     */
    @Override
    public final boolean equals( final Object o ) {
      if( o instanceof KtBuffer other ) {
        if( length() == other.length() ) {
          for( int i = length() - 1; i >= 0; i-- ) {
            if( !codeAt( i ).equals( other.codeAt( i ) ) ) {
              return false;
            }
          }

          return true;
        }
      }

      return false;
    }

    /**
     * Gives the |KtName| corresponding to the contens of the |KtBuffer|.
     *
     * @return the corresponding |KtName|.
     */
    public KtName toName() {
      KtCharCode[] codes = new KtCharCode[length()];
      data.copyInto(codes);
      return new KtName(codes);
    }
  }

  /** Representation of internal character string */
  private final KtCharCode[] codes;

  /**
   * Makes a |KtName| of given sequence of internal character codes.
   *
   * @param codes the internal character code array.
   */
  public KtName(KtCharCode[] codes) {
    this.codes = codes;
  }

  /**
   * Makes a |KtName| of given single internal character code.
   *
   * @param code the single internal character code.
   */
  public KtName(KtCharCode code) {
    codes = new KtCharCode[1];
    codes[0] = code;
  }

  /** Makes an empty |KtName| (of length 0). */
  public KtName() {
    codes = new KtCharCode[0];
  }

  /**
   * Tells the length of the |KtName|.
   *
   * @return the length of this |KtName|.
   */
  public final int length() {
    return codes.length;
  }

  /**
   * Gives an internal character code at the given index. An index ranges
   * from |0| to |length() - 1|.
   *
   * @param index the index of the internal character code.
   * @return the code at the specified index of this |KtName|.
   * @exception ArrayIndexOutOfBoundsException for invalid index.
   */
  public KtCharCode codeAt(final int index) {
    return codes[index];
  }

  public int hashCodeAt(final int index) {
    return codes[index].hashCode();
  }

  public void getCodes(int beg, int end, KtCharCode[] dst, int offset) {
    while (beg < end) dst[offset++] = codes[beg++];
  }

  public void getCodes(KtCharCode[] dst, int offset) {
    getCodes(0, codes.length, dst, offset);
  }

  /**
   * Gives a hash code for an internal character string.
   *
   * @return the hash code for this object.
   */
  @Override
  public int hashCode() {
    int h = 0;

    if( length() > 0 ) {
      for( int i = length() - 1; i >= 0; i-- ) {
        h = ((h << 5) - h) + hashCodeAt( i );
      }
    }

    return h;
  }

  /**
   * Compares this internal character string to the specified object. The result is |true| if and
   * only if the argument is not |null| and is the |KtName| object representing the same internal
   * character string.
   *
   * @param o the object to compare this internal character string against.
   * @return |true| if the argument is equal, |false| otherwise.
   */
  @Override
  public final boolean equals( final Object o ) {
    if( o instanceof KtName other ) {
      if( length() == other.length() ) {
        for( int i = length() - 1; i >= 0; i-- ) {
          if( !codeAt( i ).equals( other.codeAt( i ) ) ) {
            return false;
          }
        }

        return true;
      }
    }

    return false;
  }

  public boolean match( final KtName n ) {
    if( length() == n.length() ) {
      for( int i = length() - 1; i >= 0; i-- ) {
        if( !codeAt( i ).match( n.codeAt( i ) ) ) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public void addOn(KtLog log) {
    log.add(codes);
  }

  public void addEscapedOn(KtLog log) {
    log.addEsc().add(codes);
  }

  public void addProperlyEscapedOn(KtLog log) {
    switch( codes.length ) {
      case 0 -> log.addEsc( "csname" ).addEsc( "endcsname" );
      case 1 -> {
        log.addEsc().add( codes[ 0 ] );
        if( codes[ 0 ].isLetter() ) log.add( ' ' );
      }
      default -> log.addEsc().add( codes ).add( ' ' );
    }
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    int i = 0;

    while( i < length() ) {
      buf.append( codeAt( i++ ) );
    }

    return buf.toString();
  }
}

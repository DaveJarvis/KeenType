// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.MigratingNode
// $Id: KtMigratingNode.java,v 1.1.1.1 2000/05/27 02:27:39 ksk Exp $
package com.whitemagicsoftware.keentype.node;

public abstract class KtMigratingNode extends KtBaseNode {
  /* corresponding to adjust_node, ins_node, mark_node */

  public boolean sizeIgnored() {
    return true;
  }

  public boolean isMigrating() {
    return true;
  }

  public KtNodeEnum getMigration() {
    return KtNodeList.nodes(this);
  }

  public byte afterWord() {
    return SUCCESS;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HShiftNode
// $Id: KtHShiftNode.java,v 1.1.1.1 2000/06/06 08:23:11 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtHShiftNode extends KtAnyShiftNode {
  /* corresponding to ANY_node */

  protected KtHShiftNode(KtNode node, KtDimen shift) {
    super(node, shift);
  }

  public static KtNode shiftingUp(KtNode node, KtDimen shift) {
    return shift.isZero() ? node : new KtHShiftNode( node, shift.negative());
  }

  public static KtNode shiftingDown(KtNode node, KtDimen shift) {
    return shift.isZero() ? node : new KtHShiftNode( node, shift);
  }

  public KtDimen getHeight() {
    return node.getHeight().minus(shift);
  }

  public KtDimen getWidth() {
    return node.getWidth();
  }

  public KtDimen getDepth() {
    return node.getDepth().plus(shift);
  }

  public KtDimen getLeftX() {
    return node.getLeftX();
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    setter.moveDown(shift);
    node.typeSet(setter, sctx.shiftedUp(shift));
    setter.moveUp(shift);
  }

  public KtDimen getHeight(KtGlueSetting setting) {
    return node.getHeight(setting).minus(shift);
  }

  public KtDimen getWidth(KtGlueSetting setting) {
    return node.getWidth(setting);
  }

  public KtDimen getDepth(KtGlueSetting setting) {
    return node.getDepth(setting).plus(shift);
  }

  public KtDimen getLeftX(KtGlueSetting setting) {
    return node.getLeftX(setting);
  }

  public String toString() {
    return "HShift(" + node + "; " + shift + ')';
  }
}

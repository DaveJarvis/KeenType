// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.PenaltyNode
// $Id: KtPenaltyNode.java,v 1.1.1.1 2000/06/06 08:27:53 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtPenaltyNode extends KtDiscardableNode {
  /* root corresponding to penalty_node */

  protected final KtNum pen;

  public KtPenaltyNode(KtNum pen) {
    this.pen = pen;
  }

  public KtNum getPenalty() {
    return pen;
  }

  public boolean sizeIgnored() {
    return true;
  }

  public boolean isPenalty() {
    return true;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("penalty ").add(pen.toString());
  }

  public void addBreakDescOn(KtLog log) {
    log.addEsc("penalty");
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return pen.intVal();
  }

  public byte afterWord() {
    return SUCCESS;
  }

  public String toString() {
    return "Penalty(" + pen + ')';
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.Box
// $Id: KtBox.java,v 1.1.1.1 2000/05/02 21:17:23 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtLog;

public interface KtBox extends KtNode {

  KtBox NULL = null;

  interface KtProvider {
    KtBox getBoxValue();
  }
  // CCC jikes requires public here

  boolean isVoid();

  boolean isHBox();

  boolean isVBox();

  KtBoxSizes getSizes();

  KtBox pretendSizesCopy(KtBoxSizes sizes);

  KtBox pretendingWidth(KtDimen width);

  void addOn(KtLog log, int maxDepth, int maxCount);

  void typeSet(KtTypesetter setter);

  KtNodeEnum getHorizList();

  KtNodeEnum getVertList();

  /* STRANGE
   * box.addOn(log, maxDepth, maxCount) could be just:
   * KtCntxLog.addItem(log, box, maxDepth, maxCount)
   * (as in case of KtAnyBoxNode) but KtVoidBoxNode is an exception
   */

}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.Node
// $Id: KtNode.java,v 1.1.1.1 2000/06/06 11:51:31 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtTokenList; // DDD
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtCntxLoggable;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;

public interface KtNode extends Serializable, KtCntxLoggable {
  /* corresponding to ANY_node */

  KtNode NULL = null;
  int INF_PENALTY = KtDimen.INF_BAD;
  int EJECT_PENALTY = -INF_PENALTY;

  KtDimen getHeight();

  KtDimen getDepth();

  KtDimen getHstr();

  byte getHstrOrd();

  KtDimen getHshr();

  byte getHshrOrd();

  KtDimen getWidth();

  KtDimen getLeftX();

  KtDimen getWstr();

  byte getWstrOrd();

  KtDimen getWshr();

  byte getWshrOrd();

  boolean sizeIgnored();

  boolean isPenalty();

  boolean isKern();

  boolean hasKern(); /* STRANGE : only for [424] */

  boolean isSkip();

  boolean isMuSkip();

  boolean isBox();

  boolean isCleanBox(); /* STRANGE : only for [720] */

  boolean isMigrating();

  boolean isMark();

  boolean isInsertion();

  KtNum getPenalty();

  KtDimen getKern();

  KtGlue getSkip();

  KtGlue getMuSkip();

  KtBox getBox();

  KtDimen getItalCorr();

  KtNodeEnum getMigration();

  KtTokenList getMark();

  KtInsertion getInsertion();

  void typeSet(KtTypesetter setter, KtSettingContext sctx);

  KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric);

  void addOn(KtLog log, KtCntxLog cntx);

  void addOn(KtLog log, KtCntxLog cntx, KtDimen shift);

  void syncVertIfBox(KtTypesetter setter);

  KtDimen getHeight(KtGlueSetting setting);

  KtDimen getDepth(KtGlueSetting setting);

  KtDimen getWidth(KtGlueSetting setting);

  KtDimen getLeftX(KtGlueSetting setting);

  void addBreakDescOn(KtLog log);

  boolean discardable();

  boolean isKernBreak();

  boolean canPrecedeSkipBreak();

  boolean canFollowKernBreak();

  boolean allowsSpaceBreaking();

  boolean forbidsSpaceBreaking();

  int breakPenalty(KtBreakingCntx brCntx);

  boolean isHyphenBreak();

  KtDimen preBreakWidth();

  KtDimen postBreakWidth();

  KtNodeEnum atBreakReplacement();

  KtNodeEnum postBreakNodes();

  boolean discardsAfter();

  boolean canBePartOfDiscretionary();

  void contributeVisible(KtVisibleSummarizer summarizer);

  boolean kernAfterCanBeSpared();

  boolean isKernThatCanBeSpared();

  KtNode trailingKernSpared();

  KtNode reboxedToWidth(KtDimen width);

  byte FAILURE = 0;
  byte SKIP = 1;
  byte SUCCESS = 2;

  boolean startsWordBlock();

  byte beforeWord();

  boolean canBePartOfWord();

  KtFontMetric uniformMetric();

  KtLanguage alteringLanguage();

  void contributeCharCodes(KtName.KtBuffer buf);

  byte afterWord();

  boolean rightBoundary();

  boolean providesRebuilder(boolean prev);

  KtWordRebuilder makeRebuilder(KtTreatNode proc, boolean prev);
}

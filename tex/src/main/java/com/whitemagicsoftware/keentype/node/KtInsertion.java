// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.Insertion
// $Id: KtInsertion.java,v 1.1.1.1 2000/02/19 01:43:42 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtInsertion implements Serializable {

  public static final KtInsertion NULL = null;

  public final int num;
  public final KtNodeList list;
  public final KtDimen size;
  public final KtGlue topSkip;
  public final KtDimen maxDepth;
  public final KtNum floatCost;

  public KtInsertion(
      int num, KtNodeList list, KtDimen size, KtGlue topSkip, KtDimen maxDepth, KtNum floatCost) {
    this.num = num;
    this.list = list;
    this.size = size;
    this.topSkip = topSkip;
    this.maxDepth = maxDepth;
    this.floatCost = floatCost;
  }

  public KtInsertion makeCopy(KtNodeList list, KtDimen size) {
    return new KtInsertion(num, list, size, topSkip, maxDepth, floatCost);
  }

  /* TeXtp[188] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("insert")
        .add(num)
        .add(", natural size ")
        .add(size.toString())
        .add("; split(")
        .add(topSkip.toString())
        .add(',')
        .add(maxDepth.toString())
        .add("); float cost ")
        .add(floatCost.toString());
    cntx.addOn(log, list.nodes());
  }
}

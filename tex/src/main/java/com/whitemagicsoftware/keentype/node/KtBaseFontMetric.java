// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.BaseFontMetric
// $Id: KtBaseFontMetric.java,v 1.1.1.1 2001/02/09 12:53:15 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;

public abstract class KtBaseFontMetric implements KtFontMetric {

  public boolean isNull() {
    return false;
  }

  private final KtNum[] numPars = new KtNum[NUMBER_OF_NUM_PARAMS];

  {
    for (int i = 0; i < numPars.length; numPars[i++] = KtNum.NULL)
      ;
  }

  public KtNum getNumParam(int idx) {
    return 0 <= idx && idx < numPars.length ? numPars[idx] : KtNum.NULL;
  }

  public KtNum setNumParam(int idx, KtNum val) {
    return 0 <= idx && idx < numPars.length ? (numPars[idx] = val) : KtNum.NULL;
  }

  public boolean definesNumParams(int[] idxs) {
    for (int i = 0; i < idxs.length; i++) {
      int idx = idxs[i];
      if (idx < 0 || idx >= numPars.length || numPars[idx] == KtNum.NULL) return false;
    }
    return true;
  }

  /*
   * We make no assumptions about the order of DIMEN_PARAM constants
   * therefore we simply allocate the array for all possible parameters and
   * initialize each element to KtDimen.NULL. Subclasses of this class can
   * assign appropriate values to those parameters which they really provide
   * during construction.
   */

  private final KtDimen[] dimPars = new KtDimen[NUMBER_OF_DIMEN_PARAMS];

  {
    for (int i = 0; i < dimPars.length; dimPars[i++] = KtDimen.NULL)
      ;
    final int[] params = ALL_TEXT_DIMEN_PARAMS;
    for (int i = 0; i < params.length; dimPars[params[i++]] = KtDimen.ZERO)
      ;
  }

  public KtDimen getDimenParam(int idx) {
    return 0 <= idx && idx < dimPars.length ? dimPars[idx] : KtDimen.NULL;
  }

  public KtDimen setDimenParam(int idx, KtDimen val) {
    if (0 <= idx && idx < dimPars.length) {
      normalSpace = KtGlue.NULL;
      return dimPars[idx] = val;
    }
    return KtDimen.NULL;
  }

  public boolean definesDimenParams(int[] idxs) {
    for( int idx : idxs ) {
      if( idx < 0 || idx >= dimPars.length || dimPars[ idx ] == KtDimen.NULL ) {
        return false;
      }
    }
    return true;
  }

  private transient KtGlue normalSpace = KtGlue.NULL;

  public KtGlue getNormalSpace() {
    if (normalSpace == KtGlue.NULL)
      normalSpace =
          KtGlue.valueOf(
              dimPars[DIMEN_PARAM_SPACE],
              dimPars[DIMEN_PARAM_STRETCH],
              dimPars[DIMEN_PARAM_SHRINK]);
    return normalSpace;
  }
}

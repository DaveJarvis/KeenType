// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.NetDimen
// $Id: KtNetDimen.java,v 1.1.1.1 2000/01/28 17:17:18 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtNetDimen implements Serializable {

  public static final KtNetDimen NULL = null;

  private KtDimen natural;
  private KtDimen shrink;
  private final KtDimen[] stretch = new KtDimen[KtGlue.MAX_ORDER + 1];

  public KtDimen getNatural() {
    return natural;
  }

  public KtDimen getShrink() {
    return shrink;
  }

  public KtDimen getStretch(byte ord) {
    return stretch[ord];
  }

  public void setNatural(KtDimen dim) {
    natural = dim;
  }

  public void setShrink(KtDimen dim) {
    shrink = dim;
  }

  public void setStretch(KtDimen dim, byte ord) {
    stretch[ord] = dim;
  }

  public byte getMaxStrOrder() {
    byte ord = KtGlue.MAX_ORDER;
    while (ord > KtGlue.NORMAL && stretch[ord].isZero()) ord--;
    return ord;
  }

  public KtNetDimen() {
    this(KtDimen.ZERO);
  }

  public KtNetDimen(KtDimen d) {
    natural = d;
    shrink = KtDimen.ZERO;
    for (int i = 0; i < stretch.length; stretch[i++] = KtDimen.ZERO)
      ;
  }

  public KtNetDimen(KtGlue g) {
    natural = g.getDimen();
    shrink = g.getShrink();
    for (int i = 0; i < stretch.length; stretch[i++] = KtDimen.ZERO)
      ;
    stretch[g.getStrOrder()] = g.getStretch();
  }

  public KtNetDimen(KtNetDimen nd) {
    natural = nd.natural;
    shrink = nd.shrink;
    for (int i = 0; i < stretch.length; i++) stretch[i] = nd.stretch[i];
  }

  public final void add(KtDimen d) {
    natural = natural.plus(d);
  }

  public final void addShrink(KtDimen d) {
    shrink = shrink.plus(d);
  }

  public final void addStretch(byte ord, KtDimen d) {
    stretch[ord] = stretch[ord].plus(d);
  }

  public void add(KtGlue g) {
    add(g.getDimen());
    addShrink(g.getShrink());
    addStretch(g.getStrOrder(), g.getStretch());
  }

  public void add(KtNetDimen nd) {
    add(nd.natural);
    addShrink(nd.shrink);
    for (byte o = 0; o < stretch.length; o++) addStretch(o, nd.stretch[o]);
  }

  public final void sub(KtDimen d) {
    natural = natural.minus(d);
  }

  public final void subShrink(KtDimen d) {
    shrink = shrink.minus(d);
  }

  public final void subStretch(byte ord, KtDimen d) {
    stretch[ord] = stretch[ord].minus(d);
  }

  public void sub(KtGlue g) {
    sub(g.getDimen());
    subShrink(g.getShrink());
    subStretch(g.getStrOrder(), g.getStretch());
  }

  public void sub(KtNetDimen nd) {
    sub(nd.natural);
    subShrink(nd.shrink);
    for (byte o = 0; o < stretch.length; o++) subStretch(o, nd.stretch[o]);
  }

  public String toString() {
    return toString(null);
  }

  /* TeXtp[985] */
  public String toString(String unit) {
    StringBuffer buf = new StringBuffer(128);
    buf.append(natural.toString());
    if (unit != null) buf.append(unit);
    for (byte o = 0; o < stretch.length; o++)
      if (!stretch[o].isZero()) KtGlue.append(buf.append(" plus "), stretch[o], o, unit);
    if (!shrink.isZero()) KtGlue.append(buf.append(" minus "), shrink, KtGlue.NORMAL, unit);
    return buf.toString();
  }
}

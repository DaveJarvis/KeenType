// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.AnySkipNode
// $Id: KtAnySkipNode.java,v 1.1.1.1 2000/05/26 21:20:53 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtAnySkipNode extends KtDiscardableNode {
  /* root corresponding to glue_node */

  protected final KtGlue skip;

  public KtAnySkipNode(KtGlue skip) {
    this.skip = skip;
  }

  public KtGlue getSkip() {
    return skip;
  }

  public boolean isSkip() {
    return true;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("glue");
    String name = getName();
    if (name != null) log.add('(').addEsc(name).add(')');
    log.add(' ').add(skip.toString());
  }

  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    if (!skip.isZero()) log.add(' ');
    return metric;
  }

  public String getName() {
    return null;
  }

  public boolean canFollowKernBreak() {
    return true;
  }

  public KtNodeEnum atBreakReplacement() {
    return KtNodeList.EMPTY_ENUM;
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return brCntx.spaceBreaking() && brCntx.allowedAtSkip() ? 0 : INF_PENALTY;
  }
}

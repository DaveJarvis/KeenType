// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HorizIterator
// $Id: KtHorizIterator.java,v 1.1.1.1 2000/08/09 06:09:11 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtHorizIterator extends KtEnumSizesIterator {

  public KtHorizIterator(KtNodeEnum nodes) {
    super(nodes);
  }

  public KtDimen currHeight() {
    return curr.getLeftX();
  }

  public KtDimen currWidth() {
    return curr.getHeight();
  }

  public KtDimen currDepth() {
    return curr.getWidth();
  }

  public KtDimen currLeftX() {
    return curr.getDepth();
  }

  public KtDimen currStr() {
    return curr.getWstr();
  }

  public KtDimen currShr() {
    return curr.getWshr();
  }

  public byte currStrOrd() {
    return curr.getWstrOrd();
  }

  public byte currShrOrd() {
    return curr.getWshrOrd();
  }

  public static KtDimen totalWidth(KtNodeEnum nodes) {
    KtSizesSummarizer pack = new KtSizesSummarizer();
    summarize(nodes, pack);
    return pack.getHeight().plus(pack.getBody()).plus(pack.getDepth());
  }

  public static KtBoxSizes naturalSizes(KtNodeEnum nodes) {
    KtSizesSummarizer pack = new KtSizesSummarizer();
    summarize(nodes, pack);
    return new KtBoxSizes(
        pack.getWidth(), pack.getBody().plus(pack.getDepth()), pack.getLeftX(), pack.getHeight());
  }

  public static void summarize(KtNodeEnum nodes, KtSizesSummarizer pack) {
    new KtHorizIterator(nodes).summarize( pack);
  }
}

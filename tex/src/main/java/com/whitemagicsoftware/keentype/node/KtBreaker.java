// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.Breaker
// $Id: KtBreaker.java,v 1.1.1.1 2000/05/27 16:47:25 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public abstract class KtBreaker {

  protected static final KtBreak NULL_BREAK = null;
  protected static final KtFitness NULL_FITNESS = null;
  protected static final int INF_BAD = KtDimen.INF_BAD;
  protected static final int AWFUL_BAD = KtDimen.AWFUL_BAD;

  /* TeXtp[819,821] */
  protected static class KtBreak {

    public final int index;
    public final int count;
    public final int lineNo;
    public final KtFitness fitness;
    public final boolean hyphenated;
    public final int demerits;
    public final int serial;
    public final KtBreak prev;
    public KtNetDimen delta;

    public KtBreak(
        int index,
        int count,
        int lineNo,
        KtFitness fitness,
        boolean hyphenated,
        int demerits,
        int serial,
        KtBreak prev,
        KtNetDimen delta) {
      this.index = index;
      this.count = count;
      this.lineNo = lineNo;
      this.fitness = fitness;
      this.hyphenated = hyphenated;
      this.demerits = demerits;
      this.serial = serial;
      this.prev = prev;
      this.delta = delta;
    }
  }

  protected class KtFitness implements KtLoggable {

    private final int code;
    public final KtFitness next;
    private int minDem = AWFUL_BAD;
    private KtBreak best = NULL_BREAK;

    public KtFitness(int code, KtFitness next) {
      this.code = code;
      this.next = next;
    }

    public void addOn(KtLog log) {
      log.add(code);
    }

    public boolean adjoins(KtFitness other) {
      return this.equals( other) || other.equals( next) || this.equals( other.next);
    }

    public void update(int dem, KtBreak brk) {
      if (minDem >= dem) {
        minDem = dem;
        best = brk;
      }
    }

    public void reset() {
      minDem = AWFUL_BAD;
      best = NULL_BREAK;
    }

    public boolean fits(int limit) {
      return minDem <= limit;
    }

    public KtBreak makeFirst(int lineNo) {
      return new KtBreak(0, 0, lineNo, this, false, 0, maxSerial++, NULL_BREAK, new KtNetDimen());
    }

    public KtBreak makeBest(int idx, int cnt, boolean hyphenated, KtNetDimen delta) {
      if (best == NULL_BREAK) throw new RuntimeException("no best break");
      return new KtBreak(
          idx, cnt, best.lineNo + 1, this, hyphenated, minDem, maxSerial++, best, delta);
    }

    public String toString() {
      return Integer.toString(code);
    }
  }

  protected int minDem = AWFUL_BAD;
  protected final KtFitness TIGHT = new KtFitness(3, NULL_FITNESS);
  protected final KtFitness DECENT = new KtFitness(2, TIGHT);
  protected final KtFitness LOOSE = new KtFitness(1, DECENT);
  protected final KtFitness VERY_LOOSE = new KtFitness(0, LOOSE);
  protected final KtFitness FITNESS_HEAD = VERY_LOOSE;

  protected KtFitness getFitness(int badness, boolean stretching) {
    if (stretching) {
      if (badness > 99) return VERY_LOOSE;
      if (badness > 12) return LOOSE;
    } else if (badness > 12) return TIGHT;
    return DECENT;
  }

  protected void resetDemerits() {
    minDem = AWFUL_BAD;
    for (KtFitness fit = FITNESS_HEAD; fit != NULL_FITNESS; fit = fit.next) fit.reset();
  }

  // private KtLog		debug;

  /*
      public static KtLoggable	nod(final KtNode node) {
          return new KtLoggable() {
  	    public void		addOn(KtLog log)
  		{ KtCntxLog.addOn(log, node, 100, 10000); }
  	};
      }
  */

  private KtNodeEnum nodeEnum;
  private int nodeCount = 0;
  protected KtNode[] nodeList = new KtNode[32];

  public void refeed(KtNodeEnum nodeEnum) {
    this.nodeEnum = nodeEnum;
    nodeCount = 0;
  }

  protected final KtNode nodeAt(int i) {
    return nodeList[i];
  }

  protected final boolean stillNodeAt(int i) {
    while (nodeCount <= i) {
      if (!nodeEnum.hasMoreNodes()) return false;
      if (nodeList.length <= nodeCount) {
        int newLength = nodeList.length * 2;
        while (newLength <= nodeCount) newLength *= 2;
        KtNode[] oldList = nodeList;
        nodeList = new KtNode[newLength];
        System.arraycopy(oldList, 0, nodeList, 0, nodeCount);
      }
      nodeList[nodeCount++] = nodeEnum.nextNode();
    }
    return true;
  }

  protected /* final */ KtLinesShape shape;
  protected /* final */ int firstLineNo;
  protected /* final */ int looseness;
  protected /* final */ int linePen;
  protected /* final */ int hyphPen;
  protected /* final */ int exHyphPen;
  protected /* final */ int adjDem;
  protected /* final */ int dblHyphDem;
  protected /* final */ int finHyphDem;

  public KtBreaker(
      KtNodeEnum nodeEnum,
      KtLinesShape shape,
      int firstLineNo,
      int looseness,
      int linePen,
      int hyphPen,
      int exHyphPen,
      int adjDem,
      int dblHyphDem,
      int finHyphDem) {
    this.nodeEnum = nodeEnum;
    this.shape = shape;
    this.firstLineNo = firstLineNo;
    this.looseness = looseness;
    this.linePen = linePen;
    this.hyphPen = hyphPen;
    this.exHyphPen = exHyphPen;
    this.adjDem = adjDem;
    this.dblHyphDem = dblHyphDem;
    this.finHyphDem = finHyphDem;
    // debug = nts.command.KtCommand.normLog;
  }

  protected KtNetDimen background;
  protected int threshold;
  protected boolean finPass;
  protected List<KtBreak> breakList;
  private int maxSerial;
  protected KtBreak[] lineBreaks;
  protected int currLineIndex = 0;
  protected KtNodeEnum lastPostBreak = KtNodeEnum.NULL;

  protected void reset() {
    breakList = new LinkedList<>();
    maxSerial = 0;
    lineBreaks = null;
    currLineIndex = 0;
    lastPostBreak = KtNodeEnum.NULL;
    resetDemerits();
  }

  /* STRANGE
   * Why is the last break hyphenated?
   * the only difference is '-' traced by trace_break
   * The reason is probably only the more efficient test in TeXtp[859].
   */
  public void breakToLines(KtNetDimen background, int threshold, boolean finPass) {
    this.background = background;
    this.threshold = Math.min( threshold, INF_BAD );
    this.finPass = finPass;
    reset();
    breakList.add(DECENT.makeFirst(firstLineNo));
    if (passNodes()) tryBreak(nodeCount, KtNode.EJECT_PENALTY, true, KtDimen.ZERO, true);
    KtBreak best = bestBreak();
    if (looseness != 0 && best != NULL_BREAK) best = bestBreak(best.lineNo);
    int n = 0;
    for (KtBreak brk = best; brk != NULL_BREAK; brk = brk.prev) n++;
    if (n > 0) {
      lineBreaks = new KtBreak[n];
      currLineIndex = 1;
      for (KtBreak brk = best; brk != NULL_BREAK; brk = brk.prev) lineBreaks[--n] = brk;
    }
  }

  public boolean successfullyBroken() {
    return currLineIndex > 0;
  }

  public boolean hasMoreLines() {
    return currLineIndex > 0 && currLineIndex < lineBreaks.length;
  }

  public boolean nextLineWasHyphenated() {
    KtBreak brk = lineBreaks[currLineIndex];
    return brk.hyphenated && brk.count > 0;
  }

  public KtNodeList getNextLine() {
    int i = currLineIndex++;
    return makeList(lineBreaks[i - 1], lineBreaks[i]);
  }

  protected KtNodeList makeList(KtBreak before, KtBreak after) {
    int beg = before.index + before.count;
    int end = after.index;
    KtNodeList list;
    if (lastPostBreak != KtNodeEnum.NULL) list = new KtNodeList(lastPostBreak);
    else list = new KtNodeList(end - beg);
    list.append(nodeList, beg, end - beg);
    if (after.count > 0) {
      KtNode node = nodeList[end];
      list.append(node.atBreakReplacement());
      lastPostBreak = node.postBreakNodes();
    } else lastPostBreak = KtNodeEnum.NULL;
    return list;
  }

  /* TeXtp[874] */
  protected KtBreak bestBreak() {
    int fewestDem = AWFUL_BAD;
    KtBreak best = NULL_BREAK;
    for( final KtBreak brk : breakList ) {
      if( brk.demerits < fewestDem ) {
        best = brk;
        fewestDem = brk.demerits;
      }
    }
    return best;
  }

  /* TeXtp[875] */
  protected KtBreak bestBreak(int bestLineNo) {
    final int desiredLineNo = bestLineNo + looseness;
    int fewestDem = AWFUL_BAD;
    KtBreak best = NULL_BREAK;
    for( final KtBreak brk : breakList ) {
      if( bestLineNo < brk.lineNo && brk.lineNo <= desiredLineNo
        || bestLineNo > brk.lineNo && brk.lineNo >= desiredLineNo ) {
        best = brk;
        fewestDem = brk.demerits;
        bestLineNo = brk.lineNo;
      }
      else if( brk.lineNo == bestLineNo && brk.demerits < fewestDem ) {
        best = brk;
        fewestDem = brk.demerits;
      }
    }
    return bestLineNo == desiredLineNo || finPass ? best : NULL_BREAK;
  }

  protected class KtBreakingContext implements KtBreakingCntx {

    public boolean space = true;
    public boolean atSkip = true;

    public boolean spaceBreaking() {
      return space;
    }

    public boolean allowedAtSkip() {
      return atSkip;
    }

    public int hyphenPenalty() {
      return hyphPen;
    }

    public int exHyphenPenalty() {
      return exHyphPen;
    }
  }

  protected boolean passNodes() {
    KtBreakingContext brkContext = new KtBreakingContext();
    for (int i = 0; stillNodeAt(i); i++) {
      KtNode node = nodeAt(i);
      if (node.allowsSpaceBreaking()) brkContext.space = true;
      else if (node.forbidsSpaceBreaking()) brkContext.space = false;
      if (!node.isKernBreak() || stillNodeAt(i + 1) && nodeAt(i + 1).canFollowKernBreak()) {
        brkContext.atSkip = i > 0 && nodeAt( i - 1).canPrecedeSkipBreak();
        int pen = node.breakPenalty(brkContext);
        if (pen < KtNode.INF_PENALTY) {
          KtDimen preWidth = node.preBreakWidth();
          actWidth().add(preWidth);
          tryBreak(
              i,
              Math.max( pen, KtNode.EJECT_PENALTY ),
              node.isHyphenBreak(),
              preWidth,
              false);
          // XXX add preWidth localy in tryBreak
          if (breakList.isEmpty()) return false;
          actWidth().sub(preWidth);
        }
      }
      checkShrinkage(node);
      add(actWidth(), node);
      /*
      	    debug.startLine().add("node = ").add(nod(node)).endLine();
      	    debug.add("active width = ")
      	         .add(((KtBreak) breakList.get(0)).delta.toString())
      		 .endLine();
      */
    }
    return true;
  }

  private KtNetDimen actWidth() {
    return breakList.get(0).delta;
  }

  protected void tryBreak(int idx, int pen, boolean hyphen, KtDimen preWidth, boolean last) {
    KtNetDimen currWidth = new KtNetDimen(background);
    ListIterator<KtBreak> iterator = breakList.listIterator();
    int oldLineNo = 0;

    while (iterator.hasNext()) {
      KtBreak brk = iterator.next();

      if (brk.lineNo > oldLineNo) {
        if (looseness != 0 || !shape.isFinal(brk.lineNo)) {
          oldLineNo = brk.lineNo;
          if (minDem < AWFUL_BAD) {
            iterator.previous();
            createActive(idx, iterator, currWidth, hyphen, preWidth);
            iterator.next();
          }
        } else oldLineNo = Integer.MAX_VALUE;
      }
      currWidth.add(brk.delta);

      int badness;
      KtFitness fitness;
      KtDimen diff = shape.getWidth(brk.lineNo).minus(currWidth.getNatural());

      if (diff.moreThan(0)) {
        badness =
            currWidth.getMaxStrOrder() > KtGlue.NORMAL
                ? 0
                : diff.badness(currWidth.getStretch(KtGlue.NORMAL));
        fitness = getFitness(badness, true);
      } else {
        diff = diff.negative();
        badness =
            diff.moreThan(currWidth.getShrink())
                ? INF_BAD + 1
                : diff.badness(currWidth.getShrink());
        fitness = getFitness(badness, false);
      }

      if (badness > INF_BAD || pen == KtNode.EJECT_PENALTY) {
        if (finPass && minDem == AWFUL_BAD && breakList.size() == 1) {
          traceBreak(idx, brk.serial, badness, pen, 0, true);
          recordFeasible(brk, fitness, 0);
        } else if (badness <= threshold) {
          int dem = demerits(pen, badness) + demerits(brk, fitness, hyphen, last);
          traceBreak(idx, brk.serial, badness, pen, dem, false);
          recordFeasible(brk, fitness, dem);
        }

        iterator.remove();
        if (iterator.hasNext()) {
          iterator.next().delta.add( brk.delta);
          iterator.previous();
        }
        currWidth.sub(brk.delta);
        brk.delta = KtNetDimen.NULL;
      } else if (badness <= threshold) {
        int dem = demerits(pen, badness) + demerits(brk, fitness, hyphen, last);
        traceBreak(idx, brk.serial, badness, pen, dem, false);
        recordFeasible(brk, fitness, dem);
      }
    }
    if (minDem < AWFUL_BAD) createActive(idx, iterator, currWidth, hyphen, preWidth);
  }

  protected void createActive(
      int idx, ListIterator<KtBreak> iterator, KtNetDimen width, boolean hyphen, KtDimen preWidth) {
    int absAdjDem = Math.abs(adjDem);
    int limit = AWFUL_BAD - minDem <= absAdjDem ? AWFUL_BAD - 1 : minDem + absAdjDem;
    int cnt = breakCount(idx);
    KtNetDimen delta = breakWidth(idx, cnt);

    delta.add(preWidth);
    delta.sub(width);

    for (KtFitness fit = FITNESS_HEAD; fit != NULL_FITNESS; fit = fit.next) {
      if (fit.fits(limit)) {
        if (delta != KtNetDimen.NULL) {
          KtBreak best = fit.makeBest(idx, cnt, hyphen, delta);
          iterator.add(best);
          traceBreak(best);
          if (iterator.hasNext()) {
            iterator.next().delta.sub( best.delta);
            iterator.previous();
          }
          width.add(delta);
          delta = KtNetDimen.NULL;
        } else {
          KtBreak best = fit.makeBest(idx, cnt, hyphen, new KtNetDimen());
          iterator.add(best);
          traceBreak(best);
        }
      }
      fit.reset();
    }
    minDem = AWFUL_BAD;
  }

  protected int demerits(int pen, int badness) {
    int dem = Math.abs(linePen + badness);
    if (dem > KtNode.INF_PENALTY) dem = KtNode.INF_PENALTY;
    dem = dem * dem;
    if (pen != 0) {
      if (pen > 0) dem += pen * pen;
      else if (pen > KtNode.EJECT_PENALTY) dem -= pen * pen;
    }
    return dem;
  }

  protected int demerits(KtBreak brk, KtFitness fitness, boolean hyphen, boolean last) {
    int dem = 0;
    if (brk.hyphenated)
      if (last) dem += finHyphDem;
      else if (hyphen) dem += dblHyphDem;
    if (!fitness.adjoins(brk.fitness)) dem += adjDem;
    return dem;
  }

  protected void recordFeasible(KtBreak brk, KtFitness fitness, int dem) {
    dem += brk.demerits;
    fitness.update(dem, brk);
    if (minDem > dem) minDem = dem;
  }

  protected int breakCount(int idx) {
    int j = idx;
    if (stillNodeAt(j) && nodeAt(j++).discardsAfter())
      while (stillNodeAt(j) && nodeAt(j).discardable()) j++;
    return j - idx;
  }

  protected KtNetDimen breakWidth(int idx, int cnt) {
    KtNetDimen netDim = new KtNetDimen(background);
    if (cnt > 0) netDim.add(nodeAt(idx).postBreakWidth());
    for (int j = 0; j < cnt; j++) sub(netDim, nodeAt(idx + j));
    return netDim;
  }

  protected static void add(KtNetDimen netDim, KtNode node) {
    netDim.add(node.getLeftX());
    netDim.add(node.getWidth());
    netDim.addShrink(node.getWshr());
    netDim.addStretch(node.getWstrOrd(), node.getWstr());
  }

  protected static void sub(KtNetDimen netDim, KtNode node) {
    netDim.sub(node.getLeftX());
    netDim.sub(node.getWidth());
    netDim.subShrink(node.getWshr());
    netDim.subStretch(node.getWstrOrd(), node.getWstr());
  }

  protected void checkShrinkage(KtNode node) {} // XXX[825]

  protected abstract void traceBreak(
      int idx, int serial, int bad, int pen, int dem, boolean artificial);

  protected abstract void traceBreak(KtBreak brk);
}

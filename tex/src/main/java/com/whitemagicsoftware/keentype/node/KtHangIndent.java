// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HangIndent
// $Id: KtHangIndent.java,v 1.1.1.1 1999/11/16 17:55:52 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtHangIndent implements KtLinesShape {

  private final int boundary;
  private final KtDimen firstWidth;
  private final KtDimen firstIndent;
  private final KtDimen secondWidth;
  private final KtDimen secondIndent;

  public KtHangIndent(int b, KtDimen fw, KtDimen fi, KtDimen sw, KtDimen si) {
    boundary = b;
    firstWidth = fw;
    firstIndent = fi;
    secondWidth = sw;
    secondIndent = si;
  }

  public KtHangIndent(KtDimen sw, KtDimen si) {
    this(0, KtDimen.ZERO, KtDimen.ZERO, sw, si);
  }

  public boolean isFinal(int idx) {
    return idx >= boundary;
  }

  public KtDimen getWidth(int idx) {
    return idx < boundary ? firstWidth : secondWidth;
  }

  public KtDimen getIndent(int idx) {
    return idx < boundary ? firstIndent : secondIndent;
  }

  /* TeXtp[849] */
  public static KtLinesShape makeShape(int ha, KtDimen hi, KtDimen hs) {
    if (hi.isZero()) return new KtHangIndent(hs, KtDimen.ZERO);
    KtDimen size, ind;
    if (hi.lessThan(0)) {
      ind = KtDimen.ZERO;
      size = hs.plus(hi);
    } else {
      ind = hi;
      size = hs.minus(hi);
    }
    return ha < 0
        ? new KtHangIndent(-ha, size, ind, hs, KtDimen.ZERO)
        : new KtHangIndent(ha, hs, KtDimen.ZERO, size, ind);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VoidBoxNode
// $Id: KtVoidBoxNode.java,v 1.1.1.1 2000/06/06 08:31:21 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtVoidBoxNode extends KtBaseNode implements KtBox {
  /* corresponding to NO_node */

  public static final KtVoidBoxNode BOX = new KtVoidBoxNode();

  protected KtVoidBoxNode() {}

  public final boolean isVoid() {
    return true;
  }

  public final boolean isHBox() {
    return false;
  }

  public final boolean isVBox() {
    return false;
  }

  public KtBoxSizes getSizes() {
    return KtBoxSizes.NULL;
  }

  public KtBox pretendSizesCopy(KtBoxSizes sizes) {
    return this;
  }

  public KtBox pretendingWidth(KtDimen width) {
    return this;
  }

  public void typeSet(KtTypesetter setter) {}

  public final KtNodeEnum getHorizList() {
    return KtNodeEnum.NULL;
  }

  public final KtNodeEnum getVertList() {
    return KtNodeEnum.NULL;
  }

  public void addOn(KtLog log, int maxDepth, int maxCount) {
    log.add("void");
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.add("void");
  }

  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    return metric;
  }

  public String toString() {
    return "VoidBox";
  }
}

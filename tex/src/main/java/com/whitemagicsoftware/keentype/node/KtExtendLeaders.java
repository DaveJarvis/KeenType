// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.ExtendLeaders
// $Id: KtExtendLeaders.java,v 1.1.1.1 2000/10/15 05:22:31 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtExtendLeaders extends KtBoxLeaders {

  public static final String DESCRIPTOR = "xleaders";

  public KtExtendLeaders(KtNode node, KtMover mover) {
    super(node, mover);
  }

  protected String getDesc() {
    return DESCRIPTOR;
  }

  /* STRANGE
   * we have to fake a TeX bug here
   */
  /* TeXtp[627] */
  protected void typeSet(KtTypesetter setter, KtSettingContext sctx, KtDimen size, KtDimen nodeSize) {
    int count = size.divide(nodeSize);
    KtDimen rest = size.modulo(nodeSize);
    KtDimen gap = rest.roundDivide(count + 1);
    rest = rest.minus(gap.times(count - 1));
    if (rest.lessThan(0)) count--; // compensation for TeX bug [627]
    typeSet(setter, sctx, count, rest.over(2), gap);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.SizesIterator
// $Id: KtSizesIterator.java,v 1.1.1.1 2000/08/09 06:05:31 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public abstract class KtSizesIterator {

  public abstract boolean hasMoreElements();

  public abstract void takeNextElement();

  public abstract boolean sizeIgnored();

  public abstract KtDimen currHeight();

  public abstract KtDimen currWidth();

  public abstract KtDimen currDepth();

  public abstract KtDimen currLeftX();

  public abstract KtDimen currStr();

  public abstract KtDimen currShr();

  public abstract byte currStrOrd();

  public abstract byte currShrOrd();

  /* TeXtp[668] */
  public void summarize(KtSizesSummarizer pack) {
    boolean empty = true;
    while (hasMoreElements()) {
      takeNextElement();
      if (!sizeIgnored()) {
        if (empty) pack.setHeight(currHeight());
        else pack.add(currHeight());
        pack.setDepth(currDepth());
        pack.addStretch(currStr(), currStrOrd());
        pack.addShrink(currShr(), currShrOrd());
        pack.setMaxWidth(currWidth());
        pack.setMaxLeftX(currLeftX());
      }
      empty = false;
    }
  }
}

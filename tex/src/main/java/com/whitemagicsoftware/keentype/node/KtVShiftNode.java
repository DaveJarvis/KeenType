// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VShiftNode
// $Id: KtVShiftNode.java,v 1.1.1.1 2000/10/05 11:11:32 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtVShiftNode extends KtAnyShiftNode {
  /* corresponding to ANY_node */

  protected KtVShiftNode(KtNode node, KtDimen shift) {
    super(node, shift);
  }

  public static KtNode shiftingLeft(KtNode node, KtDimen shift) {
    return shift.isZero() ? node : new KtVShiftNode( node, shift.negative());
  }

  public static KtNode shiftingRight(KtNode node, KtDimen shift) {
    return shift.isZero() ? node : new KtVShiftNode( node, shift);
  }

  public KtDimen getHeight() {
    return node.getHeight();
  }

  public KtDimen getWidth() {
    return node.getWidth().plus(shift);
  }

  public KtDimen getDepth() {
    return node.getDepth();
  }
  // XXX revide the treatment of LeftX. Where it becomes zero?
  // public KtDimen	getLeftX() { return node.getLeftX().minus(shift); }
  public KtDimen getLeftX() {
    return KtDimen.ZERO;
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    setter.moveRight(shift);
    node.typeSet(setter, sctx.shiftedLeft(shift));
    setter.moveLeft(shift);
  }

  public KtDimen getHeight(KtGlueSetting setting) {
    return node.getHeight(setting);
  }

  public KtDimen getWidth(KtGlueSetting setting) {
    return node.getWidth(setting).plus(shift);
  }

  public KtDimen getDepth(KtGlueSetting setting) {
    return node.getDepth(setting);
  }

  public KtDimen getLeftX(KtGlueSetting setting) {
    return node.getLeftX(setting).minus(shift);
  }

  public String toString() {
    return "VShift(" + node + "; " + shift + ')';
  }
}

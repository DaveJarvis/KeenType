// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VisibleSummarizer
// $Id: KtVisibleSummarizer.java,v 1.1.1.1 2000/04/30 15:13:44 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtVisibleSummarizer {

  public final KtGlueSetting setting;
  private KtDimen visibleWidth = KtDimen.ZERO;
  private KtDimen width = KtDimen.ZERO;
  private boolean inMiddle = false;
  private boolean elasticSeen = false;

  public KtVisibleSummarizer(KtGlueSetting setting) {
    this.setting = setting;
  }

  public KtDimen getVisibleWidth() {
    return visibleWidth;
  }

  public boolean addingLeftX() {
    return inMiddle;
  }

  /* TeXt[1146] */
  public void add(KtDimen size, boolean visible) {
    if (elasticSeen) {
      if (visible) visibleWidth = KtDimen.NULL;
    } else {
      width = width.plus(size);
      if (visible) visibleWidth = width;
    }
    inMiddle = true;
  }

  public void claimElastic() {
    elasticSeen = true;
  }

  public void summarize(KtNodeEnum nodes) {
    while (visibleWidth != KtDimen.NULL && nodes.hasMoreNodes())
      nodes.nextNode().contributeVisible(this);
  }
}

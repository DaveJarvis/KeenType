// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.AnyKernNode
// $Id: KtAnyKernNode.java,v 1.1.1.1 2000/05/26 21:20:23 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtAnyKernNode extends KtDiscardableNode {
  /* root corresponding to kern_node */

  protected final KtDimen kern;

  public KtAnyKernNode(KtDimen kern) {
    this.kern = kern;
  }

  public KtDimen getKern() {
    return kern;
  }

  public boolean isKern() {
    return true;
  }

  public boolean hasKern() {
    return true;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("kern ").add(kern.toString());
  }

  public void addBreakDescOn(KtLog log) {
    log.addEsc("kern");
  }

  public boolean isKernBreak() {
    return true;
  }

  public boolean canBePartOfDiscretionary() {
    return true;
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return brCntx.spaceBreaking() ? 0 : INF_PENALTY;
  }

  public KtNodeEnum atBreakReplacement() {
    return KtNodeList.nodes(resizedCopy(KtDimen.ZERO));
  }

  protected abstract KtAnyKernNode resizedCopy(KtDimen kern);
}

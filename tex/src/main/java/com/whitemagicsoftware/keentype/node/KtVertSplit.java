// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VertSplit
// $Id: KtVertSplit.java,v 1.1.1.1 2000/04/30 07:35:50 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.util.Vector;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtVertSplit extends KtNodeList {

  protected static final int INF_BAD = KtDimen.INF_BAD;
  protected static final int DEPLORABLE = KtDimen.DEPLORABLE;
  protected static final int AWFUL_BAD = KtDimen.AWFUL_BAD;

  public KtDimen goal;
  protected KtDimen maxDepth;
  public KtNetDimen soFar;
  public KtDimen depth;

  /* TeXtp[987] */
  public void setNullSpecs() {
    goal = KtDimen.MAX_VALUE;
    maxDepth = KtDimen.ZERO;
    soFar = new KtNetDimen();
    depth = KtDimen.ZERO;
  }

  private int currIndex;
  private int bestIndex;
  private int leastCost;

  private void restart() {
    currIndex = 0;
    bestIndex = 0;
    leastCost = AWFUL_BAD;
  }

  {
    setNullSpecs();
    restart();
  }

  public KtVertSplit() {}

  public KtVertSplit(int initCap) {
    super(initCap);
  }

  public KtVertSplit(int initCap, int capIncrement) {
    super(initCap, capIncrement);
  }

  public KtVertSplit(KtNodeEnum nodes) {
    super(nodes);
  }

  public KtVertSplit(KtNode[] nodes) {
    super(nodes);
  }

  public KtVertSplit(KtNode[] nodes, int offset, int count) {
    super(nodes, offset, count);
  }

  public boolean allConsumed() {
    return currIndex >= length();
  }

  protected KtNodeEnum currPageList() {
    return nodes(0, currIndex);
  }

  protected KtNodeEnum contribList() {
    return nodes(currIndex);
  }

  protected static class KtBreakingContext implements KtBreakingCntx {

    public boolean atSkip = true;

    public boolean spaceBreaking() {
      return true;
    }

    public boolean allowedAtSkip() {
      return atSkip;
    }

    public int hyphenPenalty() {
      return 0;
    }

    public int exHyphenPenalty() {
      return 0;
    }
  }

  /* TeXtp[972,1000] */
  protected boolean findBreak() {
    KtBreakingContext brkContext = new KtBreakingContext();
    while (currIndex < length()) {
      KtNode node = nodeAt(currIndex);
      passWhileBreaking(node);
      boolean tryThisBreak = true;
      if (node.isKernBreak())
        if (currIndex + 1 < length()) tryThisBreak = nodeAt(currIndex + 1).canFollowKernBreak();
        else if (waitingForMore()) return false;
        else tryThisBreak = false;
      if (tryThisBreak) {
        brkContext.atSkip = currIndex > 0 && nodeAt( currIndex - 1).canPrecedeSkipBreak();
        int pen = node.breakPenalty(brkContext);
        if (tryBreak(pen)) return true;
      }
      if (!node.sizeIgnored()) {
        soFar.add(depth);
        soFar.add(node.getHeight());
        soFar.addShrink(node.getHshr());
        soFar.addStretch(node.getHstrOrd(), node.getHstr());
        depth = node.getDepth();
        // XXX[976,1004] infinite shrinkage
      }
      adjustDepth();
      currIndex++;
    }
    return false;
  }

  protected void passWhileBreaking(KtNode node) {}

  protected boolean waitingForMore() {
    return false;
  }

  protected void adjustDepth() {
    if (depth.moreThan(maxDepth)) {
      soFar.add(depth);
      depth = maxDepth;
      soFar.sub(depth);
    }
  }

  /* TeXtp[974,1005] */
  protected boolean tryBreak(int pen) {
    if (pen < KtNode.INF_PENALTY) {
      int badness;
      KtDimen diff = goal.minus(soFar.getNatural());
      if (diff.moreThan(0))
        badness =
            soFar.getMaxStrOrder() > KtGlue.NORMAL
                ? 0
                : diff.badness(soFar.getStretch(KtGlue.NORMAL));
      else {
        diff = diff.negative();
        badness = diff.moreThan(soFar.getShrink()) ? AWFUL_BAD : diff.badness( soFar.getShrink());
      }
      int extra = extraPenalty();
      int cost =
          badness < AWFUL_BAD
              ? pen <= KtNode.EJECT_PENALTY
                  ? pen
                  : badness < INF_BAD ? badness + pen + extra : DEPLORABLE
              : badness;
      if (extra >= INF_BAD) cost = AWFUL_BAD;
      if (cost <= leastCost) {
        bestIndex = currIndex;
        leastCost = cost;
        markBestPlace();
        traceCost(pen, badness, cost, true);
      } else traceCost(pen, badness, cost, false);
      return cost == AWFUL_BAD || pen <= KtNode.EJECT_PENALTY;
    }
    return false;
  }

  protected int extraPenalty() {
    return 0;
  }

  protected void markBestPlace() {}

  protected void traceCost(int pen, int bad, int cost, boolean best) {}

  public KtNode breakNode() {
    return bestIndex < length() ? nodeAt( bestIndex) : KtNode.NULL;
  }

  public KtNodeList makeSplitting(KtDimen height, KtDimen depth) {
    goal = height;
    maxDepth = depth;
    if (!findBreak()) tryBreak(KtNode.EJECT_PENALTY);
    return split();
  }

  /* Vector.removeRange() is protected - why? */
  protected KtNodeList split() {
    KtNodeList list = new KtNodeList( new Vector<>( data.subList( 0, bestIndex ) ));
    data = new Vector<>( data.subList( bestIndex, length() ) );
    restart();
    return list;
  }

  /* TeXtp[968,994] */
  public boolean pruneTop() {
    while (currIndex < length()) {
      KtNode node = nodeAt(currIndex);
      passWhilePrunning(node);
      if (node.discardable()) data.remove(currIndex);
      else if (node.sizeIgnored()) currIndex++;
      else {
        KtNode adj = topAdjustment(nodeAt(currIndex).getHeight());
        if (adj != KtNode.NULL) data.add(currIndex, adj);
        return true;
      }
    }
    return false;
  }

  protected void passWhilePrunning(KtNode node) {}

  protected KtNode topAdjustment(KtDimen height) {
    return KtNode.NULL;
  }
}

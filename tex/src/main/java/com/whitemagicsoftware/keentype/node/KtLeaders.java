// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.Leaders
// $Id: KtLeaders.java,v 1.1.1.1 2000/02/19 01:43:45 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public interface KtLeaders extends Serializable {

  KtLeaders NULL = null;

  KtDimen getHeight();

  KtDimen getWidth();

  KtDimen getDepth();

  KtDimen getLeftX();

  void addOn(KtLog log, KtCntxLog cntx, KtGlue skip);

  void typeSet(KtTypesetter setter, KtDimen size, KtSettingContext sctx);
}

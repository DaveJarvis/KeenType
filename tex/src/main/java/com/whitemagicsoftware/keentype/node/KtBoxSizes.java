// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.BoxSizes
// $Id: KtBoxSizes.java,v 1.1.1.1 2000/04/13 09:22:05 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public final class KtBoxSizes implements Serializable, KtLoggable {

  public static final KtBoxSizes NULL = null;

  public static final KtBoxSizes ZERO = new KtBoxSizes(KtDimen.ZERO, KtDimen.ZERO, KtDimen.ZERO, KtDimen.ZERO);

  private final KtDimen height;
  private final KtDimen width;
  private final KtDimen depth;
  private final KtDimen leftX;

  public KtBoxSizes(KtDimen h, KtDimen w, KtDimen d, KtDimen l) {
    height = h;
    width = w;
    depth = d;
    leftX = l;
  }

  public KtDimen rawHeight() {
    return height;
  }

  public KtDimen rawWidth() {
    return width;
  }

  public KtDimen rawDepth() {
    return depth;
  }

  public KtDimen rawLeftX() {
    return leftX;
  }

  public KtDimen getHeight() {
    return getSize(height);
  }

  public KtDimen getWidth() {
    return getSize(width);
  }

  public KtDimen getDepth() {
    return getSize(depth);
  }

  public KtDimen getLeftX() {
    return getSize(leftX);
  }

  private static KtDimen getSize(KtDimen x) {
    return x != KtDimen.NULL ? x : KtDimen.ZERO;
  }

  public KtBoxSizes replenished(KtBoxSizes around) {
    KtDimen h = height;
    KtDimen w = width;
    KtDimen d = depth;
    KtDimen l = leftX;
    boolean change = false;
    if (h == KtDimen.NULL) {
      h = around.getHeight();
      change = true;
    }
    if (w == KtDimen.NULL) {
      w = around.getWidth();
      change = true;
    }
    if (d == KtDimen.NULL) {
      d = around.getDepth();
      change = true;
    }
    if (l == KtDimen.NULL) {
      l = around.getLeftX();
      change = true;
    }
    return change ? new KtBoxSizes( h, w, d, l) : this;
  }

  public KtBoxSizes shiftedUp(KtDimen shift) {
    return new KtBoxSizes(
      height != KtDimen.NULL ? height.plus( shift) : height, width,
      depth != KtDimen.NULL ? depth.minus( shift) : depth, leftX);
  }

  public KtBoxSizes shiftedLeft(KtDimen shift) {
    return new KtBoxSizes(
        height, width != KtDimen.NULL ? width.minus( shift) : width,
        depth, leftX != KtDimen.NULL ? leftX.plus( shift) : leftX);
  }

  public KtBoxSizes withHeight(KtDimen height) {
    return new KtBoxSizes(height, width, depth, leftX);
  }

  public KtBoxSizes withWidth(KtDimen width) {
    return new KtBoxSizes(height, width, depth, leftX);
  }

  public KtBoxSizes withDepth(KtDimen depth) {
    return new KtBoxSizes(height, width, depth, leftX);
  }

  public KtBoxSizes withLeftX(KtDimen leftX) {
    return new KtBoxSizes(height, width, depth, leftX);
  }

  public void addOn(KtLog log) {
    log.add('(');
    addSize(log, height);
    log.add('+');
    addSize(log, depth);
    log.add(")x");
    addSize(log, width);
  }

  private static void addSize(KtLog log, KtDimen x) {
    if (x == KtDimen.NULL) log.add('*');
    else log.add(x.toString());
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.AnyBoxedNode
// $Id: KtAnyBoxedNode.java,v 1.1.1.1 2000/05/26 21:14:42 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public abstract class KtAnyBoxedNode extends KtBaseNode {
  /* corresponding to hlist_node, vlist_node, rule_node, disc_node */

  protected final KtBoxSizes sizes;

  public KtAnyBoxedNode(KtBoxSizes sizes) {
    this.sizes = sizes;
  }

  public KtBoxSizes getSizes() {
    return sizes;
  }

  public KtDimen getHeight() {
    return sizes.getHeight();
  }

  public KtDimen getWidth() {
    return sizes.getWidth();
  }

  public KtDimen getDepth() {
    return sizes.getDepth();
  }

  public KtDimen getLeftX() {
    return sizes.getLeftX();
  }
}

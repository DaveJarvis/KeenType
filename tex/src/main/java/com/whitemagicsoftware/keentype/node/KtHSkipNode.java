// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HSkipNode
// $Id: KtHSkipNode.java,v 1.1.1.1 2001/03/06 14:55:56 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtHSkipNode extends KtAnySkipNode {
  /* corresponding to glue_node */

  public KtHSkipNode(KtGlue skip) {
    super(skip);
  }

  public KtDimen getWidth() {
    return skip.getDimen();
  }

  public KtDimen getWstr() {
    return skip.getStretch();
  }

  public byte getWstrOrd() {
    return skip.getStrOrder();
  }

  public KtDimen getWshr() {
    return skip.getShrink();
  }

  public byte getWshrOrd() {
    return skip.getShrOrder();
  }

  public KtDimen getWidth(KtGlueSetting setting) {
    return setting.set(skip, true);
  }

  public void contributeVisible(KtVisibleSummarizer summarizer) {
    if (summarizer.setting.makesElastic(skip)) summarizer.claimElastic();
    super.contributeVisible(summarizer);
  }

  public boolean startsWordBlock() {
    return true;
  }

  public byte afterWord() {
    return SUCCESS;
  }

  public String toString() {
    return "HSkip(" + skip + ')';
  }
}

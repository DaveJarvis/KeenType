// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VLeadersNode
// $Id: KtVLeadersNode.java,v 1.1.1.1 2001/03/06 14:55:42 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtVLeadersNode extends KtVSkipNode {
  /* corresponding to glue_node */

  protected final KtLeaders lead;

  public KtVLeadersNode(KtGlue skip, KtLeaders lead) {
    super(skip);
    this.lead = lead;
  }

  public KtDimen getWidth() {
    return lead.getWidth();
  }

  public KtDimen getLeftX() {
    return lead.getLeftX();
  }

  protected boolean allegedlyVisible() {
    return true;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    lead.addOn(log, cntx, skip);
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    KtTypesetter.KtMark here = setter.mark();
    setter.moveUp(getHeight(sctx.setting));
    lead.typeSet(setter, sctx.setting.set(skip, true), sctx);
    here.move();
  }

  public static final KtBoxLeaders.KtMover BOX_MOVER =
      new KtBoxLeaders.KtMover() {

        public KtDimen offset(KtTypesetter.KtMark start) {
          return start.yDiff();
        }

        public KtDimen size(KtNode node) {
          return node.getHeight().plus(node.getDepth());
        }

        public void back(KtTypesetter setter, KtDimen gap) {
          setter.moveUp(gap);
        }

        public void move(KtTypesetter setter, KtDimen gap) {
          setter.moveDown(gap);
        }

        public void movePrev(KtTypesetter setter, KtNode node) {
          setter.moveDown(node.getHeight());
          setter.syncHoriz();
          setter.syncVert();
        }

        public void movePast(KtTypesetter setter, KtNode node) {
          setter.moveDown(node.getDepth());
        }
      };

  public String toString() {
    return "VLeaders(" + skip + "; " + lead + ')';
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HLeadersNode
// $Id: KtHLeadersNode.java,v 1.1.1.1 2001/03/06 14:57:05 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtHLeadersNode extends KtHSkipNode {
  /* corresponding to glue_node */

  protected final KtLeaders lead;

  public KtHLeadersNode(KtGlue skip, KtLeaders lead) {
    super(skip);
    this.lead = lead;
  }

  public KtDimen getHeight() {
    return lead.getHeight();
  }

  public KtDimen getDepth() {
    return lead.getDepth();
  }

  protected boolean allegedlyVisible() {
    return true;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    lead.addOn(log, cntx, skip);
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    KtTypesetter.KtMark here = setter.mark();
    setter.moveLeft(getLeftX(sctx.setting));
    lead.typeSet(setter, sctx.setting.set(skip, true), sctx);
    here.move();
  }

  public static final KtBoxLeaders.KtMover BOX_MOVER =
      new KtBoxLeaders.KtMover() {

        public KtDimen offset(KtTypesetter.KtMark start) {
          return start.xDiff();
        }

        public KtDimen size(KtNode node) {
          return node.getLeftX().plus(node.getWidth());
        }

        public void back(KtTypesetter setter, KtDimen gap) {
          setter.moveLeft(gap);
        }

        public void move(KtTypesetter setter, KtDimen gap) {
          setter.moveRight(gap);
        }

        public void movePrev(KtTypesetter setter, KtNode node) {
          setter.moveRight(node.getLeftX());
          setter.syncVert();
          setter.syncHoriz();
        }

        public void movePast(KtTypesetter setter, KtNode node) {
          setter.moveRight(node.getWidth());
        }
      };

  public String toString() {
    return "HLeaders(" + skip + "; " + lead + ')';
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.BoxLeaders
// $Id: KtBoxLeaders.java,v 1.1.1.1 2000/02/19 01:42:33 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtBoxLeaders implements KtLeaders {

  public interface KtMover extends Serializable {
    KtDimen offset(KtTypesetter.KtMark start);

    KtDimen size(KtNode node);

    void move(KtTypesetter setter, KtDimen gap);

    void movePrev(KtTypesetter setter, KtNode node);

    void movePast(KtTypesetter setter, KtNode node);
  }

  public static final KtMover NULL_MOVER = null;

  protected final KtNode node;
  protected final KtMover mover;

  public KtBoxLeaders(KtNode node, KtMover mover) {
    this.node = node;
    this.mover = mover;
  }

  public KtDimen getHeight() {
    return node.getHeight();
  }

  public KtDimen getWidth() {
    return node.getWidth();
  }

  public KtDimen getDepth() {
    return node.getDepth();
  }

  public KtDimen getLeftX() {
    return node.getLeftX();
  }

  /* TeXtp[190] */
  public void addOn(KtLog log, KtCntxLog cntx, KtGlue skip) {
    log.addEsc(getDesc()).add(' ').add(skip.toString());
    cntx.addOn(log, node);
  }

  private static final KtDimen compensation = KtDimen.valueOf(10, 0x10000);

  public void typeSet(KtTypesetter setter, KtDimen size, KtSettingContext sctx) {
    if (size.moreThan(0)) {
      KtDimen nodeSize = mover.size(node);
      if (nodeSize.moreThan(0))
        typeSet(setter, sctx.allowingIO(false), size.plus(compensation), nodeSize);
    }
  }

  /* TeXtp[626,628,635,637] */
  protected void typeSet(
      KtTypesetter setter, KtSettingContext sctx, int count, KtDimen start, KtDimen gap) {
    mover.move(setter, start);
    while (count-- > 0) {
      mover.movePrev(setter, node);
      node.typeSet(setter, sctx);
      mover.movePast(setter, node);
      mover.move(setter, gap);
    }
  }

  protected abstract void typeSet(
      KtTypesetter setter, KtSettingContext sctx, KtDimen size, KtDimen nodeSize);

  protected abstract String getDesc();
}

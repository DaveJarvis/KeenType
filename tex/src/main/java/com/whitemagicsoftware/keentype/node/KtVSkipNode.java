// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VSkipNode
// $Id: KtVSkipNode.java,v 1.1.1.1 2001/03/06 14:54:55 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtVSkipNode extends KtAnySkipNode {
  /* corresponding to glue_node */

  public KtVSkipNode(KtGlue skip) {
    super(skip);
  }

  public KtDimen getHeight() {
    return skip.getDimen();
  }

  public KtDimen getHstr() {
    return skip.getStretch();
  }

  public byte getHstrOrd() {
    return skip.getStrOrder();
  }

  public KtDimen getHshr() {
    return skip.getShrink();
  }

  public byte getHshrOrd() {
    return skip.getShrOrder();
  }

  public KtDimen getHeight(KtGlueSetting setting) {
    return setting.set(skip, true);
  }

  public String toString() {
    return "VSkip(" + skip + ')';
  }
}

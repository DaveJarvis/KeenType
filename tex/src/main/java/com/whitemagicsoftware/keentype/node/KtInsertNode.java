// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.InsertNode
// $Id: KtInsertNode.java,v 1.1.1.1 2000/06/06 08:25:40 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtInsertNode extends KtMigratingNode {
  /* root corresponding to ins_node */

  protected final KtInsertion ins;

  public KtInsertNode(KtInsertion ins) {
    this.ins = ins;
  }

  public boolean isInsertion() {
    return true;
  }

  public KtInsertion getInsertion() {
    return ins;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    ins.addOn(log, cntx);
  }

  public String toString() {
    return "Insert(" + ins + ')';
  }
}

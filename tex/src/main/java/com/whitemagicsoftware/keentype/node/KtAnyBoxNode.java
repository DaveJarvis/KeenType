// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.AnyBoxNode
// $Id: KtAnyBoxNode.java,v 1.1.1.1 2001/04/27 14:40:21 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtAnyBoxNode extends KtAnyBoxedNode implements KtBox {
  /* corresponding to hlist_node, vlist_node */

  protected final KtGlueSetting setting;
  protected KtNodeList list;

  public KtAnyBoxNode(KtBoxSizes sizes, KtGlueSetting setting, KtNodeList list) {
    super(sizes);
    this.setting = setting;
    this.list = list;
  }

  public KtGlueSetting getSetting() {
    return setting;
  }

  public KtNodeList getList() {
    return list;
  }

  public boolean isVoid() {
    return false;
  }

  public boolean isHBox() {
    return false;
  }

  public boolean isVBox() {
    return false;
  }

  public KtNodeEnum getHorizList() {
    return isHBox() ? list.nodes() : KtNodeEnum.NULL;
  }

  public KtNodeEnum getVertList() {
    return isVBox() ? list.nodes() : KtNodeEnum.NULL;
  }

  public boolean isBox() {
    return true;
  }

  public KtBox getBox() {
    return this;
  }

  public boolean isCleanBox() {
    return true;
  }

  public boolean canBePartOfDiscretionary() {
    return true;
  }

  protected boolean allegedlyVisible() {
    return true;
  }

  public KtBox pretendingWidth(KtDimen width) {
    return getWidth().equals(width) ? this : pretendSizesCopy( sizes.withWidth( width));
  }

  public void typeSet(KtTypesetter setter) {
    typeSet(setter, true);
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    if (!list.isEmpty()) {
      setter.push();
      typeSet(setter, sctx.allowIO);
      setter.pop();
    }
  }

  private void typeSet(KtTypesetter setter, boolean allowIO) {
    KtTypesetter.KtMark here = setter.mark();
    KtNodeEnum nodes = list.nodes();
    moveStart(setter);
    KtSettingContext sctx = new KtSettingContext(sizes, setting, setter.mark(), allowIO);
    while (nodes.hasMoreNodes()) {
      KtNode node = nodes.nextNode();
      movePrev(setter, node);
      node.typeSet(setter, sctx);
      movePast(setter, node);
    }
    here.move();
  }

  public void syncVertIfBox(KtTypesetter setter) {
    if (!list.isEmpty()) setter.syncVert();
  }

  protected abstract void moveStart(KtTypesetter setter);

  protected abstract void movePrev(KtTypesetter setter, KtNode node);

  protected abstract void movePast(KtTypesetter setter, KtNode node);

  public void addOn(KtLog log, KtCntxLog cntx, KtDimen shift) {
    log.addEsc(getDesc()).add(sizes).add(setting);
    if (shift != KtDimen.NULL && !shift.isZero()) log.add(", shifted ").add(shift.toString());
    cntx.addOn(log, list.nodes());
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    addOn(log, cntx, KtDimen.NULL);
  }

  public void addOn(KtLog log, int maxDepth, int maxCount) {
    KtCntxLog.addItem(log, this, maxDepth, maxCount);
  }

  public void addListShortlyOn(KtLog log) {
    list.addShortlyOn(log);
  }

  public abstract String getDesc();

  /* TeXtp[715] */
  public KtNode reboxedToWidth(KtDimen width) {
    return list.isEmpty() ? pretendingWidth( width) : super.reboxedToWidth( width);
  }
}

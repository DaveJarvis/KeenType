// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.SizesSummarizer
// $Id: KtSizesSummarizer.java,v 1.1.1.1 2000/08/09 08:28:31 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtSizesSummarizer {

  private KtDimen height;
  private KtDimen body;
  private KtDimen depth;
  private KtDimen width;
  private KtDimen leftX;
  private final KtDimen[] totalStr = new KtDimen[KtGlue.MAX_ORDER + 1];
  private final KtDimen[] totalShr = new KtDimen[KtGlue.MAX_ORDER + 1];

  public final KtDimen getHeight() {
    return height;
  }

  public final KtDimen getBody() {
    return body;
  }

  public final KtDimen getDepth() {
    return depth;
  }

  public final KtDimen getWidth() {
    return width;
  }

  public final KtDimen getLeftX() {
    return leftX;
  }

  public final KtDimen getTotalStr(byte ord) {
    return totalStr[ord];
  }

  public final KtDimen getTotalShr(byte ord) {
    return totalShr[ord];
  }

  public final byte maxTotalStr() {
    return maxOrd(totalStr);
  }

  public final byte maxTotalShr() {
    return maxOrd(totalShr);
  }

  public KtSizesSummarizer() {
    height = KtDimen.ZERO;
    body = KtDimen.ZERO;
    depth = KtDimen.ZERO;
    width = KtDimen.ZERO;
    leftX = KtDimen.ZERO;
    setZero(totalStr);
    setZero(totalShr);
  }

  public final void add(KtDimen dim) {
    body = body.plus(dim);
  }

  public final void setHeight(KtDimen h) {
    add(height);
    height = h;
  }

  public final void setDepth(KtDimen d) {
    add(depth);
    depth = d;
  }

  public final void setMaxWidth(KtDimen w) {
    width = width.max(w);
  }

  public final void setMaxLeftX(KtDimen l) {
    leftX = leftX.max(l);
  }

  public final void addStretch(KtDimen dim, byte ord) {
    addOrd(totalStr, dim, ord);
  }

  public final void addShrink(KtDimen dim, byte ord) {
    addOrd(totalShr, dim, ord);
  }

  public final void add(KtGlue glue) {
    body = body.plus(glue.getDimen());
    addStretch(glue.getStretch(), glue.getStrOrder());
    addShrink(glue.getShrink(), glue.getShrOrder());
  }

  public final void restrictDepth(KtDimen maxDepth) {
    if (depth.moreThan(maxDepth)) {
      body = body.plus(depth).minus(maxDepth);
      depth = maxDepth;
    }
  }

  private void setZero(KtDimen[] total) {
    for (int i = total.length; i-- > 0; total[i] = KtDimen.ZERO)
      ;
  }

  private void addOrd(KtDimen[] total, KtDimen dim, byte ord) {
    if (!dim.isZero()) total[ord] = total[ord].plus(dim);
  }

  private byte maxOrd(KtDimen[] total) {
    byte ord = KtGlue.MAX_ORDER;
    while (ord > KtGlue.NORMAL && total[ord].isZero()) ord--;
    return ord;
  }
}

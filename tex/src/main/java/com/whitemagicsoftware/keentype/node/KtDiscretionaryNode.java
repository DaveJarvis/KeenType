// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.DiscretionaryNode
// $Id: KtDiscretionaryNode.java,v 1.1.1.1 2000/06/13 22:26:09 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtDiscretionaryNode extends KtAnyBoxedNode {
  /* root corresponding to disc_node */

  /* STRANGE
   * why two different limits?
   */
  public static final int MAX_LIST_LENGTH = 255;
  public static final int MAX_LIST_RECONS_LENGTH = 127;

  public static final KtDiscretionaryNode EMPTY =
      new KtDiscretionaryNode(KtNodeList.EMPTY, KtNodeList.EMPTY, KtNodeList.EMPTY);

  protected final KtNodeList pre;
  protected final KtNodeList post;
  protected final KtNodeList list;

  public KtDiscretionaryNode(KtNodeList pre, KtNodeList post, KtNodeList list) {
    super(KtHorizIterator.naturalSizes(list.nodes()));
    this.pre = pre;
    this.post = post;
    this.list = list;
  }

  /* STRANGE
   * isn't it inconsistent?
   */
  public boolean hasKern() {
    KtNode last = list.lastNode();
    return last != KtNode.NULL && last.hasKern();
  }

  public KtDimen getKern() {
    KtNode last = list.lastNode();
    return last != KtNode.NULL ? last.getKern() : KtDimen.NULL;
  }

  /* STRANGE
   * at break the list is replaced by pre+post but \/ remains
   */
  public KtDimen getItalCorr() {
    KtNode last = list.lastNode();
    return last != KtNode.NULL ? last.getItalCorr() : KtDimen.NULL;
  }

  /* TeXtp[175] */
  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    return post.addShortlyOn(log, pre.addShortlyOn(log, metric));
  }

  /* TeXtp[195] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc());
    if (!list.isEmpty()) log.add(" replacing ").add(list.length());
    cntx.addOn(log, pre.nodes(), '.');
    cntx.addOn(log, post.nodes(), '|');
    cntx.addItems(log, list.nodes());
  }

  public String getDesc() {
    return "discretionary";
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    setter.moveLeft(getLeftX());
    KtNodeEnum nodes = list.nodes();
    while (nodes.hasMoreNodes()) {
      KtNode node = nodes.nextNode();
      setter.moveRight(node.getLeftX());
      node.typeSet(setter, sctx);
      setter.moveRight(node.getWidth());
    }
    setter.moveLeft(getWidth());
  }

  /* TeXtp[856] */
  public void addBreakDescOn(KtLog log) {
    log.addEsc("discretionary");
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return pre.isEmpty() ? brCntx.exHyphenPenalty() : brCntx.hyphenPenalty();
  }

  public boolean isHyphenBreak() {
    return true;
  }

  public KtDimen preBreakWidth() {
    return KtHorizIterator.totalWidth(pre.nodes());
  }

  public KtDimen postBreakWidth() {
    return KtHorizIterator.totalWidth(post.nodes());
  }

  /* STRANGE
   * Why is EMPTY inserted?
   * Emptied discretionary is left in list (TeXtp[882]), why?
   */
  public KtNodeEnum atBreakReplacement() {
    KtNodeList replacement = new KtNodeList(1 + pre.length());
    replacement.append(EMPTY).append(pre);
    return replacement.nodes();
  }

  public KtNodeEnum postBreakNodes() {
    return post.nodes();
  }

  public boolean discardsAfter() {
    return post.isEmpty();
  }

  public void contributeVisible(KtVisibleSummarizer summarizer) {
    summarizer.summarize(list.nodes());
  }

  public String toString() {
    return "Discretionary(" + pre + "; " + post + "; " + list + ')';
  }
}

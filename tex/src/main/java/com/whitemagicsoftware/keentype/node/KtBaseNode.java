// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.BaseNode
// $Id: KtBaseNode.java,v 1.1.1.1 2000/06/06 11:52:47 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtTokenList; // DDD
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;

public abstract class KtBaseNode implements KtNode {
  /* corresponding to ANY_node */

  public KtDimen getHeight() {
    return KtDimen.ZERO;
  }

  public KtDimen getDepth() {
    return KtDimen.ZERO;
  }

  public KtDimen getHstr() {
    return KtDimen.ZERO;
  }

  public byte getHstrOrd() {
    return KtGlue.NORMAL;
  }

  public KtDimen getHshr() {
    return KtDimen.ZERO;
  }

  public byte getHshrOrd() {
    return KtGlue.NORMAL;
  }

  public KtDimen getWidth() {
    return KtDimen.ZERO;
  }

  public KtDimen getLeftX() {
    return KtDimen.ZERO;
  }

  public KtDimen getWstr() {
    return KtDimen.ZERO;
  }

  public byte getWstrOrd() {
    return KtGlue.NORMAL;
  }

  public KtDimen getWshr() {
    return KtDimen.ZERO;
  }

  public byte getWshrOrd() {
    return KtGlue.NORMAL;
  }

  public boolean sizeIgnored() {
    return false;
  }

  public boolean isPenalty() {
    return false;
  }

  public boolean isKern() {
    return false;
  }

  public boolean hasKern() {
    return false;
  }

  public boolean isSkip() {
    return false;
  }

  public boolean isMuSkip() {
    return false;
  }

  public boolean isBox() {
    return false;
  }

  public boolean isCleanBox() {
    return false;
  }

  public boolean isMigrating() {
    return false;
  }

  public boolean isMark() {
    return false;
  }

  public boolean isInsertion() {
    return false;
  }

  public KtNum getPenalty() {
    return KtNum.NULL;
  }

  public KtDimen getKern() {
    return KtDimen.NULL;
  }

  public KtGlue getSkip() {
    return KtGlue.NULL;
  }

  public KtGlue getMuSkip() {
    return KtGlue.NULL;
  }

  public KtBox getBox() {
    return KtBox.NULL;
  }

  public KtDimen getItalCorr() {
    return KtDimen.NULL;
  }

  public KtNodeEnum getMigration() {
    return KtNodeList.EMPTY_ENUM;
  }

  public KtTokenList getMark() {
    return KtTokenList.EMPTY;
  }

  public KtInsertion getInsertion() {
    return KtInsertion.NULL;
  }

  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    log.add("[]");
    return metric;
  }

  public void addOn(KtLog log, KtCntxLog cntx, KtDimen shift) {
    addOn(log, cntx);
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {}

  public void syncVertIfBox(KtTypesetter setter) {}

  public KtDimen getHeight(KtGlueSetting setting) {
    return getHeight();
  }

  public KtDimen getDepth(KtGlueSetting setting) {
    return getDepth();
  }

  public KtDimen getWidth(KtGlueSetting setting) {
    return getWidth();
  }

  public KtDimen getLeftX(KtGlueSetting setting) {
    return getLeftX();
  }

  public void addBreakDescOn(KtLog log) {}

  public boolean discardable() {
    return false;
  }

  public boolean isKernBreak() {
    return false;
  }

  public boolean canPrecedeSkipBreak() {
    return !discardable();
  }

  public boolean canFollowKernBreak() {
    return false;
  }

  public boolean allowsSpaceBreaking() {
    return false;
  }

  public boolean forbidsSpaceBreaking() {
    return false;
  }

  public boolean isHyphenBreak() {
    return false;
  }

  public KtDimen preBreakWidth() {
    return KtDimen.ZERO;
  }

  public KtDimen postBreakWidth() {
    return KtDimen.ZERO;
  }

  public KtNodeEnum atBreakReplacement() {
    return KtNodeList.nodes(this);
  }

  public KtNodeEnum postBreakNodes() {
    return KtNodeEnum.NULL;
  }

  public boolean discardsAfter() {
    return true;
  }

  public boolean canBePartOfDiscretionary() {
    return false;
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return INF_PENALTY;
  }

  public void contributeVisible(KtVisibleSummarizer summarizer) {
    summarizer.add(
      summarizer.addingLeftX() ? getLeftX().plus( getWidth()) : getWidth(), allegedlyVisible());
  }

  protected boolean allegedlyVisible() {
    return false;
  }

  public boolean kernAfterCanBeSpared() {
    return false;
  }

  public boolean isKernThatCanBeSpared() {
    return false;
  }

  public KtNode trailingKernSpared() {
    return this;
  }

  public KtNode reboxedToWidth(KtDimen width) {
    return KtHBoxNode.reboxedToWidth(this, width);
  }

  public boolean startsWordBlock() {
    return false;
  }

  public byte beforeWord() {
    return FAILURE;
  }

  public boolean canBePartOfWord() {
    return false;
  }

  public KtFontMetric uniformMetric() {
    return KtFontMetric.NULL;
  }

  public KtLanguage alteringLanguage() {
    return KtLanguage.NULL;
  }

  public void contributeCharCodes(KtName.KtBuffer buf) {}

  public byte afterWord() {
    return FAILURE;
  }

  public boolean rightBoundary() {
    return false;
  }

  public boolean providesRebuilder(boolean prev) {
    return false;
  }

  public KtWordRebuilder makeRebuilder(KtTreatNode proc, boolean prev) {
    return KtWordRebuilder.NULL;
  }
}

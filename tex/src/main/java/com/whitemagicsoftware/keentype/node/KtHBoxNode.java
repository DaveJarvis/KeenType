// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HBoxNode
// $Id: KtHBoxNode.java,v 1.1.1.1 2000/10/25 07:13:52 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtHBoxNode extends KtAnyBoxNode {
  /* root corresponding to hlist_node */

  public static final KtHBoxNode EMPTY =
      new KtHBoxNode(KtBoxSizes.ZERO, KtGlueSetting.NATURAL, KtNodeList.EMPTY);

  public KtHBoxNode(KtBoxSizes sizes, KtGlueSetting setting, KtNodeList list) {
    super(sizes, setting, list);
  }

  public final boolean isHBox() {
    return true;
  }

  public String getDesc() {
    return "hbox";
  }

  protected void moveStart(KtTypesetter setter) {
    setter.moveLeft(getLeftX());
  }

  protected void movePrev(KtTypesetter setter, KtNode node) {
    setter.moveRight(node.getLeftX(setting));
  }

  protected void movePast(KtTypesetter setter, KtNode node) {
    setter.moveRight(node.getWidth(setting));
  }

  public KtBox pretendSizesCopy(KtBoxSizes sizes) {
    return new KtHBoxNode(sizes, setting, list);
  }

  public static KtHBoxNode packedOf(KtNodeList list) {
    return new KtHBoxNode(KtHorizIterator.naturalSizes(list.nodes()), KtGlueSetting.NATURAL, list);
  }

  // XXX could be optimized
  public static KtHBoxNode packedOf(KtNode node) {
    return packedOf(new KtNodeList(node));
  }

  /* TeXtp[721] */
  public KtNode trailingKernSpared() {
    return list.length() == 2
            && list.nodeAt(0).kernAfterCanBeSpared()
            && list.nodeAt(1).isKernThatCanBeSpared()
        ? new KtHBoxNode(sizes, setting, new KtNodeList(list.nodeAt(0)))
        : this;
  }

  /* TeXtp[715] */
  public KtNode reboxedToWidth(KtDimen width) {
    if (getWidth().equals(width)) return this;
    if (list.isEmpty()) return pretendingWidth(width);
    KtNodeList forPacking = list;
    if (list.length() == 1 && list.nodeAt(0).kernAfterCanBeSpared()) {
      KtNode node = list.nodeAt(0);
      KtDimen delta = getWidth().minus(node.getWidth());
      if (!delta.isZero()) {
        forPacking = new KtNodeList(2);
        forPacking.append(node).append(new KtChrKernNode(delta));
      }
    }
    return packedToWidth(forPacking.nodes(), width);
  }

  public static KtNode reboxedToWidth(KtNode node, KtDimen width) {
    return node.getWidth().equals(width) ? node : packedToWidth( KtNodeList.nodes( node), width);
  }

  private static KtHBoxNode packedToWidth(KtNodeEnum nodes, KtDimen width) {
    KtHSkipNode filler =
        new KtHSkipNode(KtGlue.valueOf(KtDimen.ZERO, KtDimen.UNITY, KtGlue.FIL, KtDimen.UNITY, KtGlue.FIL));
    KtNodeList list = new KtNodeList();
    list.append(filler).append(nodes).append(filler);
    KtSizesEvaluator pack = new KtSizesEvaluator();
    KtHorizIterator.summarize(list.nodes(), pack);
    width = width.minus(pack.getHeight());
    KtDimen size = pack.getBody().plus(pack.getDepth());
    pack.evaluate(width.minus(size), false);
    KtBoxSizes sizes = new KtBoxSizes(pack.getWidth(), width, pack.getLeftX(), pack.getHeight());
    return new KtHBoxNode(sizes, pack.getSetting(), list);
  }

  /* TeXt[1146] */
  public KtDimen allegedlyVisibleWidth() {
    /*
    	KtDimen		visible = KtDimen.ZERO;
    	KtDimen		width = KtDimen.ZERO;
    	boolean		addingLeftX = false;
    	boolean		elasticSeen = false;
    	KtNodeEnum	nodes = list.nodes();
    	while (nodes.hasMoreNodes()) {
    	    KtNode	node = nodes.nextNode();
    	    elasticSeen = (elasticSeen || node.hasElasticWidth(setting));
    	    if (!elasticSeen) {
    		if (addingLeftX) width = width.plus(node.getLeftX());
    		width = width.plus(node.getWidth());
    	    }
    	    addingLeftX = true;
    	    //XXX what if it grows more than KtDimen.MAX_VALUE
    	    if (node.allegedlyVisible())
    		if (elasticSeen) return KtDimen.NULL;
    		else visible = width;
    	}
    	return visible;
    */
    KtVisibleSummarizer summarizer = new KtVisibleSummarizer(setting);
    summarizer.summarize(list.nodes());
    return summarizer.getVisibleWidth();
  }

  public String toString() {
    return "HBox(" + sizes + "; " + setting + "; " + list + ')';
  }
}

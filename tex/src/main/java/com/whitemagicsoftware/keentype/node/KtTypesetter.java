// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.TypeSetter
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.tex.KtFileFormat;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.Optional;

public interface KtTypesetter {

  KtTypesetter NULL = null;

  interface KtMark {
    void move();

    KtDimen xDiff();

    KtDimen yDiff();
  }

  void set( char ch, KtFontMetric metric );

  void set( KtCharCode code, KtFontMetric metric );

  void setRule( KtDimen h, KtDimen w );

  void setSpecial( byte[] spec );

  void moveLeft( KtDimen x );

  void moveRight( KtDimen x );

  void moveUp( KtDimen y );

  void moveDown( KtDimen y );

  KtMark mark();

  void syncHoriz();

  void syncVert();

  void push();

  void pop();

  /**
   * Sets the name of the file to receive the publishable document. If
   * the document is started using {@link #startDocument(OutputStream)}
   * directly, then the file name will not necessarily be set.
   *
   * @param fileName KtName of the file to write to.
   */
  void setFileName( KtFileName fileName );

  Optional<KtFileName> getFileName();

  /**
   * Allows changing the scaling factor. This differs from magnification in
   * that it can be applied to scalable vector graphics.
   *
   * @param scale The scaling factor to use when rendering the document.
   */
  default void setScale( final double scale ) { }

  default void startDocument( final KtFileName fileName ) {
    try {
      setFileName( fileName );
      final var file = getFileName().orElse( fileName );
      startDocument( new FileOutputStream( file.getPath() ) );
    } catch( final IOException e ) {
      throw new UncheckedIOException( e );
    }
  }

  void startDocument( OutputStream os );

  void startPage(
    KtDimen yOffset, KtDimen xOffset,
    KtDimen height, KtDimen width,
    int[] nums );

  int pageCount();

  void endPage();

  void close();

  KtFileFormat getExtension();

  /**
   * Appends an extension to the given file name that is appropriate for this
   * type of {@link KtTypesetter}.
   *
   * @param fileName The {@link KtFileName} to affix with a suffix.
   * @return The given {@link KtFileName} with a suffix that represents the
   * type of {@link KtTypesetter}.
   */
  default KtFileName addExtension( final KtFileName fileName ) {
    final var name = fileName.copy();
    name.append( '.' );
    name.append( getExtension().toString() );

    return name;
  }
}

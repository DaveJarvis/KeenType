// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HyphenNodeEnum
// $Id: KtHyphenNodeEnum.java,v 1.1.1.1 2001/01/28 19:12:37 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtName;

public abstract class KtHyphenNodeEnum extends KtInsetedNodeEnum {

  private final boolean ucHyph;
  private KtLanguage currLang;
  private boolean spaceBreaking = true;

  public KtHyphenNodeEnum(KtNodeEnum in, KtLanguage lang, boolean ucHyph) {
    super(in);
    this.currLang = lang;
    this.ucHyph = ucHyph;
  }

  /* TeXtp[866] */
  private KtNode getNextNode() {
    KtNode node = super.nextNode();
    if (node.allowsSpaceBreaking()) spaceBreaking = true;
    else if (node.forbidsSpaceBreaking()) spaceBreaking = false;
    KtLanguage altLang = node.alteringLanguage();
    if (altLang != KtLanguage.NULL) currLang = altLang;
    return node;
  }

  private boolean wordBlock = false;

  /* TeXtp[866] */
  public KtNode nextNode() {
    if (wordBlock) {
      KtNodeList block = nextWordBlock();
      if (!block.isEmpty()) inseted = block.nodes();
    }
    KtNode node = getNextNode();
    wordBlock = node.startsWordBlock() && spaceBreaking;
    return node;
  }

  /* TeXtp[894-899] */
  protected KtNodeList nextWordBlock() {
    KtNodeList list = new KtNodeList();
    KtNode node;
    for (; ; ) {
      if (!hasMoreNodes()) return list;
      node = getNextNode();
      list.append(node);
      byte r = node.beforeWord();
      if (r == KtNode.FAILURE) return list;
      else if (r == KtNode.SUCCESS) break;
    }
    KtName.KtBuffer word = new KtName.KtBuffer();
    KtFontMetric wordMetric = KtFontMetric.NULL;
    int beg = list.length() - 1;
    KtLanguage lang = KtLanguage.NULL;
    KtCharCode hyphCode = KtCharCode.NULL;
    int count = 0;
    while (node.canBePartOfWord()) {
      KtFontMetric metric = node.uniformMetric();
      if (metric != KtFontMetric.NULL) {
        if (wordMetric == KtFontMetric.NULL) {
          hyphCode = hyphenChar(metric);
          if (hyphCode == KtCharCode.NULL) return list;
          wordMetric = metric;
        } else if (!metric.equals(wordMetric)) break;
      }
      node.contributeCharCodes(word);
      int cnt = word.length();
      if (cnt < 64) count = cnt;
      else break; // XXX sym const
      lang = currLang;
      if (!hasMoreNodes())
        return hyphenated(word, count, wordMetric, lang, hyphCode, list, beg, list.length());
      node = getNextNode();
      list.append(node);
    }
    int end = list.length() - 1;
    for (; ; ) {
      byte r = node.afterWord();
      if (r == KtNode.FAILURE) return list;
      else if (r == KtNode.SUCCESS || !hasMoreNodes())
        return hyphenated(word, count, wordMetric, lang, hyphCode, list, beg, end);
      node = getNextNode();
      list.append(node);
    }
  }

  protected KtNodeList hyphenated(
      KtName.KtBuffer buf,
      int count,
      KtFontMetric metric,
      KtLanguage lang,
      KtCharCode hyphCode,
      KtNodeList list,
      int beg,
      int end) {
    if (count > 0 && metric != KtFontMetric.NULL) {
      KtCharCode[] word = new KtCharCode[count];
      buf.getCodes(0, count, word, 0);
      if (ucHyph || word[0].toCanonicalLetter() == word[0].toChar()) {
        char[] canon = new char[count];
        for (int i = 0; i < count; i++) canon[i] = word[i].toCanonicalLetter();
        KtHyphens hyphens = lang.getHyphens(new String(canon));
        // System.err.println(hyphens.toString(new String(canon))); //XXX
        if (!hyphens.isEmpty())
          return new KtHyphenator(word, metric, hyphens, hyphCode, list, beg, end).hyphenated();
      }
    }
    return list;
  }

  private static class KtAppender implements KtTreatNode {
    // , nts.io.KtLoggable { //XXX

    private KtNodeList list = new KtNodeList();

    public void execute(KtNode node) {
      list.append(node);
    }

    public void clear() {
      if (!list.isEmpty()) list = new KtNodeList();
    }

    public KtNodeList takeList() {
      if (list.isEmpty()) return KtNodeList.EMPTY;
      else {
        KtNodeList curr = list;
        list = new KtNodeList();
        return curr;
      }
    }

    public String toString() {
      return list.toString();
    }

    // XXX
    // public int		length() { return list.length(); }
    // public void		addOn(nts.io.KtLog log)
    // { nts.typo.KtTypoCommand.addItemsOn(log, list.nodes()); }
    // XXX

  }

  private class KtHyphenator {

    private final KtCharCode[] word;
    private final KtFontMetric metric;
    private final KtHyphens hyphens;
    private final KtCharCode hyphCode;
    private final KtNodeList source;
    private final int beg;
    private final int end;

    public KtHyphenator(
        KtCharCode[] word,
        KtFontMetric metric,
        KtHyphens hyphens,
        KtCharCode hyphCode,
        KtNodeList source,
        int beg,
        int end) {
      this.word = word;
      this.metric = metric;
      this.hyphens = hyphens;
      this.hyphCode = hyphCode;
      this.source = source;
      this.beg = beg;
      this.end = end;
    }

    private int hyphenDone;
    private int passed;

    /* TeXtp[913] */
    public KtNodeList hyphenated() {
      findBoundary();
      KtNodeList result = new KtNodeList();
      result.append(source.nodes(0, replaceFrom()));
      KtAppender pre = new KtAppender();
      KtAppender post = new KtAppender();
      KtAppender list = new KtAppender();
      int n = word.length;
      // System.err.println("n = " + n);
      hyphenDone = -1;
      // nts.command.KtCommand.normLog.startLine()
      // .add("Reconstituing nodes from: ").add(0).endLine();
      for (int j = -1; j < n; ) {
        int l = j;
        // nts.command.KtCommand.normLog.startLine()
        // .add("next iteration from: ").add(j + 1).endLine();
        // System.err.println("l = j = " + j);
        j = passingReconstitute(j, n, list);
        // System.err.println("j = " + j + ", passed = " + passed);
        if (passed < 0) {
          result.append(list.takeList());
          if (hyphenAt(j)) {
            l = j;
            passed = j;
          }
        }
        // nts.command.KtCommand.normLog.startLine()
        // .add("j = ").add(j + 1)
        // .add(", hyphen_passed = ").add((passed < 0) ? 0 : passed)
        // .endLine();
        while (passed >= 0) {
          // nts.command.KtCommand.normLog.startLine()
          // .add("no break list (length: ")
          // .add(list.length()).add("):").add(list);
          hyphenDone = passed;
          hyphenReconstitute(l, passed, pre);
          // nts.command.KtCommand.normLog.startLine()
          // .add("pre break list:").add(pre);
          boolean bound = true;
          int i = passed;
          do {
            do {
              i = reconstitute(i, n, post, bound);
              // nts.command.KtCommand.normLog.startLine()
              // .add("post break list (prolonged):")
              // .add(post);
              bound = false;
            } while (i < j);
            while (j < i) j = reconstitute(j, n, list, false);
            // nts.command.KtCommand.normLog.startLine()
            // .add("no break list (prolonged) (length: ")
            // .add(list.length()).add("):").add(list);
          } while (i < j);
          KtNodeList main = list.takeList();
          if (main.length() > KtDiscretionaryNode.MAX_LIST_RECONS_LENGTH) {
            result.append(main);
            pre.clear();
            post.clear();
          } else result.append(new KtDiscretionaryNode(pre.takeList(), post.takeList(), main));
          passed = hyphenAt(j) ? j : -1;
          l = j;
          // if (passed >= 0)
          // nts.command.KtCommand.normLog
          // .add("and appending not justified hyphen ...")
          // .endLine();
        }
      }
      // nts.command.KtCommand.normLog.startLine()
      // .add("Hyphenation done").endLine().endLine();
      return result.append(source.nodes(end));
    }

    private int passingReconstitute(int j, int n, KtTreatNode proc) {
      KtWordRebuilder reb;
      if (j >= 0) reb = metric.getWordRebuilder(proc, false);
      else {
        j = 0;
        reb = firstRebuilder(proc);
      }
      for (passed = -1; ; j++) {
        if (passed < 0 && hyphenAt(j) && reb.prolongsCut(hyphCode)) passed = j;
        if (j >= n) finalClose(reb);
        else {
          // System.err.println("word[" + j + "] = " + word[j]);
          byte how = reb.addIfBelongsToCut(word[j]);
          // System.err.println("how = " + how);
          if (passed < 0 && how != KtWordRebuilder.INDEPENDENT && hyphenAt(j)) passed = j;
          if (how == KtWordRebuilder.BELONGING) continue;
        }
        return j;
      }
    }

    private void hyphenReconstitute(int j, int n, KtTreatNode proc) {
      KtWordRebuilder reb;
      if (j >= 0) reb = metric.getWordRebuilder(proc, false);
      else {
        j = 0;
        reb = firstRebuilder(proc);
      }
      while (j < n) reb.add(word[j++]);
      if (!reb.add(hyphCode)) complain(metric, hyphCode);
      reb.close(true);
    }

    private int reconstitute(int j, int n, KtTreatNode proc, boolean bound) {
      KtWordRebuilder reb = metric.getWordRebuilder(proc, bound);
      for (; ; j++) {
        // System.err.println("reconstitute: j = " + j + ", n = " + n
        // + ", bound = " + bound);
        if (j >= n) {
          finalClose(reb);
          return j;
        } else {
          byte code = reb.addIfBelongsToCut(word[j]);
          // System.err.println("word[j] = " + word[j]
          // + ", code = " + code);
          if (code != KtWordRebuilder.BELONGING) return j;
          // if (reb.addIfBelongsToCut(word[j])
          // != KtWordRebuilder.BELONGING) return j;
        }
      }
    }

    private boolean hyphenAt(int pos) {
      return pos > hyphenDone && hyphens.hyphenAt( pos);
    }

    private int replaceFrom() {
      return beg > 0 && metric.equals(source.nodeAt(beg - 1).uniformMetric()) ? beg - 1 : beg;
    }

    private KtWordRebuilder firstRebuilder(KtTreatNode proc) {
      if (beg > 0) {
        KtFontMetric prevMetric = source.nodeAt(beg - 1).uniformMetric();
        if (prevMetric == KtFontMetric.NULL) {
          return source.nodeAt(beg).makeRebuilder(proc, false);
        } else if (metric.equals(prevMetric)) {
          return source.nodeAt(beg - 1).makeRebuilder(proc, true);
        }
      }
      return metric.getWordRebuilder(proc, true);
    }

    private KtCharCode boundary = KtCharCode.NULL;
    private boolean rightHit = false;

    private void findBoundary() {
      if (end < source.length()) {
        KtNode node = source.nodeAt(end);
        if (metric.equals(node.uniformMetric())) {
          KtName.KtBuffer buf = new KtName.KtBuffer();
          node.contributeCharCodes(buf);
          if (buf.length() > 0) boundary = buf.codeAt(0);
        }
      }
      if (boundary == KtCharCode.NULL) rightHit = source.nodeAt(end - 1).rightBoundary();
    }

    private void finalClose(KtWordRebuilder reb) {
      if (boundary != KtCharCode.NULL) reb.close(boundary);
      else reb.close(rightHit);
    }
  }

  protected abstract KtCharCode hyphenChar(KtFontMetric metric);

  protected abstract void complain(KtFontMetric metric, KtCharCode code);
}

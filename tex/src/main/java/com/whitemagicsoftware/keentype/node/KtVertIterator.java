// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VertIterator
// $Id: KtVertIterator.java,v 1.1.1.1 2000/08/09 06:10:07 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtVertIterator extends KtEnumSizesIterator {

  public KtVertIterator(KtNodeEnum nodes) {
    super(nodes);
  }

  public KtDimen currHeight() {
    return curr.getHeight();
  }

  public KtDimen currWidth() {
    return curr.getWidth();
  }

  public KtDimen currDepth() {
    return curr.getDepth();
  }

  public KtDimen currLeftX() {
    return curr.getLeftX();
  }

  public KtDimen currStr() {
    return curr.getHstr();
  }

  public KtDimen currShr() {
    return curr.getHshr();
  }

  public byte currStrOrd() {
    return curr.getHstrOrd();
  }

  public byte currShrOrd() {
    return curr.getHshrOrd();
  }

  public static KtBoxSizes naturalSizes(KtNodeEnum nodes, KtDimen maxDepth) {
    KtSizesSummarizer pack = new KtSizesSummarizer();
    summarize(nodes, pack);
    if (maxDepth != KtDimen.NULL) pack.restrictDepth(maxDepth);
    return new KtBoxSizes(
        pack.getHeight().plus(pack.getBody()), pack.getWidth(), pack.getDepth(), pack.getLeftX());
  }

  public static void summarize(KtNodeEnum nodes, KtSizesSummarizer pack) {
    new KtVertIterator(nodes).summarize( pack);
  }
}

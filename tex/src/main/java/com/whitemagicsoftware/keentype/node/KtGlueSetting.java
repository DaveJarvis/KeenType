// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.GlueSetting
// $Id: KtGlueSetting.java,v 1.1.1.1 2001/05/15 03:58:40 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public class KtGlueSetting implements Serializable, KtLoggable {

  public static final byte RIGID = 0, STRETCHING = 1, SHRINKING = 2;

  protected final byte sign;
  protected final byte order;
  protected final double ratio;

  public static final KtGlueSetting NATURAL = new KtGlueSetting(RIGID, KtGlue.NORMAL, 0.0);

  public KtGlueSetting(byte sign, byte order, double ratio) {
    this.sign = sign;
    this.order = order;
    this.ratio = ratio;
  }

  /* TeXtp[625, 634] */
  public KtDimen set(KtGlue glue, boolean limited) {
    KtDimen dimen = glue.getDimen();
    switch( sign ) {
      case STRETCHING -> {
        if( glue.getStrOrder() == order )
          dimen =
            limited
              ? dimen.plus( glue.getStretch().limitedTimes( ratio ) )
              : dimen.plus( glue.getStretch().times( ratio ) );
      }
      case SHRINKING -> {
        if( glue.getShrOrder() == order )
          dimen =
            limited
              ? dimen.minus( glue.getShrink().limitedTimes( ratio ) )
              : dimen.minus( glue.getShrink().times( ratio ) );
      }
    }
    return dimen;
  }

  /* TeXtp[1148] */
  public boolean makesElastic(KtGlue glue) {
    return switch( sign ) {
      case STRETCHING ->
        glue.getStrOrder() == order && !glue.getStretch().isZero();
      case SHRINKING ->
        glue.getShrOrder() == order && !glue.getShrink().isZero();
      default -> false;
    };
  }

  public void addOn(KtLog log) {
    if (sign != RIGID && ratio != 0.0) {
      log.add(", glue set ");
      if (sign == SHRINKING) log.add("- ");
      if (Math.abs(ratio) > KtDimen.MAX_DOUBLE_VALUE)
        log.add( ratio > 0.0 ? ">" : "< -").add( KtGlue.toString( KtDimen.MAX_FROM_DOUBLE, order));
      else log.add(KtGlue.toString(KtDimen.valueOf(ratio), order));
    }
  }
}

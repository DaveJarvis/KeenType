// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.PageSplit
// $Id: KtPageSplit.java,v 1.1.1.1 2001/02/01 13:29:44 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtPageSplit extends KtVertSplit {

  private boolean discarding;
  private boolean nullSpecs; // nullSpecs => discarding
  private KtNode last;
  private final Map<Integer, KtInsRecord> insMap = new TreeMap<>();
  private final KtNodeList maybeOver = new KtNodeList();

  private void restart() {
    discarding = true;
    nullSpecs = true;
    last = KtNode.NULL;
    insMap.clear();
    maybeOver.clear();
  }

  {
    restart();
  }

  protected void freezeSpecs() {
    if (nullSpecs) {
      initSpecs();
      nullSpecs = false;
    }
  }

  protected abstract void initSpecs();

  public KtNode lastSpecialNode() {
    KtNode node = lastNode();
    return node != KtNode.NULL ? node : last;
  }

  public boolean canChangeDimens() {
    return !nullSpecs;
  }

  protected boolean waitingForMore() {
    return true;
  }

  protected void passWhilePrunning(KtNode node) {
    pass(node);
  }

  protected void passWhileBreaking(KtNode node) {
    pass(node);
  }

  public int insertPenalties = 0;

  protected int extraPenalty() {
    return insertPenalties;
  }

  private class KtInsRecord {

    private final int num;
    private KtDimen size;
    private boolean full = false;
    private final Vector<KtNodeList> data = new Vector<>();
    private int bestCount = 0;

    /* TeXtp[1009] */
    public KtInsRecord(int number) {
      num = number;
      KtBox box = getInsVBox(num);
      KtGlue skip = getInsSkip(num);
      size = box.getHeight().plus(box.getDepth());
      goal = goal.minus(insScale(size, num)).minus(skip.getDimen());
      soFar.add(skip.resizedCopy(KtDimen.ZERO));
    }

    public boolean isFull() {
      return full;
    }

    public void markBest() {
      bestCount = data.size();
    }

    /* TeXtp[1008] */
    public void record(KtInsertion ins) {
      KtDimen delta = goal.plus(soFar.getShrink()).minus(soFar.getNatural()).minus(depth);
      KtDimen scal = insScale(ins.size, num);
      if (!(scal.moreThan(0) && scal.moreThan(delta))
          && !size.plus(ins.size).moreThan(getInsSize(num))) {
        size = size.plus(ins.size);
        goal = goal.minus(scal);
        data.add(ins.list);
      } else split(ins);
    }

    /* TeXtp[1010] */
    private void split(KtInsertion ins) {
      int fac = getInsFactor(num);
      KtDimen space;
      if (fac <= 0) space = KtDimen.MAX_VALUE;
      else {
        space = goal.minus(soFar.getNatural()).minus(depth);
        if (fac != 1000) space = space.over(fac).times(1000);
      }
      space = space.min(getInsSize(num).minus(size));
      KtInsertSplit splitter = new KtInsertSplit(ins.list.nodes(), ins.topSkip);
      KtNodeList head = splitter.makeSplitting(space, ins.maxDepth);
      KtDimen bestSize = splitter.getBestSize();
      KtNode breakNode = splitter.breakNode();
      int cost =
          breakNode == KtNode.NULL
              ? KtNode.EJECT_PENALTY
              : breakNode.isPenalty() ? breakNode.getPenalty().intVal() : 0;
      traceSplitCost(num, space, bestSize, cost);
      full = true;
      size = size.plus(bestSize);
      goal = goal.minus(insScale(bestSize, num));
      data.add(head);
      insertPenalties += cost;
      if (!splitter.isEmpty()) {
        splitter.pruneTop();
        KtNodeList tail = new KtNodeList(splitter.nodes());
        KtVBoxNode vbox = KtVBoxNode.packedOf(tail);
        maybeOver.append(
            new KtInsertNode(ins.makeCopy(tail, vbox.getHeight().plus(vbox.getDepth()))));
      }
    }

    /* TeXtp[1018,1021] */
    public void finish() {
      if (!data.isEmpty()) {
        KtNodeList list = new KtNodeList();
        KtNodeEnum boxList = getInsVBox(num).getVertList();
        if (boxList != KtNodeEnum.NULL) list.append(boxList);
        for (int i = 0; i < bestCount; i++) list.append(data.get(i));
        setInsVBox(num, KtVBoxNode.packedOf(list));
        // XXX Maybe that the list can be constructed instead of list
        // XXX of lists. But beware of sequence of empty lists!!!
      }
    }

    /* STRANGE
     * why is xn_over_d not used?
     */
    private KtDimen insScale(KtDimen size, int num) {
      int fac = getInsFactor(num);
      return fac == 1000 ? size : size.over( 1000).times( fac);
    }

    /* STRANGE
     * Why is the size divided by 1000 even if \count<num> is 1000?
     * And why log.endLine() instead of log.startLine() ?
     */
    /* TeXtp[986] */
    public void show(KtLog log) {
      log.endLine()
          .addEsc("insert")
          .add(num)
          .add(" adds ")
          .add(size.over(1000).times(getInsFactor(num)).toString());
      if (full) log.add(", #").add(data.size()).add(" might split");
    }
  }

  protected class KtInsertSplit extends KtVertSplit {

    private final KtGlue topSkip;

    public KtInsertSplit(KtNodeEnum nodes, KtGlue topSkip) {
      super(nodes);
      this.topSkip = topSkip;
    }

    private KtDimen bestSize;

    public KtDimen getBestSize() {
      return bestSize;
    }

    protected void markBestPlace() {
      bestSize = this.soFar.getNatural().plus(this.depth);
    }

    protected KtNode topAdjustment(KtDimen height) {
      return splitTopAdjustment(height, topSkip);
    }
  }

  protected abstract void setInsVBox(int num, KtBox box);

  protected abstract KtBox getInsVBox(int num);

  protected abstract KtGlue getInsSkip(int num);

  protected abstract KtDimen getInsSize(int num);

  protected abstract int getInsFactor(int num);

  protected abstract void traceSplitCost(int num, KtDimen space, KtDimen best, int cost);

  protected abstract KtNode splitTopAdjustment(KtDimen height, KtGlue topSkip);

  /* TeXtp[996,1008] */
  private void pass(KtNode node) {
    last = node.discardable() ? node : KtNode.NULL;
    if (node.isInsertion()) {
      freezeSpecs();
      KtInsertion ins = node.getInsertion();
      Integer key = ins.num;
      KtInsRecord rec = insMap.get(key);
      if (rec == null) {
        rec = new KtInsRecord(ins.num);
        insMap.put(key, rec);
      }
      if (rec.isFull()) {
        maybeOver.append(node);
        insertPenalties += ins.floatCost.intVal();
      } else rec.record(ins);
    }
  }

  private KtDimen bestGoal;
  private final KtNodeList heldOver = new KtNodeList();

  /* TeXtp[1005] */
  protected void markBestPlace() {
    Iterator iterator = insMap.values().iterator();
    while (iterator.hasNext()) ((KtInsRecord) iterator.next()).markBest();
    heldOver.append(maybeOver);
    maybeOver.clear();
    bestGoal = goal;
  }

  /* TeXtp[994] */
  private boolean step() {
    if (discarding) {
      boolean pruned = pruneTop();
      if (!nullSpecs) adjustDepth();
      if (pruned) {
        freezeSpecs();
        discarding = false;
      } else return false;
    }
    return findBreak();
  }

  /* TeXtp[1014,1017] */
  public void build() {
    while (step()) {
      KtNodeList list = split();
      if (insertsWanted()) {
        Iterator iterator = insMap.values().iterator();
        while (iterator.hasNext()) ((KtInsRecord) iterator.next()).finish();
        insertPenalties = heldOver.length();
        list = withoutInserts(list);
      } else {
        insertPenalties = 0;
        heldOver.clear();
      }
      restart();
      if (performOutput(list, bestGoal)) return;
    }
  }

  protected abstract boolean insertsWanted();

  protected abstract boolean performOutput(KtNodeList list, KtDimen size);

  /* TeXtp[1018,1020] */
  private KtNodeList withoutInserts(KtNodeList list) {
    KtNodeList result = new KtNodeList();
    KtNodeEnum nodes = list.nodes();
    while (nodes.hasMoreNodes()) {
      KtNode node = nodes.nextNode();
      if (!node.isInsertion()) result.append(node);
    }
    return result;
  }

  public void startNextPage(KtNodeEnum output) {
    setNullSpecs();
    KtNodeList contrib = new KtNodeList(data);
    data = new Vector<>();
    append(heldOver);
    heldOver.clear();
    append(output);
    append(contrib);
  }

  public void startNextPage() {
    startNextPage(EMPTY_ENUM);
  }

  /* TeXtp[218,986] */
  protected void show(KtLog log, int depth, int breadth, boolean act) {
    KtNodeEnum nodes;
    if (act) {
      nodes = heldOver.nodes();
      if (nodes.hasMoreNodes()) {
        log.startLine().add("### current page:").add(" (held over for next output)");
        KtCntxLog.addItems(log, nodes, depth, breadth);
      }
    } else {
      nodes = currPageList();
      if (nodes.hasMoreNodes()) {
        log.startLine().add("### current page:");
        KtCntxLog.addItems(log, nodes, depth, breadth);
        if (!nullSpecs) {
          log.startLine().add("total height ").add(soFar.toString());
          log.startLine().add(" goal height ").add(goal.toString());
          Iterator iterator = insMap.values().iterator();
          while (iterator.hasNext()) ((KtInsRecord) iterator.next()).show(log);
        }
      }
    }
    nodes = contribList();
    if (nodes.hasMoreNodes()) {
      log.startLine().add("### recent contributions:");
      KtCntxLog.addItems(log, nodes, depth, breadth);
    }
  }

  public abstract void show(KtLog log, int depth, int breadth);
}

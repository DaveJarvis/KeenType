// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.AnyShiftNode
// $Id: KtAnyShiftNode.java,v 1.1.1.1 2000/06/06 11:52:29 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;

public abstract class KtAnyShiftNode extends KtBaseNode {
  /* corresponding to ANY_node */

  protected final KtNode node;
  protected final KtDimen shift;

  public KtAnyShiftNode(KtNode node, KtDimen shift) {
    this.node = node;
    this.shift = shift;
  }

  public KtDimen getShift() {
    return shift;
  }

  public abstract KtDimen getHeight();

  public abstract KtDimen getDepth();

  public abstract KtDimen getWidth();

  public abstract KtDimen getLeftX();

  public KtDimen getHstr() {
    return node.getHstr();
  }

  public byte getHstrOrd() {
    return node.getHstrOrd();
  }

  public KtDimen getHshr() {
    return node.getHshr();
  }

  public byte getHshrOrd() {
    return node.getHshrOrd();
  }

  public KtDimen getWstr() {
    return node.getWstr();
  }

  public byte getWstrOrd() {
    return node.getWstrOrd();
  }

  public KtDimen getWshr() {
    return node.getWshr();
  }

  public byte getWshrOrd() {
    return node.getWshrOrd();
  }

  public boolean sizeIgnored() {
    return node.sizeIgnored();
  }

  public boolean isPenalty() {
    return node.isPenalty();
  }

  public boolean isKern() {
    return node.isKern();
  }

  public boolean hasKern() {
    return node.hasKern();
  }

  public boolean isSkip() {
    return node.isSkip();
  }

  public boolean isMuSkip() {
    return node.isMuSkip();
  }

  public boolean isBox() {
    return node.isBox();
  }

  public boolean isMigrating() {
    return node.isMigrating();
  }

  public KtNum getPenalty() {
    return node.getPenalty();
  }

  public KtDimen getKern() {
    return node.getKern();
  }

  public KtGlue getSkip() {
    return node.getSkip();
  }

  public KtGlue getMuSkip() {
    return node.getMuSkip();
  }

  public KtBox getBox() {
    return node.getBox();
  }

  public KtDimen getItalCorr() {
    return node.getItalCorr();
  }

  public KtNodeEnum getMigration() {
    return node.getMigration();
  }

  public abstract KtDimen getHeight(KtGlueSetting setting);

  public abstract KtDimen getDepth(KtGlueSetting setting);

  public abstract KtDimen getWidth(KtGlueSetting setting);

  public abstract KtDimen getLeftX(KtGlueSetting setting);

  public abstract void typeSet(KtTypesetter setter, KtSettingContext sctx);

  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    return node.addShortlyOn(log, metric);
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    node.addOn(log, cntx, shift);
  }

  public void addOn(KtLog log, KtCntxLog cntx, KtDimen shift) {
    node.addOn(log, cntx, shift.plus(shift));
  }

  public void syncVertIfBox(KtTypesetter setter) {
    node.syncVertIfBox(setter);
  }

  public void addBreakDescOn(KtLog log) {
    node.addBreakDescOn(log);
  }

  public boolean discardable() {
    return node.discardable();
  }

  public boolean isKernBreak() {
    return node.isKernBreak();
  }

  public boolean canPrecedeSkipBreak() {
    return node.canPrecedeSkipBreak();
  }

  public boolean canFollowKernBreak() {
    return node.canFollowKernBreak();
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return node.breakPenalty(brCntx);
  }

  public KtNodeEnum atBreakReplacement() {
    return node.atBreakReplacement();
  }

  public boolean canBePartOfDiscretionary() {
    return node.canBePartOfDiscretionary();
  }

  public void contributeVisible(KtVisibleSummarizer summarizer) {
    node.contributeVisible(summarizer);
  }

  public boolean startsWordBlock() {
    return node.startsWordBlock();
  }

  public byte beforeWord() {
    return node.beforeWord();
  }

  public boolean canBePartOfWord() {
    return node.canBePartOfWord();
  }

  public KtFontMetric uniformMetric() {
    return node.uniformMetric();
  }

  public KtLanguage alteringLanguage() {
    return node.alteringLanguage();
  }

  public byte afterWord() {
    return node.afterWord();
  }

  public boolean rightBoundary() {
    return node.rightBoundary();
  }

  public void contributeCharCodes(KtName.KtBuffer buf) {
    node.contributeCharCodes(buf);
  }

  public boolean providesRebuilder(boolean prev) {
    return node.providesRebuilder(prev);
  }

  public KtWordRebuilder makeRebuilder(KtTreatNode proc, boolean prev) {
    return node.makeRebuilder(proc, prev);
  }
}

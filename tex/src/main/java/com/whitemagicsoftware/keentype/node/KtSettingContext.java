// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.SettingContext
// $Id: KtSettingContext.java,v 1.1.1.1 1999/12/06 23:13:25 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public final class KtSettingContext {

  public final KtBoxSizes around;
  public final KtGlueSetting setting;
  public final KtTypesetter.KtMark start;
  public final boolean allowIO;

  public KtSettingContext(
      KtBoxSizes around, KtGlueSetting setting, KtTypesetter.KtMark start, boolean allowIO) {
    this.around = around;
    this.setting = setting;
    this.start = start;
    this.allowIO = allowIO;
  }

  public KtSettingContext shiftedUp(KtDimen shift) {
    return new KtSettingContext(around.shiftedUp(shift), setting, start, allowIO);
  }

  public KtSettingContext shiftedLeft(KtDimen shift) {
    return new KtSettingContext(around.shiftedLeft(shift), setting, start, allowIO);
  }

  public KtSettingContext allowingIO(boolean allowIO) {
    return new KtSettingContext(around, setting, start, allowIO);
  }
}

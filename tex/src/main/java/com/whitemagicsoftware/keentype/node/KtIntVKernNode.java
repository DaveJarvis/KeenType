// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.IntVKernNode
// $Id: KtIntVKernNode.java,v 1.1.1.1 2000/06/06 08:25:55 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtIntVKernNode extends KtBaseNode {
  /* root corresponding to kern_node */

  protected final KtDimen kern;

  public KtIntVKernNode(KtDimen kern) {
    this.kern = kern;
  }

  public KtDimen getHeight() {
    return kern;
  }

  public boolean canBePartOfDiscretionary() {
    return true;
  }

  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    return metric;
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("kern").add(kern.toString());
  }

  public String toString() {
    return "IntVKern(" + kern + ')';
  }
}

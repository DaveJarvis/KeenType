// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VLeadRuleNode
// $Id: KtVLeadRuleNode.java,v 1.1.1.1 2001/03/06 14:55:26 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtVLeadRuleNode extends KtVSkipNode {
  /* corresponding to glue_node */

  protected final KtBoxSizes sizes;
  private final String desc;

  public KtVLeadRuleNode(KtGlue skip, KtBoxSizes sizes, String desc) {
    super(skip);
    this.sizes = sizes;
    this.desc = desc;
  }

  public KtBoxSizes getSizes() {
    return sizes;
  }

  public KtDimen getWidth() {
    return sizes.getWidth();
  }

  public KtDimen getLeftX() {
    return sizes.getLeftX();
  }

  protected boolean allegedlyVisible() {
    return true;
  }

  /* TeXtp[190] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(desc).add(' ').add(skip.toString());
    cntx.addOn(log, new KtRuleNode(sizes));
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    KtBoxSizes leading =
        new KtBoxSizes(sctx.setting.set(skip, true), sizes.rawWidth(), KtDimen.ZERO, sizes.rawLeftX());
    KtRuleNode.typeSet(setter, leading.replenished(sctx.around));
  }

  public String toString() {
    return "VLeadRule(" + skip + "; " + sizes + "; " + desc + ')';
  }
}

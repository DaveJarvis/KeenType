// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.VBoxNode
// $Id: KtVBoxNode.java,v 1.1.1.1 2000/06/06 08:28:33 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtVBoxNode extends KtAnyBoxNode {
  /* root corresponding to vlist_node */

  public KtVBoxNode(KtBoxSizes sizes, KtGlueSetting setting, KtNodeList list) {
    super(sizes, setting, list);
  }

  public final boolean isVBox() {
    return true;
  }

  public String getDesc() {
    return "vbox";
  }

  protected void moveStart(KtTypesetter setter) {
    setter.moveUp(getHeight());
  }

  protected void movePrev(KtTypesetter setter, KtNode node) {
    setter.moveDown(node.getHeight(setting));
    node.syncVertIfBox(setter);
  }

  protected void movePast(KtTypesetter setter, KtNode node) {
    setter.moveDown(node.getDepth(setting));
  }

  public KtBox pretendSizesCopy(KtBoxSizes sizes) {
    return new KtVBoxNode(sizes, setting, list);
  }

  public static KtVBoxNode packedOf(KtNodeList list, KtDimen maxDepth) {
    return new KtVBoxNode(
        KtVertIterator.naturalSizes(list.nodes(), maxDepth), KtGlueSetting.NATURAL, list);
  }

  public static KtVBoxNode packedOf(KtNodeList list) {
    return packedOf(list, KtDimen.NULL);
  }

  // XXX could be optimized
  public static KtVBoxNode packedOf(KtNode node) {
    return packedOf(new KtNodeList(node));
  }

  public String toString() {
    return "VBox(" + sizes + "; " + setting + "; " + list + ')';
  }
}

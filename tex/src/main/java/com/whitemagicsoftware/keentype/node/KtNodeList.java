// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.NodeList
// $Id: KtNodeList.java,v 1.1.1.1 2000/06/06 09:00:25 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import java.io.Serializable;
import java.util.Vector;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtNodeList implements Serializable {

  public static final KtNodeList NULL = null;

  public static final KtNodeList EMPTY = new KtNodeList();

  public static final KtNodeEnum EMPTY_ENUM =
      new KtNodeEnum() {
        public KtNode nextNode() {
          return KtNode.NULL;
        }

        public boolean hasMoreNodes() {
          return false;
        }
      };

  protected Vector<KtNode> data;

  protected KtNodeList(Vector<KtNode> data) {
    this.data = data;
  }

  public KtNodeList() {
    data = new Vector<>();
  }

  public KtNodeList(int initCap) {
    data = new Vector<>( initCap );
  }

  public KtNodeList(int initCap, int capIncrement) {
    data = new Vector<>( initCap, capIncrement );
  }

  public KtNodeList(KtNode node) {
    this(1);
    append(node);
  }

  public KtNodeList(KtNodeEnum nodes) {
    this();
    append(nodes);
  }

  public KtNodeList(KtNode[] nodes) {
    this(nodes.length);
    append(nodes);
  }

  public KtNodeList(KtNode[] nodes, int offset, int count) {
    this(count);
    append(nodes, offset, count);
  }

  public final int length() {
    return data.size();
  }

  public final boolean isEmpty() {
    return data.isEmpty();
  }

  protected void clear() {
    data.clear();
  }

  public final KtNode nodeAt(int idx) {
    return data.elementAt( idx);
  }

  public KtNodeList append(KtNode node) {
    data.addElement(node);
    return this;
  }

  public KtNodeList append(KtNode[] nodes, int offset, int count) {
    data.ensureCapacity(data.size() + count);
    while (count-- > 0) append(nodes[offset++]);
    return this;
  }

  public KtNodeList append(KtNode[] nodes) {
    return append(nodes, 0, nodes.length);
  }

  public KtNodeList append(KtNodeEnum nodes) {
    while (nodes.hasMoreNodes()) append(nodes.nextNode());
    return this;
  }

  public KtNodeList append(KtNodeList list) {
    return append(list.nodes());
  }

  public KtNode lastNode() {
    return length() > 0 ? nodeAt( length() - 1) : KtNode.NULL;
  }

  public void removeLastNode() {
    if (length() > 0) data.removeElementAt(length() - 1);
  }

  public KtNode lastSpecialNode() {
    return lastNode();
  }

  public KtNode[] toArray() {
    KtNode[] nodes = new KtNode[data.size()];
    return data.toArray( nodes);
  }

  private class KtEnum extends KtNodeEnum {
    private int idx;
    private final int end;

    public KtEnum(int idx, int end) {
      this.idx = idx;
      this.end = end;
    }

    public KtNode nextNode() {
      return nodeAt(idx++);
    }

    public boolean hasMoreNodes() {
      return idx < end;
    }
  }

  public KtNodeEnum nodes() {
    return new KtEnum(0, length());
  }

  public KtNodeEnum nodes(int start) {
    return new KtEnum(start, length());
  }

  public KtNodeEnum nodes(int start, int end) {
    return new KtEnum(start, end);
  }

  public static KtNodeEnum nodes(final KtNode node) {
    return new KtNodeEnum() {
      private boolean fresh = true;

      public KtNode nextNode() {
        fresh = false;
        return node;
      }

      public boolean hasMoreNodes() {
        return fresh;
      }
    };
  }

  /* TeXtp[655] */
  public KtNodeList extractedMigrations() {
    int cnt = 0, i = 0;
    while (i < length()) if (nodeAt(i++).isMigrating()) cnt++;
    if (cnt > 0) {
      KtNodeList list = new KtNodeList();
      i = 0;
      while (i < length()) {
        KtNode node = nodeAt(i);
        if (node.isMigrating()) {
          list.append(node.getMigration());
          data.remove(i);
        } else i++;
      }
      return list;
    } else return EMPTY;
  }

  /* TeXtp[174] */
  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    KtNodeEnum nodes = nodes();
    while (nodes.hasMoreNodes()) metric = nodes.nextNode().addShortlyOn(log, metric);
    return metric;
  }

  public KtFontMetric addShortlyOn(KtLog log) {
    return addShortlyOn(log, KtFontMetric.NULL);
  }

  public String toString() {
    StringBuilder buf = new StringBuilder();
    KtNodeEnum nodes = nodes();
    if (nodes.hasMoreNodes()) {
      buf.append(nodes.nextNode());
      while (nodes.hasMoreNodes()) buf.append(", ").append(nodes.nextNode());
    }
    return buf.toString();
  }
}

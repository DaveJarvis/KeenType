// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.HLeadRuleNode
// $Id: KtHLeadRuleNode.java,v 1.1.1.1 2001/03/06 14:56:18 ksk Exp $
package com.whitemagicsoftware.keentype.node;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtHLeadRuleNode extends KtHSkipNode {
  /* corresponding to glue_node */

  protected final KtBoxSizes sizes;
  private final String desc;

  public KtHLeadRuleNode(KtGlue skip, KtBoxSizes sizes, String desc) {
    super(skip);
    this.sizes = sizes;
    this.desc = desc;
  }

  public KtBoxSizes getSizes() {
    return sizes;
  }

  public KtDimen getHeight() {
    return sizes.getHeight();
  }

  public KtDimen getDepth() {
    return sizes.getDepth();
  }

  protected boolean allegedlyVisible() {
    return true;
  }

  /* TeXtp[190] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(desc).add(' ').add(skip.toString());
    cntx.addOn(log, new KtRuleNode(sizes));
  }

  public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
    KtBoxSizes leading =
        new KtBoxSizes(sizes.rawHeight(), sctx.setting.set(skip, true), sizes.rawDepth(), KtDimen.ZERO);
    KtRuleNode.typeSet(setter, leading.replenished(sctx.around));
  }

  public String toString() {
    return "HLeadRule(" + skip + "; " + sizes + "; " + desc + ')';
  }
}

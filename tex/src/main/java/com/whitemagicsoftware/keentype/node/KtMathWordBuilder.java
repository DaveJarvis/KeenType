// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.node.MathWordBuilder
// $Id: KtMathWordBuilder.java,v 1.1.1.1 2000/10/05 06:18:01 ksk Exp $
package com.whitemagicsoftware.keentype.node;

public interface KtMathWordBuilder extends KtWordBuilder {

  KtMathWordBuilder NULL = null;

  boolean lastHasCollapsed();

  // XXX also close - make it into the name of the methods
  KtNode takeLastNode();

  KtNode takeLastLargerNode();
}

/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import com.whitemagicsoftware.keentype.base.KtDimen;

/**
 * Responsible for drawing horizontal rules.
 */
public final class KtPendingRule {
  public final static KtPendingRule NULL = null;

  private final KtTurtle mTurtle;
  private final KtDimen mW;
  private final KtDimen mH;

  public KtPendingRule( final KtTurtle turtle, final KtDimen w, final KtDimen h ) {
    assert turtle != null;
    assert w != null;
    assert h != null;

    mTurtle = turtle;
    mW = w;
    mH = h;
  }

  public void put() {
    mTurtle.drawRule( mW, mH );
  }

  public void set() {
    mTurtle.advance( mW );
  }

  public boolean canBeSet() {
    return mTurtle.isWidth( mW );
  }
}

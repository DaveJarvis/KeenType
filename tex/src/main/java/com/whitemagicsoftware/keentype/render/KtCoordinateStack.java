/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import com.whitemagicsoftware.keentype.base.KtDimen;

/**
 * Responsible for tracking a stack of coordinate pairs.
 *
 * @param x    The X coordinate component.
 * @param y    The Y coordinate component.
 * @param next The next element in the stack.
 */
public record KtCoordinateStack( KtDimen x, KtDimen y, KtCoordinateStack next ) {
  public static final KtCoordinateStack NULL = null;
}

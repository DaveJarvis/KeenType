/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import com.whitemagicsoftware.keentype.tex.KtFileOpener;
import com.whitemagicsoftware.keentype.tex.KtFontInfo;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static java.awt.Font.TRUETYPE_FONT;
import static java.awt.Font.TYPE1_FONT;
import static java.awt.font.TextAttribute.LIGATURES_ON;
import static com.whitemagicsoftware.keentype.tex.KtFileFormat.*;
import static com.whitemagicsoftware.keentype.tex.KtTeXCharMapper.KtTeXFileName;

/**
 * Responsible for reading fonts into memory. This can read OTF, TTF, and
 * Type1 (PFB) file formats.
 */
public class KtFontReader {
  public static final Font DEFAULT_FONT = Font.decode( null );

  public KtFontReader() { }

  @SuppressWarnings( {"unchecked", "rawtypes"} )
  public Font createFont( final KtFontInfo info ) {
    final var dirName = new String( info.getDirName() );
    final var fileName = new String( info.getFileName() );
    final var path = Path.of( dirName, fileName );
    final var filename = new KtTeXFileName();

    filename.setPath( path.toString() );

    final var font = createFont( filename );
    final Map attrs = font.getAttributes();
    attrs.put( TextAttribute.LIGATURES, LIGATURES_ON );
    attrs.put( TextAttribute.SIZE, info.getAtSize().toInt() );

    return font.deriveFont( attrs );
  }

  private Font createFont( final KtTeXFileName filename ) {
    final var opener = new KtFileOpener();
    final var font = new AtomicReference<>( DEFAULT_FONT );

    opener.attachHandler( PFB_EXT, is -> {
      font.set( Font.createFont( TYPE1_FONT, is ) );
      is.close();
    } );
    opener.attachHandler( OTF_EXT, is -> {
      font.set( Font.createFont( TRUETYPE_FONT, is ) );
      is.close();
    } );
    opener.attachHandler( TTF_EXT, is -> {
      font.set( Font.createFont( TRUETYPE_FONT, is ) );
      is.close();
    } );

    try {
      opener.read( filename, PFB_EXT, OTF_EXT, TTF_EXT );
    } catch( final FontFormatException e ) {
      throw new UncheckedIOException( new IOException( e ) );
    } catch( final IOException e ) {
      throw new UncheckedIOException( e );
    }

    return font.get();
  }
}

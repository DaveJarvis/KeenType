/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import com.whitemagicsoftware.keentype.tex.KtFontInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Maps codepoints between fonts relative to Computer Modern.
 */
@SuppressWarnings( "SpellCheckingInspection" )
public class KtCodePointMapper {
  /**
   * Default calculation for codepoints is to return the given codepoint.
   */
  private static final Function<Integer, Integer> CODEPOINT =
    codepoint -> codepoint;

  /**
   * BaKoMa font glyph codepoint shifts relative to Computer Modern.
   */
  private static final Function<Integer, Integer> CM = codepoint -> {
    if( codepoint <= 9 ) {
      // 0 to 9 are shifted to 161 to 170
      return codepoint + 161;
    }
    else if( codepoint == 20 ) {
      // 20 is shifted to 128
      return 128;
    }
    else if( codepoint <= 32 ) {
      // 10 to 32 are shifted to 173 to 195
      return codepoint + 163;
    }

    return codepoint;
  };

  /**
   * BaKoMa fonts for Computer Modern are shifted. Codepoints are largely
   * the same, except for certain glyphs.
   */
  private static final Map<String, Function<Integer, Integer>> CODEPOINTS =
    new HashMap<>(256);

  static {
    // Computer Modern Math Italic
    CODEPOINTS.put( "cmmi5", CM );
    CODEPOINTS.put( "cmmi6", CM );
    CODEPOINTS.put( "cmmi7", CM );
    CODEPOINTS.put( "cmmi8", CM );
    CODEPOINTS.put( "cmmi9", CM );
    CODEPOINTS.put( "cmmi10", CM );
    CODEPOINTS.put( "cmmi11", CM );
    CODEPOINTS.put( "cmmi12", CM );
    // Computer Modern Math Italic Bold
    CODEPOINTS.put( "cmmib6", CM );
    CODEPOINTS.put( "cmmib7", CM );
    CODEPOINTS.put( "cmmib8", CM );
    CODEPOINTS.put( "cmmib9", CM );
    CODEPOINTS.put( "cmmib10", CM );
    // Computer Modern Symbols
    CODEPOINTS.put( "cmsy5", CM );
    CODEPOINTS.put( "cmsy6", CM );
    CODEPOINTS.put( "cmsy7", CM );
    CODEPOINTS.put( "cmsy8", CM );
    CODEPOINTS.put( "cmsy9", CM );
    CODEPOINTS.put( "cmsy10", CM );
    // Computer Modern Bold Symbols
    CODEPOINTS.put( "cmbsy6", CM );
    CODEPOINTS.put( "cmbsy7", CM );
    CODEPOINTS.put( "cmbsy8", CM );
    CODEPOINTS.put( "cmbsy9", CM );
    CODEPOINTS.put( "cmbsy10", CM );
    // Computer Modern Extended
    CODEPOINTS.put( "cmex7", CM );
    CODEPOINTS.put( "cmex8", CM );
    CODEPOINTS.put( "cmex9", CM );
    CODEPOINTS.put( "cmex10", CM );
    // Computer Modern Bold Extended
    CODEPOINTS.put( "cmbx5", CM );
    CODEPOINTS.put( "cmbx6", CM );
    CODEPOINTS.put( "cmbx7", CM );
    CODEPOINTS.put( "cmbx8", CM );
    CODEPOINTS.put( "cmbx9", CM );
    CODEPOINTS.put( "cmbx10", CM );
    CODEPOINTS.put( "cmbx12", CM );
    // Computer Modern Roman
    CODEPOINTS.put( "cmr5", CM );
    CODEPOINTS.put( "cmr6", CM );
    CODEPOINTS.put( "cmr7", CM );
    CODEPOINTS.put( "cmr8", CM );
    CODEPOINTS.put( "cmr9", CM );
    CODEPOINTS.put( "cmr10", CM );
    CODEPOINTS.put( "cmr12", CM );
    CODEPOINTS.put( "cmr17", CM );
    // Computer Modern Bold
    CODEPOINTS.put( "cmb10", CM );
    // Computer Modern Teletype Terminal
    CODEPOINTS.put( "cmtt8", CM );
    CODEPOINTS.put( "cmtt9", CM );
    CODEPOINTS.put( "cmtt10", CM );
    CODEPOINTS.put( "cmtt12", CM );
    // Computer Modern Slanted Teletype Terminal
    CODEPOINTS.put( "cmsltt10", CM );
    // Computer Modern Slanted
    CODEPOINTS.put( "cmsl8", CM );
    CODEPOINTS.put( "cmsl9", CM );
    CODEPOINTS.put( "cmsl10", CM );
    CODEPOINTS.put( "cmsl12", CM );
    // Computer Modern Unslanted
    CODEPOINTS.put( "cmu10", CM );
    // Computer Modern Text Italic
    CODEPOINTS.put( "cmti7", CM );
    CODEPOINTS.put( "cmti8", CM );
    CODEPOINTS.put( "cmti9", CM );
    CODEPOINTS.put( "cmti10", CM );
    CODEPOINTS.put( "cmti12", CM );
    // Computer Modern Slanted Extended
    CODEPOINTS.put( "cmxsl10", CM );
    // Computer Modern Text Italic Extended
    CODEPOINTS.put( "cmxti10", CM );
    // Computer Modern Bold Extended Slanted
    CODEPOINTS.put( "cmbxsl10", CM );
    // Computer Modern Bold Extended Text Italic
    CODEPOINTS.put( "cmbxti10", CM );
    // Computer Modern Text Extended
    CODEPOINTS.put( "cmtex8", CM );
    CODEPOINTS.put( "cmtex9", CM );
    CODEPOINTS.put( "cmtex10", CM );
    // Compputer Modern Dunhill
    CODEPOINTS.put( "cmdunh10", CM );
    // Computer Modern Small Caps
    CODEPOINTS.put( "cmsc8", CM );
    CODEPOINTS.put( "cmsc9", CM );
    CODEPOINTS.put( "cmsc10", CM );
    // Computer Modern Caps/Small Caps
    CODEPOINTS.put( "cmcsc10", CM );
    CODEPOINTS.put( "cmcsc8", CM );
    CODEPOINTS.put( "cmcsc9", CM );
    // Computer Modern Text Caps/Small Caps
    CODEPOINTS.put( "cmtcsc10", CM );
    // Computer Modern Funny Font
    CODEPOINTS.put( "cmff10", CM );
    // Computer Modern Funny Italic
    CODEPOINTS.put( "cmfi10", CM );
    // Computer Modern Funny Bold
    CODEPOINTS.put( "cmfib8", CM );
    // Computer Modern Italic ...?
    CODEPOINTS.put( "cminch", CM );
    CODEPOINTS.put( "cmitt10", CM );
    // Computer Modern Sans Serif
    CODEPOINTS.put( "cmss10", CM );
    CODEPOINTS.put( "cmss12", CM );
    CODEPOINTS.put( "cmss17", CM );
    CODEPOINTS.put( "cmss8", CM );
    CODEPOINTS.put( "cmss9", CM );
    // Computer Modern Sans Serif Bold Extended
    CODEPOINTS.put( "cmssbx10", CM );
    CODEPOINTS.put( "cmssdc10", CM );
    // Computer Modern Sans Serif Italic
    CODEPOINTS.put( "cmssi8", CM );
    CODEPOINTS.put( "cmssi9", CM );
    CODEPOINTS.put( "cmssi10", CM );
    CODEPOINTS.put( "cmssi12", CM );
    CODEPOINTS.put( "cmssi17", CM );
    // Computer Modern Sans Serif ...?
    CODEPOINTS.put( "cmssq8", CM );
    // Computer Modern Sans Serif ... Italic?
    CODEPOINTS.put( "cmssqi8", CM );
    // Computer Modern ??? Teletype Terminal
    CODEPOINTS.put( "cmvtt10", CM );
  }

  /**
   * Defines the lookup function, which can change when the font changes.
   */
  private Function<Integer, Integer> mCodePoint = CODEPOINT;

  /**
   * Switches the font so that the codepoint map can return a suitable
   * {@link Function} that shifts between codepoints.
   *
   * @param info The new font that directs the codepoint map to use.
   */
  public void font( final KtFontInfo info ) {
    final var fontName = new String( info.getFileName() );

    mCodePoint = CODEPOINTS.getOrDefault( fontName, CODEPOINT );
  }

  /**
   * Determines the codepoint to use based on the current font.
   *
   * @param codepoint The codepoint to re-map.
   * @return The re-mapped codepoint.
   */
  public int lookup( final int codepoint ) {
    return mCodePoint.apply( codepoint );
  }
}

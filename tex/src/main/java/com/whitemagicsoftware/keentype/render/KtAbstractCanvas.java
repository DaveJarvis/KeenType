/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import java.awt.*;

public abstract class KtAbstractCanvas<T> implements KtCanvas<T> {
  private Font mFont;

  /**
   * Changes the font to use when drawing glyphs.
   *
   * @param font The new font to use when drawing glyphs.
   */
  @Override
  public void setFont( final Font font ) {
    assert font != null;
    mFont = font;
  }

  /**
   * Returns the font to use when drawing glyphs.
   *
   * @return The font to use when drawing glyphs.
   */
  Font getFont() {
    return mFont;
  }
}

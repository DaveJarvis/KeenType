/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

/**
 * Responsible for converting a double value into a string with a specified
 * precision.
 * <p>
 * Not thread-safe.
 * </p>
 */
public final class KtFastDouble {
  private static final int[] POW10 = {1, 10, 100, 1000, 10000, 100000, 1000000};
  private static final StringBuilder BUFFER = new StringBuilder();

  /**
   * Converts the given value to a string.
   *
   * @param value     The value to convert to a string.
   * @param precision The number of decimal places.
   * @return The string representation of the given value.
   * @see <a href="https://stackoverflow.com/a/10554128/59087">source</a>
   */
  public static String toString( double value, final int precision ) {
    assert precision >= 0;
    assert precision < POW10.length;
    
    final var sb = BUFFER;
    sb.setLength( 0 );

    if( value < 0 ) {
      sb.append( '-' );
      value = -value;
    }

    final int exp = POW10[ precision ];
    final long lval = (long) (value * exp + 0.5);

    sb.append( lval / exp ).append( '.' );

    final long fval = lval % exp;

    for( int p = precision - 1; p > 0 && fval < POW10[ p ]; p-- ) {
      sb.append( '0' );
    }

    sb.append( fval );

    return sb.toString();
  }
}

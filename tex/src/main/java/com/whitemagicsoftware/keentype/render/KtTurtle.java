/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.tex.KtFontInfo;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import com.whitemagicsoftware.keentype.node.KtTypesetter.KtMark;

/**
 * Responsible for processing TeX commands as drawing primitives rendered
 * on an SVG-capable canvas. Named after the iconic Logo programming language
 * metaphor.
 */
public final class KtTurtle {
  /**
   * Allow caching fonts for all {@link KtTurtle} instances throughout time.
   */
  private final static Map<KtFontInfo, Font> sFonts = new HashMap<>( 32 );

  private final KtFontReader mFontReader = new KtFontReader();
  private final KtCodePointMapper mCodePoints = new KtCodePointMapper();

  private KtDimen mSvgX;
  private KtDimen mSvgY;
  private KtDimen mTexX;
  private KtDimen mTexY;
  private final KtCanvas<?> mCanvas;

  public KtTurtle(
    final KtDimen svgX, final KtDimen svgY,
    final KtDimen texX, final KtDimen texY,
    final KtCanvas<?> canvas
  ) {
    setSvgX( svgX );
    setSvgY( svgY );
    setTexX( texX );
    setTexY( texY );
    mCanvas = canvas;
  }

  public void sync() {
    syncHorizontal();
    syncVertical();
  }

  public void syncHorizontal() {
    if( !mSvgX.equals( mTexX ) ) {
      setSvgX( mTexX );
    }
  }

  public void syncVertical() {
    if( !mSvgY.equals( mTexY ) ) {
      setSvgY( mTexY );
    }
  }

  public void font( final KtFontInfo info ) {
    mCodePoints.font( info );

    mCanvas.setFont(
      sFonts.computeIfAbsent( info, mFontReader::createFont )
    );
  }

  public void draw( final char ch ) {
    sync();

    final var mapped = mCodePoints.lookup( ch );
    mCanvas.drawString(
      Character.toString( mapped ),
      mSvgX.toDouble(),
      mSvgY.toDouble()
    );
  }

  public void drawRule( final KtDimen w, final KtDimen h ) {
    // The magic numbers align the horizontal rule to overlap the top of
    // the radical sign to create a nicely rounded corner.
    final var x = mSvgX.toDouble();
    final var y = mSvgY.toDouble() - .2;

    // This magic number ensures that the line's stroke is always visible,
    // unless the height is negative. Don't use negatives.
    mCanvas.drawLine(
      x, y, w.plus( x ).toDouble(), y, h.toInt() + 0.406
    );
  }

  public void left( final KtDimen x ) {
    setTexX( mTexX.minus( x ) );
  }

  public void right( final KtDimen x ) {
    setTexX( mTexX.plus( x ) );
  }

  public void up( final KtDimen y ) {
    setTexY( mTexY.minus( y ) );
  }

  public void down( final KtDimen y ) {
    setTexY( mTexY.plus( y ) );
  }

  public void advance( final KtDimen w ) {
    setSvgX( mSvgX.plus( w ) );
  }

  public void moveTexTo( final KtDimen x, final KtDimen y ) {
    setTexX( x );
    setTexY( y );
  }

  public void moveSvgTo( final KtDimen x, final KtDimen y ) {
    setSvgX( x );
    setSvgY( y );
  }

  public KtMark mark() {
    return new KtMark() {
      private final KtDimen mX = KtDimen.valueOf( mTexX );
      private final KtDimen mY = KtDimen.valueOf( mTexY );

      @Override
      public void move() {
        moveTexTo( mX, mY );
      }

      @Override
      public KtDimen xDiff() {
        return mTexX.minus( mX );
      }

      @Override
      public KtDimen yDiff() {
        return mTexY.minus( mY );
      }
    };
  }

  public KtCoordinateStack stack( final KtCoordinateStack stack ) {
    return new KtCoordinateStack( mSvgX, mSvgY, stack );
  }

  public boolean isWidth( final KtDimen width ) {
    return mTexX.equals( mSvgX.plus( width ) );
  }

  private void setTexX( final KtDimen x ) {
    mTexX = x;
  }

  private void setTexY( final KtDimen y ) {
    mTexY = y;
  }

  private void setSvgX( final KtDimen x ) {
    mSvgX = x;
  }

  private void setSvgY( final KtDimen y ) {
    mSvgY = y;
  }
}

/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.render;

import java.awt.*;

/**
 * @param <T> The type of rendered object that's suitable for viewing.
 */
public interface KtCanvas<T> {
  /**
   * Number of decimal places for geometric shapes.
   */
  int DECIMALS_GEOMETRY = 4;

  void setFont( Font font );

  void drawString( String s, double x, double y );

  void drawLine( double x1, double y1, double x2, double y2, double thickness );

  /**
   * Exports the render using the running maximum extents determined from the
   * drawing primitives.
   * <p>
   * This produces a document with a canvas that minimally encompasses every
   * drawing primitive that was added.
   * </p>
   *
   * @return A publishable representation of primitives called while drawing.
   */
  default T export() {
    return export( 1.0 );
  }

  /**
   * Exports the render using the running maximum extents determined from the
   * drawing primitives.
   * <p>
   * This produces a document with a canvas that minimally encompasses every
   * drawing primitive that was added.
   * </p>
   *
   * @param scale The scaling factor to apply to the resulting document.
   *
   * @return A publishable representation of primitives called while drawing.
   */
  T export( final double scale );

  /**
   * Delegates to {@link #export(double, double, double)} without scaling.
   *
   * @param w The final document width.
   * @param h The final document height.
   * @return A publishable representation of primitives called while drawing.
   */
  default T export( final double w, final double h ) {
    return export( w, h, 1.0 );
  }

  /**
   * Call when no more graphics operations are pending and the content is safe
   * to convert to the final publishable representation.
   * <p>
   * No checks are made to ensure that a drawing primitive is called prior
   * to invoking this method. Be sure to call at least one in advance.
   * </p>
   *
   * @param w The final document width.
   * @param h The final document height.
   * @return A publishable representation of primitives called while drawing.
   */
  T export( double w, double h, double scale );

  default String toGeometryPrecision( final double value ) {
    return KtFastDouble.toString( value, DECIMALS_GEOMETRY );
  }
}

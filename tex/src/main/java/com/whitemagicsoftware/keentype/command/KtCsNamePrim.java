// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.CsNamePrim
// $Id: KtCsNamePrim.java,v 1.1.1.1 2000/03/17 14:44:13 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtName;

/* TeXtp[372] */
public class KtCsNamePrim extends KtExpandablePrim {

  public KtCsNamePrim(String name) {
    super(name);
  }

  public void expand(KtToken src) {
    KtName.KtBuffer buf = new KtName.KtBuffer();
    KtToken tok;
    KtCharCode code;
    while ((code = (tok = nextExpToken()).nonActiveCharCode()) != KtCharCode.NULL) buf.append(code);
    if (!meaningOf(tok).isEndCsName()) {
      backToken(tok);
      error("MissingEndcsname", esc("endcsname"));
    }
    tok = new KtCtrlSeqToken(buf.toName());
    if (meaningOf(tok).sameAs(KtUndefined.getUndefined())) tok.define(KtRelax.getRelax(), false);
    backToken(tok);
  }
}

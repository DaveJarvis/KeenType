// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.MuSkipPrim
// $Id: KtMuSkipPrim.java,v 1.1.1.1 1999/05/31 11:18:49 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtLog;

/** Setting glue register primitive. */
public class KtMuSkipPrim extends KtRegisterPrim {

  /**
   * Creates a new |KtMuSkipPrim| with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the |KtMuSkipPrim|
   */
  public KtMuSkipPrim(String name) {
    super(name);
  }

  public final void set(int idx, KtGlue val, boolean glob) {
    if (glob) getEqt().gput(tabKind, idx, val);
    else getEqt().put(tabKind, idx, val);
  }

  public final KtGlue get(int idx) {
    KtGlue val = (KtGlue) getEqt().get(tabKind, idx);
    return val != KtGlue.NULL ? val : KtGlue.ZERO;
  }

  public void addEqValueOn(int idx, KtLog log) {
    log.add(get(idx).toString("mu"));
  }

  public void perform(int idx, int operation, boolean glob) {
    set(idx, performFor(get(idx), operation, true), glob);
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  protected final void assign(KtToken src, boolean glob) {
    int idx = scanRegisterCode();
    skipOptEquals();
    set(idx, scanMuGlue(), glob);
  }

  public final void perform(int operation, boolean glob, KtCommand after) {
    perform(scanRegisterCode(), operation, glob);
  }

  public boolean hasMuGlueValue() {
    return true;
  }

  public KtGlue getMuGlueValue() {
    return get(scanRegisterCode());
  }
}

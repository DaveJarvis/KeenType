// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.NormalCharToken
// $Id: KtNormalCharToken.java,v 1.1.1.1 1999/08/16 12:11:05 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;

public abstract class KtNormalCharToken extends KtCharToken {

  public KtNormalCharToken(KtCharCode code) {
    super(code);
  }

  public boolean matchNormal(char c) {
    return code.match(c);
  }

  /**
   * Gives the 7 bit ascii character code of this normal (letter or other) character |KtToken|. The
   * result is |KtCharCode.NO_CHAR| if its character code is not a 7 bit ascii.
   *
   * @return the 7 bit ascii code if the code is defined, |KtCharCode.NO_CHAR| otherwise.
   */
  public char normalChar() {
    return code.toChar();
  }

  public String toString() {
    return "<NormalChar: " + code + '>';
  }

  private static KtCharHandler handler;

  public static void setHandler(KtCharHandler hnd) {
    handler = hnd;
  }

  public abstract class KtMeaning extends KtCharToken.KtMeaning {
    public void exec(KtToken src) {
      handler.handle(code, src);
    }

    public KtCharCode charCodeToAdd() {
      return code;
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.Prefix
// $Id: KtPrefix.java,v 1.1.1.1 2001/03/22 13:43:20 ksk Exp $
package com.whitemagicsoftware.keentype.command;

/** KtPrefix KtPrim which can prefix definition or assignment with LONG, OUTER or GLOBAL. */
public class KtPrefix extends KtPrefixPrim {

  /** Built in prefixes (LONG, OUTER, GLOBAL) */
  private final int prefix;

  /** Creates and registers the prefix primitive */
  public KtPrefix(String name, int prefix) {
    super(name);
    this.prefix = prefix;
  }

  public void exec(KtToken src, int prefixes) {
    KtToken tok = nextExpToken();
    meaningOf(tok).execute(tok, prefixes | prefix);
  }

  public void doAssignment(KtToken src, int prefixes) {
    KtToken tok = nextExpToken();
    meaningOf(tok).doAssignment(tok, prefixes | prefix);
  }
}

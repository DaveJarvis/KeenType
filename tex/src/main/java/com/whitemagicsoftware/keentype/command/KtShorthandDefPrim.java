// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ShorthandDefPrim
// $Id: KtShorthandDefPrim.java,v 1.1.1.1 2001/03/22 13:35:01 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtShorthandDefPrim extends KtAssignPrim {

  public KtShorthandDefPrim(String name) {
    super(name);
  }

  protected void assign(KtToken src, boolean glob) {
    KtToken tok = definableToken();
    tok.define(KtRelax.getRelax(), glob);
    skipOptEquals();
    int idx = scanRegisterCode();
    tok.define(makeShorthand(idx), glob);
  }

  protected abstract KtCommand makeShorthand(int idx);

  public abstract static class KtShorthand extends KtCommand {

    protected final int index;

    public KtShorthand(int idx) {
      index = idx;
    }

    /** Non prefixed version of exec */
    public final void exec(KtToken src) {
      exec(src, 0);
    }

    public final boolean assignable() {
      return true;
    }

    public final void doAssignment(KtToken src, int prefixes) {
      exec(src, prefixes);
    }

    /**
     * Performs itself in the process of interpretation of the macro language after sequence of
     * prefix commands.
     *
     * @param src source token for diagnostic output.
     * @param prefixes accumulated code of prefixes.
     */
    public final void exec(KtToken src, int prefixes) {
      KtPrefixPrim.beforeAssignment(this, prefixes);
      assign(src, KtPrefixPrim.globalAssignment(prefixes));
      KtPrefixPrim.afterAssignment();
    }

    protected abstract void assign(KtToken src, boolean glob);

    public final void addOn(KtLog log) {
      log.addEsc(getReg().getEqDesc()).add(index);
    }

    public boolean sameAs(KtCommand cmd) {
      return getClass() == cmd.getClass()
          && getReg().sameAs(((KtShorthand) cmd).getReg())
          && index == ((KtShorthand) cmd).index;
    }

    protected abstract KtRegisterPrim getReg();
  }
}

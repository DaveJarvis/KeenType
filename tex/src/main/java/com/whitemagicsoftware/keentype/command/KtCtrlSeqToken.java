// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.CtrlSeqToken
// $Id: KtCtrlSeqToken.java,v 1.1.1.1 2000/01/28 05:50:44 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;

/** Control sequence KtToken. */
public class KtCtrlSeqToken extends KtToken {

  public interface KtMeaninger {
    KtCommand get(KtName name);

    void set(KtName name, KtCommand cmd, boolean glob);
  }

  protected static KtMeaninger meaninger;

  public static void setMeaninger(KtMeaninger m) {
    meaninger = m;
  }

  /** The name of the control sequence */
  private final KtName name;

  /**
   * Creates a control sequence token of the given name.
   *
   * @param name the name of the control sequence
   */
  public KtCtrlSeqToken(KtName name) {
    this.name = name;
  }

  public KtCtrlSeqToken(String name) {
    this.name = makeName(name);
  }

  /**
   * Gives object (potentially) associated in table of equivalents.
   *
   * @return the KtCommand to interpret this token.
   */
  public KtCommand meaning() {
    return meaninger.get(name);
  }

  /**
   * Tells that the meaning of the |KtToken| can be redefined.
   *
   * @return |true|.
   */
  public boolean definable() {
    return true;
  }

  /**
   * Define given |KtCommand| to be equivalent in table of equivalents.
   *
   * @param cmd the object to interpret this token.
   * @param glob if |true| the equivalent is defined globaly.
   */
  public void define(KtCommand cmd, boolean glob) {
    meaninger.set(name, cmd, glob);
  }

  public boolean match(KtToken tok) {
    return tok instanceof KtCtrlSeqToken && ((KtCtrlSeqToken) tok).name.match( name);
  }

  public void addOn(KtLog log) {
    name.addEscapedOn(log);
  }

  public void addProperlyOn(KtLog log) {
    name.addProperlyEscapedOn(log);
  }

  public int numValue() {
    return name.length() == 1 ? name.codeAt( 0).numValue() : -1;
  }

  public KtName controlName() {
    return name;
  }

  public String toString() {
    return "<CtrlSequence: " + name + '>';
  }
}

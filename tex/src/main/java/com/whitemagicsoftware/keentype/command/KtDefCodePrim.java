// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.DefCodePrim
// $Id: KtDefCodePrim.java,v 1.1.1.1 2000/01/28 04:58:19 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtLog;

/** */
public class KtDefCodePrim extends KtAssignPrim implements KtNum.KtProvider {

  protected final KtNumKind tabKind = new KtNumKind();

  protected final KtNum defVal;

  protected final int maxVal;

  /**
   * Creates a new |KtDefCodePrim| with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the |KtDefCodePrim|
   */
  public KtDefCodePrim(String name, int defVal, int maxVal) {
    super(name);
    this.defVal = defVal != 0 ? KtNum.valueOf( defVal) : KtNum.ZERO;
    this.maxVal = maxVal;
  }

  protected final void set(int idx, KtNum val, boolean glob) {
    if (glob) getEqt().gput(tabKind, idx, val);
    else getEqt().put(tabKind, idx, val);
  }

  public KtNum get(int idx) {
    KtNum val = (KtNum) getEqt().get(tabKind, idx);
    return val != KtNum.NULL ? val : defVal;
  }

  public void addEqValueOn(int idx, KtLog log) {
    log.add(get(idx).toString());
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  /* TeXtp[1232] */
  protected void assign(KtToken src, boolean glob) {
    int idx = scanCharacterCode();
    skipOptEquals();
    int val = scanInt();
    if (val < 0 || val > maxVal) {
      error("CodeOutOfRange", num(val), num(maxVal));
      val = 0;
    }
    set(idx, KtNum.valueOf(val), glob);
  }

  public final boolean hasNumValue() {
    return true;
  }

  public final KtNum getNumValue() {
    return get(scanCharacterCode());
  }

  public final int intVal(int idx) {
    return get(idx).intVal();
  }

  public final void init(int idx, int val) {
    set(idx, KtNum.valueOf(val), true);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.AnyMarkPrim
// $Id: KtAnyMarkPrim.java,v 1.1.1.1 2000/01/31 21:42:34 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import java.io.IOException;
import java.io.ObjectInputStream;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtAnyMarkPrim extends KtExpandablePrim implements KtTokenList.KtMaintainer {

  private transient KtTokenList value;

  protected KtAnyMarkPrim(String name, KtTokenList val) {
    super(name);
    value = val;
  }

  public KtAnyMarkPrim(String name) {
    this(name, KtTokenList.EMPTY);
  }

  private void readObject(ObjectInputStream input) throws IOException, ClassNotFoundException {
    input.defaultReadObject();
    value = KtTokenList.EMPTY;
  }

  public KtTokenList getToksValue() {
    return value;
  }

  public void setToksValue(KtTokenList val) {
    value = val;
  }

  public boolean isEmpty() {
    return value.isEmpty();
  }

  public void expand(KtToken src) {
    tracedPushList(value, "mark");
  }

  /* TeXtp[296] */
  public void addExpandable(KtLog log, boolean full) {
    super.addExpandable(log, full);
    if (full) log.add(':').endLine().add(value);
  }

  /* TeXtp[223] */
  public void addExpandable(KtLog log, int maxCount) {
    addExpandable(log, false);
  }
}

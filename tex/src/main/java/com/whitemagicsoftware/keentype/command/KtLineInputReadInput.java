// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.LineInputReadInput
// $Id: KtLineInputReadInput.java,v 1.1.1.1 1999/05/31 11:18:48 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtInputLine;
import com.whitemagicsoftware.keentype.io.KtLineInput;

public class KtLineInputReadInput implements KtReadInput {

  public interface KtInputHandler {
    KtInputLine emptyLine();

    KtTokenizer makeTokenizer(KtInputLine line, String desc, boolean addEolc);
  }

  /** The underlying line oriented input reader */
  private final KtLineInput input;

  /** The parametrization object */
  private final KtInputHandler inpHandler;

  private final String desc;

  /**
   * Creates a |KtLineInputReadInput| with given input and parametrization object.
   *
   * @param input the underlying reader.
   * @param inpHandler the parametrization object.
   */
  public KtLineInputReadInput(KtLineInput input, KtInputHandler inpHandler, int num) {
    this.input = input;
    this.inpHandler = inpHandler;
    desc = "<read " + num + "> ";
  }

  /** Closes the underlying |KtLineInput|. */
  public void close() {
    input.close();
  }

  public KtTokenizer nextTokenizer(KtToken def, int ln) {
    KtInputLine line = input.readLine();
    return line == KtLineInput.EOF ? KtTokenizer.NULL : inpHandler.makeTokenizer( line, desc, true);
  }

  public KtTokenizer emptyLineTokenizer() {
    return inpHandler.makeTokenizer(inpHandler.emptyLine(), desc, true);
  }
}

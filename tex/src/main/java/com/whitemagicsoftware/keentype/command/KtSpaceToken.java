// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtSpaceToken extends KtToken {

  public static final KtCharCode CODE = KtCharToken.makeCharCode(' ');
  public static final KtSpaceToken TOKEN = new KtSpaceToken();

  public KtCharCode nonActiveCharCode() {
    return CODE;
  }

  public int numValue() {
    return CODE.numValue();
  }

  public KtCharCode charCode() {
    return CODE;
  }

  public KtToken makeCharToken(KtCharCode code) {
    return KtDirtySpaceToken.MAKER.make(code);
  }

  public boolean matchSpace() {
    return true;
  }

  public boolean match(KtToken tok) {
    return tok instanceof KtSpaceToken;
  }

  public boolean sameCatAs(KtToken tok) {
    return tok instanceof KtSpaceToken || tok instanceof KtDirtySpaceToken;
  }

  public void addOn(KtLog log) {
    log.add(' ');
  }

  public String toString() {
    return "<Space>";
  }

  private static KtCharHandler handler;

  public static void setHandler(KtCharHandler hnd) {
    handler = hnd;
    KtDirtySpaceToken.setHandler(hnd);
  }

  public static final String DESCRIPTION = "blank space";

  public static final KtCommand MEANING =
      new KtCommand() {

        public boolean isSpacer() {
          return true;
        }

        public KtCharCode charCode() {
          return CODE;
        }

        public void exec(KtToken src) {
          handler.handle(CODE, src);
        }

        public void exec( KtToken src, int prefixes) {
          KtToken tok = nextExpToken();
          meaningOf(tok).execute(tok, prefixes);
        }

        public void addOn(KtLog log) {
          log.add(DESCRIPTION).add(' ').add(' ');
        }

        public boolean sameAs(KtCommand cmd) {
          return getClass() == cmd.getClass();
        }

        public KtToken origin() {
          return TOKEN;
        }
      };

  public KtCommand meaning() {
    return MEANING;
  }

  static class KtDirtySpaceToken extends KtCharToken {

    public static final KtMaker MAKER =
        new KtMaker() {
          public KtToken make(KtCharCode code) {
            if (code.match(' ')) return TOKEN;
            return new KtDirtySpaceToken( code);
          }
        };

    public KtDirtySpaceToken(KtCharCode code) {
      super(code);
    }

    public boolean match(KtCharToken tok) {
      return tok instanceof KtDirtySpaceToken && tok.match( code);
    }

    public boolean sameCatAs(KtToken tok) {
      return TOKEN.sameCatAs( tok);
    }

    public KtMaker getMaker() {
      return MAKER;
    }

    public String toString() {
      return "<DirtySpace: " + code + '>';
    }

    private static KtCharHandler handler;

    public static void setHandler(KtCharHandler hnd) {
      handler = hnd;
    }

    public KtCommand meaning() {
      return new KtMeaning() {

        public void exec(KtToken src) {
          handler.handle(code, src);
        }

        public void exec( KtToken src, int prefixes) {
          KtToken tok = nextExpToken();
          meaningOf(tok).execute(tok, prefixes);
        }

        protected String description() {
          return DESCRIPTION;
        }
      };
    }
  }
}


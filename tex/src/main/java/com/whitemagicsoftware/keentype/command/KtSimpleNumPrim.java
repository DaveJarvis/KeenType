// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.SimpleNumPrim
// $Id: KtSimpleNumPrim.java,v 1.1.1.1 1999/07/23 07:27:06 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtNum;

public class KtSimpleNumPrim extends KtPrim implements KtNum.KtProvider {

  private KtNum value;

  public KtSimpleNumPrim(String name, KtNum val) {
    super(name);
    value = val;
  }

  public KtSimpleNumPrim(String name, int val) {
    this(name, KtNum.valueOf(val));
  }

  public KtSimpleNumPrim(String name) {
    this(name, KtNum.ZERO);
  }

  /* STRANGE
   * why does it talk about curren mode?
   */
  public void exec(KtToken src) {
    illegalCommand(this);
  }

  public KtNum get() {
    return value;
  }

  public void set(KtNum val) {
    value = val;
  }

  public final int intVal() {
    return get().intVal();
  }

  public final void set(int val) {
    set(KtNum.valueOf(val));
  }

  public boolean hasNumValue() {
    return true;
  }

  public KtNum getNumValue() {
    return get();
  }
}

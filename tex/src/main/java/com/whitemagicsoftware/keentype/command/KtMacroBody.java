// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;

public class KtMacroBody extends KtTokenList {

  public static final KtMacroBody NULL = null;

  public static final KtMacroBody EMPTY = new KtMacroBody();

  public static class KtBuffer extends KtTokenList.KtBuffer {

    public KtBuffer() {
      super();
    }

    public KtBuffer(int initCap) {
      super(initCap);
    }

    public KtBuffer(int initCap, int capIncrement) {
      super(initCap, capIncrement);
    }

    public KtTokenList.KtBuffer appendParam(int digit, KtCharCode matchCode) {
      return append(new KtParamToken(digit, matchCode));
    }

    public KtMacroBody toMacroBody() {
      if (super.length() == 0) return EMPTY;
      KtToken[] tokens = new KtToken[super.length()];
      // CCC super. required by jikes
      data.copyInto(tokens);
      return new KtMacroBody(tokens);
    }

    public KtTokenList toTokenList() {
      return toMacroBody();
    }
  }

  public KtMacroBody() {
    super();
  }

  public KtMacroBody(KtToken[] tokens) {
    super(tokens);
  }

  public KtMacroBody(KtToken[] tokens, int offset, int count) {
    super(tokens, offset, count);
  }

  public KtMacroBody(KtToken tok) {
    super(tok);
  }

  public KtMacroBody(String str) {
    super(str);
  }

  public KtMacroBody(KtName name) {
    super(name);
  }

  private static final String PREP = "->";
  private static final int PLEN = PREP.length();

  public void addOn(KtLog log) {
    log.add(PREP);
    super.addOn(log);
  }

  public void addOn(KtLog log, int maxCount) {
    if (maxCount > PLEN) {
      log.add(PREP);
      super.addOn(log, maxCount - PLEN);
    } else if (maxCount > 0) log.add(PREP.substring(0, maxCount));
  }

  public void addContext(KtLog left, KtLog right, int pos, int maxCount) {
    if (maxCount > PLEN) {
      left.add(PREP);
      super.addContext(left, right, pos, maxCount - PLEN);
    } else if (maxCount > 0) left.add(PREP.substring(0, maxCount));
  }

  /** KtToken for substituting a parameter of a macro. */
  static class KtParamToken extends KtToken {

    /** the number of the parameter 1-9 (coded as 0..8) */
    private final int number;

    /** the character code for symbolic printing */
    private final KtCharCode code;

    /**
     * Creates parameter token with given parameter number.
     *
     * @param number the parameter number
     * @param code the parameter character code
     */
    public KtParamToken(int number, KtCharCode code) {
      this.number = number;
      this.code = code;
    }

    /**
     * Tells that this |KtToken| is a macro parameter.
     *
     * @return |true|.
     */
    public boolean isMacroParameter() {
      return true;
    }

    /**
     * Tells the number of parameter.
     *
     * @return the macro parameter number.
     */
    public int macroParameterNumber() {
      return number;
    }

    public boolean match(KtToken tok) {
      return number == tok.macroParameterNumber();
    }

    /**
     * Prints its symbolic representation on the KtLog.
     *
     * @param log the KtLog to log on
     */
    public void addOn(KtLog log) {
      log.add(code);
      log.add( 0 <= number && number < 9 ? Character.forDigit( number + 1, 10) : '!');
    }
  }
}


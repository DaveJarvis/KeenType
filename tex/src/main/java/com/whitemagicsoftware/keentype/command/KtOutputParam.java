// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.OutputParam
// $Id: KtOutputParam.java,v 1.1.1.1 1999/05/31 11:18:50 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtOutputParam extends KtToksParam {

  /**
   * Creates a new KtOutputParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtOutputParam
   * @param val the value of the KtOutputParam
   */
  public KtOutputParam(String name, KtTokenList val) {
    super(name, val);
  }

  /**
   * Creates a new KtOutputParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtOutputParam
   */
  public KtOutputParam(String name) {
    super(name);
  }

  protected KtTokenList scanToksValue(KtToken src) {
    return scanToks(src, true);
  }
}

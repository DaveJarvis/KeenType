// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtName;

/**
 * Base class |KtToken| defines rather complicated interface because it is used
 * in many situations (thanks to TeX). It tries to provide reasonable default
 * behavior for all methods.
 * <p>
 * In many situations we are interested in |KtCommand| currently associated
 * as the meaning of a |KtToken|. For some kinds of (non-definable) |KtToken|s
 * the meaning is hardwired (most character tokens), for some it is
 * stored in table of equivalents --- therefore the parameter |eqtab|.
 * </p>
 * <p>
 * Note that the class |KtCommand| is used only as the output type of this
 * method in this class (no member is ever referenced) and therefore makes
 * unnecessary dependencies between classes.
 * </p>
 */
public abstract class KtToken implements Serializable, KtLoggable {

  /** Symbolic constant for non-existing |KtToken| */
  public static final KtToken NULL = null;

  /**
   * Tells whether the meaning of the |KtToken| can be redefined.
   *
   * @return |true| if the token is re-definable, |false| otherwise.
   */
  public boolean definable() {
    return false;
  }

  /**
   * Tells whether this |KtToken| was not created from the input but was
   * inserted during some error recovery.
   *
   * @return |true| if the token is from error recovery.
   */
  public boolean frozen() {
    return false;
  }

  /**
   * Gives object (potentially) associated in table of equivalents.
   *
   * @return the KtCommand to interpret this token.
   */
  public KtCommand meaning() {
    return KtCommand.NULL;
  }

  /**
   * Define given |KtCommand| to be equivalent in table of equivalents.
   *
   * @param cmd the object to interpret this token.
   * @param glob if |true| the equivalent is defined globaly.
   */
  public void define(KtCommand cmd, boolean glob) {
    throw new RuntimeException("Attempt to redefine meaning of a non-definable KtToken");
  }

  public abstract boolean match(KtToken tok);

  public boolean sameCatAs(KtToken tok) {
    return getClass() == tok.getClass();
  }

  public KtToken category() {
    return KtToken.NULL;
  }

  /*
   * The following few methods test if this |KtToken| matches in TeX terms a
   * character token of some category:
   * |matchLeftBrace|:	category 1
   * |matchRightBrace|:	category 2
   * |matchMacroParam|:	category 6
   * |matchSpace|:		category 10
   * |matchLetter|:		category 11 and given 7 bit ascii character
   * |matchOther|:		category 12 and given 7 bit ascii character
   */

  /**
   * Tests whether this |KtToken| matches left brace.
   *
   * @return |true| if it matches.
   */
  public boolean matchLeftBrace() {
    return false;
  }

  /**
   * Tests whether this |KtToken| matches right brace.
   *
   * @return |true| if it matches.
   */
  public boolean matchRightBrace() {
    return false;
  }

  /**
   * Tests whether this |KtToken| matches macro parameter indicator.
   *
   * @return |true| if it matches.
   */
  //    public boolean	matchMacroParam() { return false; }

  /**
   * Tests whether this |KtToken| matches spacer.
   *
   * @return |true| if it matches.
   */
  public boolean matchSpace() {
    return false;
  }

  /**
   * Tests whether this |KtToken| matches letter of particular character code.
   *
   * @param letter the character code to be matched.
   * @return |true| if it matches.
   */
  //    public boolean	matchLetter(char c) { return false; }

  /**
   * Tests whether this |KtToken| matches other char of particular char code.
   *
   * @param c the character code to be matched.
   * @return |true| if it matches.
   */
  public boolean matchOther(char c) {
    return false;
  }

  /**
   * Tests whether this |KtToken| matches normal (letter or other) char of particular char code.
   *
   * @param normal the character code to be matched.
   * @return |true| if it matches.
   */
  //    public boolean	matchNormal(char c) { return false; }

  /**
   * Gives the 7 bit ascii character code of letter character |KtToken|. The result is
   * |KtCharCode.NO_CHAR| if the |KtToken| is not a letter or if its character code is not a 7 bit
   * ascii.
   *
   * @return the 7 bit ascii code if the category is matched and the code is defined,
   *     |KtCharCode.NO_CHAR| otherwise.
   */
  public char letterChar() {
    return KtCharCode.NO_CHAR;
  }

  /**
   * Gives the 7 bit ascii character code of other character |KtToken|. The result is
   * |KtCharCode.NO_CHAR| if the |KtToken| is not an other or if its character code is not a 7 bit
   * ascii.
   *
   * @return the 7 bit ascii code if the category is matched and the code is defined,
   *     |KtCharCode.NO_CHAR| otherwise.
   */
  public char otherChar() {
    return KtCharCode.NO_CHAR;
  }

  /**
   * Gives the 7 bit ascii character code of normal (letter or other) character |KtToken|. The result
   * is |KtCharCode.NO_CHAR| if the |KtToken| is not normal (letter or other) or if its character code
   * is not a 7 bit ascii.
   *
   * @return the 7 bit ascii code if the category is matched and the code is defined,
   *     |KtCharCode.NO_CHAR| otherwise.
   */
  //    public char		normalChar() { return KtCharCode.NO_CHAR; }

  /**
   * Gives the |KtCharCode| of non active character |KtToken|. The result is |KtCharCode.NULL| if the
   * |KtToken| is not a character or is an active character,
   *
   * @return the |KtCharCode| if the category is matched and the code is defined, |KtCharCode.NULL|
   *     otherwise.
   */
  public KtCharCode nonActiveCharCode() {
    return KtCharCode.NULL;
  }

  /**
   * Gives the 7 bit ascii character code of non active character |KtToken|. The result is
   * |KtCharCode.NO_CHAR| if the |KtToken| is active character, is not a character or if its character
   * code is not a 7 bit ascii.
   *
   * @return the 7 bit ascii code if the category is matched and the code is defined,
   *     |KtCharCode.NO_CHAR| otherwise.
   */
  //    public final char	nonActiveChar() {
  //	KtCharCode	code = nonActiveCharCode();
  //	return (code != KtCharCode.NULL) ? code.toChar()
  //					  : KtCharCode.NO_CHAR;
  //    }

  public int numValue() {
    return -1;
  }

  public KtCharCode charCode() {
    return KtCharCode.NULL;
  }

  public KtToken makeCharToken(KtCharCode code) {
    return KtToken.NULL;
  }

  public KtName controlName() {
    return KtName.NULL;
  }

  public boolean isMacroParameter() {
    return false;
  } // XXX better name

  public int macroParameterNumber() {
    return -1;
  }

  public void addProperlyOn(KtLog log) {
    addOn(log);
  }

  private static KtCharCode.KtMaker maker;

  public static void setCharCodeMaker(KtCharCode.KtMaker mak) {
    maker = mak;
  }

  public static KtCharCode makeCharCode(char chr) {
    return maker.make(chr);
  }

  public static KtCharCode makeCharCode(int num) {
    return maker.make(num);
  }

  public static KtName makeName(String str) {
    KtCharCode[] codes = new KtCharCode[str.length()];
    for (int i = 0; i < codes.length; i++) codes[i] = maker.make(str.charAt(i));
    return new KtName(codes);
  }

  public interface KtCharHandler {
    void handle(KtCharCode code, KtToken src);
  }
}

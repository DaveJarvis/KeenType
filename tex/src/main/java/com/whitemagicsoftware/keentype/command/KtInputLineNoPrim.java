// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.InputLineNoPrim
// $Id: KtInputLineNoPrim.java,v 1.1.1.1 2000/02/07 16:58:32 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtNum;

public class KtInputLineNoPrim extends KtPrim implements KtNum.KtProvider {

  public KtInputLineNoPrim(String name) {
    super(name);
  }

  /* STRANGE
   * why does it talk about curren mode?
   */
  public void exec(KtToken src) {
    illegalCommand(this);
  }

  public boolean hasNumValue() {
    return true;
  }

  public KtNum getNumValue() {
    return KtNum.valueOf(currLineNumber());
  }
}

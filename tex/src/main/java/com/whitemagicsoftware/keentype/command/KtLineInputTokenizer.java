// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.LineInputTokenizer
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtInputLine;
import com.whitemagicsoftware.keentype.io.KtLineInput;

/**
 * The |KtLineInputTokenizer| reads internal character codes from a
 * |KtLineInput| and converts them into |KtToken|s.
 */
public class KtLineInputTokenizer extends KtSequenceTokenizer {

  public interface KtInputHandler {
    void closeInput();

    KtInputLine emptyLine();

    KtInputLine confirmLine( KtInputLine line );

    KtTokenizer makeTokenizer( KtInputLine line, String desc, boolean addEolc );
  }

  /**
   * The underlying line oriented input reader
   */
  private final KtLineInput mLineInput;

  /**
   * The parametrization object
   */
  private final KtInputHandler mInputHandler;

  private final KtFileName mFileName;

  private boolean forceEOF;
  private boolean used;
  private int lineNum;

  /**
   * Creates an |LineInputTokenizer| with given input and parametrization
   * object.
   *
   * @param lineInput    the underlying reader.
   * @param inputHandler the parametrization object.
   * @param fileName     the name of the file containing input to tokenize.
   */
  public KtLineInputTokenizer(
    final KtLineInput lineInput,
    final KtInputHandler inputHandler,
    final KtFileName fileName ) {
    assert lineInput != null;
    assert inputHandler != null;
    assert fileName != null;

    mLineInput = lineInput;
    mInputHandler = inputHandler;
    mFileName = fileName;
  }

  /**
   * Closes the underlying |KtLineInput|.
   */
  @Override
  public boolean close() {
    mLineInput.close();
    mInputHandler.closeInput();
    return true;
  }

  @Override
  public boolean endInput() {
    forceEOF = true;
    return true;
  }

  @Override
  public KtTokenizer nextTokenizer() {
    if( forceEOF ) { return KtTokenizer.NULL; }

    KtInputLine line = mLineInput.readLine();
    lineNum = mLineInput.getLineNumber();

    if( line == KtLineInput.EOF ) {
      // XXX maybe use parameter first of SequenceTokenizer
      if( used ) { return KtTokenizer.NULL; }
      line = mInputHandler.emptyLine();
      lineNum = 1;
    }

    used = true;
    line = mInputHandler.confirmLine( line );
    return mInputHandler.makeTokenizer( line, "l." + lineNum + ' ', true );
  }

  @Override
  public boolean enoughContext() {
    return true;
  }

  @Override
  public KtFilePos filePos() {
    return new KtFilePos( mFileName, lineNum );
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.GlueParam
// $Id: KtGlueParam.java,v 1.1.1.1 2000/06/27 10:37:35 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtGlue;

/** Setting glue parameter primitive. */
public class KtGlueParam extends KtAnyGlueParam implements KtGlue.KtProvider {

  /**
   * Creates a new KtGlueParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtGlueParam
   * @param val the value of the KtGlueParam
   */
  public KtGlueParam(String name, KtGlue val) {
    super(name, val);
  }

  /**
   * Creates a new KtGlueParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtGlueParam
   */
  public KtGlueParam(String name) {
    super(name);
  }

  protected void scanValue(KtToken src, boolean glob) {
    set(scanGlue(), glob);
    // System.err.println("= Assignment \\" + getName() + " = " + get());
  }

  protected void perform(int operation, boolean glob) {
    set(performFor(get(), operation, false), glob);
  }

  public String getUnit() {
    return "pt";
  }

  public boolean hasGlueValue() {
    return true;
  }

  public KtGlue getGlueValue() {
    return get();
  }
}

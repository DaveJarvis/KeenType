// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.MeaningPrim
// $Id: KtMeaningPrim.java,v 1.1.1.1 1999/05/31 11:18:49 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.io.KtLog;

/* TeXtp[470] */
public class KtMeaningPrim extends KtExpandablePrim {

  public KtMeaningPrim(String name) {
    super(name);
  }

  public void expand(KtToken src) {
    KtBoolPar exp = new KtBoolPar();
    KtToken tok = nextUncheckedRawToken(exp);
    KtTokenList.KtBuffer buf = new KtTokenList.KtBuffer();
    KtLog log = makeLog(new KtTokenListOutput(buf));
    meaningOf(tok, exp.get()).addExpandable(log, true);
    insertList(buf.toTokenList());
  }
}

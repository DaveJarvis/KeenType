// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.AfterAssignment
// $Id: KtAfterAssignment.java,v 1.1.1.1 1999/06/06 13:32:15 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtAfterAssignment extends KtPrim {

  private KtToken after = KtToken.NULL;

  public KtAfterAssignment(String name) {
    super(name);
  }

  public void exec(KtToken src) {
    after = nextRawToken();
  }

  public void apply() {
    if (after != KtToken.NULL) {
      backToken(after);
      after = KtToken.NULL;
    }
  }
}

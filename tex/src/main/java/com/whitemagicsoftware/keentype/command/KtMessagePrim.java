// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.MessagePrim
// $Id: KtMessagePrim.java,v 1.1.1.1 1999/07/28 08:07:12 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtMessagePrim extends KtPrim {

  public KtMessagePrim(String name) {
    super(name);
  }

  public void exec(KtToken src) {
    KtTokenList list = scanTokenList(src, true);
    int len = normLog.voidCounter().add(list).getCount();
    normLog.sepRoom(len + 2).add(list).flush();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.IfPrim
// $Id: KtIfPrim.java,v 1.1.1.1 1999/05/31 11:18:47 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtIfPrim extends KtAnyIfPrim {

  public KtIfPrim(String name) {
    super(name);
  }

  protected final boolean holds() {
    KtCharCode first = getCode();
    KtCharCode second = getCode();
    return first != KtCharCode.NULL
        ? second != KtCharCode.NULL && first.match( second)
        : second == KtCharCode.NULL;
  }

  private KtCharCode getCode() {
    KtToken tok = nextExpToken();
    KtCommand cmd = meaningOf(tok);
    return cmd.expandable() ? tok.charCode() : cmd.charCode();
  }
}

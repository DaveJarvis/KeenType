// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.BaseToksChecker
// $Id: KtBaseToksChecker.java,v 1.1.1.1 1999/05/31 11:18:43 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public abstract class KtBaseToksChecker implements KtInpTokChecker {

  protected final String tokError;
  protected final String eofError;
  protected final KtLoggable source;

  public KtBaseToksChecker(String tokErr, String eofErr, KtLoggable src) {
    tokError = tokErr;
    eofError = eofErr;
    source = src;
  }

  public KtToken checkToken(KtToken tok, KtBoolPar canExpand) {
    if (KtCommand.meaningOf(tok, canExpand.get()).isOuter()) {
      handleOuterToken(tok);
      reportError(tokError);
      canExpand.set(true);
      return KtSpaceToken.TOKEN;
    }
    return tok;
  }

  public void checkEndOfFile() {
    reportError(eofError);
  }

  protected void reportError(String ident) {
    tryToFix();
    reportRunAway();
    KtLoggable[] params = {source};
    KtCommand.nonDelError(ident, params);
  }

  protected void handleOuterToken(KtToken tok) {
    KtCommand.backTokenWithoutCleaning(tok);
  }

  protected abstract void tryToFix();

  protected abstract void reportRunAway();
}

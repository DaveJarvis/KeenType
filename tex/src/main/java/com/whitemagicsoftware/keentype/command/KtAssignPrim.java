// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.AssignPrim
// $Id: KtAssignPrim.java,v 1.1.1.1 2001/02/01 16:01:45 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

/** Abstract ancestor of each assign primitive but \def. */
public abstract class KtAssignPrim extends KtPrefixPrim {

  /**
   * Creates a new KtAssignPrim with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtAssignPrim
   */
  protected KtAssignPrim(String name) {
    super(name);
  }

  /**
   * Performs itself in the process of interpretation of the macro language after sequence of prefix
   * commands.
   *
   * @param src source token for diagnostic output.
   * @param prefixes accumulated code of prefixes.
   */
  public final void exec(KtToken src, int prefixes) {
    beforeAssignment(this, prefixes);
    assign(src, globalAssignment(prefixes));
    afterAssignment();
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  protected abstract void assign(KtToken src, boolean glob);

  /* TeXtp[1226] */
  public static KtTokenList scanToks(KtToken src, boolean wrapup) {
    KtTokenList.KtBuffer buf = new KtTokenList.KtBuffer(30);
    KtToken tok = nextNonRelax();
    KtCommand cmd = meaningOf(tok);
    if (!cmd.isLeftBrace()) {
      if (cmd.hasToksValue()) return cmd.getToksValue();
      backToken(tok);
      error("MissingLeftBrace");
    }
    /* STRANGE
     * the descriptor ("text") below is included in the error message
     * ("OuterInToks") too. The same holds for "definition" and
     * "preamble", the "argument"/"use" is the only exception.
     */
    scanBalanced(buf, new KtScanToksChecker("OuterInToks", "EOFinToks", "text", buf, src));
    if (wrapup && buf.length() != 0) {
      buf.insertTokenAt(KtLeftBraceToken.TOKEN, 0);
      buf.append(KtRightBraceToken.TOKEN);
    }
    return buf.toTokenList();
  }

  private static void scanBalanced(KtTokenList.KtBuffer buf, KtInpTokChecker tchk) {
    KtInpTokChecker savedChk = setTokenChecker(tchk);
    for (int balance = 1; ; ) {
      KtToken tok = nextRawToken();
      if (tok.matchLeftBrace()) ++balance;
      else if (tok.matchRightBrace() && --balance == 0) break;
      buf.append(tok);
    }
    setTokenChecker(savedChk);
  }

  public class KtNumKind extends KtPrefixPrim.KtNumKind {

    protected void addDescOn(int idx, KtLog log) {
      log.addEsc(getEqDesc()).add(idx);
    }

    protected void addValueOn(int idx, KtLog log) {
      addEqValueOn(idx, log);
    }
  }

  public String getEqDesc() {
    return getName();
  }

  public void addEqValueOn(int idx, KtLog log) {}
}

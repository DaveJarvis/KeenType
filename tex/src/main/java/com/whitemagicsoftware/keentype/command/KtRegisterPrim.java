// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.RegisterPrim
// $Id: KtRegisterPrim.java,v 1.1.1.1 2000/01/28 04:58:53 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

/** */
public abstract class KtRegisterPrim extends KtAssignPrim {

  /** Kind of equivalence in |KtEqTable| */
  protected final KtNumKind tabKind = new KtNumKind();

  /**
   * Creates a new KtRegisterPrim with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtRegisterPrim
   */
  protected KtRegisterPrim(String name) {
    super(name);
  }

  public abstract void addEqValueOn(int idx, KtLog log);
}

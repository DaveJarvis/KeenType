// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.MagParam
// $Id: KtMagParam.java,v 1.1.1.1 1999/07/12 11:04:07 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtNum;

/** Setting number parameter primitive. */
public class KtMagParam extends KtNumParam {

  /**
   * Creates a new KtMagParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtMagParam
   * @param val the value of the KtMagParam
   */
  public KtMagParam(String name, KtNum val) {
    super(name, val);
  }

  public KtMagParam(String name, int val) {
    super(name, val);
  }

  /**
   * Creates a new KtMagParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtMagParam
   */
  public KtMagParam(String name) {
    super(name);
  }

  private KtNum magSet = KtNum.NULL;

  public KtNum get() {
    KtNum mag = super.get();
    if (magSet != KtNum.NULL && !magSet.equals(mag)) {
      error("IncompatMag", str(mag), str(magSet));
      mag = magSet;
      set(mag, true);
    }
    if (!mag.moreThan(0) || mag.moreThan(32768)) {
      error("IllegalMag", str(mag));
      mag = KtNum.valueOf(1000);
      set(mag, true);
    }
    magSet = mag;
    return mag;
  }
}

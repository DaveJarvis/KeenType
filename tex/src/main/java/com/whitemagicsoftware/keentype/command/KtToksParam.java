// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ToksParam
// $Id: KtToksParam.java,v 1.1.1.1 2000/01/05 00:41:15 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

/** Setting tokens parameter primitive. */
public class KtToksParam extends KtParamPrim implements KtTokenList.KtProvider, KtTokenList.KtInserter {

  private KtTokenList value;

  /**
   * Creates a new KtToksParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtToksParam
   * @param val the value of the KtToksParam
   */
  public KtToksParam(String name, KtTokenList val) {
    super(name);
    value = val;
  }

  /**
   * Creates a new KtToksParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtToksParam
   */
  public KtToksParam(String name) {
    this(name, KtTokenList.EMPTY);
  }

  public final Object getEqValue() {
    return value;
  }

  public final void setEqValue(Object val) {
    value = (KtTokenList) val;
  }

  public final void addEqValueOn(KtLog log) {
    value.addOn(log, getConfig().getIntParam(INTP_MAX_TLRES_TRACE));
  }

  public final KtTokenList get() {
    return value;
  }

  public void set(KtTokenList val, boolean glob) {
    beforeSetting(glob);
    value = val;
  }

  protected void scanValue(KtToken src, boolean glob) {
    set(scanToksValue(src), glob);
  }

  protected KtTokenList scanToksValue(KtToken src) {
    return scanToks(src, false);
  }

  public boolean hasToksValue() {
    return true;
  }

  public KtTokenList getToksValue() {
    return value;
  }

  public void insertToks() {
    tracedPushList(value, getName());
  }

  public boolean isEmpty() {
    return value.isEmpty();
  }
}

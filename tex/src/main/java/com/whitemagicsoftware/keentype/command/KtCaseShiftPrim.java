// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.CaseShiftPrim
// $Id: KtCaseShiftPrim.java,v 1.1.1.1 1999/06/06 13:32:16 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;

public abstract class KtCaseShiftPrim extends KtPrim {

  protected KtCaseShiftPrim(String name) {
    super(name);
  }

  public void exec(KtToken src) {
    KtTokenList list = scanTokenList(src, false);
    KtToken[] dest = new KtToken[list.length()];
    for (int i = 0; i < list.length(); i++) {
      KtToken tok = list.tokenAt(i);
      KtCharCode code = tok.charCode();
      if (code != KtCharCode.NULL) {
        KtToken ntok = tok.makeCharToken(shiftCase(code));
        if (ntok != KtToken.NULL) tok = ntok;
      }
      dest[i] = tok;
    }
    backList(new KtTokenList(dest));
  }

  protected abstract KtCharCode shiftCase(KtCharCode code);
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.DimenPrim
// $Id: KtDimenPrim.java,v 1.1.1.1 1999/06/24 10:44:01 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtLog;

/** Setting dimension register primitive. */
public class KtDimenPrim extends KtRegisterPrim implements KtDimen.KtProvider {

  /**
   * Creates a new |KtDimenPrim| with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the |KtDimenPrim|
   */
  public KtDimenPrim(String name) {
    super(name);
  }

  public final void set(int idx, KtDimen val, boolean glob) {
    if (glob) getEqt().gput(tabKind, idx, val);
    else getEqt().put(tabKind, idx, val);
  }

  public final KtDimen get(int idx) {
    KtDimen val = (KtDimen) getEqt().get(tabKind, idx);
    return val != KtDimen.NULL ? val : KtDimen.ZERO;
  }

  public void addEqValueOn(int idx, KtLog log) {
    log.add(get(idx).toString("pt"));
  }

  public void perform(int idx, int operation, boolean glob) {
    set(idx, performFor(get(idx), operation), glob);
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  protected final void assign(KtToken src, boolean glob) {
    int idx = scanRegisterCode();
    skipOptEquals();
    set(idx, scanDimen(), glob);
  }

  public final void perform(int operation, boolean glob, KtCommand after) {
    perform(scanRegisterCode(), operation, glob);
  }

  public boolean hasDimenValue() {
    return true;
  }

  public KtDimen getDimenValue() {
    return get(scanRegisterCode());
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ScanToksChecker
// $Id: KtScanToksChecker.java,v 1.1.1.1 1999/05/31 11:18:51 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtMaxLoggable;

public class KtScanToksChecker extends KtBaseToksChecker {

  protected final String desc;
  protected final KtMaxLoggable list;
  protected final KtToken insTok;

  public KtScanToksChecker(
      String tokErr, String eofErr, String desc, KtMaxLoggable list, KtLoggable src, KtToken ins) {
    super(tokErr, eofErr, src);
    this.desc = desc;
    this.list = list;
    insTok = ins;
  }

  public KtScanToksChecker(
      String tokErr, String eofErr, String desc, KtMaxLoggable list, KtLoggable src) {
    super(tokErr, eofErr, src);
    this.desc = desc;
    this.list = list;
    insTok = KtRightBraceToken.TOKEN;
  }

  protected void tryToFix() {
    KtCommand.insertTokenWithoutCleaning(insTok);
  }

  protected void reportRunAway() {
    KtCommand.runAway(desc, list);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.MacroParamToken
// $Id: KtMacroParamToken.java,v 1.1.1.1 2000/02/16 11:01:01 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtMacroParamToken extends KtCharToken {

  public static final KtCharCode CODE = makeCharCode('#');
  public static final KtMacroParamToken TOKEN = new KtMacroParamToken(CODE);
  public static final KtMaker MAKER =
      new KtMaker() {
        public KtToken make(KtCharCode code) {
          return new KtMacroParamToken(code);
        }
      };

  public KtMacroParamToken(KtCharCode code) {
    super(code);
  }

  public boolean matchMacroParam() {
    return true;
  }

  public void addProperlyOn(KtLog log) {
    log.add(code).add(code);
  }

  public boolean match(KtCharToken tok) {
    return tok instanceof KtMacroParamToken && tok.match( code);
  }

  public KtMaker getMaker() {
    return MAKER;
  }

  public String toString() {
    return "<MacroParam: " + code + '>';
  }

  public KtCommand meaning() {
    return new KtMeaning() {

      public boolean isMacroParam() {
        return true;
      }

      /* STRANGE
       * why does it talk about curren mode?
       */
      public void exec(KtToken src) {
        illegalCommand(this);
      }

      protected String description() {
        return "macro parameter character";
      }
    };
  }
}

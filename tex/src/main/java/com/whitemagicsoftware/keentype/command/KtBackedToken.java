// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.BackedToken
// $Id: KtBackedToken.java,v 1.1.1.1 2000/08/04 14:46:32 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.io.KtLog;

/** |KtTokenizer| for a single |KtToken|. */
/* See TeXtp[325]. */
public class KtBackedToken extends KtTokenizer {

  /** The |KtToken| */
  private final KtToken token;

  private boolean readed = false;

  private final boolean expOK;

  /**
   * Creates |KtTokenizer| for a single |KtToken|.
   *
   * @param tok the single |KtToken|
   * @param exp tells whether the backed |KtToken| can be expanded (e.g. was not preceded by
   *     \noexpand).
   */
  public KtBackedToken(KtToken tok, boolean exp) {
    token = tok;
    expOK = exp;
  }

  /**
   * Gives the single |KtToken| on the first call.
   *
   * @param canExpand boolean output parameter querying whether the acquired |KtToken| can be expanded
   *     (e.g. was not preceded by \noexpand).
   * @return the single |KtToken| or |KtToken.NULL| on next calls.
   */
  public KtToken nextToken(KtBoolPar canExpand) {
    canExpand.set(expOK);
    if (readed) return KtToken.NULL;
    readed = true;
    return token;
  }

  public boolean finishedList() {
    return readed;
  }

  public int show(KtContextDisplay disp, boolean force, int lines) {
    String desc;
    KtLog where;
    if (readed) {
      if (!force) return 0;
      desc = "<recently read> ";
      where = disp.left();
    } else {
      desc = "<to be read again> ";
      where = disp.right();
    }
    disp.normal().startLine().add(desc);
    if (!expOK) where.addEsc("notexpanded: ");
    token.addProperlyOn(where);
    disp.show();
    return 1;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.RightBrace
// $Id: KtRightBrace.java,v 1.1.1.1 2000/06/22 19:09:46 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

public class KtRightBrace extends KtGroupCommand {

  public void exec(KtGroup grp, KtToken src) {
    popLevel();
  }

  public void addOn(KtLog log) {
    log.add("internal end group");
  }

  public static final KtClosing EXTRA =
      new KtClosing() {
        /* TeXtp[1069] */
        public void exec(KtGroup grp, KtToken src) {
          error("ExtraOrForgotten", grp.expectedToken());
          adjustBraceNesting(1);
        }
      };
}

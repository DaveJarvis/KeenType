// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.SkipDefPrim
// $Id: KtSkipDefPrim.java,v 1.1.1.1 1999/06/24 10:56:04 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtGlue;

public class KtSkipDefPrim extends KtShorthandDefPrim {

  private final KtSkipPrim reg;

  public KtSkipDefPrim(String name, KtSkipPrim reg) {
    super(name);
    this.reg = reg;
  }

  protected final KtCommand makeShorthand(int idx) {
    return new KtShorthand(idx);
  }

  public class KtShorthand extends KtShorthandDefPrim.KtShorthand implements KtGlue.KtProvider {

    public KtShorthand(int idx) {
      super(idx);
    }

    protected KtRegisterPrim getReg() {
      return reg;
    }

    /**
     * Performs the assignment.
     *
     * @param src source token for diagnostic output.
     * @param glob indication that the assignment is global.
     */
    protected final void assign(KtToken src, boolean glob) {
      skipOptEquals();
      reg.set(index, scanGlue(), glob);
    }

    public final void perform(int operation, boolean glob, KtCommand after) {
      reg.perform(index, operation, glob);
    }

    public boolean hasGlueValue() {
      return true;
    }

    public KtGlue getGlueValue() {
      return reg.get(index);
    }
  }
}

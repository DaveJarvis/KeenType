// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtMaxLoggable;

/** KtPrim \def which defines a new macro and stores it to the table of equivalents. */
public class KtDefPrim extends KtPrefixPrim {

  /** Should the body be expanded during scanning? */
  private final boolean xpand;

  /** Built in prefixes (LONG, OUTER, GLOBAL) */
  private final int prefix;

  /** Creates and registers the |\def| primitive */
  public KtDefPrim(String name, boolean xpand, int prefix) {
    super(name);
    this.xpand = xpand;
    this.prefix = prefix;
  }

  /** Scans a macro definition and stores the new macro to table of equivalents */
  public void exec(KtToken src, int prefixes) {
    KtToken tok = definableToken();
    KtMacroDefining def = new KtMacroDefining(tok);
    scanDef(tok, def);
    prefixes |= prefix;
    KtMacro mac = def.toMacro(prefixes & (LONG | OUTER));
    tok.define(mac, globalAssignment(prefixes));
    afterAssignment();
  }

  /** Scans a definition of a macro. */
  public void scanDef(KtToken defMac, KtMacroDefining def) {
    KtToken hashBrace = KtToken.NULL;
    boolean valid = true;
    KtInpTokChecker savedChk = setTokenChecker(def);
    for (; ; ) {
      KtToken tok = nextRawToken();
      KtCommand cmd = meaningOf(tok);
      if (cmd.isMacroParam()) {
        def.matchCode = cmd.charCode();
        tok = nextRawToken();
        if (meaningOf(tok).isLeftBrace()) {
          hashBrace = tok;
          def.addToMask(tok);
          break;
        }
        if (def.paramCnt() < 9) {
          if (!tok.matchOther(Character.forDigit(def.paramCnt() + 1, 10))) {
            backToken(tok);
            error("NonConseqParams");
          }
          def.addParam();
          continue;
        } else error("TooManyParams");
      } else if (tok.matchLeftBrace()) break;
      else if (tok.matchRightBrace()) {
        adjustBraceNesting(1);
        valid = false;
        error("MissingLeftDefBrace");
        break;
      }
      def.addToMask(tok);
    }
    def.bodyBuf = new KtMacroBody.KtBuffer(30);
    if (valid) scanBody(defMac, def);
    if (hashBrace != KtToken.NULL) def.bodyBuf.append(hashBrace);
    setTokenChecker(savedChk);
  }

  /* STRANGE
   * Note that the character code of |KtParamToken| created in the following
   * method is not its original code but the code of the last match in the
   * parameter mask.  This simulates the TeX behavior where the |out_param|
   * token uses its character code for parameter number.
   */

  /** Scans a macro body with possible parameters. */
  private void scanBody(KtToken defMac, KtMacroDefining def) {
    KtMacroBody.KtBuffer buf = def.bodyBuf;
    for (int balance = 1; ; ) {
      KtToken tok = nextScannedToken(xpand, buf);
      if (tok != KtToken.NULL) {
        if (meaningOf(tok).isMacroParam()) {
          KtToken prevTok = tok;
          tok = xpand ? nextExpToken() : nextRawToken();
          if (!meaningOf(tok).isMacroParam()) {
            char dig = tok.otherChar();
            int digit;
            if (dig != KtCharCode.NO_CHAR && dig >= '1' && (digit = dig - '1') < def.paramCnt()) {
              buf.appendParam(digit, def.matchCode);
              continue;
            } else {
              backToken(tok);
              tok = prevTok;
              error("IllegalParamNum", defMac);
            }
          }
        } else if (tok.matchLeftBrace()) ++balance;
        else if (tok.matchRightBrace() && --balance == 0) break;
        buf.append(tok);
      }
    }
  }

  static class KtMacroDefining extends KtBaseToksChecker implements KtMaxLoggable {

    public KtCharCode matchCode = KtMacroParamToken.CODE;
    public KtMacroBody.KtBuffer bodyBuf = null;

    private final KtCharCode[] matchCodes = new KtCharCode[9];
    private final KtTokenList[] paramMask = new KtTokenList[10];
    private KtTokenList.KtBuffer paramBuf = new KtTokenList.KtBuffer(10, 10);
    private int count = 0;

    public KtMacroDefining(KtToken source) {
      super("OuterInDef", "EOFinDef", source);
    }

    public int paramCnt() {
      return count;
    }

    public void addToMask(KtToken tok) {
      paramBuf.append(tok);
    }

    public void addParam() {
      matchCodes[count] = matchCode;
      paramMask[count++] = paramBuf.toTokenList();
      paramBuf = new KtTokenList.KtBuffer(10, 10);
    }

    public KtMacro toMacro(int prefixes) {
      int i = count;
      if (i > 0 || paramBuf.length() > 0) paramMask[i++] = paramBuf.toTokenList();
      KtTokenList[] mask = new KtTokenList[i];
      System.arraycopy(paramMask, 0, mask, 0, i);
      KtCharCode[] codes = null;
      if (count > 0) {
        codes = new KtCharCode[count];
        System.arraycopy(matchCodes, 0, codes, 0, count);
      }
      KtTokenList body = bodyBuf != null ? bodyBuf.toTokenList() : KtTokenList.EMPTY;
      return new KtMacro(mask, codes, body, prefixes);
    }

    protected void tryToFix() {
      insertTokenWithoutCleaning( KtRightBraceToken.TOKEN);
    }

    protected void reportRunAway() {
      runAway( "definition", this);
    }

    public void addOn(KtLog log, int maxCount) {
      toMacro(0).addMaxOn(log, maxCount);
    }
  }
}


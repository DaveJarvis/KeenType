// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.OtherToken
// $Id: KtOtherToken.java,v 1.1.1.1 2000/02/16 11:01:06 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtOtherToken extends KtNormalCharToken {

  public static final KtMaker MAKER =
      new KtMaker() {
        public KtToken make(KtCharCode code) {
          return new KtOtherToken(code);
        }
      };

  public KtOtherToken(KtCharCode code) {
    super(code);
  }

  public boolean matchOther(char c) {
    return code.match(c);
  }

  /**
   * Gives the 7 bit ascii character code of this other character |KtToken|. The result is
   * |KtCharCode.NO_CHAR| if its character code is not a 7 bit ascii.
   *
   * @return the 7 bit ascii code if the code is defined, |KtCharCode.NO_CHAR| otherwise.
   */
  public char otherChar() {
    return code.toChar();
  }

  public boolean match(KtCharToken tok) {
    return tok instanceof KtOtherToken && tok.match( code);
  }

  public KtMaker getMaker() {
    return MAKER;
  }

  public String toString() {
    return "<Other: " + code + '>';
  }

  public KtCommand meaning() {
    return new KtMeaning() {
      protected String description() {
        return "the character";
      }
    };
  }
}

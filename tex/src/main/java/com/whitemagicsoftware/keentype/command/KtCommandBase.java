// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.base.KtBytePar;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtEqTable;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtIntPar;
import com.whitemagicsoftware.keentype.base.KtLevelEqTable;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.*;
import com.whitemagicsoftware.keentype.io.*;

/**
 * The language interpreter
 */
public abstract class KtCommandBase {

  /** The |KtEqTable| of the macro language interpreter */
  private static KtLevelEqTable eqTable;

  /**
   * Assigns a table of equivalents for the macro language interpreter.
   *
   * @param eqtab the table of equivalents.
   */
  public static void setEqt(KtLevelEqTable eqtab) {
    eqTable = eqtab;
  }

  /**
   * Gives the table of equivalents of the macro language interpreter.
   *
   * @return the current table of equivalents.
   */
  public static KtEqTable getEqt() {
    return eqTable;
  }

  public static KtCommand meaningOf(final KtToken tok) {
    final KtCommand cmd;

    return tok == KtToken.NULL || (cmd = tok.meaning()) == KtCommand.NULL
      ? KtUndefined.getUndefined()
      : cmd;
  }

  public static KtCommand meaningOf(KtToken tok, boolean expOK) {
    return meaningOf(tok).meaning(expOK);
  }

  /** The input tokenizer stack of the macro language interpreter */
  private static KtTokenizerStack tokStack;

  /**
   * Assigns a tokenizer stack for the macro language interpreter.
   *
   * @param tokstack the input tokenizer stack.
   */
  public static void setTokStack(KtTokenizerStack tokstack) {
    tokStack = tokstack;
  }

  /**
   * Gives the tokenizer stack of the macro language interpreter.
   *
   * @return the current input tokenizer stack.
   */
  public static KtTokenizerStack getTokStack() {
    return tokStack;
  }

  public static KtInpTokChecker setTokenChecker(KtInpTokChecker chk) {
    return tokStack.setChecker(chk);
  }

  public static int currLineNumber() {
    return tokStack.lineNumber();
  }

  public static int braceNestingDifference( final KtToken tok ) {
    assert tok != null;

    return tok.matchLeftBrace()
      ? 1
      : tok.matchRightBrace()
      ? -1
      : 0;
  }

  /**
   * Gives the next unexpanded input |KtToken| of macro language interpreter.
   *
   * @param canExpand boolean output parameter querying whether the acquired |KtToken| can be expanded
   *     (e.g. was not preceded by \noexpand).
   * @return next |KtToken|.
   */
  public static KtToken nextRawToken(KtBoolPar canExpand) {
    KtToken tok;

    do {
      tok = tokStack.nextToken( canExpand );
      final KtCommand cmd = meaningOf( tok );

      if( cmd.explosive() ) {
        cmd.detonate( tok );
      }
      else {
        break;
      }
    }
    while( tok != KtToken.NULL );

    if( tok != KtToken.NULL ) {
      adjustBraceNesting( braceNestingDifference( tok ) );
    }

    return tok;
  }

  /**
   * Gives the next unexpanded input |KtToken| of macro language interpreter.
   *
   * @return next |KtToken|.
   */
  public static KtToken nextRawToken() {
    return nextRawToken(KtBoolPar.NULL);
  }
  // XXX make dummy here instead of in KtTokenizerStack

  public static KtToken nextUncheckedRawToken(KtBoolPar canExpand) {
    KtInpTokChecker savedChk = setTokenChecker(KtInpTokChecker.NULL);
    KtToken tok = nextRawToken(canExpand);
    setTokenChecker(savedChk);
    return tok;
  }

  public static KtToken nextUncheckedRawToken() {
    return nextUncheckedRawToken(KtBoolPar.NULL);
  }

  /**
   * Pushes a |KtToken| back to be read again by macro language interpreter.
   *
   * @param tok the |KtToken| to be pushed back.
   */
  public static void backToken(KtToken tok) {
    tokStack.cleanFinishedLists();
    backTokenWithoutCleaning(tok);
  }

  /* STRANGE why we don't simply always clean the stack? */
  public static void backTokenWithoutCleaning( final KtToken tok ) {
    tokStack.backUp( tok, true );
    adjustBraceNesting( -braceNestingDifference( tok ) );
  }

  public static void backList(KtTokenList list) {
    tokStack.backUp(list);
  }

  public static void pushToken(KtToken tok, String desc) {
    tokStack.push(tok, '<' + desc + "> ");
  }

  public static void pushList(KtTokenList list, String desc) {
    tokStack.push(list, '<' + desc + "> ");
  }

  public static void insertToken(KtToken tok) {
    tokStack.cleanFinishedLists();
    tokStack.push(tok, "<inserted text> ");
  }

  public static void insertTokenWithoutCleaning(KtToken tok) {
    tokStack.push(tok, "<inserted text> ");
  }

  public static void insertList(KtTokenList list) {
    tokStack.push(list, "<inserted text> ");
  }

  /**
   * Gives the next expanded |KtToken| for macro language interpreter.
   *
   * @return next |KtToken|
   */
  /* See TeXtp[370]. */
  public static KtToken nextExpToken() {
    KtToken tok;

    do {
      tok = onlyRawToken();
    }
    while( tok == KtToken.NULL );

    return tok;
  }

  public static KtToken onlyRawToken() {
    final KtBoolPar exp = new KtBoolPar();
    final KtToken tok = nextRawToken(exp);
    final KtCommand cmd = meaningOf(tok);

    if( cmd.expandable() && exp.get() ) {
      cmd.doExpansion( tok );
      return KtToken.NULL;
    }

    return cmd instanceof KtUndefined
      ? new KtEofToken()
      : tok;
  }

  public static KtToken nextExpToken(KtTokenList.KtBuffer buf) {
    KtBoolPar exp = new KtBoolPar();
    KtToken tok = nextRawToken(exp);
    KtCommand cmd = meaningOf(tok);
    if (exp.get()) {
      if (cmd.appendToks(buf)) return KtToken.NULL;
      if (cmd.expandable()) {
        cmd.doExpansion(tok);
        return KtToken.NULL;
      }
    }
    return tok;
  }

  /* STRANGE
   * the track of brace nesting is kept only for alignment which
   * might be otherwise completely independent
   */
  private static KtBraceNesting braceNesting = count -> {};

  public static KtBraceNesting setBraceNesting(KtBraceNesting nesting) {
    KtBraceNesting old = braceNesting;
    braceNesting = nesting;
    return old;
  }

  public static void adjustBraceNesting(int count) {
    braceNesting.adjust(count);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  private static int MAX_INT_PARAM = 0;
  private static int MAX_DIM_PARAM = 0;
  private static int MAX_GLUE_PARAM = 0;
  private static int MAX_TOKS_PARAM = 0;
  private static final int MAX_NODE_PARAM = 0;
  private static int MAX_BOOL_PARAM = 0;

  public static int maxIntParam() {
    return MAX_INT_PARAM;
  }

  public static int maxDimParam() {
    return MAX_DIM_PARAM;
  }

  public static int maxGlueParam() {
    return MAX_GLUE_PARAM;
  }

  public static int maxToksParam() {
    return MAX_TOKS_PARAM;
  }

  public static int maxBoolParam() {
    return MAX_BOOL_PARAM;
  }

  protected static int newIntParam() {
    return MAX_INT_PARAM++;
  }

  protected static int newDimParam() {
    return MAX_DIM_PARAM++;
  }

  protected static int newGlueParam() {
    return MAX_GLUE_PARAM++;
  }

  protected static int newToksParam() {
    return MAX_TOKS_PARAM++;
  }

  protected static int newBoolParam() {
    return MAX_BOOL_PARAM++;
  }

  public interface KtConfig {
    int getIntParam(int param);

    KtDimen getDimParam(int param);

    KtGlue getGlueParam(int param);

    KtTokenList getToksParam(int param);

    boolean getBoolParam(int param);

    String getGlueName(int param);

    KtTokenList.KtInserter getToksInserter(int param);

    boolean enableInput(boolean val);

    boolean enableAfterAssignment(boolean val);

    void afterAssignment();

    boolean formatLoaded();

    KtToken frozenFi();
  }

  private static KtConfig config;

  public static void setConfig(KtConfig conf) {
    config = conf;
  }

  public static KtConfig getConfig() {
    return config;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public interface KtIOHandler {

    KtName getJobName();

    void ensureOpenLog();

    void openInput(KtFileName name);

    KtReadInput openRead(KtFileName name, int num);

    KtReadInput defaultRead(int num);

    KtLog makeLog(KtLineOutput out);

    KtLog makeStringLog();

    KtLog openWrite(KtFileName name, int num);

    KtFileName makeFileName();

    void resetErrorCount();

    void error( String ident, KtLoggable[] params, boolean delAllowed);

    void fatalError(String ident);

    void errMessage(KtTokenList message);

    void logMode();

    void completeShow();

    void illegalCommand(KtCommand cmd);
  }

  /** The input handler of the macro language interpreter */
  private static KtIOHandler ioHandler;

  /**
   * Assigns an input handler for the macro language interpreter.
   *
   * @param ioHand the input handler.
   */
  public static void setIOHandler(KtIOHandler ioHand) {
    ioHandler = ioHand;
  }

  public static KtIOHandler getIOHandler() {
    return ioHandler;
  }

  public static KtLog makeLog(KtLineOutput out) {
    return ioHandler.makeLog(out);
  }

  public static void ensureOpenLog() {
    ioHandler.ensureOpenLog();
  }

  public static void startInput() {
    ioHandler.openInput(scanFileName());
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  private static KtLineOutput terminal = KtLineOutput.NULL;
  private static KtLineOutput logFile = KtLineOutput.NULL;
  private static KtLineOutput termAndLog = KtLineOutput.NULL;

  public static KtLog termLog = KtNullLog.LOG;
  public static KtLog fileLog = KtNullLog.LOG;
  public static KtLog normLog = KtNullLog.LOG;
  public static KtLog diagLog = KtNullLog.LOG;

  private static boolean termEnable = false;
  private static boolean diagOnTerm = false;

  public static boolean isTermLogActive() {
    return termLog != KtNullLog.LOG;
  } // SSS

  public static boolean isFileLogActive() {
    return fileLog != KtNullLog.LOG;
  } // SSS

  public static boolean termDiagActive() {
    return diagOnTerm;
  }

  private static void setupLogs(KtLineOutput term, KtLineOutput file, boolean ten) {
    KtLineOutput both = termAndLog;
    if (term != terminal || file != logFile)
      both =
          term != KtLineOutput.NULL && file != KtLineOutput.NULL
              ? new KtDoubleLineOutput(term, file)
              : term != KtLineOutput.NULL ? term : file;
    if (ten != termEnable || term != terminal)
      termLog = ten && term != KtLineOutput.NULL ? makeLog( term) : KtNullLog.LOG;
    if (file != logFile) fileLog = file != KtLineOutput.NULL ? makeLog( file) : KtNullLog.LOG;
    if (ten != termEnable || both != termAndLog)
      normLog = !ten ? fileLog : both != KtLineOutput.NULL ? makeLog( both) : KtNullLog.LOG;
    diagLog = diagOnTerm ? normLog : fileLog;
    terminal = term;
    logFile = file;
    termEnable = ten;
    termAndLog = both;
  }

  public static void setTermEnable(boolean ten) {
    if (ten != termEnable) setupLogs(terminal, logFile, ten);
  }

  public static void setDiagOnTerm(boolean dot) {
    diagLog = (diagOnTerm = dot) ? normLog : fileLog;
  }

  public static void setTerminal(KtLineOutput term) {
    setupLogs(term, logFile, termEnable);
  }

  public static void setLogFile(KtLineOutput file) {
    setupLogs(terminal, file, termEnable);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  private static KtLineInput input;

  public static void setInput(KtLineInput in) {
    input = in;
  }

  public static KtInputLine promptInput( String prompt) {
    if (terminal != KtLineOutput.NULL) {
      termAndLog.add(prompt);
      terminal.flush();
      KtInputLine line = input.readLine();
      if (line != KtLineInput.EOF) {
        terminal.setStartLine();
        fileLog.add(line).endLine();
      } else fatalError("EOFonTerm");
      return line;
    }
    return KtLineInput.EOF;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static KtLoggable num(final int n) {
    return new KtLoggable() {
      public void addOn(KtLog log) {
        log.add(n);
      }
    };
  }

  public static KtLoggable str(final String s) {
    return new KtLoggable() {
      public void addOn(KtLog log) {
        log.add(s);
      }
    };
  }

  public static KtLoggable str(final Object o) {
    return new KtLoggable() {
      public void addOn(KtLog log) {
        log.add(o.toString());
      }
    };
  }

  public static KtLoggable esc(final String s) {
    return new KtLoggable() {
      public void addOn(KtLog log) {
        log.addEsc(s);
      }
    };
  }

  public static KtLoggable esc(final KtName n) {
    return new KtLoggable() {
      public void addOn(KtLog log) {
        n.addEscapedOn(log);
      }
    };
  }

  public static void error(String ident) {
    ioHandler.error(ident, null, true);
  }

  public static void error(String ident, KtLoggable p1) {
    KtLoggable[] params = {p1};
    ioHandler.error(ident, params, true);
  }

  public static void error(String ident, KtLoggable p1, KtLoggable p2) {
    KtLoggable[] params = {p1, p2};
    ioHandler.error(ident, params, true);
  }

  public static void error(String ident, KtLoggable p1, KtLoggable p2, KtLoggable p3) {
    KtLoggable[] params = {p1, p2, p3};
    ioHandler.error(ident, params, true);
  }

  public static void error(String ident, KtLoggable p1, KtLoggable p2, KtLoggable p3, KtLoggable p4) {
    KtLoggable[] params = {p1, p2, p3, p4};
    ioHandler.error(ident, params, true);
  }

  public static void error(String ident, KtLoggable[] params) {
    ioHandler.error(ident, params, true);
  }

  public static void nonDelError(String ident, KtLoggable[] params) {
    ioHandler.error(ident, params, false);
  }

  public static void fatalError(String ident) {
    ioHandler.fatalError(ident);
  }

  public static void errMessage(KtTokenList message) {
    ioHandler.errMessage(message);
  }

  public static void completeShow() {
    ioHandler.completeShow();
  }

  public static void illegalCommand(KtCommand cmd) {
    ioHandler.illegalCommand(cmd);
  }

  public static final int INTP_MAX_RUNAWAY_WIDTH = newIntParam();

  public static void runAway(String desc, KtMaxLoggable list) {
    normLog.startLine().add("Runaway ").add(desc).add('?').endLine();
    list.addOn(normLog, getConfig().getIntParam(INTP_MAX_RUNAWAY_WIDTH));
  }

  /* TeXtp[299] */
  public static void traceCommand(KtCommand cmd) {
    diagLog.startLine().add('{');
    ioHandler.logMode();
    cmd.addOn(diagLog);
    diagLog.add('}').startLine();
  }

  /* TeXtp[299] */
  public static void traceExpandable(KtCommand cmd) {
    diagLog.startLine().add('{');
    ioHandler.logMode();
    cmd.addExpandable(diagLog);
    diagLog.add('}').startLine();
  }

  public static final int BOOLP_TRACING_TOKEN_LISTS = newBoolParam();

  public static void tracedPushXList(KtTokenList list, String desc) {
    if (getConfig().getBoolParam(BOOLP_TRACING_TOKEN_LISTS))
      diagLog.startLine().addEsc(desc).add("->").add(list).startLine();
    pushList(list, desc);
  }

  public static void tracedPushList(KtTokenList list, String desc) {
    if (!list.isEmpty()) tracedPushXList(list, desc);
  }

  public static final int INTP_MAX_TLRES_TRACE = newIntParam();
  public static final int BOOLP_TRACING_RESTORES = newBoolParam();

  public abstract static class KtNumKind extends KtEqTable.KtNumKind implements Serializable {

    public final void restored(int key, Object oldVal) {
      trace("restoring", key);
    }

    public final void retained(int key) {
      trace("retaining", key);
    }

    private void trace(String action, int key) {
      if (getConfig().getBoolParam(BOOLP_TRACING_RESTORES)) {
        diagLog.add('{').add(action).add(' ');
        addDescOn(key, diagLog);
        diagLog.add('=');
        addValueOn(key, diagLog);
        diagLog.add('}').startLine();
      }
    }

    protected abstract void addDescOn(int key, KtLog log);

    protected abstract void addValueOn(int key, KtLog log);
  }

  public abstract static class KtTokKind extends KtEqTable.KtObjKind implements Serializable {

    public final void restored(Object key, Object oldVal) {
      trace("restoring", key);
    }

    public final void retained(Object key) {
      trace("retaining", key);
    }

    private void trace(String action, Object key) {
      if (getConfig().getBoolParam(BOOLP_TRACING_RESTORES)) {
        KtToken tok = getToken(key);
        diagLog.add('{').add(action).add(' ');
        diagLog.add(tok).add('=');
        meaningOf(tok).addExpandable(diagLog, getConfig().getIntParam(INTP_MAX_TLRES_TRACE));
        diagLog.add('}').startLine();
      }
    }

    protected abstract KtToken getToken(Object key);
  }

  public static final int RESTORING = 0, RETAINING = 1;

  public static void traceRestore(int action, KtEqTraceable eqt) {
    if (getConfig().getBoolParam(BOOLP_TRACING_RESTORES)) {
      String s = switch( action ) {
        case RESTORING -> "restoring";
        case RETAINING -> "retaining";
        default ->
          throw new RuntimeException( "invalid action for traceRestore (" + action + ')' );
      };
      diagLog.add('{').add(s).add(' ');
      eqt.addEqDescOn(diagLog);
      diagLog.add('=');
      eqt.addEqValueOn(diagLog);
      diagLog.add('}').startLine();
    }
  }

  public abstract static class KtExtEquiv implements KtEqTable.KtExtEquiv, KtEqTraceable {

    private int eqLevel = 0;

    public final int getEqLevel() {
      return eqLevel;
    }

    public final void setEqLevel(int lev) {
      eqLevel = lev;
    }

    public final void retainEqValue() {
      traceRestore(RETAINING, this);
    }

    public final void restoreEqValue(Object val) {
      setEqValue(val);
      traceRestore(RESTORING, this);
    }

    protected final void beforeSetting(boolean glob) {
      getEqt().beforeSetting(this, glob);
    }

    public abstract Object getEqValue();

    public abstract void setEqValue(Object val);

    public abstract void addEqDescOn(KtLog log);

    public abstract void addEqValueOn(KtLog log);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  private static class KtCurrGroup implements KtEqTable.KtExtEquiv {

    private int eqLevel = 0;

    public final int getEqLevel() {
      return eqLevel;
    }

    public final void setEqLevel(int lev) {
      eqLevel = lev;
    }

    private KtGroup group = new KtBottomGroup();

    public final KtGroup get() {
      return group;
    }

    public void push(KtGroup grp) {
      grp.open();
      eqTable.pushLevel();
      eqTable.save(this);
      grp.start();
      group = grp;
    }

    public void pop() {
      KtGroup grp = group;
      grp.stop();
      eqTable.popLevel();
      grp.close();
    }

    public void kill() {
      eqTable.popLevel();
    }

    public final Object getEqValue() {
      return group;
    }

    public final void retainEqValue() {}

    public final void restoreEqValue(Object val) {
      group.unsaveAfter();
      group = (KtGroup) val;
    }
  }

  private static final KtCurrGroup currGroup = new KtCurrGroup();

  public static KtGroup getGrp() {
    return currGroup.get();
  }

  public static void pushLevel(KtGroup grp) {
    currGroup.push(grp);
  }

  public static void popLevel() {
    currGroup.pop();
  }

  /* STRANGE
   * killLevel() is there only for killing the KtDisplayGroup
   * when $$\halign{...}$$ occurs
   */
  public static void killLevel() {
    currGroup.kill();
  }

  public static void finishGroups() {
    if (eqTable.getLevel() > 0)
      normLog
          .startLine()
          .add('(')
          .addEsc("end")
          .add(" occurred inside a group at level ")
          .add(eqTable.getLevel())
          .add(')');
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  /* STRANGE
   * In case of font identifier the token is backed and read immediately to
   * mimmic TeX log output.
   */
  /* TeXtp[465] */
  protected static KtTokenList theToks() {
    KtToken tok = nextExpToken();
    KtCommand cmd = meaningOf(tok);
    if (cmd.hasToksValue()) return cmd.getToksValue();
    if (cmd.hasFontTokenValue()) {
      backToken(tok);
      nextRawToken();
      return new KtTokenList(cmd.getFontTokenValue());
    }
    String s;
    if (cmd.hasCrazyValue()) s = "0";
    else if (cmd.hasMuGlueValue()) s = cmd.getMuGlueValue().toString("mu");
    else if (cmd.hasGlueValue()) s = cmd.getGlueValue().toString("pt");
    else if (cmd.hasMuDimenValue()) s = cmd.getMuDimenValue().toString("mu");
    else if (cmd.hasDimenValue()) s = cmd.getDimenValue().toString("pt");
    else if (cmd.hasNumValue()) s = cmd.getNumValue().toString();
    else {
      s = "0";
      error("CantAfterThe", cmd, esc("the"));
    }
    return new KtTokenList(s);
  }

  public static int scanInt() {
    return scanInt(KtIntPar.NULL);
  }

  public static KtNum scanNum() {
    return KtNum.valueOf(scanInt(KtIntPar.NULL));
  }

  public static KtDimen scanDimen() {
    return scanDimen(false, KtBytePar.NULL);
  }

  public static KtDimen scanMuDimen() {
    return scanDimen(true, KtBytePar.NULL);
  }

  public static KtGlue scanGlue() {
    return scanGlue(false);
  }

  public static KtGlue scanMuGlue() {
    return scanGlue(true);
  }

  protected static int scanInt(KtIntPar radix) {
    int rdx = 0;
    long val = 0;
    KtCommand cmd;
    KtBoolPar negative = new KtBoolPar(false);
    KtToken tok = scanSign(negative);
    if (tok.matchOther('`')) {
      tok = nextRawToken();
      if ((val = tok.numValue()) < 0) {
        val = '0';
        backToken(tok);
        error("NonNumericToken");
      } else {
        adjustBraceNesting( -braceNestingDifference( tok ) );
        skipOptExpSpacer();
      }
    } else if ((cmd = meaningOf(tok)).hasNumValue()) {
      val = cmd.getNumValue().intVal();
    } else {
      if (tok.matchOther('\'')) {
        rdx = 8;
        tok = nextExpToken();
      } else if (tok.matchOther('\"')) {
        rdx = 16;
        tok = nextExpToken();
      } else rdx = 10;
      boolean empty = true;
      for (boolean ok = true; ; tok = nextExpToken()) {
        int digit = -1;
        char dig = tok.otherChar();
        if (dig != KtCharCode.NO_CHAR) {
          if (rdx > 10 && dig >= 'A') digit = dig - 'A' + 10;
          else if (dig >= '0' && dig <= '9') digit = dig - '0';
        } else if (rdx > 10) {
          dig = tok.letterChar();
          if (dig != KtCharCode.NO_CHAR && dig >= 'A') digit = dig - 'A' + 10;
        }
        if (digit < 0 || digit >= rdx) break;
        if (ok) {
          empty = false;
          val = val * rdx + digit;
          if (val > KtNum.MAX_INT_VALUE) {
            ok = false;
            error("NumberTooBig");
            val = KtNum.MAX_INT_VALUE;
          }
        }
      }
      if (empty) {
        backToken(tok);
        error("MissingNumber");
      } else skipOptSpacer(tok);
    }
    KtIntPar.set(radix, rdx);
    return negative.get() ? -(int) val : (int) val;
  }

  protected static final int DIMEN_DENOMINATOR = 0x10000;

  /* TeXtp[448] */
  protected static KtDimen scanDimen(boolean mu, KtBytePar glueOrder) {
    KtDimen val = KtDimen.NULL;
    KtBytePar.set(glueOrder, KtGlue.NORMAL);
    KtBoolPar negative = new KtBoolPar(false);
    KtToken tok = scanSign(negative);
    KtCommand cmd = meaningOf(tok);
    if (mu) {
      /* STRANGE
       * This branch is really ugly (but compatible with TeX).
       * Note the asymetry with the else (!mu) branch.
       */
      if (cmd.hasMuDimenValue()) val = cmd.getMuDimenValue();
      else if (cmd.hasDimenValue()) {
        int n = cmd.getDimenValue().toInt(DIMEN_DENOMINATOR);
        muError();
        val = convToDimenUnit(n, KtDimen.ZERO, mu, glueOrder);
      } else if (hasOtherValue(cmd)) {
        backToken(tok);
        error("MissingNumber");
        muError();
        val = convToDimenUnit(0, KtDimen.ZERO, mu, glueOrder);
      }
    } else {
      if (cmd.hasDimenValue()) val = cmd.getDimenValue();
      else if (cmd.hasMuDimenValue()) {
        val = cmd.getMuDimenValue();
        muError();
      } else if (hasOtherValue(cmd)) {
        val = KtDimen.ZERO;
        backToken(tok);
        error("MissingNumber");
      }
    }
    if (val == KtDimen.NULL) {
      if (cmd.hasNumValue()) {
        int n = cmd.getNumValue().intVal();
        if (n < 0) {
          negative.negate();
          n = -n;
        }
        val = convToDimenUnit(n, KtDimen.ZERO, mu, glueOrder);
      } else {
        KtIntPar radix = new KtIntPar(10);
        boolean point = isPoint(tok);
        int whole = 0;
        KtDimen fract = KtDimen.ZERO;
        if (!point) {
          backToken(tok);
          whole = scanInt(radix);
          tok = nextExpToken();
          point = isPoint(tok);
        }
        if (point && radix.get() == 10) {
          StringBuilder buf = new StringBuilder();
          buf.append('0').append('.');
          for (; ; ) {
            tok = nextExpToken();
            char dig = tok.otherChar();
            if (dig != KtCharCode.NO_CHAR && dig >= '0' && dig <= '9') buf.append(dig);
            else break;
          }
          try {
            fract = KtDimen.valueOf(buf.toString());
          } catch (NumberFormatException e) {
            whole = KtNum.MAX_INT_VALUE + 1;
          }
        }
        skipOptSpacer(tok);
        val = convToDimenUnit(whole, fract, mu, glueOrder);
      }
    }
    val = checkDimen(val);
    return negative.get() ? val.negative() : val;
  }

  private static KtDimen convToDimenUnit(int whole, KtDimen fract, boolean mu, KtBytePar glueOrder) {
    if (glueOrder != KtBytePar.NULL) {
      glueOrder.set(scanGlueOrderUnit());
      if (glueOrder.get() != KtGlue.NORMAL) return fract.plus(whole);
    }
    return convToDimenUnit(whole, fract, mu);
  }

  private static byte scanGlueOrderUnit() {
    if (scanKeyword("fil")) {
      byte glord = KtGlue.FIL;
      while (scanKeyword("l")) {
        if (glord < KtGlue.MAX_ORDER) glord++;
        else error("IllegalFil");
      }
      skipOptExpSpacer();
      return glord;
    }
    return KtGlue.NORMAL;
  }

  public static final int DIMP_EM = newDimParam();
  public static final int DIMP_EX = newDimParam();

  private static KtDimen convToDimenUnit(int whole, KtDimen fract, boolean mu) {
    KtDimen val = KtDimen.NULL;
    KtToken tok = nextExpNonSpacer();
    KtCommand cmd = meaningOf(tok);
    if (mu) {
      if (cmd.hasMuDimenValue()) val = cmd.getMuDimenValue();
      else if (cmd.hasDimenValue()) {
        val = cmd.getDimenValue();
        muError();
      } else if (hasOtherValue(cmd)) {
        backToken(tok);
        error("MissingNumber");
        val = KtDimen.ZERO;
        muError();
      } else if (cmd.hasNumValue()) {
        val = KtDimen.valueOf(cmd.getNumValue().intVal(), DIMEN_DENOMINATOR);
        muError();
      }
    } else {
      if (cmd.hasDimenValue()) val = cmd.getDimenValue();
      else if (cmd.hasMuDimenValue()) {
        val = cmd.getMuDimenValue();
        muError();
      } else if (hasOtherValue(cmd)) {
        val = KtDimen.ZERO;
        backToken(tok);
        error("MissingNumber");
      } else if (cmd.hasNumValue())
        val = KtDimen.valueOf(cmd.getNumValue().intVal(), DIMEN_DENOMINATOR);
    }
    if (val == KtDimen.NULL) {
      backToken(tok);
      if (mu) {
        if (!scanKeyword("mu")) error("IllegalMu");
        skipOptExpSpacer();
        return fract.plus(whole);
      } else if (scanKeyword("em")) val = getConfig().getDimParam(DIMP_EM);
      else if (scanKeyword("ex")) val = getConfig().getDimParam(DIMP_EX);
      else {
        val = convToDimenUnit(whole, fract);
        skipOptExpSpacer();
        return val;
      }
      skipOptExpSpacer();
    }
    return val.times(whole).plus(val.times(fract));
  }

  private static class KtDimUnitDesc {
    String id; /* identifier */
    int num; /* numerator */
    int den; /* denominator */

    KtDimUnitDesc(String i, int n, int d) {
      id = i;
      num = n;
      den = d;
    }

    KtDimUnitDesc(String i, int n) {
      id = i;
      num = n;
      den = 0;
    }
  }

  private static final KtDimUnitDesc[] dimUnits = {
    new KtDimUnitDesc("pt", 0),
    new KtDimUnitDesc("in", 7227, 100),
    new KtDimUnitDesc("pc", 12, 1),
    new KtDimUnitDesc("cm", 7227, 254),
    new KtDimUnitDesc("mm", 7227, 2540),
    new KtDimUnitDesc("bp", 7227, 7200),
    new KtDimUnitDesc("dd", 1238, 1157),
    new KtDimUnitDesc("cc", 14856, 1157),
    new KtDimUnitDesc("sp", -16),
  };

  public static KtDimen makeDimen(int dim, String unit) {
    for( final KtDimUnitDesc dimUnit : dimUnits )
      if( dimUnit.id.equals( unit ) ) {
        final int num = dimUnit.num;
        final int den = dimUnit.den;
        return den != 0
          ? KtDimen.valueOf( dim ).times( num, den )
          : KtDimen.shiftedValueOf( dim, num );
      }
    throw new RuntimeException("Illegal unit of measure (" + unit + ")");
  }

  public static final int INTP_MAGNIFICATION = newIntParam();

  private static KtDimen convToDimenUnit(int whole, KtDimen fract) {
    if (scanKeyword("true")) {
      int mag = getConfig().getIntParam(INTP_MAGNIFICATION);
      long w = whole * 1000L;
      fract = fract.times(1000).plus((int) (w % mag)).over(mag);
      int i = fract.toInt();
      whole = (int) (w / mag) + i;
      fract = fract.minus(i);
    }
    for( final KtDimUnitDesc dimUnit : dimUnits )
      if( scanKeyword( dimUnit.id ) ) {
        int num = dimUnit.num;
        int den = dimUnit.den;
        /*
         *	int	p = whole * num;
         *	return fract.times(num).plus(p % den).over(den).plus(p / den);
         */
        return den != 0
          ? fract.plus( whole ).times( num, den )
          : KtDimen.shiftedValueOf( whole, num ).plus( fract.shifted( num ) );
      }
    error("IllegalUnit");
    return fract.plus(whole);
  }

  protected static void muError() {
    error("MixedGlueUnits");
  }

  private static KtDimen checkDimen(KtDimen val) {
    if (val.moreThan(KtDimen.MAX_VALUE)) {
      val = KtDimen.MAX_VALUE;
      error("DimenTooLarge");
    }
    return val;
  }

  protected static KtGlue scanGlue(boolean mu) {
    KtGlue val = KtGlue.NULL;
    KtDimen width = KtDimen.NULL;
    KtDimen plus = KtDimen.ZERO;
    KtDimen minus = KtDimen.ZERO;
    KtBytePar plusOrder = new KtBytePar(KtGlue.NORMAL);
    KtBytePar minusOrder = new KtBytePar(KtGlue.NORMAL);
    KtBoolPar negative = new KtBoolPar(false);
    KtToken tok = scanSign(negative);
    KtCommand cmd = meaningOf(tok);
    if (mu) {
      if (cmd.hasMuGlueValue()) val = cmd.getMuGlueValue();
      else if (cmd.hasGlueValue()) {
        val = cmd.getGlueValue();
        muError();
      } else if (cmd.hasMuDimenValue()) width = cmd.getMuDimenValue();
      else if (cmd.hasDimenValue()) {
        width = cmd.getDimenValue();
        muError();
      } else if (hasOtherValue(cmd)) {
        backToken(tok);
        error("MissingNumber");
        width = KtDimen.ZERO;
        muError();
      }
    } else {
      if (cmd.hasGlueValue()) val = cmd.getGlueValue();
      else if (cmd.hasMuGlueValue()) {
        val = cmd.getMuGlueValue();
        muError();
      } else if (cmd.hasDimenValue()) width = cmd.getDimenValue();
      else if (cmd.hasMuDimenValue()) {
        width = cmd.getMuDimenValue();
        muError();
      } else if (hasOtherValue(cmd)) {
        width = KtDimen.ZERO;
        backToken(tok);
        error("MissingNumber");
      }
    }
    if (val != KtGlue.NULL) return negative.get() ? val.negative() : val;
    if (width == KtDimen.NULL) {
      if (cmd.hasNumValue()) {
        int n = cmd.getNumValue().intVal();
        if (n < 0) {
          negative.negate();
          n = -n;
        }
        width = convToDimenUnit(n, KtDimen.ZERO, mu);
        width = checkDimen(width);
      } else {
        backToken(tok);
        width = scanDimen(mu, KtBytePar.NULL);
      }
    }
    if (negative.get()) width = width.negative();
    if (scanKeyword("plus")) plus = scanDimen(mu, plusOrder);
    if (scanKeyword("minus")) minus = scanDimen(mu, minusOrder);
    return KtGlue.valueOf(width, plus, plusOrder.get(), minus, minusOrder.get());
  }

  private static boolean isPoint(KtToken tok) {
    return tok.matchOther( '.') || tok.matchOther( ',');
  }

  private static boolean hasOtherValue(KtCommand cmd) {
    return cmd.hasToksValue() || cmd.hasFontTokenValue();
  }

  protected static KtToken scanSign(KtBoolPar negative) {
    KtToken tok;
    for (; ; ) {
      tok = nextExpNonSpacer();
      if (tok.matchOther('-')) negative.set(!negative.get());
      else if (!tok.matchOther('+')) return tok;
    }
  }

  protected static void skipOptSpacer( KtToken tok) {
    if (!meaningOf(tok).isSpacer()) backToken(tok);
  }

  protected static void skipOptExpSpacer() {
    skipOptSpacer(nextExpToken());
  }

  /* TeXtp[406] */
  protected static KtToken nextExpNonSpacer() {
    KtToken tok;
    do tok = nextExpToken();
    while (meaningOf(tok).isSpacer());
    return tok;
  }

  /** Skips an optional equal sign preceded by optional spaces. */
  /* TeXtp[405] */
  public static void skipOptEquals() {
    KtToken tok = nextExpNonSpacer();
    if (!tok.matchOther('=')) backToken(tok);
  }

  /* TeXtp[404] */
  protected static KtToken nextNonRelax() {
    KtToken tok;
    KtCommand cmd;
    do {
      tok = nextExpToken();
      cmd = meaningOf(tok);
    } while (cmd.isSpacer() || cmd.isRelax());
    return tok;
  }

  /* TeXtp[404] */
  protected static KtToken nextNonAssignment() {
    for (; ; ) {
      KtToken tok = nextNonRelax();
      KtCommand cmd = meaningOf(tok);
      if (cmd.assignable()) cmd.doAssignment(tok, 0);
      else return tok;
    }
  }

  /* TeXtp[403] */
  public static void scanLeftBrace() {
    KtToken tok = nextNonRelax();
    if (!meaningOf(tok).isLeftBrace()) {
      backToken(tok);
      error("MissingLeftBrace");
      adjustBraceNesting(1);
    }
  }

  /* TeXtp[407] */
  protected static boolean scanKeyword(String keyword) {
    KtToken[] backup = new KtToken[keyword.length()];
    KtToken tok = nextExpNonSpacer();
    for (int i = 0; ; ) {
      KtCharCode code = tok.nonActiveCharCode();
      if (code == KtCharCode.NULL
          || !code.match(keyword.charAt(i))
              && !code.match(Character.toUpperCase(keyword.charAt(i)))) {
        backToken(tok);
        if (i > 0) backList(new KtTokenList(backup, 0, i));
        return false;
      }
      backup[i++] = tok;
      if (i >= keyword.length()) return true;
      tok = nextExpToken();
    }
  }

  /* TeXtp[526] */
  protected static KtFileName scanFileName() {
    boolean inpEnbl = getConfig().enableInput(false);
    KtFileName name = ioHandler.makeFileName();
    KtToken tok;
    KtCommand cmd;
    int i;
    do {
      tok = nextExpToken();
      cmd = meaningOf(tok);
    } while (cmd.isSpacer());
    for (; ; ) {
      KtCharCode code = cmd.charCode();
      i = code != KtCharCode.NULL ? name.accept( code) : -1;
      if (i <= 0) break;
      tok = nextExpToken();
      cmd = meaningOf(tok);
    }
    if (i < 0) backToken(tok);
    getConfig().enableInput(inpEnbl);
    return name;
  }
}

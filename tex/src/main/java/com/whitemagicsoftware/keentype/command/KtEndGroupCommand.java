// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.EndGroupCommand
// $Id: KtEndGroupCommand.java,v 1.1.1.1 2000/08/05 16:07:00 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

public class KtEndGroupCommand extends KtGroupCommand {

  private final String name;

  public KtEndGroupCommand(String name) {
    this.name = name;
  }

  public void exec(KtGroup grp, KtToken src) {
    grp.reject(src);
  }

  public static final KtClosing NORMAL =
      new KtClosing() {
        public void exec(KtGroup grp, KtToken src) {
          popLevel();
        }
      };

  public final void addOn(KtLog log) {
    log.add(name);
  }

  public final String toString() {
    return "@(" + name + ')';
  }
}

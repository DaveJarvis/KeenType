// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.IfEofPrim
// $Id: KtIfEofPrim.java,v 1.1.1.1 1999/07/28 08:07:11 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtIfEofPrim extends KtAnyIfPrim {

  private final KtReadPrim read;

  public KtIfEofPrim(String name, KtReadPrim read) {
    super(name);
    this.read = read;
  }

  protected final boolean holds() {
    return read.eof(KtPrim.scanFileCode());
  }
}

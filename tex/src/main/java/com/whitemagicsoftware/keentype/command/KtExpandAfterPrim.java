// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ExpandAfterPrim
// $Id: KtExpandAfterPrim.java,v 1.1.1.1 1999/05/31 11:18:46 ksk Exp $
package com.whitemagicsoftware.keentype.command;

/* TeXtp[368] */
public class KtExpandAfterPrim extends KtExpandablePrim {

  public KtExpandAfterPrim(String name) {
    super(name);
  }

  public void expand(KtToken src) {
    KtToken first = nextRawToken();
    KtToken second = onlyRawToken();
    if (second != KtToken.NULL) backToken(second);
    backToken(first);
  }
}

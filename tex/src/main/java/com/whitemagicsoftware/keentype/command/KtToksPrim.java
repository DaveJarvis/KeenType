// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ToksPrim
// $Id: KtToksPrim.java,v 1.1.1.1 1999/06/24 10:59:05 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

/** Setting tokens register primitive. */
public class KtToksPrim extends KtRegisterPrim implements KtTokenList.KtProvider {

  /**
   * Creates a new |KtToksPrim| with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the |KtToksPrim|
   */
  public KtToksPrim(String name) {
    super(name);
  }

  public final void set(int idx, KtTokenList val, boolean glob) {
    if (glob) getEqt().gput(tabKind, idx, val);
    else getEqt().put(tabKind, idx, val);
  }

  public final KtTokenList get(int idx) {
    KtTokenList val = (KtTokenList) getEqt().get(tabKind, idx);
    return val != KtTokenList.NULL ? val : KtTokenList.EMPTY;
  }

  public void addEqValueOn(int idx, KtLog log) {
    get(idx).addOn(log, getConfig().getIntParam(INTP_MAX_TLRES_TRACE));
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  protected final void assign(KtToken src, boolean glob) {
    int idx = scanRegisterCode();
    skipOptEquals();
    set(idx, scanToks(src, false), glob);
  }

  public boolean hasToksValue() {
    return true;
  }

  public KtTokenList getToksValue() {
    return get(scanRegisterCode());
  }
}

// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.SequenceTokenizer
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;

public abstract class KtSequenceTokenizer extends KtTokenizer {

  /**
   * The current input line
   */
  private KtTokenizer curr;

  public KtSequenceTokenizer() {
    curr = KtTokenizer.NULL;
  }

  public KtSequenceTokenizer( KtTokenizer first ) {
    curr = first;
  }

  /**
   * Gets the next {@link KtToken} from the input.
   *
   * @param canExpand boolean output parameter querying whether the acquired
   *                  {@link KtToken} can be expanded (e.g. was not preceded by
   *                  {@code \noexpand}).
   * @return the next token from the input or {@link KtToken#NULL} if the input is
   * finished.
   */
  public KtToken nextToken( KtBoolPar canExpand ) {
    if( curr == KtTokenizer.NULL ) { curr = nextTokenizer(); }

    while( curr != KtTokenizer.NULL ) {
      KtToken tok = curr.nextToken( canExpand );
      if( tok != KtToken.NULL ) { return tok; }
      curr = nextTokenizer();
    }

    return KtToken.NULL;
  }

  public boolean enoughContext() {
    return curr != KtTokenizer.NULL && curr.enoughContext();
  }

  public int show( KtContextDisplay disp, boolean force, int lines ) {
    return curr != KtTokenizer.NULL ? curr.show( disp, force, lines ) : 0;
  }

  public abstract KtTokenizer nextTokenizer();
}

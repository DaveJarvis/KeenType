// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.Command
// $Id: KtCommand.java,v 1.1.1.1 2001/03/22 13:31:36 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtEofToken;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.node.KtBox; // DDD
import com.whitemagicsoftware.keentype.node.KtBoxSizes; // DDD
import com.whitemagicsoftware.keentype.node.KtFontMetric; // DDD
import com.whitemagicsoftware.keentype.node.KtTreatBox; // DDD

/** KtCommand of the macro language. */
public abstract class KtCommand extends KtCommandBase implements Serializable, KtLoggable {

  /** Symbolic constant for nonexisting |KtCommand| */
  public static final KtCommand NULL = null;

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Each TeX primitive is implemented as a subclass of |KtCommand| class.
   * So are all the user defined macros. The |KtCommand|s are assigned as a
   * meaning of input |KtToken|s. Some of them can be expanded (macros and
   * primitives of expand processor) and they cause some tokens to be pushed
   * on the token input. The others can be executed.
   *
   * The main loop of the system reads the |KtToken|s from the input, gets the
   * associated |KtCommand|s and tries to expand them. If they are not
   * expandable they are executed then. The |KtToken|s and associated
   * |KtCommand|s can be fetched not only in the main loop but also some
   * commands can read them as parameters. In some of such cases there is no
   * attempt to expand them.
   *
   * First come the non-static methods which define the interface for every
   * piece of command.
   */

  /**
   * Tells whether this |KtCommand| is expandable.
   *
   * @return |true| if it is expandable.
   */
  public boolean expandable() {
    return false;
  }

  /**
   * Expand itself in the process of macro expansion.
   *
   * @param src source |KtToken| for diagnostic output.
   */
  public void doExpansion(KtToken src) {
    throw new RuntimeException("not expandable");
  }

  public void addExpandable(KtLog log) {
    addExpandable(log, false);
  }

  public void addExpandable(KtLog log, boolean full) {
    addOn(log);
  }

  public void addExpandable(KtLog log, int maxCount) {
    addExpandable(log, true);
  }

  public KtCommand meaning(boolean expOK) {
    return this;
  }

  public boolean sameAs(KtCommand cmd) {
    return cmd == this;
  }

  public KtToken origin() {
    return KtToken.NULL;
  }

  public static final int BOOLP_TRACING_COMMANDS = newBoolParam();

  /* TeXtp[1031] */
  public final void execute(KtToken src) {
    if (getConfig().getBoolParam(BOOLP_TRACING_COMMANDS)) traceCommand(this);
    exec(src);
  }

  /* TeXtp[1031] */
  public final void execute(KtToken src, int prefixes) {
    // if (getConfig().getBoolParam(BOOLP_TRACING_COMMANDS))
    // traceCommand(this);
    exec(src, prefixes);
  }

  /**
   * Performs itself in the process of interpretation of the macro language.
   *
   * @param src source |KtToken| for diagnostic output.
   */
  public abstract void exec(KtToken src);

  /**
   * Performs itself in the process of interpretation of the macro language after sequence of prefix
   * commands.
   *
   * @param src source |KtToken| for diagnostic output.
   * @param prefixes accumulated prefixes.
   */
  public void exec(KtToken src, int prefixes) {
    /* See TeXtp[1212]. */
    backToken(src);
    error("NonPrefixCommand", this);
  }

  public boolean immedExec(KtToken src) {
    return false;
  }

  public void perform(int operation, boolean glob, KtCommand after) {
    error("CantAfterThe", this, after);
  }

  public boolean assignable() {
    return false;
  }

  public void doAssignment(KtToken src, int prefixes) {
    throw new RuntimeException("not assignable");
  }

  public boolean isSpacer() {
    return false;
  }

  public boolean isRelax() {
    return false;
  }

  public boolean isLeftBrace() {
    return false;
  }

  public boolean isRightBrace() {
    return false;
  }

  public boolean isMacroParam() {
    return false;
  }

  public boolean isEndCsName() {
    return false;
  }

  public boolean isOuter() {
    return false;
  }

  public boolean isConditional() {
    return false;
  }

  public int endBranchLevel() {
    return 0;
  }

  public boolean isNoBoundary() {
    return false;
  }

  public boolean isMathShift() {
    return false;
  }
  // XXX only for alignment preamble
  public boolean isTabSkip() {
    return false;
  }
  // XXX only for alignment body
  public boolean isNoAlign() {
    return false;
  }

  public boolean isOmit() {
    return false;
  }

  public boolean isCrCr() {
    return false;
  }
  // XXX only for alignment (preamble & body)
  public boolean isCarRet() {
    return false;
  }

  public boolean isTabMark() {
    return false;
  }

  public boolean isSpan() {
    return false;
  }
  // XXX only for alignment body
  public boolean explosive() {
    return false;
  }

  public void detonate(KtToken src) {
    throw new RuntimeException("not explosive");
  }

  public KtCharCode charCode() {
    return KtCharCode.NULL;
  }

  public KtCharCode charCodeToAdd() {
    return KtCharCode.NULL;
  }

  public boolean appendToks(KtTokenList.KtBuffer buf) {
    return false;
  }

  public boolean hasNumValue() {
    return hasDimenValue() || hasMuDimenValue();
  }

  public boolean hasDimenValue() {
    return hasGlueValue();
  }

  public boolean hasMuDimenValue() {
    return hasMuGlueValue();
  }

  public boolean hasGlueValue() {
    return false;
  }

  public boolean hasMuGlueValue() {
    return false;
  }

  public boolean hasToksValue() {
    return false;
  }

  public boolean hasFontTokenValue() {
    return false;
  }

  public boolean hasFontMetricValue() {
    return false;
  }

  public boolean hasRuleValue() {
    return false;
  }

  public boolean hasBoxValue() {
    return false;
  }

  public boolean canMakeBoxValue() {
    return false;
  }

  public boolean hasCrazyValue() {
    return false;
  }

  public boolean hasMathCodeValue() {
    return false;
  }

  public boolean hasDelCodeValue() {
    return false;
  }

  public KtNum getNumValue() {
    if (hasDimenValue()) return KtNum.valueOf(getDimenValue().toInt(DIMEN_DENOMINATOR));
    if (hasMuDimenValue()) {
      KtNum n = KtNum.valueOf(getMuDimenValue().toInt(DIMEN_DENOMINATOR));
      muError(); /* STRANGE but compatible with TeX */
      return n;
    }
    return KtNum.NULL;
  }

  public KtDimen getDimenValue() {
    return hasGlueValue() ? getGlueValue().getDimen() : KtDimen.NULL;
  }

  public KtDimen getMuDimenValue() {
    return hasMuGlueValue() ? getMuGlueValue().getDimen() : KtDimen.NULL;
  }

  public KtGlue getGlueValue() {
    return KtGlue.NULL;
  }

  public KtGlue getMuGlueValue() {
    return KtGlue.NULL;
  }

  public KtTokenList getToksValue() {
    return KtTokenList.NULL;
  }

  public KtToken getFontTokenValue() {
    return KtToken.NULL;
  }

  public KtFontMetric getFontMetricValue() {
    return KtFontMetric.NULL;
  }

  /* STRANGE only for \leaders */
  public KtBoxSizes getRuleValue() {
    return KtBoxSizes.NULL;
  }

  public KtBox getBoxValue() {
    return KtBox.NULL;
  }

  public void makeBoxValue(KtTreatBox proc) {
    throw new RuntimeException("Cannot make a box");
  }

  /* STRANGE only for \leaders */
  public KtGlue getSkipForLeaders() {
    return KtGlue.NULL;
  }

  public int getMathCodeValue() {
    return 0;
  }

  public int getDelCodeValue() {
    return 0;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * The language interpreter
   */

  public static final int TOKSP_EVERY_JOB = newToksParam();

  private static boolean working;
  private static boolean dumping;

  public static boolean mainLoop() {
    working = true;
    getConfig().getToksInserter( TOKSP_EVERY_JOB ).insertToks();

    do {
      final KtToken tok = nextExpToken();

      if( tok == KtToken.NULL || tok instanceof KtEofToken ) {
        working = false;
      }
      else {
        meaningOf( tok ).execute( tok );
      }
    } while( working );

    return dumping;
  }

  public static void endMainLoop(boolean dump) {
    dumping = dump;
    working = false;
  }

  public static void cleanUp() {
    ensureOpenLog();
    getTokStack().close();
    finishGroups();
    // XXX TeXtp[1335]
  }
}

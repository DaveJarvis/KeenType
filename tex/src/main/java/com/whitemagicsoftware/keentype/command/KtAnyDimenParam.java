// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.AnyDimenParam
// $Id: KtAnyDimenParam.java,v 1.1.1.1 1999/06/08 22:03:01 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtLog;

/** */
public abstract class KtAnyDimenParam extends KtPerformableParam {

  private KtDimen value;

  /**
   * Creates a new KtAnyDimenParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtAnyDimenParam
   * @param val the value of the KtAnyDimenParam
   */
  public KtAnyDimenParam(String name, KtDimen val) {
    super(name);
    value = val;
  }

  /**
   * Creates a new KtAnyDimenParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtAnyDimenParam
   */
  public KtAnyDimenParam(String name) {
    this(name, KtDimen.ZERO);
  }

  public final Object getEqValue() {
    return value;
  }

  public final void setEqValue(Object val) {
    value = (KtDimen) val;
  }

  public final void addEqValueOn(KtLog log) {
    log.add(value.toString(getUnit()));
  }

  public abstract String getUnit();

  public final KtDimen get() {
    return value;
  }

  public void set(KtDimen val, boolean glob) {
    beforeSetting(glob);
    value = val;
  }
}

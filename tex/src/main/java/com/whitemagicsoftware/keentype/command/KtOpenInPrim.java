// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.OpenInPrim
// $Id: KtOpenInPrim.java,v 1.1.1.1 1999/07/28 08:07:12 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtOpenInPrim extends KtPrim {

  private final KtReadPrim read;

  public KtOpenInPrim(String name, KtReadPrim read) {
    super(name);
    this.read = read;
  }

  /* TeXtp[1275] */
  public void exec(KtToken src) {
    int num = scanFileCode();
    skipOptEquals();
    read.set(num, getIOHandler().openRead(scanFileName(), num));
  }
}

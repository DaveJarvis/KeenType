// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ExpandablePrim
// $Id: KtExpandablePrim.java,v 1.1.1.1 2000/03/17 14:41:27 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtExpandablePrim extends KtExpandable implements KtPrimitive {

  /** The name of the primitive */
  private final String name;

  /**
   * Creates a new KtPrim with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtPrim
   */
  protected KtExpandablePrim(String name) {
    this.name = name;
  }

  public final String getName() {
    return name;
  }

  public final KtCommand getCommand() {
    return this;
  }

  public final String toString() {
    return "@" + name;
  }

  /* TeXtp[367] */
  public final void doExpansion(KtToken src) {
    if (getConfig().getBoolParam(BOOLP_TRACING_ALL_COMMANDS)) traceExpandable(this);
    expand(src);
  }

  public abstract void expand(KtToken src);

  public void addExpandable(KtLog log, boolean full) {
    log.addEsc(name);
  }

  /* TeXtp[379] */
  protected static void insertRelax() {
    insertToken(new KtFrozenToken("relax", KtRelax.getRelax()));
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.FrozenToken
// $Id: KtFrozenToken.java,v 1.1.1.1 2000/03/03 11:29:04 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;

/** Frozen control sequence KtToken. */
public class KtFrozenToken extends KtToken {

  /** The name of the control sequence */
  private final KtName name;

  private final KtCommand cmd;

  /**
   * Creates a frozen control sequence token of the given name.
   *
   * @param name the name of the frozencontrol sequence
   * @param cmd the meaning of the frozen control sequence
   */
  public KtFrozenToken(KtName name, KtCommand cmd) {
    this.name = name;
    this.cmd = cmd;
  }

  public KtFrozenToken(String name, KtCommand cmd) {
    this.name = makeName(name);
    this.cmd = cmd;
  }

  public KtFrozenToken(KtPrimitive prim) {
    this.name = makeName(prim.getName());
    this.cmd = prim.getCommand();
  }

  /**
   * Gives object (potentially) associated in table of equivalents.
   *
   * @param eqtab table of equivalents.
   * @return the KtCommand to interpret this token.
   */
  public KtCommand meaning() {
    return cmd;
  }

  public boolean frozen() {
    return true;
  }

  public boolean match(KtToken tok) {
    // XXX does it match anything at all?
    // XXXX it should probably match KtCtrlSeqToken of the same name
    // XXXX is there any case, where it is tried to match?
    // XXXX (scanning mac params?)
    // return (  tok instanceof KtFrozenToken
    //     && ((KtFrozenToken) tok).name.match(name)  );
    return false;
  }

  public void addOn(KtLog log) {
    name.addEscapedOn(log);
  }

  public void addProperlyOn(KtLog log) {
    name.addProperlyEscapedOn(log);
  }

  // is the rest OK ?

  public int numValue() {
    return name.length() == 1 ? name.codeAt( 0).numValue() : -1;
  }

  public KtName controlName() {
    return name;
  }

  public String toString() {
    return "<Frozen: " + name + '>';
  }
}

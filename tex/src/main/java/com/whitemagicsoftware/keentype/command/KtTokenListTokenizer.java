// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.TokenListTokenizer
// $Id: KtTokenListTokenizer.java,v 1.1.1.1 2000/08/04 14:46:43 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;

/** |KtTokenizer| for sequence of |KtToken|s in an array. */
/* See TeXtp[323]. */
public abstract class KtTokenListTokenizer extends KtTokenizer {

  /** Array of |KtToken|s where the sequence is stored */
  private final KtTokenList list;

  /** The position of the next |KtToken| in the list */
  private int pos;

  /** Position after the last |KtToken| in the sequence */
  private final int end;

  /**
   * Creates |KtTokenizer| for a portion of a |KtTokenList|.
   *
   * @param list the |KtTokenList|.
   * @param start position of the first |KtToken| in the sequence.
   * @param end position after the last |KtToken| in the sequence.
   */
  public KtTokenListTokenizer(KtTokenList list, int start, int end) {
    this.list = list;
    this.pos = start;
    this.end = end;
  }

  /**
   * Creates |KtTokenizer| for a |KtTokenList|.
   *
   * @param list the |KtTokenList|.
   */
  public KtTokenListTokenizer(KtTokenList list) {
    this.list = list;
    this.pos = 0;
    this.end = list.length();
  }

  /**
   * Gives the next |KtToken| from the sequence.
   *
   * @param canExpand boolean output parameter querying whether the acquired |KtToken| can be expanded
   *     (e.g. was not preceded by \noexpand).
   * @return next |KtToken| or |KtToken.NULL| when the sequence is finished.
   */
  public KtToken nextToken(KtBoolPar canExpand) {
    canExpand.set(true);
    return pos < end ? list.tokenAt( pos++) : KtToken.NULL;
  }

  public boolean finishedList() {
    return pos >= end;
  }

  public int showList(KtContextDisplay disp, int lines) {
    list.addContext(disp.left(), disp.right(), pos, 100000);
    disp.show(); // XXX why so much?
    return 1;
  }
}

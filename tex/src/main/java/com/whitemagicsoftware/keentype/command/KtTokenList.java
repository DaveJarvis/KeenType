// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.TokenList
// $Id: KtTokenList.java,v 1.1.1.1 2000/01/28 17:09:24 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import java.io.Serializable;
import java.util.Vector;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtMaxLoggable;
import com.whitemagicsoftware.keentype.io.KtName;

public class KtTokenList implements Serializable, KtLoggable, KtMaxLoggable {

  public static final KtTokenList NULL = null;

  public static final KtTokenList EMPTY = new KtTokenList();

  public interface KtProvider {
    KtTokenList getToksValue();
  }

  public interface KtMaintainer extends KtProvider {
    void setToksValue(KtTokenList list);

    boolean isEmpty();
  }

  public interface KtInserter {
    void insertToks();

    boolean isEmpty();
  }

  private static KtToken tokenFor(char chr) {
    if (chr == ' ') return KtSpaceToken.TOKEN;
    else return new KtOtherToken(KtToken.makeCharCode(chr));
  }

  private static KtToken tokenFor(KtCharCode code) {
    if (code.match(' ')) return KtSpaceToken.TOKEN;
    else return new KtOtherToken(code);
  }

  public static class KtBuffer implements KtLoggable, KtMaxLoggable {

    protected Vector<KtToken> data;

    public KtBuffer() {
      data = new Vector<>();
    }

    public KtBuffer(int initCap) {
      data = new Vector<>( initCap );
    }

    public KtBuffer(int initCap, int capIncrement) {
      data = new Vector<>( initCap, capIncrement );
    }

    public KtBuffer append(KtToken tok) {
      data.addElement(tok);
      return this;
    }

    public KtBuffer append(char chr) {
      return append(tokenFor(chr));
    }

    public KtBuffer append(KtCharCode code) {
      return append(tokenFor(code));
    }

    public KtBuffer append(KtToken[] tokens, int offset, int count) {
      data.ensureCapacity(data.size() + count);
      while (count-- > 0) data.addElement(tokens[offset++]);
      return this;
    }

    public KtBuffer append(KtToken[] tokens) {
      return append(tokens, 0, tokens.length);
    }

    public KtBuffer append(KtTokenList list) {
      return append(list.tokens);
    }

    public void insertTokenAt(KtToken tok, int idx) {
      data.insertElementAt(tok, idx);
    }

    public void insertTokenAt(KtCharCode code, int idx) {
      data.insertElementAt(tokenFor(code), idx);
    }

    public void insertTokenAt(char chr, int idx) {
      data.insertElementAt(tokenFor(chr), idx);
    }

    public int length() {
      return data.size();
    }

    public void setLength(int size) {
      data.setSize(size);
    }

    public KtToken tokenAt(int idx) {
      return data.elementAt( idx);
    }

    public KtToken lastToken() {
      return data.elementAt( data.size() - 1);
    }

    public void removeTokenAt(int idx) {
      data.removeElementAt(idx);
    }

    public void removeLastToken() {
      data.removeElementAt(data.size() - 1);
    }

    public void getTokens(int beg, int end, KtToken[] dst, int offset) {
      while (beg < end) dst[offset++] = tokenAt(beg++);
    }

    public KtTokenList toTokenList() {
      if (length() == 0) return EMPTY;
      KtToken[] tokens = new KtToken[length()];
      data.copyInto(tokens);
      return new KtTokenList(tokens);
    }

    public void addOn(KtLog log) {
      toTokenList().addOn(log);
    }

    public void addOn(KtLog log, int maxCount) {
      toTokenList().addOn(log, maxCount);
    }
  }

  private final KtToken[] tokens;

  public KtTokenList() {
    tokens = new KtToken[0];
  }

  public KtTokenList(KtToken[] tokens) {
    this.tokens = tokens;
  }

  public KtTokenList(KtToken[] tokens, int offset, int count) {
    this.tokens = new KtToken[count];
    System.arraycopy(tokens, offset, this.tokens, 0, count);
  }

  public KtTokenList(KtToken tok) {
    tokens = new KtToken[1];
    tokens[0] = tok;
  }

  /* TeXtp[464] */
  public KtTokenList(String str) {
    tokens = new KtToken[str.length()];
    for (int i = 0; i < str.length(); i++) tokens[i] = tokenFor(str.charAt(i));
  }

  public KtTokenList(KtName name) {
    tokens = new KtToken[name.length()];
    for (int i = 0; i < name.length(); i++) tokens[i] = tokenFor(name.codeAt(i));
  }

  public final int length() {
    return tokens.length;
  }

  public final boolean isEmpty() {
    return length() == 0;
  }

  public final KtToken tokenAt(int idx) {
    return tokens[idx];
  }

  public final boolean match(KtTokenList list) {
    if (length() == list.length()) {
      for (int i = 0; i < length(); i++) if (!tokenAt(i).match(list.tokenAt(i))) return false;
      return true;
    }
    return false;
  }

  public void addOn(KtLog log) {
    for (int i = 0; i < length(); i++) tokenAt(i).addProperlyOn(log);
  }

  public void addOn(KtLog log, int maxCount) {
    int i;
    maxCount += log.getCount();
    for (i = 0; i < length() && log.getCount() < maxCount; i++) tokenAt(i).addProperlyOn(log);
    if (i < length()) log.addEsc("ETC.");
  }

  public void addContext(KtLog left, KtLog right, int pos, int maxCount) {
    KtLog log = left;
    int i;
    maxCount += log.getCount();
    for (i = 0; i < length() && log.getCount() < maxCount; i++) {
      if (i == pos) {
        maxCount -= log.getCount();
        log = right;
        maxCount += log.getCount();
      }
      tokenAt(i).addProperlyOn(log);
    }
    if (i < length()) log.addEsc("ETC.");
  }

  public String toString() {
    StringBuilder buf = new StringBuilder();
    for (int i = 0; i < length(); buf.append(tokenAt(i++)))
      ;
    return buf.toString();
  }
}

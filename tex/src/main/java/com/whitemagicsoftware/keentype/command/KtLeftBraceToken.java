// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.LeftBraceToken
// $Id: KtLeftBraceToken.java,v 1.1.1.1 2000/02/16 11:00:42 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtLeftBraceToken extends KtCharToken {

  public static final KtCharCode CODE = makeCharCode('{');
  public static final KtLeftBraceToken TOKEN = new KtLeftBraceToken(CODE);
  public static final KtMaker MAKER = KtLeftBraceToken::new;

  public KtLeftBraceToken(KtCharCode code) {
    super(code);
  }

  @Override
  public boolean matchLeftBrace() {
    return true;
  }

  public boolean match(KtCharToken tok) {
    return tok instanceof KtLeftBraceToken && tok.match( code);
  }

  public KtMaker getMaker() {
    return MAKER;
  }

  @Override
  public String toString() {
    return "<KtLeftBrace: " + code + '>';
  }

  private static KtCommand command;

  public static void setCommand(KtCommand cmd) {
    command = cmd;
  }

  @Override
  public KtCommand meaning() {
    return new KtMeaning() {

      @Override
      public boolean isLeftBrace() {
        return true;
      }

      public void exec(KtToken src) {
        command.exec(src);
      }

      protected String description() {
        return "begin-group character";
      }
    };
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.CharToken
// $Id: KtCharToken.java,v 1.1.1.1 2000/05/04 21:22:25 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;

/** Character KtToken. */
public abstract class KtCharToken extends KtToken {

  public interface KtMaker {
    KtToken make(KtCharCode code);
  }

  protected KtCharCode code;

  public KtCharToken(KtCharCode code) {
    this.code = code;
  }

  public final KtCharCode charCode() {
    return code;
  }

  public KtToken makeCharToken(KtCharCode code) {
    KtMaker maker = getMaker();
    return maker != null ? maker.make( code) : KtToken.NULL;
  }

  public KtCharCode nonActiveCharCode() {
    return code;
  }

  public int numValue() {
    return code.numValue();
  }

  public void addOn(KtLog log) {
    log.add(code);
  }

  public final boolean match(KtToken tok) {
    return tok instanceof KtCharToken && match( (KtCharToken) tok);
  }

  public final boolean match(KtCharCode chr) {
    return code.match(chr);
  }

  public abstract boolean match(KtCharToken tok);

  public KtToken category() {
    return this;
  }

  public abstract KtMaker getMaker();

  public String toString() {
    return "<KtChar: " + code + '>';
  }

  public abstract class KtMeaning extends KtCommand {

    public final KtCharCode charCode() {
      return code;
    }

    public final void addOn(KtLog log) {
      log.add(description()).add(' ').add(code);
    }

    public boolean sameAs(KtCommand cmd) {
      return this.getClass() == cmd.getClass() && code.match( cmd.charCode());
      // XXX is cast to KtMeaning necessary?
    }

    public final KtToken origin() {
      return KtCharToken.this;
    }

    public final String toString() {
      return "[" + description() + ' ' + code + ']';
    }

    protected abstract String description();
  }
}

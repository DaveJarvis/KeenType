// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.TokenizerStack
// $Id: KtTokenizerStack.java,v 1.1.1.1 2000/08/04 04:51:07 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;

/**
 * Stack of |KtTokenizer|s. It provides methods |push| for |KtTokenizer|, array
 * of |KtToken|s and single |KtToken|. Method |nextToken| calls the same method
 * of the |KtTokenizer| on the top of stack.
 */
public class KtTokenizerStack {

  /*
   * There are at least two possibilities how to implement stack of
   * tokenizers. First is to use some generic container and second is to
   * make base class for tokenizers which can chain itself into an linked
   * list. I chose the second method because the operations are so simple.
   * It contains another member related to pushing and popping anyway --- see
   * the paragraph below. The generic stack in Java on the other hand needs
   * casting for accessing elements.
   *
   * Each tokenizer contains the reference to the stack where it is pushed.
   * The reason is that some tokenizers might want to push another tokenizer
   * on the top of stack (for example a macro expansion needs to push a
   * parameter --- it is much simpler than treating two levels of expansion
   * in single object) and then it needs to know the reference to the stack.
   * But after some thinking it seems quite logical that one tokenizer is
   * used in only one stack at the some time so the binding to one stack is
   * OK from this point of view too.
   *
   * The base tokenizer is implemented as an inner class. This guarantees
   * that only the class itself and the |KtTokenizerStack| have access to its
   * private members which maintain chaining. The method |nextToken| is
   * protected so the subclasses can override it but beside them only
   * |KtTokenizerStack| can call it. Provided that the |KtToken|s are retrieved
   * from a |KtTokenizer| only by |KtTokenizerStack| where it belongs to and
   * also the synchronizations seems to be sufficient on the level of
   * |nextToken| method of the |KtTokenizerStack|.
   *
   * For maintaining of input stack in TeX see TeXtp[301,321,322].
   */

  /** The current |KtTokenizer| - top of the stack */
  private KtTokenizer top = KtTokenizer.NULL;

  public KtTokenizer getTop() {
    return top;
  }

  /**
   * Pushes a |KtTokenizer| on the top of the stack.
   *
   * @param tokenizer the |KtTokenizer| to be pushed.
   * @exception RuntimeException if the |tokenizer| is already pushed somewhere.
   */
  public synchronized void push(KtTokenizer tokenizer) {
    KtTokenizer oldTop = top;
    top = tokenizer;
    top.pushTo(this, oldTop);
  }

  /** Pops one |KtTokenizer| from the top of the stack. */
  private synchronized KtTokenizer pop() {
    if (top != KtTokenizer.NULL) {
      KtTokenizer oldTop = top;
      top = oldTop.getNext();
      oldTop.popFrom(this);
      return oldTop;
    }
    return KtTokenizer.NULL;
  }

  public synchronized void dropPop() {
    if (top != KtTokenizer.NULL) pop().close();
  }

  public synchronized void dropFinishedPop() {
    if (top != KtTokenizer.NULL && top.finished()) pop().close();
  }

  /**
   * Creates a |KtTokenizer| for a portion of a |KtTokenList| and pushes it on the top of the stack.
   *
   * @param list the |KtTokenList|.
   * @param start position of the first |KtToken| in the sequence.
   * @param end position after the last |KtToken| in the sequence.
   */
  //	    public void		push(KtTokenList list, int start, int end)
  //		{ push(new KtTokenListTokenizer(list, start, end)); }

  /**
   * Creates a KtTokenizer for a |KtTokenList| and pushes it on the top of the stack.
   *
   * @param list the |KtTokenList|.
   */
  public void push(KtTokenList list, String desc) {
    push(new KtInsertedTokenList(list, desc));
  }

  public void backUp(KtTokenList list) {
    push(new KtBackedTokenList(list));
  }

  public void push(KtToken tok, String desc) {
    push(new KtInsertedToken(tok, desc));
  }

  /**
   * Creates a |KtTokenizer| for a single |KtToken| and pushes it on the top of the stack.
   *
   * @param tok the single |KtToken|.
   * @param exp tells whether the backed |KtToken| can be expanded (e.g. was not preceded by
   *     \noexpand).
   */
  public void backUp(KtToken tok, boolean exp) {
    push(new KtBackedToken(tok, exp));
  }

  private KtInpTokChecker checker = KtInpTokChecker.NULL;

  public KtInpTokChecker setChecker(KtInpTokChecker chk) {
    KtInpTokChecker old = checker;
    checker = chk;
    return old;
  }

  private final KtBoolPar CAN_EXPAND = new KtBoolPar();

  /**
   * Gives the next |KtToken| from the |KtTokenizer| stack. It calls the |nextToken| method of the
   * |KtTokenizer| on the top of the stack. If the |KtTokenizer| on top finishes (returns |KtToken.NULL|)
   * it is poped out and the next stack top |KtTokenizer| is asked. When the stack is empty it returns
   * |KtToken.NULL| to indicate that the whole stack is finished.
   *
   * @param canExpand boolean output parameter querying whether the acquired |KtToken| can be expanded
   *     (e.g. was not preceded by \noexpand).
   * @return the next |KtToken| or |KtToken.NULL| if the stack is finished.
   */
  public synchronized KtToken nextToken(KtBoolPar canExpand) {
    KtToken tok;
    if (canExpand == KtBoolPar.NULL) canExpand = CAN_EXPAND;
    while (top != KtTokenizer.NULL) {
      if ((tok = top.nextToken(canExpand)) != KtToken.NULL)
        return checker != KtInpTokChecker.NULL ? checker.checkToken( tok, canExpand) : tok;
      if (pop().close() && checker != KtInpTokChecker.NULL) checker.checkEndOfFile();
    }
    canExpand.set(false);
    return KtToken.NULL;
  }

  public synchronized void close() {
    while (top != KtTokenizer.NULL) pop().close();
  }

  public synchronized void cleanFinishedLists() {
    while (top != KtTokenizer.NULL && top.finishedList()) pop().close();
  }

  public synchronized void cleanFinishedInserts() {
    while (top != KtTokenizer.NULL && top.finishedInsert()) pop().close();
  }

  public synchronized void endTopmostInput() {
    for (KtTokenizer curr = top; curr != KtTokenizer.NULL; curr = curr.getNext())
      if (curr.endInput()) break;
  }

  public synchronized KtFilePos filePos() {
    KtFilePos pos = KtFilePos.NULL;
    for (KtTokenizer curr = top; curr != KtTokenizer.NULL; curr = curr.getNext()) {
      pos = curr.filePos();
      if (pos != KtFilePos.NULL) break;
    }
    return pos;
  }

  public int lineNumber() {
    KtFilePos pos = filePos();
    return pos != KtFilePos.NULL ? pos.line : 0;
  }

  public synchronized void show(KtContextDisplay display) {
    int lines = display.lines();
    boolean dots = lines > 0;
    KtTokenizer curr = top;
    if (curr != KtTokenizer.NULL)
      for (; ; curr = curr.getNext()) {
        boolean bottom = curr.enoughContext() || curr.getNext() == KtTokenizer.NULL;
        boolean force = bottom || curr == top; // SSS
        if (force || lines > 0) {
          display.reset();
          lines -= curr.show(display, force, lines);
        } else if ( dots ) {
          display.normal().startLine().add("...");
          dots = false;
        }
        if (bottom) break;
      }
  }
}

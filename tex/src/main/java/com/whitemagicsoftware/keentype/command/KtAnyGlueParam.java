// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.AnyGlueParam
// $Id: KtAnyGlueParam.java,v 1.1.1.1 2001/02/25 22:02:37 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtLog;

/** */
public abstract class KtAnyGlueParam extends KtPerformableParam {

  private KtGlue value;

  /**
   * Creates a new KtAnyGlueParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtAnyGlueParam
   * @param val the value of the KtAnyGlueParam
   */
  public KtAnyGlueParam(String name, KtGlue val) {
    super(name);
    value = val;
  }

  /**
   * Creates a new KtAnyGlueParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtAnyGlueParam
   */
  public KtAnyGlueParam(String name) {
    this(name, KtGlue.ZERO);
  }

  public final Object getEqValue() {
    return value;
  }

  public final void setEqValue(Object val) {
    value = (KtGlue) val;
  }

  public final void addEqValueOn(KtLog log) {
    log.add(value.toString(getUnit()));
  }

  public abstract String getUnit();

  public final KtGlue get() {
    return value;
  }

  public void set(KtGlue val, boolean glob) {
    beforeSetting(glob);
    value = val;
  }

  /* STRANGE
   * only for making glue in a paragraph finite
   */
  public void makeShrinkFinite() {
    value = value.withFiniteShrink();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.InputLineTokenizer
// $Id: KtInputLineTokenizer.java,v 1.1.1.1 2000/01/27 14:31:13 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtInputLine;
import com.whitemagicsoftware.keentype.io.KtName;

/**
 * The |KtInputLineTokenizer| reads internal character codes from a |LineReader| and converts them
 * into |KtToken|s.
 */
public class KtInputLineTokenizer extends KtTokenizer {

  public static final KtInputLineTokenizer NULL = null;

  /** The current input line */
  private final KtInputLine line;

  /** The parametrization object */
  private final KtTokenMaker maker;

  private final String desc;

  /**
   * Creates an |KtInputLineTokenizer| with given input and parametrization object.
   *
   * @param line the underlying reader.
   * @param maker the parametrization object.
   */
  public KtInputLineTokenizer(KtInputLine line, KtTokenMaker maker, String desc) {
    this.line = line;
    this.maker = maker;
    this.desc = desc;
  }

  /** Value of |state| when the scanner is on the begining of line */
  private static final byte NEW_LINE = 0;

  /** Value of |state| when the scanner is in the middle of line */
  private static final byte MID_LINE = 1;

  /** Value of |state| when the scanner is ignoring blank spaces */
  private static final byte SKIP_BLANKS = 2;

  /** The state of the scanner. Can be |NEW_LINE| or |MID_LINE| or |SKIP_BLANKS|. */
  private byte state = NEW_LINE;

  public void setMidLine() {
    state = MID_LINE;
  }

  /**
   * Gets the next |KtToken| from the input line.
   *
   * @param canExpand boolean output parameter querying whether the acquired |KtToken| can be expanded
   *     (e.g. was not preceded by \noexpand).
   * @return the next token from the input or |KtToken.NULL| if the input is finished.
   */
  public KtToken nextToken( final KtBoolPar canExpand ) {
    canExpand.set( true );
    return nextToken();
  }

  /**
   * The scanning category indicating that the corresponding character is the escape character ---
   * i.e. it starts a control sequence.
   */
  public static final byte ESCAPE = 0;

  /**
   * The scanning category indicating that the coresponding character can be a part of multi-letter
   * control sequence.
   */
  public static final byte LETTER = 1;

  /**
   * The scanning category indicating that the coresponding character is a blank space --- i.e. the
   * following characters of the same category are ignored.
   */
  public static final byte SPACER = 2;

  /**
   * The scanning category similar to |SPACER|, only consequent characters of this category generate
   * blank lines (paragraph ends).
   */
  public static final byte ENDLINE = 3;

  /**
   * The scanning category indicating that the corresponding character and the rest of line should
   * be ignored.
   */
  public static final byte COMMENT = 4;

  /** The scanning category indicating that the corresponding character should be ignored. */
  public static final byte IGNORE = 5;

  /**
   * The scanning category indicating that no special processing should be taken for corresponding
   * character.
   */
  public static final byte OTHER = 6;

  /**
   * A |KtTokenMaker| is used for parametrization of tokenization process. It provides parameters
   * which are dependent on current state of the language interpreter.
   */
  public interface KtTokenMaker {

    /**
     * Gives the scanning category of given internal character code. The return value must be one
     * of: |ESCAPE|, |LETTER|, |EXPAND|, |SPACER|, |ENDLINE|, |COMMENT|, |IGNORE|, |OTHER|.
     *
     * @param code internal character code.
     * @return scanning category of |code|.
     */
    byte scanCat(KtCharCode code);

    /**
     * Makes a control sequence |KtToken| with given name.
     *
     * @param name the name of the control sequence.
     * @return the control sequence |KtToken|.
     */
    KtToken make(KtName name);

    /**
     * Makes a character |KtToken| for given internal character code.
     *
     * @param code the internal character code.
     * @return the character |KtToken|.
     */
    KtToken make(KtCharCode code);

    /**
     * Makes a |KtToken| corresponding to space.
     *
     * @return the space |KtToken|.
     */
    KtToken makeSpace();

    /**
     * makes a |KtToken| corresponding to blank line (paragraph end).
     *
     * @return the paragraph |KtToken|.
     */
    KtToken makePar();
  }

  /**
   * Gives the |KtToken| constructed from the next internal character code read from input line or
   * from a sequence of such codes.
   *
   * @return the next token from the input line or |KtToken.NULL| if the input line is finished.
   */
  public KtToken nextToken() {
    for (; ; ) {
      KtCharCode code = line.getNext();
      if (code == KtInputLine.EOL) return KtToken.NULL;
      else
        switch (maker.scanCat(code)) {
          case ESCAPE:
            code = line.getNext();
            if (code == KtInputLine.EOL) {
              /* empty control sequence */
              return maker.make(new KtName());
            } else {
              int cat = maker.scanCat(code);
              if (cat != LETTER) {
                /* non letter control sequence */
                state = cat == SPACER ? SKIP_BLANKS : MID_LINE;
                return maker.make(new KtName(code));
              } else {
                /* multiletter control sequence */
                KtName.KtBuffer buf = new KtName.KtBuffer();
                buf.append(code);
                while ((code = line.peekNext()) != KtInputLine.EOL && maker.scanCat(code) == LETTER) {
                  buf.append(code);
                  line.getNext();
                }
                state = SKIP_BLANKS;
                return maker.make(buf.toName());
              }
            }
            // break;	/* unreachable statement */
          case ENDLINE:
            line.skipAll();
            if (state == MID_LINE) return maker.makeSpace();
            else if (state == NEW_LINE) return maker.makePar();
            break;
          case SPACER:
            if (state == MID_LINE) {
              state = SKIP_BLANKS;
              return maker.makeSpace();
            }
            break;
          case COMMENT:
            line.skipAll();
            break;
          case IGNORE:
            break;
          default:
            state = MID_LINE;
            return maker.make(code);
        }
    }
  }

  public boolean finished() {
    return line.empty();
  }

  public boolean finishedInsert() {
    return line.empty();
  }

  public int show( KtContextDisplay disp, boolean force, int lines ) {
    disp.normal().startLine().add( desc );
    line.addContext( disp.left(), disp.right() );
    disp.show();
    return 1;
  }
}

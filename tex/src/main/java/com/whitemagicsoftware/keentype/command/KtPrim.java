// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.Prim
// $Id: KtPrim.java,v 1.1.1.1 2001/03/13 09:24:23 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;

/**
 * KtPrim command of the macro language. It can be invoked by its name using control sequence KtToken.
 */
public abstract class KtPrim extends KtCommand implements KtPrimitive {

  /** The name of the primitive */
  private final String name;

  /**
   * Creates a new KtPrim with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtPrim
   */
  protected KtPrim(String name) {
    this.name = name;
  }

  public final String getName() {
    return name;
  }

  public final KtCommand getCommand() {
    return this;
  }

  public final void addOn(KtLog log) {
    log.addEsc(name);
  }

  public final String toString() {
    return "@" + name;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static final int ADVANCE = 0;
  public static final int MULTIPLY = 1;
  public static final int DIVIDE = 2;

  protected static KtNum performFor(KtNum val, int operation) {
    scanKeyword("by");
    int i = scanInt();
    try {
      switch( operation ) {
        case ADVANCE -> val = val.plus( i );
        case MULTIPLY -> val = val.times( i );
        case DIVIDE -> val = val.over( i );
      }
    } catch (ArithmeticException e) {
      val = KtNum.ZERO;
      error("ArithOverflow");
    }
    // XXXX arith_error
    return val;
  }

  protected static KtDimen performFor(KtDimen val, int operation) {
    scanKeyword("by");
    try {
      switch( operation ) {
        case ADVANCE -> val = val.plus( scanDimen() );
        case MULTIPLY -> val = val.times( scanInt() );
        case DIVIDE -> val = val.over( scanInt() );
      }
    } catch (ArithmeticException e) {
      val = KtDimen.ZERO;
      error("ArithOverflow");
    }
    // XXXX arith_error
    return val;
  }

  protected static KtGlue performFor(KtGlue val, int operation, boolean mu) {
    scanKeyword("by");
    try {
      switch( operation ) {
        case ADVANCE -> val = val.plus( scanGlue( mu ) );
        case MULTIPLY -> val = val.times( scanInt() );
        case DIVIDE -> val = val.over( scanInt() );
      }
    } catch (ArithmeticException e) {
      val = KtGlue.ZERO;
      error("ArithOverflow");
    }
    // XXXX arith_error
    return val;
  }

  public static int scanAnyCode(int par, String err) {
    int max = getConfig().getIntParam(par);
    int i = scanInt();
    if (i < 0 || i > max) {
      error(err, num(i), num(0), num(max));
      return 0;
    }
    return i;
  }

  public static final int INTP_MAX_REG_CODE = newIntParam();
  public static final int INTP_MAX_FILE_CODE = newIntParam();
  public static final int INTP_MAX_CHAR_CODE = newIntParam();

  /* TeXtp[433] */
  public static int scanRegisterCode() {
    return scanAnyCode(INTP_MAX_REG_CODE, "BadRegister");
  }

  /* TeXtp[435] */
  public static int scanFileCode() {
    return scanAnyCode(INTP_MAX_FILE_CODE, "BadFileNum");
  }

  /* TeXtp[434] */
  public static int scanCharacterCode() {
    return scanAnyCode(INTP_MAX_CHAR_CODE, "BadCharCode");
  }

  public static KtTokenList scanTokenList(KtLoggable src, boolean xpand) {
    KtTokenList.KtBuffer buf = new KtTokenList.KtBuffer(30);
    KtInpTokChecker savedChk =
        setTokenChecker(new KtScanToksChecker("OuterInToks", "EOFinToks", "text", buf, src));
    scanLeftBrace();
    for (int balance = 1; ; ) {
      KtToken tok = nextScannedToken(xpand, buf);
      if (tok != KtToken.NULL) {
        if (tok.matchLeftBrace()) ++balance;
        else if (tok.matchRightBrace() && --balance == 0) break;
        buf.append(tok);
      }
    }
    setTokenChecker(savedChk);
    return buf.toTokenList();
  }

  public static KtToken nextScannedToken(boolean xpand, KtTokenList.KtBuffer buf) {
    return xpand ? nextExpToken( buf) : nextRawToken();
  }
}

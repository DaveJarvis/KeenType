// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;
import java.util.HashMap;
import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtMaxLoggable;

public class KtReadPrim extends KtAssignPrim {

  private transient HashMap<Integer, KtReadInput> table;

  private void initTable() {
    table = new HashMap<>( 64 );
  }

  public KtReadPrim(String name) {
    super(name);
    initTable();
  }

  @Serial
  private void readObject( ObjectInputStream input )
    throws IOException, ClassNotFoundException {
    input.defaultReadObject();
    initTable();
  }

  public KtReadInput get(int num) {
    return table.get( num );
  }

  private KtReadInput replace(int num, KtReadInput input) {
    return table.put( num, input);
    // XXX remove if NULL
  }

  public void set(int num, KtReadInput input) {
    KtReadInput old = replace(num, input);
    if (old != input && old != KtReadInput.NULL) old.close();
  }

  public boolean eof(int num) {
    return get( num) == KtReadInput.NULL;
  }

  private final KtToken FROZEN_END_READ = new KtFrozenToken("endread", KtCommand.NULL);

  /* TeXtp[1225,482] */
  protected void assign(KtToken src, boolean glob) {
    KtBoolPar canExpand = new KtBoolPar();
    int num = scanInt();
    if (!scanKeyword("to")) error("MissingToForRead");
    KtToken def = definableToken();
    KtMacroBody.KtBuffer buf = new KtMacroBody.KtBuffer();
    KtInpTokChecker savedChk = setTokenChecker(new KtReadToksChecker(buf, def));
    KtReadInput input = get(num);
    int balance = 0;
    int ln = 0;
    if (input == KtReadInput.NULL) input = getIOHandler().defaultRead(num);
    do {
      getTokStack().push(new KtSentinelToken(FROZEN_END_READ));
      KtTokenizer tokenizer = input.nextTokenizer(def, ln++);
      if (tokenizer != KtTokenizer.NULL) getTokStack().push(tokenizer);
      else {
        set(num, KtReadInput.NULL);
        tokenizer = input.emptyLineTokenizer();
        getTokStack().push(tokenizer);
        if (balance > 0) {
          runAway("definition", buf);
          error("EOFinRead", esc("read"));
          balance = 0;
        }
      }
      for (; ; ) {
        KtToken tok = nextRawToken();
        if (tok == FROZEN_END_READ) break;
        if (balance >= 0) {
          if (tok.matchLeftBrace()) ++balance;
          else if (tok.matchRightBrace() && --balance < 0) continue;
          /* STRANGE
           * Note that we could pop the current tokenizer off the
           * stack (and insertions above it) if the balance
           * become < 0, but in such case we would loose the
           * errors 'Forbidden control sequence found ...'
           * past the unmatched right brace token.
           */
          buf.append(tok);
        }
      }
      getTokStack().dropPop();
    } while (balance > 0);
    setTokenChecker(savedChk);
    def.define(new KtMacro(buf.toMacroBody(), 0), glob);
  }

  static class KtSentinelToken extends KtTokenizer { // XXX compare to KtWritePrim

    private final KtToken token;
    private boolean readed = false;

    public KtSentinelToken(KtToken tok) {
      token = tok;
    }

    public KtToken nextToken(KtBoolPar canExpand) {
      canExpand.set(false);
      if (readed) return KtToken.NULL;
      readed = true;
      return token;
    }

    public boolean finishedList() {
      return readed;
    }

    public int show(KtContextDisplay disp, boolean force, int lines) {
      return 0;
    }
  }

  static class KtReadToksChecker extends KtScanToksChecker {

    public KtReadToksChecker(KtMaxLoggable list, KtLoggable src) {
      super("OuterInDef", "EOFinDef", "definition", list, src);
    }

    protected void handleOuterToken(KtToken tok) {}
  }
}


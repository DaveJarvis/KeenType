// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ActiveCharToken
// $Id: KtActiveCharToken.java,v 1.1.1.1 2000/02/16 11:00:31 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtActiveCharToken extends KtCharToken {

  public static final KtMaker MAKER =
      new KtMaker() {
        public KtToken make(KtCharCode code) {
          return new KtActiveCharToken(code);
        }
      };

  public KtActiveCharToken(KtCharCode code) {
    super(code);
  }

  public interface KtMeaninger {
    KtCommand get(KtCharCode code);

    void set(KtCharCode code, KtCommand cmd, boolean glob);
  }

  protected static KtMeaninger meaninger;

  public static void setMeaninger(KtMeaninger m) {
    meaninger = m;
  }

  /**
   * Gives KtCommand associated in table of equivalents.
   *
   * @return the KtCommand to interpret this token.
   */
  public KtCommand meaning() {
    return meaninger.get(code);
  }

  /**
   * Tells that the meaning of the |KtToken| can be redefined.
   *
   * @return |true|.
   */
  public boolean definable() {
    return true;
  }

  /**
   * Define given |KtCommand| to be equivalent in table of equivalents.
   *
   * @param cmd the object to interpret this token.
   * @param glob if |true| the equivalent is defined globaly.
   */
  public void define(KtCommand cmd, boolean glob) {
    meaninger.set(code, cmd, glob);
  }

  /**
   * Gives |KtCharCode.NULL| to indicate that this |KtToken| is an active character |KtToken| and is not
   * willing to provide |KtCharCode| upon this method call.
   *
   * @return |KtCharCode.NO_CHAR|.
   */
  public KtCharCode nonActiveCharCode() {
    return KtCharCode.NULL;
  }

  public boolean match(KtCharToken tok) {
    return tok instanceof KtActiveCharToken && tok.match( code);
  }

  public KtMaker getMaker() {
    return MAKER;
  }

  public String toString() {
    return "<ActiveChar: " + code + '>';
  }
}

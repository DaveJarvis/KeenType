// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.OperatorPrim
// $Id: KtOperatorPrim.java,v 1.1.1.1 1999/05/31 11:18:50 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtOperatorPrim extends KtAssignPrim {

  private final int operation;

  public KtOperatorPrim(String name, int operation) {
    super(name);
    this.operation = operation;
  }

  protected void assign(KtToken src, boolean glob) {
    KtToken tok = nextExpToken();
    meaningOf(tok).perform(operation, glob, this);
  }
}

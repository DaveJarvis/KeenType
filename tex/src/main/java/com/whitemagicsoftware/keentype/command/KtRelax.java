// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.Relax
// $Id: KtRelax.java,v 1.1.1.1 2000/03/17 14:16:45 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtRelax extends KtCommand {

  public static final String NAME = "relax";

  public final boolean isRelax() {
    return true;
  }

  public final void exec(KtToken src) {}

  public final void exec(KtToken src, int prefixes) {
    KtToken tok = nextExpToken();
    meaningOf(tok).execute(tok, prefixes);
  }

  public final void addOn(KtLog log) {
    log.addEsc(NAME);
  }

  private static KtRelax relax;

  public static KtRelax getRelax() {
    return relax;
  }

  public static void makeStaticData() {
    relax = new KtRelax();
  }

  public static void writeStaticData(ObjectOutputStream output) throws IOException {
    output.writeObject(relax);
  }

  public static void readStaticData(ObjectInputStream input)
      throws IOException, ClassNotFoundException {
    relax = (KtRelax) input.readObject();
  }

  public String toString() {
    return sameAs(KtExpandable.getUnExpanded()) ? "@RELAX" : "@relax";
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.NumParam
// $Id: KtNumParam.java,v 1.1.1.1 1999/06/24 10:37:26 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtIntProvider;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtLog;

/** Setting number parameter primitive. */
public class KtNumParam extends KtPerformableParam implements KtIntProvider, KtNum.KtProvider {

  private KtNum value;

  /**
   * Creates a new KtNumParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtNumParam
   * @param val the value of the KtNumParam
   */
  public KtNumParam(String name, KtNum val) {
    super(name);
    value = val;
  }

  public KtNumParam(String name, int val) {
    this(name, KtNum.valueOf(val));
  }

  /**
   * Creates a new KtNumParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtNumParam
   */
  public KtNumParam(String name) {
    this(name, KtNum.ZERO);
  }

  public final Object getEqValue() {
    return value;
  }

  public final void setEqValue(Object val) {
    value = (KtNum) val;
  }

  public final void addEqValueOn(KtLog log) {
    log.add(value.toString());
  }

  public KtNum get() {
    return value;
  }

  public void set(KtNum val, boolean glob) {
    beforeSetting(glob);
    value = val;
  }

  public final int intVal() {
    return get().intVal();
  }

  protected void scanValue(KtToken src, boolean glob) {
    set(scanNum(), glob);
  }

  protected void perform(int operation, boolean glob) {
    set(performFor(get(), operation), glob);
  }

  public boolean hasNumValue() {
    return true;
  }

  public KtNum getNumValue() {
    return get();
  }
}

// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;

/** Base abstract class for all |KtTokenizer|s. */
public abstract class KtTokenizer {

  public static final KtTokenizer NULL = null;

  /** The stack where this instance is pushed to */
  private KtTokenizerStack stack = null;

  /** The next |KtTokenizer| in the stack (the one under this one) */
  private KtTokenizer next = NULL;

  /**
   * Gets the stack of |KtTokenizer|s where this |KtTokenizer| is pushed.
   *
   * @return the stack which this |KtTokenizer| belongs to or |null| if this |KtTokenizer| is not
   *     pushed.
   */
  public KtTokenizerStack getStack() {
    return stack;
  }

  public KtTokenizer getNext() {
    return next;
  }

  synchronized void pushTo(KtTokenizerStack stack, KtTokenizer next) {
    if (this.stack != null) throw new RuntimeException("KtTokenizer is already in a stack");
    if( this != stack.getTop() ) // SSS
    { throw new RuntimeException( "Misused KtTokenizer.pushTo()" ); }
    this.stack = stack;
    this.next = next;
  }

  synchronized void popFrom(KtTokenizerStack stack) {
    if( this.stack != stack || this.next != stack.getTop() ) // SSS
    { throw new RuntimeException( "Misused KtTokenizer.popFrom()" ); }
    this.stack = null;
    this.next = null;
  }

  /**
   * Gets the next |KtToken| from this |KtTokenizer|.
   *
   * @param canExpand boolean output parameter querying whether the acquired |KtToken| can be expanded
   *     (e.g. was not preceded by \noexpand).
   * @return the next |KtToken| or |KtToken.NULL| if this |KtTokenizer| is finished.
   */
  public abstract KtToken nextToken(KtBoolPar canExpand);

  /** Should be overriden by a subclass in order to release resources used by this |KtTokenizer|. */
  public boolean close() {
    return false;
  }

  public boolean finishedList() {
    return false;
  }

  public boolean finishedInsert() {
    return false;
  }

  public boolean finished() {
    return finishedList();
  }

  public boolean endInput() {
    return false;
  }

  public boolean enoughContext() {
    return false;
  }

  public KtFilePos filePos() {
    return KtFilePos.NULL;
  }

  public abstract int show(KtContextDisplay disp, boolean force, int lines);
}

// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public abstract class KtCondPrim extends KtExpandablePrim {

  public static final int NOTHING = 0;
  public static final int WAIT = 1;
  public static final int FI = 2;
  public static final int ELSE = 3;
  public static final int OR = 4;

  protected static boolean conforms(int endLev, int limit) {
    return endLev <= limit;
  }

  protected static boolean closesBranch(int endLev) {
    return endLev > WAIT;
  }

  public KtCondPrim(String name) {
    super(name);
  }

  protected static final class KtCondEntry {
    public static final KtCondEntry NULL = null;

    public final KtCondEntry next;
    public int limit;
    public final KtExpandable curr;
    public final KtFilePos fpos;

    KtCondEntry(KtCondEntry next, int limit, KtExpandable curr, KtFilePos fpos) {
      this.next = next;
      this.limit = limit;
      this.curr = curr;
      this.fpos = fpos;
    }
  }

  private static KtCondEntry condTop = KtCondEntry.NULL;

  protected static KtCondEntry pushCond(int limit, KtExpandable curr) {
    KtFilePos fpos = getTokStack().filePos();
    return condTop = new KtCondEntry(condTop, limit, curr, fpos);
  }

  protected static void popCond() {
    condTop = condTop.next;
  }

  protected static KtCondEntry topCond() {
    return condTop;
  }

  protected static boolean noCond() {
    return condTop == KtCondEntry.NULL;
  }

  /* TeXtp[1335] */
  public static void cleanUp() {
    while (condTop != KtCondEntry.NULL) {
      normLog.startLine().add('(').addEsc("end").add(" occurred when ");
      condTop.curr.addExpandable(normLog);
      if (condTop.fpos != KtFilePos.NULL) normLog.add(" on line ").add(condTop.fpos.line);
      normLog.add(" was incomplete)");
      popCond();
    }
  }

  /* TeXtp[494] */
  protected static int skipBranch() {
    int endLev;
    KtBoolPar exp = new KtBoolPar();
    KtLoggable[] params = {exp(condTop.curr), num(currLineNumber())};
    KtInpTokChecker savedChk = setTokenChecker(new KtSkipToksChecker(params));
    for (int level = 0; ; ) {
      KtToken tok = nextRawToken(exp);
      KtCommand cmd = meaningOf(tok, exp.get());
      endLev = cmd.endBranchLevel();
      if (closesBranch(endLev)) {
        if (level == 0) break;
        if (endLev == FI) --level;
      } else if (cmd.isConditional()) ++level;
    }
    setTokenChecker(savedChk);
    return endLev;
  }

  static class KtSkipToksChecker implements KtInpTokChecker {

    private final KtLoggable[] params;

    public KtSkipToksChecker(KtLoggable[] params) {
      this.params = params;
    }

    public KtToken checkToken(KtToken tok, KtBoolPar canExpand) {
      if( meaningOf( tok, canExpand.get() ).isOuter() ) {
        backTokenWithoutCleaning( tok );
        reportError( "OuterInSkipped" );
        canExpand.set( true );
        return KtSpaceToken.TOKEN;
      }

      return tok;
    }

    public void checkEndOfFile() {
      reportError("EOFinSkipped");
    }

    protected void reportError( String ident ) {
      insertToken( getConfig().frozenFi() );
      nonDelError( ident, params );
    }
  }
}


// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.Expandable
// $Id: KtExpandable.java,v 1.1.1.1 2000/03/17 13:56:25 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public abstract class KtExpandable extends KtRelax {

  public static final int BOOLP_TRACING_ALL_COMMANDS = newBoolParam();

  public final boolean expandable() {
    return true;
  }

  public abstract void doExpansion(KtToken src);

  public abstract void addExpandable(KtLog log, boolean full);

  private static KtRelax unExpanded;

  public static KtRelax getUnExpanded() {
    return unExpanded;
  }

  public final KtCommand meaning(boolean expOK) {
    return expOK ? this : unExpanded;
  }

  public static void makeStaticData() {
    unExpanded = new KtRelax();
  }

  public static void writeStaticData(ObjectOutputStream output) throws IOException {
    output.writeObject(unExpanded);
  }

  public static void readStaticData(ObjectInputStream input)
      throws IOException, ClassNotFoundException {
    unExpanded = (KtRelax) input.readObject();
  }

  protected static KtLoggable exp(final KtExpandable e) {
    return new KtLoggable() {
      public void addOn(KtLog log) {
        e.addExpandable(log, false);
      }
    };
  }
}

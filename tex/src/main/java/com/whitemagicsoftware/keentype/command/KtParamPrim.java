// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.ParamPrim
// $Id: KtParamPrim.java,v 1.1.1.1 1999/06/17 11:33:12 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtEqTable;
import com.whitemagicsoftware.keentype.io.KtEqTraceable;
import com.whitemagicsoftware.keentype.io.KtLog;

/** Abstract ancestor of each parameter primitive. */
public abstract class KtParamPrim extends KtAssignPrim implements KtEqTable.KtExtEquiv, KtEqTraceable {

  private int eqLevel = 0;

  /**
   * Creates a new KtParamPrim with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtParamPrim
   */
  protected KtParamPrim(String name) {
    super(name);
  }

  public final int getEqLevel() {
    return eqLevel;
  }

  public final void setEqLevel(int lev) {
    eqLevel = lev;
  }

  public final void addEqDescOn(KtLog log) {
    log.addEsc(getName());
  }

  public final void retainEqValue() {
    traceRestore(RETAINING, this);
  }

  public final void restoreEqValue(Object val) {
    setEqValue(val);
    traceRestore(RESTORING, this);
  }

  public abstract Object getEqValue();

  public abstract void setEqValue(Object val);

  public abstract void addEqValueOn(KtLog log);

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  protected final void assign(KtToken src, boolean glob) {
    skipOptEquals();
    scanValue(src, glob);
  }

  protected abstract void scanValue(KtToken src, boolean glob);

  protected final void beforeSetting(boolean glob) {
    getEqt().beforeSetting(this, glob);
  }
}

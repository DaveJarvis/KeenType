// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.Undefined
// $Id: KtUndefined.java,v 1.1.1.1 2000/03/17 14:00:38 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtUndefined extends KtExpandable {

  public static final String NAME = "undefined";

  /* TeXtp[367] */
  public final void doExpansion(KtToken src) {
    if (getConfig().getBoolParam(BOOLP_TRACING_ALL_COMMANDS)) traceExpandable(this);
    error("UndefinedToken");
  }

  public final String toString() {
    return "@" + NAME;
  }

  public final void addExpandable(KtLog log, boolean full) {
    log.add(NAME);
  }

  private static KtUndefined undefined;

  public static KtUndefined getUndefined() {
    return undefined;
  }

  public static void makeStaticData() {
    undefined = new KtUndefined();
  }

  public static void writeStaticData(ObjectOutputStream output) throws IOException {
    output.writeObject(undefined);
  }

  public static void readStaticData(ObjectInputStream input)
      throws IOException, ClassNotFoundException {
    undefined = (KtUndefined) input.readObject();
  }
}

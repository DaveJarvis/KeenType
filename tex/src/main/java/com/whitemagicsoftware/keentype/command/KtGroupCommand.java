// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.GroupCommand
// $Id: KtGroupCommand.java,v 1.1.1.1 2000/04/16 10:26:00 ksk Exp $
package com.whitemagicsoftware.keentype.command;

import com.whitemagicsoftware.keentype.base.KtClassAssoc;

public abstract class KtGroupCommand extends KtCommand {

  private static final KtClassAssoc assoc = new KtClassAssoc( KtGroup.class);

  public static void registerGroup(Class grp) {
    assoc.record(grp);
  }

  public final KtGroupCommand defineClosing(Class grp, KtClosing clos) {
    assoc.put(grp, this, clos);
    return this;
  }

  public final KtClosing getClosing(Class grp) {
    return (KtClosing) assoc.get(grp, this);
  }

  public final void exec(KtToken src) {
    KtGroup grp = getGrp();
    KtClosing clos = getClosing(grp.getClass());
    if (clos == KtClosing.NULL) exec(grp, src);
    else clos.exec(grp, src);
  }

  public abstract void exec(KtGroup grp, KtToken src);
}

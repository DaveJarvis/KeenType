// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.command.IfCatPrim
// $Id: KtIfCatPrim.java,v 1.1.1.1 1999/05/31 11:18:47 ksk Exp $
package com.whitemagicsoftware.keentype.command;

public class KtIfCatPrim extends KtAnyIfPrim {

  public KtIfCatPrim(String name) {
    super(name);
  }

  protected final boolean holds() {
    KtToken first = getTok();
    KtToken second = getTok();
    return first != KtToken.NULL
        ? second != KtToken.NULL && first.sameCatAs( second)
        : second == KtToken.NULL;
  }

  private KtToken getTok() {
    KtToken tok = nextExpToken();
    KtCommand cmd = meaningOf(tok);
    return cmd.expandable() ? tok.category() : cmd.origin();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tfm.TeXFontMetric
// $Id: KtTeXFontMetric.java,v 1.1.1.1 2001/02/09 12:36:21 ksk Exp $
package com.whitemagicsoftware.keentype.tfm;

import com.whitemagicsoftware.keentype.base.KtBinFraction;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.node.KtBaseFontMetric;
import com.whitemagicsoftware.keentype.node.KtBaseNode;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtChrKernNode;
import com.whitemagicsoftware.keentype.node.KtDiscretionaryNode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtSettingContext;
import com.whitemagicsoftware.keentype.node.KtTreatNode;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtWordBuilder;
import com.whitemagicsoftware.keentype.node.KtWordRebuilder;

public class KtTeXFontMetric extends KtBaseFontMetric {

  private static final int[] pTab = new int[NUMBER_OF_DIMEN_PARAMS];

  /* Each element is either initialised explicitly or it has a negative
   * value
   */

  static {
    for (int i = 0; i < pTab.length; pTab[i++] = -1)
      ;

    pTab[DIMEN_PARAM_SLANT] = KtTeXFm.FP_SLANT;
    pTab[DIMEN_PARAM_SPACE] = KtTeXFm.FP_SPACE;
    pTab[DIMEN_PARAM_STRETCH] = KtTeXFm.FP_STRETCH;
    pTab[DIMEN_PARAM_SHRINK] = KtTeXFm.FP_SHRINK;
    pTab[DIMEN_PARAM_X_HEIGHT] = KtTeXFm.FP_X_HEIGHT;
    pTab[DIMEN_PARAM_QUAD] = KtTeXFm.FP_QUAD;
    pTab[DIMEN_PARAM_EXTRA_SPACE] = KtTeXFm.FP_EXTRA_SPACE;
    pTab[DIMEN_PARAM_MATH_X_HEIGHT] = KtTeXMathSymFm.FP_MATH_X_HEIGHT;
    pTab[DIMEN_PARAM_MATH_QUAD] = KtTeXMathSymFm.FP_MATH_QUAD;
    pTab[DIMEN_PARAM_NUM1] = KtTeXMathSymFm.FP_NUM1;
    pTab[DIMEN_PARAM_NUM2] = KtTeXMathSymFm.FP_NUM2;
    pTab[DIMEN_PARAM_NUM3] = KtTeXMathSymFm.FP_NUM3;
    pTab[DIMEN_PARAM_DENOM1] = KtTeXMathSymFm.FP_DENOM1;
    pTab[DIMEN_PARAM_DENOM2] = KtTeXMathSymFm.FP_DENOM2;
    pTab[DIMEN_PARAM_SUP1] = KtTeXMathSymFm.FP_SUP1;
    pTab[DIMEN_PARAM_SUP2] = KtTeXMathSymFm.FP_SUP2;
    pTab[DIMEN_PARAM_SUP3] = KtTeXMathSymFm.FP_SUP3;
    pTab[DIMEN_PARAM_SUB1] = KtTeXMathSymFm.FP_SUB1;
    pTab[DIMEN_PARAM_SUB2] = KtTeXMathSymFm.FP_SUB2;
    pTab[DIMEN_PARAM_SUP_DROP] = KtTeXMathSymFm.FP_SUP_DROP;
    pTab[DIMEN_PARAM_SUB_DROP] = KtTeXMathSymFm.FP_SUB_DROP;
    pTab[DIMEN_PARAM_DELIM1] = KtTeXMathSymFm.FP_DELIM1;
    pTab[DIMEN_PARAM_DELIM2] = KtTeXMathSymFm.FP_DELIM2;
    pTab[DIMEN_PARAM_AXIS_HEIGHT] = KtTeXMathSymFm.FP_AXIS_HEIGHT;
    pTab[DIMEN_PARAM_DEFAULT_RULE_THICKNESS] = KtTeXMathExtFm.FP_DEFAULT_RULE_THICKNESS;
    pTab[DIMEN_PARAM_BIG_OP_SPACING1] = KtTeXMathExtFm.FP_BIG_OP_SPACING1;
    pTab[DIMEN_PARAM_BIG_OP_SPACING2] = KtTeXMathExtFm.FP_BIG_OP_SPACING2;
    pTab[DIMEN_PARAM_BIG_OP_SPACING3] = KtTeXMathExtFm.FP_BIG_OP_SPACING3;
    pTab[DIMEN_PARAM_BIG_OP_SPACING4] = KtTeXMathExtFm.FP_BIG_OP_SPACING4;
    pTab[DIMEN_PARAM_BIG_OP_SPACING5] = KtTeXMathExtFm.FP_BIG_OP_SPACING5;
  }

  public static int numberOfRawDimenPars() {
    return pTab.length;
  }

  public static int rawDimenParNumber(int idx) {
    return pTab[idx];
  }

  private final KtName name;
  private final KtTeXFm tfm;
  private final KtDimen atSize;
  private KtName ident;

  public KtTeXFontMetric(KtName name, KtTeXFm tfm, KtDimen atSize, KtName ident) {
    this.name = name;
    this.tfm = tfm;
    this.atSize = atSize;
    this.ident = ident;
    for (int i = 0; i < pTab.length; i++) if (pTab[i] >= 0) setTfmParam(i, pTab[i]);
  }

  /*
   * If a parameter is provided by underlying KtTeXFm we initialize the
   * corresponding dimension parameter in KtFontMetric. Otherwise we leave
   * it to be NULL.
   */

  private void setTfmParam(int i, int tfmIdx) {
    if (0 <= tfmIdx && tfmIdx < tfm.paramCount()) {
      KtFixWord param = tfm.getParam(tfmIdx);
      if (param != KtFixWord.NULL) {
        KtDimen val = tfmIdx == KtTeXFm.FP_SLANT ? KtDimen.valueOf( param) : atSize.times( param);
        setDimenParam(i, val);
      }
    }
  }

  public KtName getName() {
    return name;
  }

  public KtDimen getAtSize() {
    return atSize;
  }

  public KtName getIdent() {
    return ident;
  }

  public void setIdent(KtName ident) {
    this.ident = ident;
  }

  public int getCheckSum() {
    return tfm.getCheckSum();
  }

  public KtDimen getDesignSize() {
    return KtDimen.valueOf(tfm.getDesignSize());
  }

  /* TeXtp[1261] */
  public void addDescOn(KtLog log) {
    log.add(name);
    if (!atSize.equals(tfm.getDesignSize())) log.add(" at " + atSize + "pt");
  }

  /* TeXtp[134] */
  protected abstract class KtAnyNode extends KtBaseNode {
    /* corresponding to char_node, ligature_node */

    private final KtDimen width;
    private final KtDimen height;
    private final KtDimen depth;

    public KtAnyNode(KtDimen w, KtDimen h, KtDimen d) {
      width = w;
      height = h;
      depth = d;
    }

    public KtDimen getWidth() {
      return width;
    }

    public KtDimen getLeftX() {
      return KtDimen.ZERO;
    }

    public KtDimen getHeight() {
      return height;
    }

    public KtDimen getDepth() {
      return depth;
    }

    protected boolean allegedlyVisible() {
      return true;
    }

    public final KtFontMetric getFontMetric() {
      return KtTeXFontMetric.this;
    }

    public void addOn(KtLog log, KtCntxLog cntx) {
      getIdent().addEscapedOn(log);
      log.add(' ');
      finishAddOn(log);
    }

    public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
      // if (metric != KtTeXFontMetric.this) {	//SSS
      if (!KtTeXFontMetric.this.equals(metric)) {
        getIdent().addEscapedOn(log);
        log.add(' ');
        metric = KtTeXFontMetric.this;
      }
      finishShortlyAddOn(log);
      return metric;
    }

    public KtDimen getItalCorr() {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(getIdx());
      return info != KtTeXFm.KtCharInfo.NULL ? atSize.times( info.getItalic()) : KtDimen.NULL;
    }

    public abstract short getIdx();

    public abstract void finishAddOn(KtLog log);

    public abstract void finishShortlyAddOn(KtLog log);

    public boolean canBePartOfDiscretionary() {
      return true;
    }

    public boolean kernAfterCanBeSpared() {
      return true;
    }

    public KtFontMetric uniformMetric() {
      return KtTeXFontMetric.this;
    }

    public byte afterWord() {
      return SKIP;
    }
  }

  /* TeXtp[134] */
  protected class KtCharNode extends KtAnyNode {
    /* root corresponding to char_node */

    private final KtCharCode code;

    public KtCharNode(KtDimen w, KtDimen h, KtDimen d, KtCharCode c) {
      super(w, h, d);
      code = c;
    }

    public short getIdx() {
      return (short) code.toChar();
    }

    public void finishAddOn(KtLog log) {
      log.add(code);
    }

    public void finishShortlyAddOn(KtLog log) {
      log.add(code);
    }

    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      setter.set(code, KtTeXFontMetric.this);
    }

    public byte beforeWord() {
      return code.toCanonicalLetter() != KtCharCode.NO_CHAR ? SUCCESS : SKIP;
    }

    public boolean canBePartOfWord() {
      return code.toCanonicalLetter() != KtCharCode.NO_CHAR;
    }

    public void contributeCharCodes(KtName.KtBuffer buf) {
      buf.append(code);
    }

    public boolean providesRebuilder(boolean prev) {
      return true;
    }

    public KtWordRebuilder makeRebuilder(KtTreatNode proc, boolean prev) {
      return prev ? new KtLigKernBuilder( code, proc) : new KtLigKernBuilder( false, proc);
    }

    public String toString() {
      return "KtChar(" + code + ')';
    }
  }

  /* TeXtp[143] */
  protected class KtLigNode extends KtAnyNode {
    /* root corresponding to ligature_node */

    private final short index;
    private final KtName subst;
    private final boolean leftHit;
    private final boolean rightHit;

    public KtLigNode(KtDimen w, KtDimen h, KtDimen d, short i, KtName s, boolean lh, boolean rh) {
      super(w, h, d);
      index = i;
      subst = s;
      leftHit = lh;
      rightHit = rh;
    }

    public short getIdx() {
      return index;
    }

    public void finishAddOn(KtLog log) {
      log.add((char) index).add(" (ligature ");
      if (leftHit) log.add('|');
      log.add(subst);
      if (rightHit) log.add('|');
      log.add(')');
    }

    public void finishShortlyAddOn(KtLog log) {
      log.add(subst);
    }

    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      setter.set((char) index, KtTeXFontMetric.this);
    }

    public byte beforeWord() {
      return subst.length() > 0 && subst.codeAt(0).toCanonicalLetter() != KtCharCode.NO_CHAR
          ? SUCCESS
          : SKIP;
    }

    public boolean canBePartOfWord() {
      for (int i = 0; i < subst.length(); i++)
        if (subst.codeAt(i).toCanonicalLetter() == KtCharCode.NO_CHAR) return false;
      return true;
    }

    public void contributeCharCodes(KtName.KtBuffer buf) {
      buf.append(subst);
    }

    public boolean rightBoundary() {
      return rightHit;
    }

    public boolean providesRebuilder(boolean prev) {
      return true;
    }

    public KtWordRebuilder makeRebuilder(KtTreatNode proc, boolean prev) {
      return !prev && !leftHit
          ? new KtLigKernBuilder(false, proc)
          : !leftHit || prev && subst.length() > 0
              ? new KtLigKernBuilder(index, subst, leftHit, proc)
              : new KtLigKernBuilder(true, proc);
    }

    public String toString() {
      return "Lig(" + index + "; " + subst + "; " + leftHit + "; " + rightHit + ')';
    }
  }

  protected class KtLigKernBuilder extends KtTeXLigKernBuilder {

    protected final KtTreatNode proc;
    protected final boolean discs;
    protected final boolean zeroKerns;

    public KtLigKernBuilder(boolean leftBoundary, KtTreatNode proc, boolean discs, boolean zeroKerns) {
      super(leftBoundary);
      this.proc = proc;
      this.discs = discs;
      this.zeroKerns = zeroKerns;
    }

    public KtLigKernBuilder(boolean leftBoundary, KtTreatNode proc) {
      this(leftBoundary, proc, false, false);
    }

    private KtLigKernBuilder(KtCharCode code, KtTreatNode proc) {
      super(code);
      this.proc = proc;
      this.discs = false;
      this.zeroKerns = false;
    }

    private KtLigKernBuilder(short index, KtName subst, boolean leftHit, KtTreatNode proc) {
      super(index, subst, leftHit);
      this.proc = proc;
      this.discs = false;
      this.zeroKerns = false;
    }

    protected boolean exists(short index) {
      return tfm.getCharInfo( index) != KtTeXFm.KtCharInfo.NULL;
    }

    protected KtTeXFm.KtLigKern getLigKern(short left, short right) {
      return tfm.getLigKern(left, right);
    }

    protected void makeChar(KtCharCode code) {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo((short) code.toChar());
      if (info != KtTeXFm.KtCharInfo.NULL)
        proc.execute(
            new KtCharNode(
                atSize.times(info.getWidth()),
                atSize.times(info.getHeight()),
                atSize.times(info.getDepth()),
                code));
      makeDisc(code);
    }

    protected void makeLig(short lig, KtName subst, boolean lh, boolean rh) {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(lig);
      if (info != KtTeXFm.KtCharInfo.NULL)
        proc.execute(
            new KtLigNode(
                atSize.times(info.getWidth()),
                atSize.times(info.getHeight()),
                atSize.times(info.getDepth()),
                lig,
                subst,
                lh,
                rh));
      int l = subst.length();
      if (l > 0) makeDisc(subst.codeAt(l - 1));
    }

    protected void makeKern(KtBinFraction kern) {
      KtDimen dim = atSize.times(kern);
      if (zeroKerns || !dim.isZero()) proc.execute(new KtChrKernNode(dim));
    }

    private void makeDisc(KtCharCode last) {
      if (discs) {
        KtNum num = getNumParam(NUM_PARAM_HYPHEN_CHAR);
        if (num != KtNum.NULL && last.match(num)) proc.execute(KtDiscretionaryNode.EMPTY);
      }
    }

    protected KtNode makeChar(KtCharCode code, boolean larger) {
      return larger ? getLargerNode( code) : getCharNode( code);
    }

    protected KtNode makeLig(short lig, KtName subst, boolean lh, boolean rh, boolean larger) {
      throw new RuntimeException("larger ligatures not supported");
    }
  }

  protected class KtMathLigKernBuilder extends KtLigKernBuilder {

    public KtMathLigKernBuilder(KtTreatNode proc) {
      super(false, proc, false, true);
    }

    protected void makeLig(short lig, KtName subst, boolean lh, boolean rh) {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(lig);
      if (info != KtTeXFm.KtCharInfo.NULL) proc.execute(indexNode(lig, info));
      /*
          int		l = subst.length();
          if (l > 0) makeDisc(subst.codeAt(l - 1));
      */
    }

    protected KtNode makeLig(short lig, KtName subst, boolean lh, boolean rh, boolean larger) {
      KtNode node = KtNode.NULL;
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(lig);
      if (info != KtTeXFm.KtCharInfo.NULL)
        node = larger ? largerNode( lig, info) : indexNode( lig, info);
      return node;
    }
  }

  private short indexFrom(char chr) {
    return chr != KtCharCode.NO_CHAR ? (short) chr : KtTeXFm.NO_CHAR_CODE;
  }

  private KtTeXFm.KtCharInfo getCharInfo(short index) {
    return index == KtTeXFm.NO_CHAR_CODE ? KtTeXFm.KtCharInfo.NULL : tfm.getCharInfo( index);
  }

  private KtTeXFm.KtCharInfo getCharInfo(KtCharCode code) {
    char chr = code.toChar();
    return chr != KtCharCode.NO_CHAR ? tfm.getCharInfo( (short) chr) : KtTeXFm.KtCharInfo.NULL;
  }

  public KtNode getCharNode(KtCharCode code) {
    KtTeXFm.KtCharInfo info = getCharInfo(code);
    return info == KtTeXFm.KtCharInfo.NULL
        ? KtNode.NULL
        : new KtCharNode(
            atSize.times(info.getWidth()),
            atSize.times(info.getHeight()),
            atSize.times(info.getDepth()),
            code);
  }

  /*
      public KtBoxSizes	getCharSizes(KtCharCode code) {
  	KtTeXFm.KtCharInfo	info = getCharInfo(code);
  	return (info == KtTeXFm.KtCharInfo.NULL) ? KtBoxSizes.NULL
  	     : new KtBoxSizes(atSize.times(info.getHeight()),
  			    atSize.times(info.getWidth()),
  			    atSize.times(info.getDepth()), KtDimen.ZERO);
      }
  */

  public KtDimen getCharWidth(char chr) {
    KtTeXFm.KtCharInfo info = tfm.getCharInfo((short) chr);
    return info != KtTeXFm.KtCharInfo.NULL ? atSize.times( info.getWidth()) : KtDimen.NULL;
  }

  public KtWordBuilder getWordBuilder(KtTreatNode proc, boolean boundary, boolean discretionaries) {
    return new KtLigKernBuilder(boundary, proc, discretionaries, true);
  }

  public KtWordRebuilder getWordRebuilder(KtTreatNode proc, boolean boundary) {
    return new KtLigKernBuilder(boundary, proc);
  }

  public KtMathWordBuilder getMathWordBuilder(KtTreatNode proc) {
    return new KtMathLigKernBuilder(proc);
  }

  /*
   * possible optimization:
   * the nodes can be cached just one for each char, made on demand.
   * getCharSizes() is not necessary, cached node provides the same
   * information.
   */

  // XXX is KtCharNode necessary? Isn't KtIndexNode as good as it?
  /* TeXtp[134] */
  protected class KtIndexNode extends KtAnyNode {
    /* root corresponding to char_node */

    private final short index;

    public KtIndexNode(KtDimen w, KtDimen h, KtDimen d, short i) {
      super(w, h, d);
      index = i;
    }

    public short getIdx() {
      return index;
    }

    public void finishAddOn(KtLog log) {
      log.add((char) index);
    }

    public void finishShortlyAddOn(KtLog log) {
      log.add((char) index);
    }

    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      setter.set((char) index, KtTeXFontMetric.this);
    }

    public String toString() {
      return "Index(" + index + ')';
    }
  }

  /* TeXtp[749] */
  public KtNode getLargerNode(KtCharCode code) {
    KtNode node = KtNode.NULL;
    short index = indexFrom(code.toChar());
    if (index != KtTeXFm.NO_CHAR_CODE) {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(index);
      if (info != KtTeXFm.KtCharInfo.NULL) node = largerNode(index, info);
    }
    return node;
  }

  private KtNode largerNode(short index, KtTeXFm.KtCharInfo info) {
    short nextIndex = info.nextChar();
    if (nextIndex != KtTeXFm.NO_CHAR_CODE) {
      KtTeXFm.KtCharInfo nextInfo = tfm.getCharInfo(nextIndex);
      if (nextInfo != KtTeXFm.KtCharInfo.NULL) {
        index = nextIndex;
        info = nextInfo;
      }
    }
    return indexNode(index, info);
  }

  /* TeXtp[708,710] */
  public KtNode getSufficientNode(KtCharCode code, KtDimen desired) {
    short maxIndex = KtTeXFm.NO_CHAR_CODE;
    KtTeXFm.KtCharInfo maxInfo = KtTeXFm.KtCharInfo.NULL;
    KtDimen maxSize = KtDimen.ZERO;
    short index = indexFrom(code.toChar());
    while (index != KtTeXFm.NO_CHAR_CODE) {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(index);
      if (info == KtTeXFm.KtCharInfo.NULL) break;
      if (info.extRep() != KtTeXFm.NO_CHAR_CODE) return makeExt(info, desired);
      KtDimen size = heightPlusDepth(info);
      if (size.moreThan(maxSize)) {
        maxIndex = index;
        maxInfo = info;
        maxSize = size;
        if (!size.lessThan(desired)) break;
      }
      index = info.nextChar();
    }
    return maxInfo != KtTeXFm.KtCharInfo.NULL ? pretendingCharBox( maxIndex, maxInfo) : KtNode.NULL;
  }

  /* TeXtp[714,713,711] */
  private KtNode makeExt(KtTeXFm.KtCharInfo info, KtDimen size) {
    KtTeXFm.KtCharInfo topInfo = getCharInfo(info.extTop());
    KtTeXFm.KtCharInfo midInfo = getCharInfo(info.extMid());
    KtTeXFm.KtCharInfo botInfo = getCharInfo(info.extBot());
    KtTeXFm.KtCharInfo repInfo = getCharInfo(info.extRep());
    if (repInfo != KtTeXFm.KtCharInfo.NULL) {
      KtDimen total = KtDimen.ZERO;
      KtDimen rep = heightPlusDepth(repInfo);
      if (topInfo != KtTeXFm.KtCharInfo.NULL) total = total.plus(heightPlusDepth(topInfo));
      if (midInfo != KtTeXFm.KtCharInfo.NULL) {
        total = total.plus(heightPlusDepth(midInfo));
        rep = rep.times(2);
      }
      if (botInfo != KtTeXFm.KtCharInfo.NULL) total = total.plus(heightPlusDepth(botInfo));
      int cnt = 0;
      if (rep.moreThan(0))
        while (total.lessThan(size)) {
          total = total.plus(rep);
          cnt++;
        }
      KtNodeList list = new KtNodeList();
      KtNode repNode = pretendingCharBox(info.extRep(), repInfo);
      if (topInfo != KtTeXFm.KtCharInfo.NULL) list.append(pretendingCharBox(info.extTop(), topInfo));
      for (int i = cnt; i-- > 0; list.append(repNode))
        ;
      if (midInfo != KtTeXFm.KtCharInfo.NULL) {
        list.append(pretendingCharBox(info.extMid(), midInfo));
        for (int i = cnt; i-- > 0; list.append(repNode))
          ;
      }
      if (botInfo != KtTeXFm.KtCharInfo.NULL) list.append(pretendingCharBox(info.extBot(), botInfo));
      KtDimen height = list.isEmpty() ? KtDimen.ZERO : list.nodeAt( 0).getHeight();
      KtDimen width = atSize.times(repInfo.getWidth()).plus(atSize.times(repInfo.getItalic()));
      return new KtVBoxNode(
          new KtBoxSizes(height, width, total.minus(height), KtDimen.ZERO), KtGlueSetting.NATURAL, list);
    }
    return KtNode.NULL;
  }

  /* TeXtp[740] */
  public KtBox getFittingWidthBox(KtCharCode code, KtDimen desired) {
    KtBox box = KtBox.NULL;
    short index = indexFrom(code.toChar());
    if (index != KtTeXFm.NO_CHAR_CODE) {
      KtTeXFm.KtCharInfo info = tfm.getCharInfo(index);
      if (info != KtTeXFm.KtCharInfo.NULL) {
        short maxIndex;
        KtTeXFm.KtCharInfo maxInfo;
        do {
          maxIndex = index;
          maxInfo = info;
          index = info.nextChar();
          if (index == KtTeXFm.NO_CHAR_CODE) break;
          info = tfm.getCharInfo(index);
        } while (info != KtTeXFm.KtCharInfo.NULL && !atSize.times(info.getWidth()).moreThan(desired));
        box = pretendingCharBox(maxIndex, maxInfo);
      }
    }
    return box;
  }

  /* TeXtp[741] */
  public KtDimen getKernBetween(KtCharCode left, KtCharCode right) {
    KtTeXFm.KtLigKern ligKern = tfm.getLigKern(indexFrom(left.toChar()), indexFrom(right.toChar()));
    if (ligKern != KtTeXFm.KtLigKern.NULL) {
      KtFixWord kern = ligKern.getKern();
      if (kern != KtFixWord.NULL) return atSize.times(kern);
    }
    return KtDimen.NULL;
  }

  /* TeXtp[712] */
  private KtDimen heightPlusDepth(KtTeXFm.KtCharInfo info) {
    return atSize.times(info.getHeight()).plus(atSize.times(info.getDepth()));
  }

  private KtIndexNode indexNode(short index, KtTeXFm.KtCharInfo info) {
    return new KtIndexNode(
        atSize.times(info.getWidth()),
        atSize.times(info.getHeight()),
        atSize.times(info.getDepth()),
        index);
  }

  /* TeXtp[709] */
  private KtHBoxNode pretendingCharBox(short index, KtTeXFm.KtCharInfo info) {
    KtIndexNode node = indexNode(index, info);
    KtDimen width = node.getWidth();
    KtDimen ital = node.getItalCorr();
    if (ital != KtDimen.NULL) width = width.plus(ital);
    return new KtHBoxNode(
        new KtBoxSizes(node.getHeight(), width, node.getDepth(), node.getLeftX()),
        KtGlueSetting.NATURAL,
        new KtNodeList(node));
  }
}

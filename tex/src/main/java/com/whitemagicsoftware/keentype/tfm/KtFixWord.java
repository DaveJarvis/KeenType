// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tfm.FixWord
// $Id: KtFixWord.java,v 1.1.1.1 2000/01/31 15:58:53 ksk Exp $
package com.whitemagicsoftware.keentype.tfm;

import com.whitemagicsoftware.keentype.base.KtBinFraction;

/*
 * The dimensions are represented in the same way as in tfm files.
 * Higher 12 bits is the whole part and lower 20 bits is the fractional
 * part. Using the same representation we do not loose any precision.
 * The higher levels of the system may use different accuracy but the
 * rounding and scaling the font dimensions is made on the level of
 * interface to the enclosing class. It is also important that we do not
 * loose precision while printing the property lists.
 * See TFtoPL[9].
 *
 * During internal computation in cases where 32 bit |int| arithmetic can
 * overflow the operands are temporarily extended to 64 bit |long|s.
 *
 * That all is however only implementation detail, to the outside the
 * |KtFixWord| behaves as a fraction and communicate via numerator and
 * optional denominator.
 */

public final class KtFixWord extends KtBinFraction {

  private static final int POINT_SHIFT = 20;
  private static final int MAX_REPR_VALUE = Integer.MAX_VALUE;

  private KtFixWord(int val) {
    super(val);
  }

  protected int pointShift() {
    return POINT_SHIFT;
  }

  public static final KtFixWord NULL = null;
  public static final KtFixWord ZERO = valueOf(0);
  public static final KtFixWord UNITY = valueOf(1);
  public static final KtFixWord MAX_VALUE = new KtFixWord(MAX_REPR_VALUE);
  public static final KtFixWord BAD_VALUE = new KtFixWord(-MAX_REPR_VALUE - 1);

  private static int makeRepr(int num) {
    return num << POINT_SHIFT;
  }

  private static int makeRepr(int num, int den) {
    return (int) (((long) num << POINT_SHIFT) / den);
  }

  private static int makeRepr(KtBinFraction x) {
    return makeRepr(x, POINT_SHIFT);
  }

  public static KtFixWord valueOf(KtFixWord d) {
    return new KtFixWord(d.value);
  }

  public static KtFixWord valueOf(int num) {
    return new KtFixWord(makeRepr(num));
  }

  public static KtFixWord valueOf(int num, int den) {
    return new KtFixWord(makeRepr(num, den));
  }

  public static KtFixWord valueOf(KtBinFraction x) {
    return new KtFixWord(makeRepr(x));
  }

  public static KtFixWord shiftedValueOf(int num, int offs) {
    return new KtFixWord( (offs += POINT_SHIFT) < 0 ? num >> -offs : num << offs);
  }

  public static KtFixWord valueOf(String s) throws NumberFormatException {
    int pointIndex = s.indexOf('.');
    if (pointIndex < 0) return new KtFixWord(makeRepr(Integer.parseInt(s)));
    else {
      long val = Integer.parseInt(s.substring(0, pointIndex));
      final int SHIFT = POINT_SHIFT + 1;
      int frac = 0;
      int i = pointIndex + SHIFT + 1;
      if (i > s.length()) i = s.length();
      while (--i > pointIndex) {
        int digit = Character.digit(s.charAt(i), 10);
        if (digit < 0) throw new NumberFormatException(s);
        frac = (frac + (digit << SHIFT)) / 10;
      }
      boolean negative = val < 0;
      if (negative) val = -val;
      val <<= POINT_SHIFT;
      val |= frac + 1 >>> 1;
      if (val > MAX_REPR_VALUE) throw new NumberFormatException(s);
      return new KtFixWord( negative ? (int) -val : (int) val);
    }
  }

  public int sign() {
    return Integer.compare( value, 0 );
  }

  public boolean isZero() {
    return value == 0;
  }

  public boolean equals(KtFixWord d) {
    return value == d.value;
  }

  public boolean equals(int num) {
    return value == makeRepr( num);
  }

  public boolean equals(int num, int den) {
    return value == makeRepr( num, den);
  }

  public boolean equals(KtBinFraction x) {
    return value == makeRepr( x);
  }

  public boolean lessThan(KtFixWord d) {
    return value < d.value;
  }

  public boolean lessThan(int num) {
    return value < makeRepr( num);
  }

  public boolean lessThan(int num, int den) {
    return value < makeRepr( num, den);
  }

  public boolean lessThan(KtBinFraction x) {
    return value < makeRepr( x);
  }

  public boolean moreThan(KtFixWord d) {
    return value > d.value;
  }

  public boolean moreThan(int num) {
    return value > makeRepr( num);
  }

  public boolean moreThan(int num, int den) {
    return value > makeRepr( num, den);
  }

  public boolean moreThan(KtBinFraction x) {
    return value > makeRepr( x);
  }

  public KtFixWord negative() {
    return new KtFixWord(-value);
  }

  public KtFixWord plus(KtFixWord d) {
    return new KtFixWord(value + d.value);
  }

  public KtFixWord plus(int num) {
    return new KtFixWord(value + makeRepr(num));
  }

  public KtFixWord plus(int num, int den) {
    return new KtFixWord(value + makeRepr(num, den));
  }

  public KtFixWord plus(KtBinFraction x) {
    return new KtFixWord(value + makeRepr(x));
  }

  public KtFixWord minus(KtFixWord d) {
    return new KtFixWord(value - d.value);
  }

  public KtFixWord minus(int num) {
    return new KtFixWord(value - makeRepr(num));
  }

  public KtFixWord minus(int num, int den) {
    return new KtFixWord(value - makeRepr(num, den));
  }

  public KtFixWord minus(KtBinFraction x) {
    return new KtFixWord(value - makeRepr(x));
  }

  public KtFixWord times(KtFixWord d) {
    return new KtFixWord((int) ((long) value * d.value >> POINT_SHIFT));
  }

  public KtFixWord times(int num) {
    return new KtFixWord((int) ((long) value * num));
  }

  public KtFixWord times(int num, int den) {
    return new KtFixWord((int) ((long) value * num / den));
  }

  public KtFixWord times(KtBinFraction x) {
    return new KtFixWord(reprTimes(x));
  }

  public KtFixWord over(KtFixWord d) {
    return new KtFixWord((int) (((long) value << POINT_SHIFT) / d.value));
  }

  public KtFixWord over(int num) {
    return new KtFixWord((int) ((long) value / num));
  }

  public KtFixWord over(int num, int den) {
    return new KtFixWord((int) ((long) value * den / num));
  }

  public KtFixWord over(KtBinFraction x) {
    return new KtFixWord(reprOver(x));
  }

  public KtFixWord shifted(int offs) {
    return new KtFixWord( offs < 0 ? value >> -offs : value << offs);
  }

  public int toInt() {
    return value >>> POINT_SHIFT;
  }

  public int toInt(int den) {
    return (int) ((long) value * den >>> POINT_SHIFT);
  }

  public String toString(String unit) {
    return toString() + unit;
  }

  public String toString() {
    StringBuilder buf = new StringBuilder();
    int v = value;
    final int UNITY = 1 << POINT_SHIFT;
    final int MASK = UNITY - 1;
    if (v < 0) {
      buf.append('-');
      v = -v;
    }
    buf.append(v >>> POINT_SHIFT);
    buf.append('.');
    v = 10 * (v & MASK) + 5;
    int delta = 10;
    do {
      if (delta > UNITY) v += UNITY / 2 - delta / 2;
      buf.append(Character.forDigit(v >>> POINT_SHIFT, 10));
      v = 10 * (v & MASK);
    } while (v > (delta *= 10));
    return buf.toString();
  }

  public int hashCode() {
    return 383 * POINT_SHIFT * value;
  }

  public boolean equals( final Object o ) {
    return o instanceof KtFixWord f && f.value == value;
  }
}

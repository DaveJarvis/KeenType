// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tfm.TeXLigKernBuilder
// $Id: KtTeXLigKernBuilder.java,v 1.1.1.1 2001/01/28 22:23:03 ksk Exp $
package com.whitemagicsoftware.keentype.tfm;

import java.util.Stack;
import com.whitemagicsoftware.keentype.base.KtBinFraction;
import com.whitemagicsoftware.keentype.base.KtBoolPar;
import com.whitemagicsoftware.keentype.base.KtIntPar;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtWordRebuilder;
import com.whitemagicsoftware.keentype.tfm.KtTeXFm.*;

public abstract class KtTeXLigKernBuilder implements KtWordRebuilder, KtMathWordBuilder {

  public static final short NO_CHAR_CODE = KtTeXFm.NO_CHAR_CODE;

  /** KtChar, ligature or boundary */
  private interface KtCell {

    /** no instance */
    KtCell NULL = null;

    /** index to KtTeXFontMetric tables */
    short getIdx();

    /** Character code for character and ligture */
    KtCharCode getCode();

    /** Is it left or right boundary? */
    boolean isBoundary();

    /** Make node with possible left or right hit */
    void makeNode(boolean lh, boolean rh);

    /** Make larger node (for math operators) with possible left or right hit */
    KtNode makeNode(boolean lh, boolean rh, boolean larger);
  }

  /** left hand working cell */
  private KtCell left = KtCell.NULL;

  /** stack of right hand ligatures, maybe with character on the bottom */
  private final Stack<KtCell> stack = new Stack<>();

  /** accumulated string of character for a ligature */
  private final KtName.KtBuffer buffer = new KtName.KtBuffer();

  /** Did the last character collapsed to previous ligature */
  private boolean collapsed = false;

  /** Did the last character collapsed to previous ligature */
  public boolean lastHasCollapsed() {
    return collapsed;
  }

  /** Push cell on the ligature stack */
  private void push(KtCell cell) {
    stack.push(cell);
    collapsed = false;
  }

  /** Pop a cell from the ligature stack */
  private KtCell pop() {
    return stack.pop();
  }

  /** What is the right hand cell (top of ligature stack) */
  private KtCell peek() {
    return stack.peek();
  }

  /** Nothing on the right hand side? */
  private boolean empty() {
    return stack.empty();
  }

  /** KtCell for ordinary character */
  private class KtCharCell implements KtCell {
    private final KtCharCode code;

    public KtCharCell(KtCharCode code) {
      this.code = code;
    }

    public short getIdx() {
      return (short) code.toChar();
    }

    public KtCharCode getCode() {
      return code;
    }

    public boolean isBoundary() {
      return false;
    }

    public void makeNode(boolean lh, boolean rh) {
      // System.err.println("... CHAR(" + code + ')');
      makeChar(code);
    }

    public KtNode makeNode(boolean lh, boolean rh, boolean larger) {
      return makeChar(code, larger);
    }
    // public String	toString() { return "CHAR(" + code + ')'; }
  }

  /** KtCell for ligature */
  private class KtLigCell implements KtCell {
    private final short index;
    private final KtCharCode code;

    public KtLigCell(short index, KtCharCode code) {
      this.index = index;
      this.code = code;
    }

    public KtLigCell(short index) {
      this(index, KtCharCode.NULL);
    }

    public short getIdx() {
      return index;
    }

    public KtCharCode getCode() {
      return code;
    }

    public boolean isBoundary() {
      return false;
    }

    public void makeNode(boolean lh, boolean rh) {
      // System.err.println("... LIG(" + index + ", `" + buffer.toName()
      //		       + "', " + lh + ", " + rh + ')');
      makeLig(index, buffer.toName(), lh, rh);
    }

    public KtNode makeNode(boolean lh, boolean rh, boolean larger) {
      return makeLig(index, buffer.toName(), lh, rh, larger);
    }
    // public String	toString() {
    //     return "LIG(" + index
    //    	  + ((code != KtCharCode.NULL) ? ", " + code : "") + ')';
    // }
  }

  /** KtCell for left or right boundary */
  private static class KtBoundCell implements KtCell {
    public short getIdx() {
      return NO_CHAR_CODE;
    }

    public KtCharCode getCode() {
      return KtCharCode.NULL;
    }

    public boolean isBoundary() {
      return true;
    }

    public void makeNode(boolean lh, boolean rh) {
      // System.err.println("... ignoring boundary");
    }

    public KtNode makeNode(boolean lh, boolean rh, boolean larger) {
      return KtNode.NULL;
    }
    // public String	toString() { return "BOUNDARY"; }
  }

  /* STRANGE
   * this serves only for reconstitution after hyphenation.
   * isBoundary() returns true which is not correct.
   * But it's compatible with TeX. Maybe it's even a bug.
   */
  /** KtCell for right boundary which is ordinary character */
  private static class KtCharBoundCell extends KtBoundCell {
    private final KtCharCode code;

    public KtCharBoundCell(KtCharCode code) {
      this.code = code;
    }

    public short getIdx() {
      return (short) code.toChar();
    }
    // public String	toString() { return "BOUNDARY(" + code + ')'; }
  }

  /** Boundary instance */
  private static final KtCell BOUNDARY = new KtBoundCell();

  /**
   * Add one character to ligature/kern building process.
   *
   * @param code the character code to be added.
   * @return |true| if the character exists in the font metric, |false| otherwise.
   */
  /* TeXtp[1034] */
  public boolean add(KtCharCode code) {
    char index = code.toChar();
    if (index != KtCharCode.NO_CHAR && exists((short) index)) {
      KtCharCell cell = new KtCharCell(code);
      if (left == KtCell.NULL) {
        left = cell;
        buffer.append(code);
      } else {
        push(cell);
        proceed();
      }
      return true;
    }
    close();
    return false;
  }

  /** Does the last ligature keep left hand cell (/=:, ...)? */
  private final KtBoolPar keepLeft = new KtBoolPar();

  /** Does the last ligature keep right hand cell (=:/, ...)? */
  private final KtBoolPar keepRight = new KtBoolPar();

  /** How many cells to skip after last ligature (&gt;) */
  private final KtIntPar stepOver = new KtIntPar();

  /* TeXtp[906] */
  public byte addIfBelongsToCut(KtCharCode code) {
    char index = code.toChar();
    // System.err.println("addIfBelongsToCut('" + code + "'):");
    if (index != KtCharCode.NO_CHAR && exists((short) index)) {
      KtCharCell cell = new KtCharCell(code);
      if (left == KtCell.NULL) {
        left = cell;
        buffer.append(code);
        // System.err.println("virgin rebuilder");
      } else {
        byte how = INDEPENDENT;
        push(cell);
        while (!empty()) {
          // System.err.print("ligKern(" +  left.getIdx()
          // + ", " + peek().getIdx() + ") = ");
          KtLigKern lk = getLigKern(left.getIdx(), peek().getIdx());
          if (lk != KtLigKern.NULL) {
            how = AFFECTING;
            short lig = lk.getLig(keepLeft, keepRight, stepOver);
            if (lig != NO_CHAR_CODE) {
              // System.err.println("lig: " + lig);
              ligStep(lig);
              continue;
            } else {
              KtFixWord kern = lk.getKern();
              if (kern != KtFixWord.NULL) {
                // System.err.println("kern: " + kern);
                if (peek() == cell) {
                  pop();
                  close();
                  makeKern(kern);
                  // System.err.println((how == AFFECTING) ?
                  // "AFFECTING" : "INDEPENDENT");
                  return how;
                }
                move();
                makeKern(kern);
                continue;
              }
            }
          }
          // System.err.println("nothing");
          if (peek() == cell) {
            pop();
            close();
            // System.err.println((how == AFFECTING) ?
            // "AFFECTING" : "INDEPENDENT");
            return how;
          }
          move();
        }
      }
      // System.err.println("BELONGING");
      return BELONGING;
    }
    close();
    // System.err.println("INVALID");
    return INDEPENDENT;
  }

  /**
   * Is the character dependent on the previous characters?
   *
   * @param code the character which is tested for independence.
   * @return |false| if it is independent, |true| if the character produces ligature or kern with
   *     characters added before.
   */
  /* TeXtp[909] */
  public boolean prolongsCut(KtCharCode code) {
    if (left != KtCell.NULL) {
      char index = code.toChar();
      if (index != KtCharCode.NO_CHAR && exists((short) index)) {
        KtLigKern lk = getLigKern(left.getIdx(), (short) index);
        return lk != KtLigKern.NULL;
      }
    }
    return false;
  }

  /** Go on with the ligature/kern process until the ligature stack is empty. */
  /* TeXtp[1040] */
  private void proceed() {
    while (!empty()) {
      // System.err.println("left = " + left + ", lh = " + leftHit
      // 			+ ", buffer = `" + buffer.toName() + '\'');
      // System.err.println("stack = " + stack + ", rh = " + rightHit);
      KtLigKern lk = getLigKern(left.getIdx(), peek().getIdx());
      if (lk != KtLigKern.NULL) {
        short lig = lk.getLig(keepLeft, keepRight, stepOver);
        if (lig != NO_CHAR_CODE) {
          ligStep(lig);
          continue;
        } else {
          KtFixWord kern = lk.getKern();
          if (kern != KtFixWord.NULL) {
            // System.err.println("KRN");
            move();
            makeKern(kern);
            // System.err.println("... KRN");
            continue;
          }
        }
      }
      move();
    }
  }

  /*
   * leftHit and rightHit cannot be attribures of KtLigCell because:
   * (1) both can be replaced by another ligature
   * (2) right ligature can be followed by another
   *     if the right boundary is kept
   */
  private boolean leftHit = false;
  private boolean rightHit = false;

  public KtTeXLigKernBuilder(boolean leftBoundary) {
    if (leftBoundary) left = BOUNDARY;
  }

  protected KtTeXLigKernBuilder(KtCharCode code) {
    left = new KtCharCell(code);
  }

  protected KtTeXLigKernBuilder(short index, KtName subst, boolean leftHit) {
    left = new KtLigCell(index);
    buffer.append(subst);
    this.leftHit = leftHit;
  }

  /**
   * Perform the ligature operation. It expect that |keepLeft|, |keepRight| and |stepOver| are set
   * correctly so |KtLigKern.getLig(keepLeft, keepRight, stepOver)| must be called before and the
   * result must by used as the parameter |lig|.
   *
   * @param lig ligature code, result of previous |KtLigKern.getLig()|.
   */
  /* TeXtp[1040] */
  private void ligStep(short lig) {

    // StringBuffer	buf = new StringBuffer();
    // if (keepLeft.get()) buf.append('/');
    // buf.append("LIG");
    // if (keepRight.get()) buf.append('/');
    // for (int i = stepOver.get(); i > 0; i--) buf.append('>');
    // System.err.println(buf.toString());

    if (left.isBoundary()) leftHit = true;
    else if (peek().isBoundary()) rightHit = true;
    KtCharCode code = keepRight.get() ? KtCharCode.NULL : pop().getCode();
    KtLigCell cell = new KtLigCell(lig, code);
    if (keepLeft.get()) push(cell);
    else {
      setLeft(cell);
      collapsed = empty();
    }
    for (int i = stepOver.get(); i > 0; i--) move();
    // XXX is collapsed really correct? Test it thoroughly!
  }

  /** Close the building of ligatures/kerns with boundary. */
  public void close(boolean rightBoundary) {
    if (rightBoundary) close(BOUNDARY);
    else close();
  }

  /**
   * Close the building of ligatures/kerns with special character boundary.
   *
   * @param code the character which is treated like boundary.
   */
  public void close(KtCharCode code) {
    close(new KtCharBoundCell(code));
  }

  /**
   * Close the building of ligatures/kerns.
   *
   * @param cell an explicitly given boundary cell.
   */
  private void close(KtCell cell) {
    if (left != KtCell.NULL && !left.isBoundary()) {
      push(cell);
      proceed();
    }
    close();
  }

  /** Close the building of ligatures/kerns. */
  public void close() {
    if (left != KtCell.NULL) {
      left.makeNode(leftHit, rightHit);
      left = KtCell.NULL;
    }
    leftHit = rightHit = false;
    buffer.clear();
    // System.err.println("========================================");
    // System.err.println();
  }

  /**
   * Give the node produced from left hand cell.
   *
   * @param larger give larger variant (for math operators) if true.
   * @return node or |KtNode.NULL| if there was nothing left.
   */
  private KtNode takeLastNode(boolean larger) {
    KtNode node = KtNode.NULL;
    if (left != KtCell.NULL) {
      node = left.makeNode(leftHit, rightHit, larger);
      left = KtCell.NULL;
    }
    leftHit = rightHit = false;
    buffer.clear();
    return node;
  }

  /**
   * Move the building ligature cursor one step. Left hand cell produces its output and is forgoten.
   * Right hand cell (top of the ligature stack) moves to the left.
   */
  /* TeXtp[1036,1037] */
  private void move() {
    move(pop());
  }

  /**
   * Move the building ligature cursor one step. Left hand cell produces its output and is forgoten.
   * Right hand cell moves to the left.
   *
   * @param right explicitly given right hand cell
   */
  /* TeXtp[1036,1037] */
  private void move(KtCell right) {
    if (rightHit && right.isBoundary()) {
      left.makeNode(leftHit, true);
      rightHit = false;
    } else left.makeNode(leftHit, false);
    if (!left.isBoundary()) leftHit = false;
    buffer.clear();
    setLeft(right);
  }

  /**
   * Set left hand cell and accumulate its character code to buffer.
   *
   * @param cell the cell to be set.
   */
  private void setLeft(KtCell cell) {
    left = cell;
    KtCharCode code = cell.getCode();
    if (code != KtCharCode.NULL) buffer.append(code);
  }

  public KtNode takeLastNode() {
    return takeLastNode(false);
  }

  public KtNode takeLastLargerNode() {
    return takeLastNode(true);
  }

  protected abstract boolean exists(short index);

  protected abstract KtLigKern getLigKern(short left, short right);

  protected abstract void makeChar(KtCharCode code);

  protected abstract void makeLig(short lig, KtName subst, boolean lh, boolean rh);

  protected abstract void makeKern(KtBinFraction kern);

  protected abstract KtNode makeChar(KtCharCode code, boolean larger);

  protected abstract KtNode makeLig(short lig, KtName subst, boolean lh, boolean rh, boolean larger);
}

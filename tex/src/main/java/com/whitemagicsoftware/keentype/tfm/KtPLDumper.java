// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tfm.PLDumper
package com.whitemagicsoftware.keentype.tfm;

/**
 * Interface |KtPLDumper| defines methods which class should provide to produce a property list.
 *
 * <p>Each property list consist of property name followed by zero or more property values. The
 * values in turn may be property lists too. For detailed description of property list see the
 * documentation of pltotf program.
 *
 * <p>The interface is abstract in the sense that it makes no assumption about output formatting or
 * media.
 *
 * <p>Each of output methods returns reference to KtPLDumper (typically |this| reference). It allows
 * for chaining several output steps together. For example:
 * |dumper.open("NUMBER").addDec(number).close();|
 */
public interface KtPLDumper {

  /**
   * Opens a new property list with given property name.
   *
   * @param p the name of the property to be listed.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper open(String p);

  /**
   * Finishes the current list.
   *
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper close();

  /**
   * Add a decimal number as a property value.
   *
   * @param i the number to be added to current list.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addDec(int i);

  /**
   * Add an octal number as a property value.
   *
   * @param i the number to be added to current list.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addOct(int i);

  /**
   * Add a real number as a property value. An |Object| reference is used for representation of the
   * real number. The |toString| method of such object is used to get the textual representation.
   *
   * @param o the object which represents the real number.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addReal(Object o);

  /**
   * Add a Xerox face code as a property value.
   *
   * @param f the numeric Xerox face code.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addFace(int f);

  /**
   * Add a character string as a property value.
   *
   * @param s the string to be added to current list.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addStr(String s);

  /**
   * Add a character code as a property value.
   *
   * @param c the character code to be added to current list.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addChar(short c);

  /**
   * Add a boolean as a property value.
   *
   * @param b the boolean value to be added to current list.
   * @return |KtPLDumper| for further output.
   */
  KtPLDumper addBool(boolean b);

  /**
   * Tells the |KtPLDumper| that it should use numeric notation for character
   * codes even in cases where it would output the symbolic form.
   */
  void forceNumChars();
}

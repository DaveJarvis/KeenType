// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.BaseNodeEgg
// $Id: KtBaseNodeEgg.java,v 1.1.1.1 2000/08/13 01:22:36 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtNode;

public abstract class KtBaseNodeEgg extends KtEgg {

  protected final KtNode node;

  public KtBaseNodeEgg(KtNode node) {
    this.node = node;
  }

  public KtDimen getHeight() {
    return node != KtNode.NULL ? node.getHeight() : KtDimen.ZERO;
  }

  public KtDimen getDepth() {
    return node != KtNode.NULL ? node.getDepth() : KtDimen.ZERO;
  }

  public void chipShell(KtNodery nodery) {
    if (node != KtNode.NULL && !((node.isSkip() || node.isKern()) && nodery.ignoresSpace()))
      nodery.append(node);
    /* There could by a special SpaceEgg which knows that the node is skip
     * or kern, it is probably known when the KtNodeNoad containing the node
     * is constructed. The KtBaseNodeEgg then need not care. */
  }

  public boolean isPenalty() {
    return node != KtNode.NULL && node.isPenalty();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.OverNoad
// $Id: KtOverNoad.java,v 1.1.1.1 2000/04/13 09:41:20 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtOverNoad extends KtNucleusNoad {

  public KtOverNoad(KtField nucleus) {
    super(nucleus);
  }

  protected String getDesc() {
    return "overline";
  }

  protected byte spacingType() {
    return SPACING_TYPE_ORD;
  }

  /* TeXtp[734] */
  public KtEgg convert(KtConverter conv) {
    KtNode node = nucleus.cleanBox(conv, CRAMPED);
    KtDimen dim = conv.getDimPar(DP_DEFAULT_RULE_THICKNESS);
    return new KtStSimpleNodeEgg(makeOverBar(node, dim.times(3), dim, dim), spacingType());
  }

  public boolean isJustChar() {
    return false;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.RadicalNoad
// $Id: KtRadicalNoad.java,v 1.1.1.1 2000/04/15 18:04:20 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public class KtRadicalNoad extends KtScriptableNoad {

  protected final KtDelimiter radical;
  protected final KtField nucleus;

  public KtRadicalNoad(KtDelimiter radical, KtField nucleus) {
    this.radical = radical;
    this.nucleus = nucleus;
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc()).add(radical);
    nucleus.addOn(log, cntx, '.');
  }

  protected String getDesc() {
    return "radical";
  }

  protected byte spacingType() {
    return SPACING_TYPE_ORD;
  }

  public KtEgg convert(KtConverter conv) {
    KtNode node = nucleus.cleanBox(conv, CRAMPED);
    KtDimen drt = conv.getDimPar(DP_DEFAULT_RULE_THICKNESS);
    KtDimen clr =
        drt.plus(
            (conv.getStyle() != DISPLAY_STYLE ? drt : conv.getDimPar( DP_MATH_X_HEIGHT))
                .absolute()
                .over(4));
    KtDimen size = node.getHeight().plus(node.getDepth()).plus(clr);
    KtNode rad = conv.fetchSufficientNode(radical, size.plus(drt));
    KtDimen height = rad.getHeight();
    size = rad.getDepth().minus(size);
    if (size.moreThan(0)) clr = clr.plus(size.halved());
    rad = KtHShiftNode.shiftingUp(rad, node.getHeight().plus(clr));
    KtNodeList list = new KtNodeList(2);
    list.append(rad).append(makeOverBar(node, clr, height, height));
    return new KtStSimpleNodeEgg(KtHBoxNode.packedOf(list), spacingType());
  }
}

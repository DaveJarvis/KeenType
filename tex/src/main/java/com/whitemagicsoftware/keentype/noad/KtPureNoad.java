// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.PureNoad
// $Id: KtPureNoad.java,v 1.1.1.1 2000/08/09 06:15:49 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtIntVKernNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtRuleNode;
import com.whitemagicsoftware.keentype.node.KtSizesSummarizer;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVertIterator;

public abstract class KtPureNoad extends KtBaseNoad {

  public final boolean isNode() {
    return false;
  }

  public KtNode getNode() {
    return KtNode.NULL;
  }

  /* TeXtp[704] */
  public static KtNode makeRule(KtDimen height) {
    return new KtRuleNode(new KtBoxSizes(height, KtDimen.NULL, KtDimen.ZERO, KtDimen.NULL));
  }

  /* TeXtp[705] */
  public static KtNode makeOverBar(KtNode node, KtDimen clearence, KtDimen thickness, KtDimen extra) {
    KtNodeList list = new KtNodeList(4);
    list.append(new KtIntVKernNode(extra));
    list.append(makeRule(thickness));
    list.append(new KtIntVKernNode(clearence));
    list.append(node);
    return KtVBoxNode.packedOf(list);
  }

  /* TeXtp[735] */
  public static KtNode makeUnderBar(KtNode node, KtDimen clearence, KtDimen thickness, KtDimen extra) {
    KtNodeList list = new KtNodeList(3);
    list.append(node);
    list.append(new KtIntVKernNode(clearence));
    list.append(makeRule(thickness));
    KtSizesSummarizer pack = new KtSizesSummarizer();
    KtVertIterator.summarize(list.nodes(), pack);
    KtBoxSizes sizes =
        new KtBoxSizes(
            pack.getHeight(),
            pack.getWidth(),
            pack.getBody().plus(pack.getDepth()).plus(extra),
            pack.getLeftX());
    return new KtVBoxNode(sizes, KtGlueSetting.NATURAL, list);
  }

  /* TeXtp[706] */
  public static KtNode varDelimiter(KtDelimiter del, KtDimen size, KtTransformer transf) {
    KtNode node = transf.fetchSufficientNode(del, size);
    KtDimen shift =
        node.getHeight().minus(node.getDepth()).halved().minus(transf.getDimPar(DP_AXIS_HEIGHT));
    return KtHShiftNode.shiftingDown(node, shift);
  }
}

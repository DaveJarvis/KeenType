// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.VoidField
// $Id: KtVoidField.java,v 1.1.1.1 2000/10/12 11:06:49 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtCntxLoggable;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtNode;

/* replaces sub_mlist with info(p) = null [681] */
public class KtVoidField extends KtField implements KtCntxLoggable {

  public static final KtVoidField FIELD = new KtVoidField();

  /* TeXtp[692] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.add("{}");
  }

  public void addOn(KtLog log, KtCntxLog cntx, char p) {
    cntx.addOn(log, this, p);
  }

  public KtNode convertedBy(KtConverter conv) {
    return KtHBoxNode.EMPTY;
  }

  /* TeXtp[720] */
  public KtNode cleanBox(KtConverter conv, byte how) {
    return KtHBoxNode.EMPTY;
  }
}

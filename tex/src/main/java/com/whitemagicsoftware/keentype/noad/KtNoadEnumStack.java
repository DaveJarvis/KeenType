// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NoadEnumStack
// $Id: KtNoadEnumStack.java,v 1.1.1.1 2000/03/20 19:17:47 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import java.util.Stack;

public class KtNoadEnumStack extends KtNoadEnum {

  private KtNoadEnum top;
  private final Stack<KtNoadEnum> stack = new Stack<>();

  public KtNoadEnumStack(KtNoadEnum top) {
    this.top = top;
  }

  public KtNoad nextNoad() {
    while (!top.hasMoreNoads())
      if (stack.empty()) return KtNoad.NULL;
      else top = stack.pop();
    return top.nextNoad();
  }

  public boolean hasMoreNoads() {
    while (!top.hasMoreNoads())
      if (stack.empty()) return false;
      else top = stack.pop();
    return true;
  }

  public void push(KtNoadEnum noads) {
    stack.push(top);
    top = noads;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NucleusNoad
// $Id: KtNucleusNoad.java,v 1.1.1.1 2000/10/04 21:24:07 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtNode;

public abstract class KtNucleusNoad extends KtScriptableNoad {

  protected final KtField nucleus;

  public KtNucleusNoad(KtField nucleus) {
    this.nucleus = nucleus;
  }

  protected abstract String getDesc();

  protected abstract byte spacingType();

  public boolean isJustChar() {
    return nucleus.isJustChar();
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc());
    nucleus.addOn(log, cntx, '.');
  }

  public KtEgg convert(KtConverter conv) {
    return makeEgg(nucleus.convertedBy(conv));
  }

  protected KtEgg makeEgg(KtNode node) {
    return new KtStItalNodeEgg(node, spacingType());
  }
}

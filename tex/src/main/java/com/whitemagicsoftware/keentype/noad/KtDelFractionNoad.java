// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.DelFractionNoad
// $Id: KtDelFractionNoad.java,v 1.1.1.1 2001/03/22 21:50:26 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtDelFractionNoad extends KtFractionNoad {

  protected final KtDelimiter left;
  protected final KtDelimiter right;

  public KtDelFractionNoad(
      KtField numerator, KtField denominator, KtDimen thickness, KtDelimiter left, KtDelimiter right) {
    super(numerator, denominator, thickness);
    this.left = left;
    this.right = right;
  }

  public KtDelimiter getLeftDelimiter() {
    return left;
  }

  public KtDelimiter getRightDelimiter() {
    return right;
  }

  public KtFractionNoad numerated(KtField numerator) {
    return new KtDelFractionNoad(numerator, denominator, thickness, left, right);
  }

  public KtFractionNoad denominated(KtField denominator) {
    return new KtDelFractionNoad(numerator, denominator, thickness, left, right);
  }

  /* TeXtp[697] */
  protected void addDelimitersOn(KtLog log) {
    if (!left.isVoid()) log.add(", left-delimiter ").add(left);
    if (!right.isVoid()) log.add(", right-delimiter ").add(right);
  }
}

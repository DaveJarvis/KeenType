// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.MSkipNoad
// $Id: KtMSkipNoad.java,v 1.1.1.1 2000/05/26 21:14:42 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtMSkipNoad extends KtBaseNodeNoad {

  private final KtGlue mskip;

  public KtMSkipNoad(KtGlue mskip) {
    this.mskip = mskip;
  }

  public KtEgg convert(KtConverter conv) {
    return new KtSimpleNodeEgg(new KtHSkipNode(conv.muToPt(mskip)));
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("glue").add('(').addEsc("mskip").add(')').add(' ').add(mskip.toString("mu"));
  }

  public KtNode getNode() {
    return new KtInternalSkipNode();
  }

  protected class KtInternalSkipNode extends KtHSkipNode {
    /* corresponding to glue_node */
    public KtInternalSkipNode() {
      super(KtMSkipNoad.this.mskip);
    }

    public boolean isMuSkip() {
      return true;
    }

    public KtGlue getMuSkip() {
      return mskip;
    }

    public void addOn(KtLog log, KtCntxLog cntx) {
      KtMSkipNoad.this.addOn(log, cntx);
    }
  }
}

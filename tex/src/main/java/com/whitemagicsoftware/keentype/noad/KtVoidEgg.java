// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.VoidEgg
// $Id: KtVoidEgg.java,v 1.1.1.1 2000/04/10 19:29:54 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtVoidEgg extends KtEgg {

  public static final KtVoidEgg EGG = new KtVoidEgg();

  public KtDimen getHeight() {
    return KtDimen.ZERO;
  }

  public KtDimen getDepth() {
    return KtDimen.ZERO;
  }

  public void chipShell(KtNodery nodery) {}

  public byte spacingType() {
    return SPACING_TYPE_NULL;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NoadListField
// $Id: KtNoadListField.java,v 1.1.1.1 2000/10/11 21:15:04 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public class KtNoadListField extends KtField {

  private final KtNoadList list;

  public KtNoadListField(KtNoadList list) {
    this.list = list;
  }

  /* TeXtp[692] */
  public void addOn(KtLog log, KtCntxLog cntx, char p) {
    cntx.addOn(log, list.noads(), p);
  }

  public KtNoad ordinaryNoad() {
    if (list.length() == 1) {
      KtNoad noad = list.noadAt(0);
      if (noad.isOrdinary()) return noad;
    }
    return KtNoad.NULL;
  }

  public KtNode convertedBy(KtConverter conv) {
    return KtHBoxNode.packedOf(conv.convert(list.noads()));
  }

  /* TeXtp[720] */
  public KtNode cleanBox(KtConverter conv, byte how) {
    KtNodeList nodeList = conv.convert(list.noads(), how);
    if (nodeList.length() == 1) {
      KtNode node = nodeList.nodeAt(0);
      if (node.isCleanBox()) return node;
    }
    return KtHBoxNode.packedOf(nodeList).trailingKernSpared();
  }
}

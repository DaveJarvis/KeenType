// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.CharField
// $Id: KtCharField.java,v 1.1.1.1 2000/10/17 13:26:06 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtCntxLoggable;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;

public class KtCharField extends KtField implements KtLoggable, KtCntxLoggable {

  public static final KtCharField NULL = null;

  private final byte fam;
  private final KtCharCode code;

  public KtCharField(byte fam, KtCharCode code) {
    this.fam = fam;
    this.code = code;
  }

  /* TeXtp[691] */
  public void addOn(KtLog log) {
    log.addEsc("fam").add(fam).add(' ').add(code);
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    addOn(log);
  }

  public void addOn(KtLog log, KtCntxLog cntx, char p) {
    cntx.addOn(log, this, p);
  }

  public boolean isJustChar() {
    return true;
  }

  public KtDimen skewAmount(KtConverter conv) {
    return conv.skewAmount(fam, code);
  }

  public KtNode convertedBy(KtConverter conv) {
    return conv.fetchCharNode(fam, code);
  }

  public KtBox fittingTo(KtConverter conv, KtDimen width) {
    return conv.fetchFittingWidthBox(fam, code, width);
  }

  public KtDimen xHeight(KtConverter conv) {
    return conv.getXHeight(fam);
  }

  /* TeXtp[720] */
  public KtNode cleanBox(KtConverter conv, byte how) {
    return packedWithItalCorr(conv.fetchCharNode(fam, code, how));
  }

  private static KtNode packedWithItalCorr(KtNode node) {
    if (node == KtNode.NULL) return KtHBoxNode.EMPTY;
    KtBox box = KtHBoxNode.packedOf(node);
    KtDimen ital = node.getItalCorr();
    return ital == KtDimen.NULL ? box : box.pretendingWidth( box.getWidth().plus( ital));
  }

  protected static class KtCharOperator implements KtOperator {

    private final KtNode node;
    private final KtDimen shift;

    public KtCharOperator(KtNode node, KtDimen shift) {
      this.node = node;
      this.shift = shift;
    }

    public KtNode getNodeToBeLimited() {
      KtNode box = packedWithItalCorr(node);
      return shift.isZero() ? box : KtHBoxNode.packedOf( KtHShiftNode.shiftingDown( box, shift));
    }

    public KtEgg getEggToBeScripted(byte spType) {
      return new KtOperatorEgg(node, shift, spType);
    }

    public KtDimen getItalCorr() {
      return node == KtNode.NULL ? KtDimen.NULL : node.getItalCorr();
    }
  }

  /* TeXtp[749] */
  private static KtOperator makeOperator(KtNode node, KtConverter conv) {
    KtDimen middle = conv.getDimPar(DP_AXIS_HEIGHT);
    KtDimen shift =
        (node == KtNode.NULL
                ? KtDimen.ZERO
                : node.getHeight().max(KtDimen.ZERO).minus(node.getDepth().max(KtDimen.ZERO)).halved())
            .minus(middle);
    return new KtCharOperator(node, shift);
  }

  /* TeXtp[749] */
  public KtOperator makeOperator(KtConverter conv, boolean larger) {
    return makeOperator(
      larger ? conv.fetchLargerNode( fam, code) : conv.fetchCharNode( fam, code), conv);
  }

  public KtMathWordBuilder getMathWordBuilder(KtConverter conv, KtTreatNode proc) {
    return conv.getWordBuilder(fam, proc);
  }

  public byte wordFamily() {
    return fam;
  }

  public void contributeToWord(KtMathWordBuilder word) {
    word.add(code);
  }

  public KtOperator takeLastOperator(KtMathWordBuilder word, KtConverter conv, boolean larger) {
    return makeOperator( larger ? word.takeLastLargerNode() : word.takeLastNode(), conv);
  }
}

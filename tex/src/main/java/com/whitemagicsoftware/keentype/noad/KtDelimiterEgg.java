// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.DelimiterEgg
// $Id: KtDelimiterEgg.java,v 1.1.1.1 2000/04/12 10:15:46 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;

public class KtDelimiterEgg extends KtEgg {

  protected final KtDelimiter delimiter;
  protected final byte spType;

  public KtDelimiterEgg(KtDelimiter delimiter, byte spType) {
    this.delimiter = delimiter;
    this.spType = spType;
  }

  public byte spacingType() {
    return spType;
  }

  public KtDimen getHeight() {
    return KtDimen.ZERO;
  }

  public KtDimen getDepth() {
    return KtDimen.ZERO;
  }

  public void chipShell(KtNodery nodery) {
    KtDimen size = nodery.delimiterSize();
    nodery.append(KtPureNoad.varDelimiter(delimiter, size, nodery));
  }
}

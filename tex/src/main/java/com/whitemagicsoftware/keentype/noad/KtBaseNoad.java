// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.BaseNoad
// $Id: KtBaseNoad.java,v 1.1.1.1 2000/10/18 10:33:22 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtIntVKernNode;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtTreatNode;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVShiftNode;

public abstract class KtBaseNoad implements KtNoad {

  public boolean acceptsLimits() {
    return false;
  }

  public boolean isScriptable() {
    return false;
  }

  public boolean isOrdinary() {
    return false;
  }

  public boolean isJustChar() {
    return false;
  }

  public boolean alreadySuperScripted() {
    return false;
  }

  public boolean alreadySubScripted() {
    return false;
  }

  public boolean canPrecedeBin() {
    return true;
  }

  public boolean canFollowBin() {
    return true;
  }

  public KtNoad withLimits(byte limits) {
    return this;
  }

  public KtField ordinaryField() {
    return KtField.NULL;
  }

  /* TeXtp[696] */
  public void addOnWithScripts(KtLog log, KtCntxLog cntx, KtField sup, KtField sub) {
    addOn(log, cntx);
    sup.addOn(log, cntx, '^');
    sub.addOn(log, cntx, '_');
  }

  /* TeXtp[756] */
  public KtEgg convertWithScripts(KtConverter conv, KtField sup, KtField sub) {
    return makeScriptsTo(convert(conv), !isJustChar(), conv, sup, sub);
  }

  /* TeXtp[756] */
  protected static KtEgg makeScriptsTo(
      KtEgg body, boolean notJustChar, KtConverter conv, KtField sup, KtField sub) {
    KtNode supNode = KtNode.NULL;
    KtNode subNode = KtNode.NULL;
    KtDimen supShift = KtDimen.ZERO;
    KtDimen subShift = KtDimen.ZERO;
    KtDimen delta = KtDimen.ZERO;
    if (!sup.isEmpty()) {
      supNode = makeScriptBox(sup, conv, SUPER_SCRIPT);
      if (notJustChar) {
        supShift =
            body.getHeight().max(KtDimen.ZERO).minus(conv.getDimPar(DP_SUP_DROP, SUPER_SCRIPT));
      }
    }
    if (!sub.isEmpty()) {
      subNode = makeScriptBox(sub, conv, SUB_SCRIPT);
      if (notJustChar) {
        subShift = body.getDepth().max(KtDimen.ZERO).plus(conv.getDimPar(DP_SUB_DROP, SUB_SCRIPT));
      }
      KtDimen italCorr = body.getItalCorr();
      if (italCorr != KtDimen.NULL) {
        delta = italCorr;
        body.suppressItalCorr();
      }
    }
    KtNode scripts;
    KtDimen xHeight = conv.getDimPar(DP_MATH_X_HEIGHT).absolute();
    if (supNode == KtNode.NULL) {
      if (subNode == KtNode.NULL) return body;
      subShift = subShift.max(conv.getDimPar(DP_SUB1));
      subShift = subShift.max(subNode.getHeight().minus(xHeight.times(4, 5)));
      scripts = KtHShiftNode.shiftingDown(subNode, subShift);
    } else {
      supShift =
          supShift.max(
              conv.getDimPar(
                  conv.isCramped()
                      ? DP_SUP3
                      : conv.getStyle() == KtNoad.DISPLAY_STYLE ? DP_SUP1 : DP_SUP2));
      supShift = supShift.max(supNode.getDepth().plus(xHeight.over(4)));
      if (subNode == KtNode.NULL) scripts = KtHShiftNode.shiftingUp(supNode, supShift);
      else {
        subShift = subShift.max(conv.getDimPar(DP_SUB2));
        KtDimen clr =
            conv.getDimPar(DP_DEFAULT_RULE_THICKNESS)
                .times(4)
                .minus(
                    supShift.minus(supNode.getDepth()).minus(subNode.getHeight().minus(subShift)));
        if (clr.moreThan(0)) {
          subShift = subShift.plus(clr);
          clr = xHeight.times(4, 5).minus(supShift.minus(supNode.getDepth()));
          if (clr.moreThan(0)) {
            supShift = supShift.plus(clr);
            subShift = subShift.minus(clr);
          }
        }
        KtNodeList list = new KtNodeList(3);
        list.append(KtVShiftNode.shiftingRight(supNode, delta))
            .append(
                new KtIntVKernNode(
                    supShift.minus(supNode.getDepth()).minus(subNode.getHeight().minus(subShift))))
            .append(subNode);
        scripts = KtVBoxNode.packedOf(list);
        scripts = KtHShiftNode.shiftingDown(scripts, subShift);
      }
    }
    return new KtFollowNodeEgg(body, scripts);
  }

  private static KtNode makeScriptBox(KtField field, KtConverter conv, byte how) {
    KtNode node = field.cleanBox(conv, how);
    KtDimen dim = conv.scriptSpace();
    KtBox box = node.isBox() ? node.getBox() : KtHBoxNode.packedOf( node);
    return box.pretendingWidth(box.getWidth().plus(dim));
  }

  public boolean startsWord() {
    return false;
  }

  public boolean canBePartOfWord() {
    return false;
  }

  public boolean finishesWord() {
    return true;
  }

  public KtMathWordBuilder getMathWordBuilder(KtConverter conv, KtTreatNode proc) {
    throw new RuntimeException("not start of math word");
  }

  public byte wordFamily() {
    return -1;
  }

  public void contributeToWord(KtMathWordBuilder word) {
    throw new RuntimeException("cannot contribute to math word");
  }

  public KtEgg wordFinishingEgg(KtMathWordBuilder word, KtConverter conv) {
    throw new RuntimeException("not part of math word");
  }

  public KtEgg wordFinishingEggWithScripts(
      KtMathWordBuilder word, KtConverter conv, KtField sup, KtField sub) {
    return wordFinishingEgg(word, conv);
  }
}

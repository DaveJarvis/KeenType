// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.SuperScriptNoad
// $Id: KtSuperScriptNoad.java,v 1.1.1.1 2000/10/05 07:48:47 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;

public class KtSuperScriptNoad extends KtScriptNoad {

  public KtSuperScriptNoad(KtNoad body, KtField script) {
    super(body, script);
  }

  public final boolean alreadySuperScripted() {
    return true;
  }

  protected KtScriptNoad rebodiedCopy(KtNoad body) {
    return new KtSuperScriptNoad(body, script);
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    body.addOnWithScripts(log, cntx, script, KtEmptyField.FIELD);
  }

  /* TeXtp[696] */
  public void addOnWithScripts(KtLog log, KtCntxLog cntx, KtField sup, KtField sub) {
    body.addOnWithScripts(log, cntx, script, sub);
  }

  public KtEgg convert(KtConverter conv) {
    return body.convertWithScripts(conv, script, KtEmptyField.FIELD);
  }

  public KtEgg convertWithScripts(KtConverter conv, KtField sup, KtField sub) {
    return body.convertWithScripts(conv, script, sub);
  }

  public KtEgg wordFinishingEgg(KtMathWordBuilder word, KtConverter conv) {
    return body.wordFinishingEggWithScripts(word, conv, script, KtEmptyField.FIELD);
  }

  public KtEgg wordFinishingEggWithScripts(
      KtMathWordBuilder word, KtConverter conv, KtField sup, KtField sub) {
    return body.wordFinishingEggWithScripts(word, conv, script, sub);
  }
}

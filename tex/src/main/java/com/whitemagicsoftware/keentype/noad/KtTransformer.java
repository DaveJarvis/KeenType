// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.Transformer
// $Id: KtTransformer.java,v 1.1.1.1 2000/10/17 02:32:31 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public interface KtTransformer {

  void setStyle(byte style);

  byte getStyle();

  boolean isCramped();

  KtDimen muToPt(KtDimen dim);

  KtGlue muToPt(KtGlue skip);

  KtDimen scriptSpace();

  KtDimen getDimPar(int param);

  KtDimen getDimPar(int param, byte how);

  KtNode fetchCharNode(byte fam, KtCharCode code);

  KtNode fetchCharNode(byte fam, KtCharCode code, byte how);

  KtNode fetchLargerNode(byte fam, KtCharCode code);

  KtNode fetchSufficientNode(KtDelimiter del, KtDimen desired);

  KtBox fetchFittingWidthBox(byte fam, KtCharCode code, KtDimen desired);

  KtDimen skewAmount(byte fam, KtCharCode code);

  KtDimen getXHeight(byte fam);

  KtNodeList convert(KtNoadEnum noads);

  KtNodeList convert(KtNoadEnum noads, byte how);
}

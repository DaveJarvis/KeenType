// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.BinNoad
// $Id: KtBinNoad.java,v 1.1.1.1 2000/10/04 21:24:29 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.node.KtNode;

public class KtBinNoad extends KtWordPartNoad {

  public KtBinNoad(KtField nucleus) {
    super(nucleus);
  }

  protected KtEgg makeEgg(KtNode node) {
    return new KtBinItalNodeEgg(node);
  }

  protected byte spacingType() {
    return SPACING_TYPE_BIN;
  }

  protected String getDesc() {
    return "mathbin";
  }

  public boolean canPrecedeBin() {
    return false;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.MathNode
// $Id: KtMathNode.java,v 1.1.1.1 2000/05/26 21:16:46 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBreakingCntx;
import com.whitemagicsoftware.keentype.node.KtDiscardableNode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public abstract class KtMathNode extends KtDiscardableNode {
  /* root corresponding to math_node */

  protected final KtDimen kern;

  public KtMathNode(KtDimen kern) {
    this.kern = kern;
  }

  public KtDimen getKern() {
    return kern;
  }

  public KtDimen getWidth() {
    return kern;
  }

  /* TeXtp[192] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc( isOn() ? "mathon" : "mathoff");
    if (!kern.isZero()) log.add(", surrounded ").add(kern.toString());
  }

  public KtFontMetric addShortlyOn(KtLog log, KtFontMetric metric) {
    log.add('$');
    return metric;
  }

  public void addBreakDescOn(KtLog log) {
    log.addEsc("math");
  }

  public boolean isKernBreak() {
    return true;
  }

  public int breakPenalty(KtBreakingCntx brCntx) {
    return brCntx.spaceBreaking() ? 0 : INF_PENALTY;
  }

  public KtNodeEnum atBreakReplacement() {
    return KtNodeList.nodes(resizedCopy(KtDimen.ZERO));
  }

  protected abstract boolean isOn();

  protected abstract KtMathNode resizedCopy(KtDimen kern);
}

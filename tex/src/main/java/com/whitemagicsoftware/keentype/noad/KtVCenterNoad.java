// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.VCenterNoad
// $Id: KtVCenterNoad.java,v 1.1.1.1 2001/03/22 15:44:37 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtVCenterNoad extends KtScriptableNoad {

  protected final KtNodeField nucleus;

  public KtVCenterNoad(KtNodeField nucleus) {
    this.nucleus = nucleus;
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc());
    nucleus.addOn(log, cntx, '.');
  }

  protected String getDesc() {
    return "vcenter";
  }

  public boolean isJustChar() {
    return nucleus.isJustChar();
  }
  // XXX would it be true (for script drops) even if the result of nucleus
  // XXX conversion was really just char?

  /* STRANGE
   * why \vcenter lays about box sizes instead of simply shifting it?
   */
  /* TeXtp[736] */
  public KtEgg convert(KtConverter conv) {
    KtNode node = nucleus.getNode();
    KtDimen middle = conv.getDimPar(DP_AXIS_HEIGHT);
    if (node.isBox()) {
      KtBox box = node.getBox();
      KtBoxSizes sizes = box.getSizes();
      KtDimen shift = delta(sizes.getHeight(), sizes.getDepth(), middle);
      node = box.pretendSizesCopy(sizes.shiftedUp(shift));
    } else {
      KtDimen shift = delta(node.getHeight(), node.getDepth(), middle);
      node = KtHShiftNode.shiftingUp(node, shift);
    }
    return new KtStItalNodeEgg(node, SPACING_TYPE_ORD);
  }

  private KtDimen delta(KtDimen height, KtDimen depth, KtDimen middle) {
    return depth.minus(height).halved().plus(middle);
    // KtDimen		total = height.plus(depth);
    // return total.halved().plus(middle).minus(height);
  }
}

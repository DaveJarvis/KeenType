// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.ConvStyle
// $Id: KtConvStyle.java,v 1.1.1.1 2001/03/22 07:02:19 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;

public abstract class KtConvStyle implements KtTransfConstants {

  protected final byte style;
  protected final boolean cramped;
  protected final KtDimen currMu;

  /* TeXtp[702] */
  protected KtConvStyle(byte style, boolean cramped) {
    this.style = style;
    this.cramped = cramped;
    setupFontSize();
    currMu = getDimPar(DP_MATH_QUAD).over(18); // XXX config
  }

  protected abstract void setupFontSize();

  protected abstract KtFontMetric fetchFontMetric(byte fam);

  public byte getStyle() {
    return style;
  }

  public boolean isCramped() {
    return cramped;
  }

  public KtDimen muToPt(KtDimen dim) {
    return dim.times(currMu);
  }

  public KtGlue muToPt(KtGlue skip) {
    return skip.timesTheFinite(currMu);
  }

  public abstract KtNode fetchCharNode(byte fam, KtCharCode code);

  public abstract KtNode fetchLargerNode(byte fam, KtCharCode code);

  public abstract KtNode fetchSufficientNode(KtDelimiter delimiter, KtDimen desired);

  public abstract KtBox fetchFittingWidthBox(byte fam, KtCharCode code, KtDimen desired);

  public abstract KtDimen skewAmount(byte fam, KtCharCode code);

  public abstract KtDimen getXHeight(byte fam);

  public abstract KtDimen delimiterSize(KtDimen height, KtDimen depth);

  public abstract KtDimen scriptSpace();

  public abstract KtNode getSpacing(byte left, byte right);

  public abstract KtNum getPenalty(byte left, byte right);

  public abstract boolean isScript();

  public abstract KtConvStyle makeNew(byte style);

  public abstract boolean forcedItalCorr(byte fam);

  public abstract KtMathWordBuilder getWordBuilder(byte fam, KtTreatNode proc);

  /* TeXtp[702] */
  public KtConvStyle derived(byte how) {
    byte s = style;
    boolean c = cramped;
    switch (how) {
      case CRAMPED:
        c = true;
        break;
      case SUB_SCRIPT:
        c = true; /* fall */
      case SUPER_SCRIPT:
        s = switch( style ) {
          case KtNoad.DISPLAY_STYLE, KtNoad.TEXT_STYLE -> KtNoad.SCRIPT_STYLE;
          default -> KtNoad.SCRIPT_SCRIPT_STYLE;
        };
        ;
        break;
      case DENOMINATOR:
        c = true; /* fall */
      case NUMERATOR:
        s = switch( style ) {
          case KtNoad.DISPLAY_STYLE -> KtNoad.TEXT_STYLE;
          case KtNoad.TEXT_STYLE -> KtNoad.SCRIPT_STYLE;
          default -> KtNoad.SCRIPT_SCRIPT_STYLE;
        };
        ;
        break;
    }
    return deriveNew(s, c);
  }

  protected abstract KtConvStyle deriveNew(byte style, boolean cramped);

  private static final byte[] dimFamilies;
  private static final int[] dimFontPars;

  public KtDimen getDimPar(int param) {
    return fetchFontMetric(dimFamilies[param]).getDimenParam(dimFontPars[param]);
  }

  public static final byte SYMBOL_FAMILY = 2; // XXX config
  public static final byte EXTENSION_FAMILY = 3; // XXX config

  private static void initSymbolPar(int param, int fontPar) {
    dimFamilies[param] = SYMBOL_FAMILY;
    dimFontPars[param] = fontPar;
  }

  private static void initExtensionPar(int param, int fontPar) {
    dimFamilies[param] = EXTENSION_FAMILY;
    dimFontPars[param] = fontPar;
  }

  static {
    dimFamilies = new byte[NUMBER_OF_DIM_PARS];
    dimFontPars = new int[NUMBER_OF_DIM_PARS];

    initSymbolPar(DP_MATH_X_HEIGHT, KtFontMetric.DIMEN_PARAM_MATH_X_HEIGHT);
    initSymbolPar(DP_MATH_QUAD, KtFontMetric.DIMEN_PARAM_MATH_QUAD);
    initSymbolPar(DP_NUM1, KtFontMetric.DIMEN_PARAM_NUM1);
    initSymbolPar(DP_NUM2, KtFontMetric.DIMEN_PARAM_NUM2);
    initSymbolPar(DP_NUM3, KtFontMetric.DIMEN_PARAM_NUM3);
    initSymbolPar(DP_DENOM1, KtFontMetric.DIMEN_PARAM_DENOM1);
    initSymbolPar(DP_DENOM2, KtFontMetric.DIMEN_PARAM_DENOM2);
    initSymbolPar(DP_SUP1, KtFontMetric.DIMEN_PARAM_SUP1);
    initSymbolPar(DP_SUP2, KtFontMetric.DIMEN_PARAM_SUP2);
    initSymbolPar(DP_SUP3, KtFontMetric.DIMEN_PARAM_SUP3);
    initSymbolPar(DP_SUB1, KtFontMetric.DIMEN_PARAM_SUB1);
    initSymbolPar(DP_SUB2, KtFontMetric.DIMEN_PARAM_SUB2);
    initSymbolPar(DP_SUP_DROP, KtFontMetric.DIMEN_PARAM_SUP_DROP);
    initSymbolPar(DP_SUB_DROP, KtFontMetric.DIMEN_PARAM_SUB_DROP);
    initSymbolPar(DP_DELIM1, KtFontMetric.DIMEN_PARAM_DELIM1);
    initSymbolPar(DP_DELIM2, KtFontMetric.DIMEN_PARAM_DELIM2);
    initSymbolPar(DP_AXIS_HEIGHT, KtFontMetric.DIMEN_PARAM_AXIS_HEIGHT);
    initExtensionPar(DP_DEFAULT_RULE_THICKNESS, KtFontMetric.DIMEN_PARAM_DEFAULT_RULE_THICKNESS);
    initExtensionPar(DP_BIG_OP_SPACING1, KtFontMetric.DIMEN_PARAM_BIG_OP_SPACING1);
    initExtensionPar(DP_BIG_OP_SPACING2, KtFontMetric.DIMEN_PARAM_BIG_OP_SPACING2);
    initExtensionPar(DP_BIG_OP_SPACING3, KtFontMetric.DIMEN_PARAM_BIG_OP_SPACING3);
    initExtensionPar(DP_BIG_OP_SPACING4, KtFontMetric.DIMEN_PARAM_BIG_OP_SPACING4);
    initExtensionPar(DP_BIG_OP_SPACING5, KtFontMetric.DIMEN_PARAM_BIG_OP_SPACING5);
  }
}

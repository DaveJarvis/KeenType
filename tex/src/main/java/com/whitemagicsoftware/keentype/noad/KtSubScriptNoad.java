// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.SubScriptNoad
// $Id: KtSubScriptNoad.java,v 1.1.1.1 2000/10/05 07:48:43 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;

public class KtSubScriptNoad extends KtScriptNoad {

  public KtSubScriptNoad(KtNoad body, KtField script) {
    super(body, script);
  }

  public final boolean alreadySubScripted() {
    return true;
  }

  protected KtScriptNoad rebodiedCopy(KtNoad body) {
    return new KtSubScriptNoad(body, script);
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    body.addOnWithScripts(log, cntx, KtEmptyField.FIELD, script);
  }

  /* TeXtp[696] */
  public void addOnWithScripts(KtLog log, KtCntxLog cntx, KtField sup, KtField sub) {
    body.addOnWithScripts(log, cntx, sup, script);
  }

  public KtEgg convert(KtConverter conv) {
    return body.convertWithScripts(conv, KtEmptyField.FIELD, script);
  }

  public KtEgg convertWithScripts(KtConverter conv, KtField sup, KtField sub) {
    return body.convertWithScripts(conv, sup, script);
  }

  public KtEgg wordFinishingEgg(KtMathWordBuilder word, KtConverter conv) {
    return body.wordFinishingEggWithScripts(word, conv, KtEmptyField.FIELD, script);
  }

  public KtEgg wordFinishingEggWithScripts(
      KtMathWordBuilder word, KtConverter conv, KtField sup, KtField sub) {
    return body.wordFinishingEggWithScripts(word, conv, sup, script);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.FractionNoad
// $Id: KtFractionNoad.java,v 1.1.1.1 2001/03/22 16:46:48 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtIntVKernNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;

public class KtFractionNoad extends KtPureNoad {

  public static final KtFractionNoad NULL = null;

  protected final KtField numerator;
  protected final KtField denominator;
  protected final KtDimen thickness;

  public KtField getNumerator() {
    return numerator;
  }

  public KtField getDenominator() {
    return denominator;
  }

  public KtDimen getThickness() {
    return thickness;
  }

  public KtFractionNoad(KtField numerator, KtField denominator, KtDimen thickness) {
    this.numerator = numerator;
    this.denominator = denominator;
    this.thickness = thickness;
  }

  public KtFractionNoad numerated(KtField numerator) {
    return new KtFractionNoad(numerator, denominator, thickness);
  }

  public KtFractionNoad denominated(KtField denominator) {
    return new KtFractionNoad(numerator, denominator, thickness);
  }

  /* TeXtp[697] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc()).add(", thickness ");
    if (thickness == KtDimen.NULL) log.add("= default");
    else log.add(thickness.toString());
    addDelimitersOn(log);
    numerator.addOn(log, cntx, '\\');
    denominator.addOn(log, cntx, '/');
  }

  public final boolean influencesBin() {
    return true;
  }

  protected String getDesc() {
    return "fraction";
  }

  protected void addDelimitersOn(KtLog log) {}

  public KtDelimiter getLeftDelimiter() {
    return KtDelimiter.VOID;
  }

  public KtDelimiter getRightDelimiter() {
    return KtDelimiter.VOID;
  }

  protected byte spacingType() {
    return SPACING_TYPE_INNER;
  }

  /* TeXtp[743] */
  public KtEgg convert(KtConverter conv) {
    KtNode numNode = numerator.cleanBox(conv, NUMERATOR);
    KtNode denNode = denominator.cleanBox(conv, DENOMINATOR);
    KtDimen width = numNode.getWidth().max(denNode.getWidth());
    numNode = numNode.reboxedToWidth(width);
    denNode = denNode.reboxedToWidth(width);
    KtDimen numShift;
    KtDimen denShift;
    KtDimen clr, delta;
    KtDimen middle = conv.getDimPar(DP_AXIS_HEIGHT);
    KtDimen drt = conv.getDimPar(DP_DEFAULT_RULE_THICKNESS);
    KtDimen thick = thickness != KtDimen.NULL ? thickness : drt;
    boolean display = conv.getStyle() == DISPLAY_STYLE;
    KtNodeList list = new KtNodeList(5);
    list.append(numNode);
    if (thick.isZero()) {
      if (display) {
        numShift = conv.getDimPar(DP_NUM1);
        denShift = conv.getDimPar(DP_DENOM1);
        clr = drt.times(7);
      } else {
        numShift = conv.getDimPar(DP_NUM3);
        denShift = conv.getDimPar(DP_DENOM2);
        clr = drt.times(3);
      }
      delta =
          clr.minus(numShift.minus(numNode.getDepth()).minus(denNode.getHeight().minus(denShift)))
              .halved();
      if (delta.moreThan(0)) {
        numShift = numShift.plus(delta);
        denShift = denShift.plus(delta);
      }
      list.append(
          new KtIntVKernNode(
              numShift.minus(numNode.getDepth()).minus(denNode.getHeight().minus(denShift))));
    } else {
      if (display) {
        numShift = conv.getDimPar(DP_NUM1);
        denShift = conv.getDimPar(DP_DENOM1);
        clr = thick.times(3);
      } else {
        numShift = conv.getDimPar(DP_NUM2);
        denShift = conv.getDimPar(DP_DENOM2);
        clr = thick;
      }
      KtDimen halfThick = thick.halved();
      delta = clr.minus(numShift.minus(numNode.getDepth()).minus(middle.plus(halfThick)));
      if (delta.moreThan(0)) numShift = numShift.plus(delta);
      delta = clr.minus(middle.minus(halfThick).minus(denNode.getHeight().minus(denShift)));
      if (delta.moreThan(0)) denShift = denShift.plus(delta);
      list.append(
              new KtIntVKernNode(numShift.minus(numNode.getDepth()).minus(middle.plus(halfThick))))
          .append(makeRule(thick))
          .append(
              new KtIntVKernNode(middle.minus(halfThick).minus(denNode.getHeight().minus(denShift))));
    }
    list.append(denNode);
    KtNode fraction =
        new KtVBoxNode(
            new KtBoxSizes(
                numNode.getHeight().plus(numShift),
                width,
                denNode.getDepth().plus(denShift),
                numNode.getLeftX().max(denNode.getLeftX())),
            KtGlueSetting.NATURAL,
            list);
    KtDimen delSize = display ? conv.getDimPar( DP_DELIM1) : conv.getDimPar( DP_DELIM2);
    list = new KtNodeList(3);
    list.append(varDelimiter(getLeftDelimiter(), delSize, conv))
        .append(fraction)
        .append(varDelimiter(getRightDelimiter(), delSize, conv));
    return new KtStSimpleNodeEgg(KtHBoxNode.packedOf(list), spacingType());
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.ItalNodeEgg
// $Id: KtItalNodeEgg.java,v 1.1.1.1 2000/04/15 11:16:15 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtChrKernNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public abstract class KtItalNodeEgg extends KtBaseNodeEgg {

  public KtItalNodeEgg(KtNode node) {
    super(node);
  }

  private boolean suppressed = false;

  public void suppressItalCorr() {
    suppressed = true;
  }

  public KtDimen getItalCorr() {
    return suppressed || node == KtNode.NULL ? KtDimen.NULL : node.getItalCorr();
  }

  public void chipShell(KtNodery nodery) {
    if (node != KtNode.NULL) {
      nodery.append(node);
      if (!suppressed) {
        KtDimen ital = node.getItalCorr();
        if (ital != KtDimen.NULL && !ital.isZero()) nodery.append(new KtChrKernNode(ital));
      }
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NodeField
// $Id: KtNodeField.java,v 1.1.1.1 2000/04/11 20:52:52 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtNodeField extends KtField {

  private final KtNode node;

  public KtNodeField(KtNode node) {
    this.node = node;
  }

  public KtNode getNode() {
    return node;
  }

  /* TeXtp[693] */
  public void addOn(KtLog log, KtCntxLog cntx, char p) {
    cntx.addOn(log, node, p);
  }

  public KtNode convertedBy(KtConverter conv) {
    return node;
  }

  /* TeXtp[720] */
  public KtNode cleanBox(KtConverter conv, byte how) {
    return node.isCleanBox() ? node.trailingKernSpared() : KtHBoxNode.packedOf( node);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.FollowNodeEgg
// $Id: KtFollowNodeEgg.java,v 1.1.1.1 2000/04/11 00:21:01 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtFollowNodeEgg extends KtEgg {

  private final KtEgg egg;
  private final KtNode node;

  public KtFollowNodeEgg(KtEgg egg, KtNode node) {
    this.egg = egg;
    this.node = node;
  }

  public KtDimen getHeight() {
    return egg.getHeight().max(node.getHeight());
  }

  public KtDimen getDepth() {
    return egg.getDepth().max(node.getDepth());
  }

  public void chipShell(KtNodery nodery) {
    egg.chipShell(nodery);
    nodery.append(node);
  }

  public byte spacingType() {
    return egg.spacingType();
  }
}

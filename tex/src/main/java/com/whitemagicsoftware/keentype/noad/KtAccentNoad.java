// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.AccentNoad
// $Id: KtAccentNoad.java,v 1.1.1.1 2000/10/17 02:39:55 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtIntVKernNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVShiftNode;

public class KtAccentNoad extends KtScriptableNoad {

  protected final KtCharField accent;
  protected final KtField nucleus;

  public KtAccentNoad(KtCharField accent, KtField nucleus) {
    this.accent = accent;
    this.nucleus = nucleus;
  }

  protected String getDesc() {
    return "accent";
  }

  public boolean isOrdinary() {
    return true;
  }

  protected byte spacingType() {
    return SPACING_TYPE_ORD;
  }

  /* if accent char is not available in current font it becomes just a char;
   * otherwise if nucleus is a char the scripts are swapped below the accent
   * so for them it is still just a char
   */
  public boolean isJustChar() {
    return nucleus.isJustChar();
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc()).add(accent);
    nucleus.addOn(log, cntx, '.');
  }

  public KtEgg convert(KtConverter conv) {
    return sharedConvert(conv, KtEmptyField.FIELD, KtEmptyField.FIELD);
  }

  public KtEgg convertWithScripts(KtConverter conv, KtField sup, KtField sub) {
    return nucleus.isJustChar()
        ? sharedConvert(conv, sup, sub)
        : super.convertWithScripts(conv, sup, sub);
  }

  /* STRANGE
   * the first test (accent.convertedBy(conv) != KtNode.NULL) is redundant
   * but we need this order of char warnings for compatibility
   */
  /* TeXtp[738] */
  private KtEgg sharedConvert(KtConverter conv, KtField sup, KtField sub) {
    if (accent.convertedBy(conv) != KtNode.NULL) {
      KtDimen skew = nucleus.skewAmount(conv);
      KtNode nuclNode = nucleus.cleanBox(conv, CRAMPED);
      KtDimen height = nuclNode.getHeight();
      KtDimen width = nuclNode.getWidth();
      KtDimen delta = height.min(accent.xHeight(conv));
      KtBox accBox = accent.fittingTo(conv, width);
      if (accBox != KtBox.NULL) {
        if (!(sup.isEmpty() && sub.isEmpty())) {
          KtNoad noad = new KtOrdNoad(nucleus);
          if (!sub.isEmpty()) noad = new KtSubScriptNoad(noad, sub);
          if (!sup.isEmpty()) noad = new KtSuperScriptNoad(noad, sup);
          nuclNode = new KtNoadListField(new KtNoadList(noad)).cleanBox( conv, CURRENT);
          delta = delta.plus(nuclNode.getHeight().minus(height));
          height = nuclNode.getHeight();
        }
        KtNodeList list = new KtNodeList(3);
        KtDimen shift = width.minus(accBox.getWidth()).halved().plus(skew);
        accBox =
            accBox.pretendSizesCopy(
                new KtBoxSizes(
                    accBox.getHeight(), KtDimen.ZERO,
                    accBox.getDepth(), KtDimen.ZERO));
        list.append(KtVShiftNode.shiftingRight(accBox, shift))
            .append(new KtIntVKernNode(delta.negative()))
            .append(nuclNode);
        KtBox box = KtVBoxNode.packedOf(list);
        if (box.getHeight().lessThan(height)) {
          KtNodeList list2 = new KtNodeList(4);
          list2.append(new KtIntVKernNode(height.minus(box.getHeight()))).append(list);
          box = KtVBoxNode.packedOf(list2);
        }
        box =
            box.pretendSizesCopy(
                new KtBoxSizes(
                    box.getHeight(), nuclNode.getWidth(),
                    box.getDepth(), nuclNode.getLeftX()));
        return new KtStSimpleNodeEgg(box, spacingType());
      }
    }
    return new KtStItalNodeEgg(nucleus.convertedBy(conv), spacingType());
  }
}

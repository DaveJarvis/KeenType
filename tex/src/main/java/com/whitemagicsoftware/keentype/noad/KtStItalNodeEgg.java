// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.StItalNodeEgg
// $Id: KtStItalNodeEgg.java,v 1.1.1.1 2000/04/10 20:41:52 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.node.KtNode;

public class KtStItalNodeEgg extends KtItalNodeEgg {

  protected final byte spType;

  public KtStItalNodeEgg(KtNode node, byte spType) {
    super(node);
    this.spType = spType;
  }

  public byte spacingType() {
    return spType;
  }
}

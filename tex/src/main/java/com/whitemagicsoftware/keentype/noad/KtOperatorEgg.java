// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.OperatorEgg
// $Id: KtOperatorEgg.java,v 1.1.1.1 2000/10/16 11:03:27 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtOperatorEgg extends KtEgg {

  protected final KtNode node;
  protected final KtDimen shift;
  protected final byte spType;

  /* STRANGE
   * node might be null only if the metric or char was not available.
   * In that case it becomes empty hbox like in case of epmty field but
   * that hbox is surprisingly shifted -- see TeXtp[749].
   * It is the only reason for all tests (node == KtNode.NULL).
   */

  public KtOperatorEgg(KtNode node, KtDimen shift, byte spType) {
    this.node = node;
    this.shift = shift;
    this.spType = spType;
  }

  private boolean suppressed = false;

  public void suppressItalCorr() {
    suppressed = true;
  }

  public KtDimen getItalCorr() {
    return suppressed || node == KtNode.NULL ? KtDimen.NULL : node.getItalCorr();
  }

  public byte spacingType() {
    return spType;
  }

  public KtDimen getHeight() {
    return node == KtNode.NULL ? KtDimen.ZERO : node.getHeight().max( KtDimen.ZERO).minus( shift);
  }

  public KtDimen getDepth() {
    return node == KtNode.NULL ? KtDimen.ZERO : node.getDepth().max( KtDimen.ZERO).plus( shift);
  }

  public void chipShell(KtNodery nodery) {
    KtBox box = node == KtNode.NULL ? KtHBoxNode.EMPTY : KtHBoxNode.packedOf( node);
    KtDimen ital = getItalCorr();
    if (ital != KtDimen.NULL) box = box.pretendingWidth(box.getWidth().plus(ital));
    nodery.append(KtHShiftNode.shiftingDown(box, shift));
  }

  public boolean isPenalty() {
    return node != KtNode.NULL && node.isPenalty();
  }
}

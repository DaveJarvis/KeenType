// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NonScriptNoad
// $Id: KtNonScriptNoad.java,v 1.1.1.1 2000/05/26 21:14:42 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtNonScriptNoad extends KtBaseNodeNoad {

  public KtEgg convert(KtConverter conv) {
    return new KtNonScriptEgg(getNode());
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc("glue").add('(').addEsc("nonscript").add(')');
  }

  public KtNode getNode() {
    return new KtNonScriptNode();
  }

  protected static class KtNonScriptNode extends KtHSkipNode {
    /* corresponding to glue_node */

    public KtNonScriptNode() {
      super(KtGlue.ZERO);
    }

    public void addOn(KtLog log, KtCntxLog cntx) {
      log.addEsc("glue").add('(').addEsc("nonscript").add(')');
    }
  }
}

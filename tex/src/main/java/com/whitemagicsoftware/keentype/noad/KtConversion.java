// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.Conversion
// $Id: KtConversion.java,v 1.1.1.1 2000/10/17 13:40:54 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import java.util.Iterator;
import java.util.Vector;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtPenaltyNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;

public class KtConversion implements KtTransfConstants {

  private final KtNoadEnumStack stack;
  private final KtConvStyle convStyle;

  protected KtConversion(KtNoadEnum noads, KtConvStyle convStyle) {
    stack = new KtNoadEnumStack(noads);
    this.convStyle = convStyle;
  }

  /* TeXtp[726] */
  protected KtNodeList convert() {
    KtHen converter = new KtHen(convStyle);
    KtClutch clutch = new KtClutch();
    while (stack.hasMoreNoads()) {
      KtNoad noad = stack.nextNoad();
      boolean again;
      do {
        again = false;
        if (noad.startsWord()) {
          KtMathWordBuilder word = noad.getMathWordBuilder(converter, clutch.appender());
          if (word == KtMathWordBuilder.NULL) {
            clutch.add(noad, noad.convert(converter));
            break;
          }
          byte fam = noad.wordFamily();
          clutch.suppressingAllowed = !converter.forcedItalCorr(fam);
          noad.contributeToWord(word);
          for (; ; ) {
            if (!stack.hasMoreNoads()) {
              clutch.addOrd(word.takeLastNode(), false);
              break;
            }
            noad = stack.nextNoad();
            if (noad.canBePartOfWord() && noad.wordFamily() == fam) {
              noad.contributeToWord(word);
              if (noad.finishesWord()) {
                KtEgg egg = noad.wordFinishingEgg(word, converter);
                // XXX incorrect
                // XXX if last has collapsed but has scripts
                // XXX noad must be changed to an KtOrdNoad
                if (egg != KtEgg.NULL) {
                  clutch.add(noad, egg);
                  break;
                }
              }
            } else {
              clutch.addOrd(word.takeLastNode(), false);
              again = true;
              break;
            }
          }
        } else clutch.add(noad, noad.convert(converter));
      } while (again);
    }
    clutch.finish();
    byte leftPenType = SPACING_TYPE_NULL;
    byte leftSpacType = SPACING_TYPE_NULL;
    KtCoop coop = new KtCoop(convStyle, clutch.maxHeight, clutch.maxDepth);
    Iterator iter = clutch.iterator();
    while (iter.hasNext()) {
      KtEgg egg = (KtEgg) iter.next();
      byte right = egg.spacingType();
      if (!egg.isPenalty()) {
        KtNum pen = coop.getConv().getPenalty(leftPenType, right);
        if (pen != KtNum.NULL && pen.lessThan(KtNode.INF_PENALTY)) coop.append(new KtPenaltyNode(pen));
      }
      leftPenType = right;
      if (right != SPACING_TYPE_NULL) {
        if (leftSpacType != SPACING_TYPE_NULL) {
          KtNode space = coop.getConv().getSpacing(leftSpacType, right);
          if (space != KtNode.NULL) coop.append(space);
        }
        leftSpacType = right;
      }
      egg.chipShell(coop);
      coop.setIgnoreSpace(egg.ignoreNextScriptSpace());
    }
    return coop.getList();
  }

  private class KtClutch {

    public KtDimen maxHeight = KtDimen.ZERO;
    public KtDimen maxDepth = KtDimen.ZERO;
    private final Vector<KtEgg> data = new Vector<>();
    private boolean prevCanPrecedeBin = false;
    private KtEgg lastInfluencing = KtVoidEgg.EGG;
    public boolean suppressingAllowed = true;

    public Iterator iterator() {
      return data.iterator();
    }

    public KtTreatNode appender() {
      return new KtTreatNode() {
        public void execute(KtNode node) {
          // XXX misused property of character node
          if (node.uniformMetric() != KtFontMetric.NULL) addOrd(node, true);
          else addEgg(new KtSimpleNodeEgg(node));
        }
      };
    }

    public void add(KtNoad noad, KtEgg egg) {
      if (noad.influencesBin()) {
        boolean forced = egg.isBin() && !prevCanPrecedeBin;
        if (forced) {
          egg.dontBeBin();
          prevCanPrecedeBin = true;
        } else {
          if (!noad.canFollowBin() && lastInfluencing.isBin()) lastInfluencing.dontBeBin();
          prevCanPrecedeBin = noad.canPrecedeBin();
        }
        lastInfluencing = egg;
      }
      addEgg(egg);
    }

    public void addOrd(KtNode node, boolean suppress) {
      prevCanPrecedeBin = true;
      KtEgg egg = new KtStItalNodeEgg(node, SPACING_TYPE_ORD);
      if (suppress && suppressingAllowed) egg.suppressItalCorr();
      lastInfluencing = egg;
      addEgg(egg);
    }

    private void addEgg(KtEgg egg) {
      maxHeight = maxHeight.max(egg.getHeight());
      maxDepth = maxDepth.max(egg.getDepth());
      data.add(egg);
    }

    public void finish() {
      if (lastInfluencing.isBin()) lastInfluencing.dontBeBin();
    }
  }

  protected class KtTransf implements KtTransformer {

    protected KtConvStyle conv;

    public KtTransf(KtConvStyle conv) {
      this.conv = conv;
    }

    public KtConvStyle getConv() {
      return conv;
    }

    public void setStyle(byte style) {
      conv = conv.makeNew(style);
    }

    public byte getStyle() {
      return conv.getStyle();
    }

    public boolean isCramped() {
      return conv.isCramped();
    }

    public KtDimen muToPt(KtDimen dim) {
      return conv.muToPt(dim);
    }

    public KtGlue muToPt(KtGlue skip) {
      return conv.muToPt(skip);
    }

    public KtDimen scriptSpace() {
      return conv.scriptSpace();
    }

    public KtDimen getDimPar(int param) {
      return conv.getDimPar(param);
    }

    public KtDimen getDimPar(int param, byte how) {
      return conv.derived(how).getDimPar(param);
    }

    public KtNode fetchCharNode(byte fam, KtCharCode code) {
      return conv.fetchCharNode(fam, code);
    }

    public KtNode fetchCharNode(byte fam, KtCharCode code, byte how) {
      return conv.derived(how).fetchCharNode(fam, code);
    }

    public KtNode fetchLargerNode(byte fam, KtCharCode code) {
      return conv.fetchLargerNode(fam, code);
    }

    /* var delimiters are not affected by local style change */
    public KtNode fetchSufficientNode(KtDelimiter del, KtDimen desired) {
      return convStyle.fetchSufficientNode(del, desired);
    }

    public KtBox fetchFittingWidthBox(byte fam, KtCharCode code, KtDimen desired) {
      return conv.fetchFittingWidthBox(fam, code, desired);
    }

    public KtDimen skewAmount(byte fam, KtCharCode code) {
      return conv.skewAmount(fam, code);
    }

    public KtDimen getXHeight(byte fam) {
      return conv.getXHeight(fam);
    }

    /* derived(CURRENT)
     * is necessary for turning penalties off */
    public KtNodeList convert(KtNoadEnum noads) {
      return madeOf(noads, conv.derived(CURRENT));
    }

    public KtNodeList convert(KtNoadEnum noads, byte how) {
      return madeOf(noads, conv.derived(how));
    }
  }

  protected class KtHen extends KtTransf implements KtConverter {

    public KtHen(KtConvStyle conv) {
      super(conv);
    }

    public void push(KtNoadEnum noads) {
      stack.push(noads);
    }

    public KtMathWordBuilder getWordBuilder(byte fam, KtTreatNode proc) {
      return conv.getWordBuilder(fam, proc);
    }

    public boolean forcedItalCorr(byte fam) {
      return conv.forcedItalCorr(fam);
    }
  }

  protected class KtCoop extends KtTransf implements KtNodery {

    private final KtDimen height;
    private final KtDimen depth;

    public KtCoop(KtConvStyle conv, KtDimen height, KtDimen depth) {
      super(conv);
      this.height = height;
      this.depth = depth;
    }

    private final KtNodeList list = new KtNodeList();
    private boolean ignore = false;

    public KtNodeList getList() {
      return list;
    }

    public void setIgnoreSpace(boolean ign) {
      ignore = ign;
    }

    public boolean ignoresSpace() {
      return ignore && conv.isScript();
    }

    public void append(KtNode node) {
      list.append(node);
    }

    public void append(KtNodeEnum nodes) {
      list.append(nodes);
    }

    public KtDimen delimiterSize() {
      KtDimen middle = getDimPar(DP_AXIS_HEIGHT);
      return conv.delimiterSize(height.minus(middle), depth.plus(middle));
    }
  }

  public static KtNodeList madeOf(KtNoadEnum noads, KtConvStyle conv) {
    return new KtConversion(noads, conv).convert();
  }
}

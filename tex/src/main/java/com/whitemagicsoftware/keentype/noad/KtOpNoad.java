// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.OpNoad
// $Id: KtOpNoad.java,v 1.1.1.1 2000/10/05 07:48:36 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtIntVKernNode;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVShiftNode;

public class KtOpNoad extends KtWordPartNoad {

  public static final byte SIDE_LIMITS = 0;
  public static final byte USUAL_LIMITS = 1;
  public static final byte DEFAULT_LIMITS = 2;

  private final byte limits;

  public KtOpNoad(KtField nucleus, byte limits) {
    super(nucleus);
    this.limits = limits;
  }

  public KtOpNoad(KtField nucleus) {
    super(nucleus);
    this.limits = DEFAULT_LIMITS;
  }

  protected String getDesc() {
    return "mathop";
  }

  public boolean acceptsLimits() {
    return true;
  }

  public boolean canPrecedeBin() {
    return false;
  }

  protected byte spacingType() {
    return SPACING_TYPE_OP;
  }

  public KtNoad withLimits(byte limits) {
    return new KtOpNoad(nucleus, limits);
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc());
    switch( limits ) {
      case SIDE_LIMITS -> log.addEsc( "nolimits" );
      case USUAL_LIMITS -> log.addEsc( "limits" );
    }
    nucleus.addOn(log, cntx, '.');
  }

  protected boolean usingLimits(KtConverter conv, boolean display) {
    return switch( limits ) {
      case SIDE_LIMITS -> false;
      case USUAL_LIMITS -> true;
      default -> display;
    };
  }

  public boolean isJustChar() {
    return false;
  }

  public KtEgg convert(KtConverter conv) {
    boolean display = conv.getStyle() == DISPLAY_STYLE;
    return makeEgg(nucleus.makeOperator(conv, display), conv, display);
  }

  public KtEgg wordFinishingEgg(KtMathWordBuilder word, KtConverter conv) {
    if (word.lastHasCollapsed()) return KtEgg.NULL;
    boolean display = conv.getStyle() == DISPLAY_STYLE;
    return makeEgg(nucleus.takeLastOperator(word, conv, display), conv, display);
  }

  private KtEgg makeEgg(KtOperator oper, KtConverter conv, boolean display) {
    return usingLimits(conv, display)
        ? new KtStSimpleNodeEgg(KtVBoxNode.packedOf(oper.getNodeToBeLimited()), spacingType())
        : oper.getEggToBeScripted(spacingType());
  }

  /* TeXtp[750] */
  public KtEgg convertWithScripts(KtConverter conv, KtField sup, KtField sub) {
    boolean display = conv.getStyle() == DISPLAY_STYLE;
    if (!usingLimits(conv, display)) return super.convertWithScripts(conv, sup, sub);
    return makeEggWithScripts(nucleus.makeOperator(conv, display), conv, sup, sub);
  }

  public KtEgg wordFinishingEggWithScripts(
      KtMathWordBuilder word, KtConverter conv, KtField sup, KtField sub) {
    boolean display = conv.getStyle() == DISPLAY_STYLE;
    if (word.lastHasCollapsed() || !usingLimits(conv, display))
      return super.wordFinishingEggWithScripts(word, conv, sup, sub);
    return makeEggWithScripts(nucleus.takeLastOperator(word, conv, display), conv, sup, sub);
  }

  private KtEgg makeEggWithScripts(KtOperator oper, KtConverter conv, KtField sup, KtField sub) {
    KtNode body = oper.getNodeToBeLimited();
    KtDimen width = body.getWidth();
    KtNode supNode = KtNode.NULL;
    KtNode subNode = KtNode.NULL;
    if (!sup.isEmpty()) {
      supNode = sup.cleanBox(conv, SUPER_SCRIPT);
      width = width.max(supNode.getWidth());
    }
    if (!sub.isEmpty()) {
      subNode = sub.cleanBox(conv, SUB_SCRIPT);
      width = width.max(subNode.getWidth());
    }
    body = body.reboxedToWidth(width);
    KtDimen height = body.getHeight();
    KtDimen depth = body.getDepth();
    KtDimen leftX = body.getLeftX();
    KtNodeList list = new KtNodeList();
    KtDimen extra = conv.getDimPar(DP_BIG_OP_SPACING5);
    KtDimen delta = oper.getItalCorr();
    if (delta != KtDimen.NULL) delta = delta.halved();
    if (supNode != KtNode.NULL) {
      supNode = supNode.reboxedToWidth(width);
      leftX = leftX.max(supNode.getLeftX());
      KtDimen clr = conv.getDimPar(DP_BIG_OP_SPACING3).minus(supNode.getDepth());
      clr = clr.max(conv.getDimPar(DP_BIG_OP_SPACING1));
      height = height.plus(extra).plus(supNode.getHeight()).plus(supNode.getDepth()).plus(clr);
      list.append(new KtIntVKernNode(extra));
      list.append( delta == KtDimen.NULL ? supNode : KtVShiftNode.shiftingRight( supNode, delta));
      list.append(new KtIntVKernNode(clr));
    }
    list.append(body);
    if (subNode != KtNode.NULL) {
      subNode = subNode.reboxedToWidth(width);
      leftX = leftX.max(subNode.getLeftX());
      KtDimen clr = conv.getDimPar(DP_BIG_OP_SPACING4).minus(subNode.getHeight());
      clr = clr.max(conv.getDimPar(DP_BIG_OP_SPACING2));
      depth = depth.plus(extra).plus(subNode.getHeight()).plus(subNode.getDepth()).plus(clr);
      list.append(new KtIntVKernNode(clr));
      list.append( delta == KtDimen.NULL ? subNode : KtVShiftNode.shiftingLeft( subNode, delta));
      list.append(new KtIntVKernNode(extra));
    }
    return new KtStSimpleNodeEgg(
        new KtVBoxNode(new KtBoxSizes(height, width, depth, leftX), KtGlueSetting.NATURAL, list),
        spacingType());
  }
}

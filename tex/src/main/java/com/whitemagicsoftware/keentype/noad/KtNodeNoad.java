// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NodeNoad
// $Id: KtNodeNoad.java,v 1.1.1.1 2000/04/07 15:40:03 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtNodeNoad extends KtBaseNodeNoad {

  private final KtNode node;

  public KtNodeNoad(KtNode node) {
    this.node = node;
  }

  public final KtNode getNode() {
    return node;
  }

  public final KtEgg convert(KtConverter conv) {
    return new KtSimpleNodeEgg(node);
  }

  public void addOn(KtLog log, KtCntxLog cntx) {
    node.addOn(log, cntx);
  }
}

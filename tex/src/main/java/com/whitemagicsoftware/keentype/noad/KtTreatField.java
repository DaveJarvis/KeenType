// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.TreatField
// $Id: KtTreatField.java,v 1.1.1.1 2000/02/28 08:07:35 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

public interface KtTreatField {

  KtTreatField NULL = null;

  void execute(KtField field);
}

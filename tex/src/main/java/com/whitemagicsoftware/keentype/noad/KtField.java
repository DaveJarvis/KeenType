// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.Field
// $Id: KtField.java,v 1.1.1.1 2000/10/17 13:26:15 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;

public abstract class KtField implements Serializable, KtTransfConstants {

  public static final KtField NULL = null;

  public abstract void addOn(KtLog log, KtCntxLog cntx, char p);

  public KtNoad ordinaryNoad() {
    return KtNoad.NULL;
  }

  public boolean isEmpty() {
    return false;
  }

  public boolean isJustChar() {
    return false;
  }

  public KtDimen skewAmount(KtConverter conv) {
    return KtDimen.ZERO;
  }

  public KtNode convertedBy(KtConverter conv) {
    return KtNode.NULL;
  }

  /* STRANGE
   * why is cleanBox(x, CURRENT) != convertedBy(x)?
   */
  public KtNode cleanBox(KtConverter conv, byte how) {
    return KtHBoxNode.EMPTY;
  }

  public KtOperator makeOperator(final KtConverter conv, final boolean larger) {
    return new KtOperator() {

      public KtNode getNodeToBeLimited() {
        return cleanBox(conv, CURRENT);
      }

      public KtEgg getEggToBeScripted(byte spType) {
        return new KtStItalNodeEgg(convertedBy(conv), spType);
      }

      public KtDimen getItalCorr() {
        return KtDimen.NULL;
      }
    };
  }

  public KtMathWordBuilder getMathWordBuilder(KtConverter conv, KtTreatNode proc) {
    throw new RuntimeException("non char field cannot provide math word builder");
  }

  public byte wordFamily() {
    return -1;
  }

  public void contributeToWord(KtMathWordBuilder word) {}

  public KtOperator takeLastOperator(KtMathWordBuilder word, KtConverter conv, boolean larger) {
    throw new RuntimeException("non char field cannot be part of math word");
  }
}

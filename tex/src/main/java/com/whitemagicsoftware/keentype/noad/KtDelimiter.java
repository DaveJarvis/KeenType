// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.Delimiter
// $Id: KtDelimiter.java,v 1.1.1.1 2000/04/08 11:54:34 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;

public class KtDelimiter implements KtLoggable {

  public static final KtDelimiter NULL = null;
  public static final KtDelimiter VOID =
      new KtDelimiter((byte) 0, KtCharCode.NULL, (byte) 0, KtCharCode.NULL);

  private final byte smallFam;
  private final KtCharCode smallCode;
  private final byte largeFam;
  private final KtCharCode largeCode;

  public KtDelimiter(byte smallFam, KtCharCode smallCode, byte largeFam, KtCharCode largeCode) {
    this.smallFam = smallFam;
    this.smallCode = smallCode;
    this.largeFam = largeFam;
    this.largeCode = largeCode;
  }

  public byte getSmallFam() {
    return smallFam;
  }

  public KtCharCode getSmallCode() {
    return smallCode;
  }

  public byte getLargeFam() {
    return largeFam;
  }

  public KtCharCode getLargeCode() {
    return largeCode;
  }

  public boolean isVoid() {
    return smallFam == 0
        && smallCode == KtCharCode.NULL
        && largeFam == 0
        && largeCode == KtCharCode.NULL;
  }

  /* STRANGE
   * why not print mnemonic values?
   */
  /* TeXtp[691] */
  public void addOn(KtLog log) {
    int n = (famCharNum(smallFam, smallCode) << 12) + famCharNum(largeFam, largeCode);
    log.add('"').add(Integer.toHexString(n).toUpperCase());
  }

  private int famCharNum(byte fam, KtCharCode code) {
    int n = fam << 8;
    if (code != KtCharCode.NULL) n += code.numValue();
    return n;
  }
}

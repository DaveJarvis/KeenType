// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.WordPartNoad
// $Id: KtWordPartNoad.java,v 1.1.1.1 2000/10/17 13:25:47 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;

public abstract class KtWordPartNoad extends KtNucleusNoad {

  public KtWordPartNoad(KtField nucleus) {
    super(nucleus);
  }

  public boolean canBePartOfWord() {
    return nucleus.isJustChar();
  }

  public byte wordFamily() {
    return nucleus.wordFamily();
  }

  public KtMathWordBuilder getMathWordBuilder(KtConverter conv, KtTreatNode proc) {
    return nucleus.getMathWordBuilder(conv, proc);
  }

  public void contributeToWord(KtMathWordBuilder word) {
    nucleus.contributeToWord(word);
  }

  public KtEgg wordFinishingEgg(KtMathWordBuilder word, KtConverter conv) {
    if (word.lastHasCollapsed()) return KtEgg.NULL;
    return makeEgg(word.takeLastNode());
  }

  public KtEgg wordFinishingEggWithScripts(
      KtMathWordBuilder word, KtConverter conv, KtField sup, KtField sub) {
    KtNode node = word.takeLastNode();
    KtEgg egg = word.lastHasCollapsed() ? new KtStItalNodeEgg( node, SPACING_TYPE_ORD) : makeEgg( node);
    return makeScriptsTo(egg, false, conv, sup, sub);
  }
}

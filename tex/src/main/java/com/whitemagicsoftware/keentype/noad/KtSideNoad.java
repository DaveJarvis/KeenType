// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.SideNoad
// $Id: KtSideNoad.java,v 1.1.1.1 2000/04/08 13:04:18 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtSideNoad extends KtPureNoad {

  protected final KtDelimiter delimiter;

  public KtSideNoad(KtDelimiter delimiter) {
    this.delimiter = delimiter;
  }

  public final boolean influencesBin() {
    return true;
  }

  public KtEgg convert(KtConverter conv) {
    return new KtDelimiterEgg(delimiter, spacingType());
  }

  /* TeXtp[696] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc()).add(delimiter);
  }

  protected abstract String getDesc();

  protected abstract byte spacingType();
}

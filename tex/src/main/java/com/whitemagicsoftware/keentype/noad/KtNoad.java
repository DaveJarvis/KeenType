// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.Noad
// $Id: KtNoad.java,v 1.1.1.1 2000/10/17 13:34:53 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtCntxLoggable;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;

public interface KtNoad extends Serializable, KtCntxLoggable, KtTransfConstants {

  KtNoad NULL = null;

  byte DISPLAY_STYLE = 0;
  byte TEXT_STYLE = 1;
  byte SCRIPT_STYLE = 2;
  byte SCRIPT_SCRIPT_STYLE = 3;
  byte NUMBER_OF_STYLES = 4;

  boolean acceptsLimits();

  boolean isScriptable();

  boolean isOrdinary();

  boolean isJustChar();

  boolean alreadySuperScripted();

  boolean alreadySubScripted();

  void addOnWithScripts(KtLog log, KtCntxLog cntx, KtField sup, KtField sub);

  KtNoad withLimits(byte limits);

  KtField ordinaryField();

  boolean isNode();

  KtNode getNode();

  KtEgg convert(KtConverter conv);

  KtEgg convertWithScripts(KtConverter conv, KtField sup, KtField sub);

  boolean influencesBin();

  boolean canPrecedeBin();

  boolean canFollowBin();

  boolean startsWord();

  boolean canBePartOfWord();

  boolean finishesWord();

  KtMathWordBuilder getMathWordBuilder(KtConverter conv, KtTreatNode proc);

  byte wordFamily();

  void contributeToWord(KtMathWordBuilder word);

  KtEgg wordFinishingEgg(KtMathWordBuilder word, KtConverter conv);

  KtEgg wordFinishingEggWithScripts(KtMathWordBuilder word, KtConverter conv, KtField sup, KtField sub);
}

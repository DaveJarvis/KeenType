// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.noad.NoadList
// $Id: KtNoadList.java,v 1.1.1.1 2001/03/22 15:54:53 ksk Exp $
package com.whitemagicsoftware.keentype.noad;

import java.io.Serializable;
import java.util.Vector;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;

public class KtNoadList implements Serializable {

  public static final KtNoadList NULL = null;

  public static final KtNoadList EMPTY = new KtNoadList();

  public static final KtNoadEnum EMPTY_ENUM =
      new KtNoadEnum() {
        public KtNoad nextNoad() {
          return KtNoad.NULL;
        }

        public boolean hasMoreNoads() {
          return false;
        }
      };

  protected Vector<KtNoad> data;

  protected KtNoadList(Vector<KtNoad> data) {
    this.data = data;
  }

  public KtNoadList() {
    data = new Vector<>();
  }

  public KtNoadList(int initCap) {
    data = new Vector<>( initCap );
  }

  public KtNoadList(int initCap, int capIncrement) {
    data = new Vector<>( initCap, capIncrement );
  }

  public KtNoadList(KtNoad noad) {
    this(1);
    append(noad);
  }

  public KtNoadList(KtNoadEnum noads) {
    this();
    append(noads);
  }

  public KtNoadList(KtNoad[] noads) {
    this(noads.length);
    append(noads);
  }

  public KtNoadList(KtNoad[] noads, int offset, int count) {
    this(count);
    append(noads, offset, count);
  }

  public final int length() {
    return data.size();
  }

  public final boolean isEmpty() {
    return data.isEmpty();
  }

  protected void clear() {
    data.clear();
  }

  public final KtNoad noadAt(int idx) {
    return data.elementAt(idx);
  }

  public KtNoadList append(KtNoad noad) {
    data.addElement(noad);
    return this;
  }

  public KtNoadList append(KtNoad[] noads, int offset, int count) {
    data.ensureCapacity(data.size() + count);
    while (count-- > 0) append(noads[offset++]);
    return this;
  }

  public KtNoadList append(KtNoad[] noads) {
    return append(noads, 0, noads.length);
  }

  public KtNoadList append(KtNoadEnum noads) {
    while (noads.hasMoreNoads()) append(noads.nextNoad());
    return this;
  }

  public KtNoadList append(KtNoadList list) {
    return append(list.noads());
  }

  public KtNoad lastNoad() {
    return length() > 0 ? noadAt( length() - 1) : KtNoad.NULL;
  }

  public void removeLastNoad() {
    if (length() > 0) data.removeElementAt(length() - 1);
  }

  public void replaceLastNoad(KtNoad noad) {
    if (length() > 0) data.set(length() - 1, noad);
  }

  public KtNoad[] toArray() {
    KtNoad[] noads = new KtNoad[data.size()];
    return data.toArray( noads);
  }

  private class KtEnum extends KtNoadEnum {
    private int idx;
    private final int end;

    public KtEnum(int idx, int end) {
      this.idx = idx;
      this.end = end;
    }

    public KtNoad nextNoad() {
      return noadAt(idx++);
    }

    public boolean hasMoreNoads() {
      return idx < end;
    }
  }

  public KtNoadEnum noads() {
    return new KtEnum(0, length());
  }

  public KtNoadEnum noads(int start) {
    return new KtEnum(start, length());
  }

  public KtNoadEnum noads(int start, int end) {
    return new KtEnum(start, end);
  }

  public static KtNoadEnum noads(final KtNoad noad) {
    return new KtNoadEnum() {
      private boolean fresh = true;

      public KtNoad nextNoad() {
        fresh = false;
        return noad;
      }

      public boolean hasMoreNoads() {
        return fresh;
      }
    };
  }

  public String toString() {
    StringBuilder buf = new StringBuilder();
    KtNoadEnum noads = noads();
    while (noads.hasMoreNoads()) buf.append(noads.nextNoad()).append(", ");
    return buf.toString();
  }

  /* ****************************************************************** */

  public KtNoadList append(KtNode node) {
    append(new KtNodeNoad(node));
    return this;
  }

  public KtNoadList append(KtNodeEnum nodes) {
    while (nodes.hasMoreNodes()) append(nodes.nextNode());
    return this;
  }

  public KtNode lastNode() {
    KtNoad last = lastNoad();
    return last != KtNoad.NULL && last.isNode() ? last.getNode() : KtNode.NULL;
  }

  public void removeLastNode() {
    KtNoad last = lastNoad();
    if (last != KtNoad.NULL && last.isNode()) removeLastNoad();
  }

  public KtNode lastSpecialNode() {
    return lastNode();
  }
}

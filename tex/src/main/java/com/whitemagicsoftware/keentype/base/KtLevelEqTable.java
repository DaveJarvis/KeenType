// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.base.LevelEqTable
// $Id: KtLevelEqTable.java,v 1.1.1.1 2000/02/01 00:22:32 ksk Exp $
package com.whitemagicsoftware.keentype.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * |KtLevelEqTable| maintains equivalents for numbers and |Object|s. The equivalent objects can be
 * stored by |put| and retrieved by |get| method. The first parameter of these methods determine the
 * kind of equivalence.
 *
 * <p>|KtLevelEqTable| also maintains levels. When the level is pushed by method |pushLevel| the new
 * equivalents override the old ones for the same key. After invoking |popLevel| the equivalents
 * from top level are thrown away and the old ones are restored. Methods |gput| assigns equivalents
 * globally.
 *
 * <p>Additionally it supports so-called external equivalents. Those are objects which have some
 * associated value which should be saved and restored when the level of |KtLevelEqTable| pushes and
 * pops like the values associated to the keys. Such objects must implement interface |KtExtEquiv| and
 * are maintained by methods |save| and |drop|.
 */
/* For TeX version of table of equivalents see TeXtp[220]. */
public class KtLevelEqTable extends KtEqTable implements Serializable {

  /** abstract interface for internal representation of keys */
  private interface KtTabKey extends Serializable {
    void restored(Object oldVal);

    void retained();
  }

  /** internal representation of numerical keys */
  private static final class KtNumKey implements KtTabKey {

    private final KtNumKind kind;

    /** The numerical key */
    private final int num;

    /**
     * Creates |KtNumKey| for given kind and number.
     *
     * @param k the kind of equivalence.
     * @param n the numerical key.
     */
    public KtNumKey(KtNumKind k, int n) {
      kind = k;
      num = n;
    }

    @Override
    public void restored(Object oldVal) {
      kind.restored(num, oldVal);
    }

    @Override
    public void retained() {
      kind.retained(num);
    }

    /**
     * Hash code of the key representation (used for table lookup).
     *
     * @return the hash code.
     */
    @Override
    public int hashCode() {
      return kind.hashCode() * 383 + num;
    }

    /**
     * Compares the internal key representation to another object. The result is |true| if and only
     * if the object is the same key representation of the same kind.
     *
     * @param o the object to compare this key against.
     * @return |true| if the argument is equal, |false| otherwise.
     */
    @Override
    public boolean equals(final Object o) {
      return o instanceof KtNumKey k &&
        k.kind.equals( kind ) && k.num == num;
    }
  }

  /** internal representation of |Object| keys */
  private static final class KtObjKey implements KtTabKey {

    private final KtObjKind kind;

    /** The object key */
    private final Object obj;

    /**
     * Creates |KtObjKey| for given kind and object.
     *
     * @param k the kind of equivalence.
     * @param o the object key.
     */
    public KtObjKey( final KtObjKind k, final Object o ) {
      kind = k;
      obj = o;
    }

    @Override
    public void restored(final Object oldVal) {
      kind.restored(obj, oldVal);
    }

    @Override
    public void retained() {
      kind.retained(obj);
    }

    /**
     * Compares the internal key representation to another object. The result
     * is |true| if and only if the object is the same key representation of
     * the same kind.
     *
     * @param o the object to compare this key against.
     * @return |true| if the argument is equal, |false| otherwise.
     */
    @Override
    public boolean equals( final Object o ) {
      return o instanceof KtObjKey k &&
        k.kind.equals( kind ) && k.obj.equals( obj );
    }

    /**
     * Hash code of the key representation (used for table lookup).
     *
     * @return the hash code.
     */
    @Override
    public int hashCode() {
      return kind.hashCode() * 383 + obj.hashCode();
    }
  }

  /** internal representation of equivalent */
  private static final class KtTabEntry implements Serializable {

    /** The equivalent value */
    Object val;

    /** The level on which it was pushed */
    int lev;

    /**
     * Creates |KtTabEntry| for given equivalent and level.
     *
     * @param v the equivalent object.
     * @param l the level on which it is pushed.
     */
    KtTabEntry(final Object v, final int l) {
      val = v;
      lev = l;
    }
  }

  /** base class for |KtIntSavEntry| and |KtExtSavEntry| */
  private abstract static class KtSavEntry implements Serializable {

    /** The level on which it was saved */
    final int sav;

    /** The next item in the save stack */
    final KtSavEntry next;

    /**
     * Creates |KtSavEntry|.
     *
     * @param s the save level.
     * @param x the entry below in the stack.
     */
    KtSavEntry(final int s, final KtSavEntry x) {
      sav = s;
      next = x;
    }

    abstract void restore(Map<KtTabKey, KtTabEntry> tab);
  }

  /** Internal representation of saved equivalent */
  private static final class KtIntSavEntry extends KtSavEntry {

    /** The representation of the key */
    final KtTabKey key;

    /** The saved equivalent entry */
    final KtTabEntry ent;

    /**
     * Creates |KtIntSavEntry|.
     *
     * @param k the key.
     * @param e the equivalent entry.
     * @param s the save level.
     * @param x the entry below in the stack.
     */
    KtIntSavEntry( final KtTabKey k,
                 final KtTabEntry e,
                 final int s,
                 final KtSavEntry x ) {
      super(s, x);
      key = k;
      ent = e;
    }

    @Override
    void restore(final Map<KtTabKey, KtTabEntry> tab) {
      final KtTabEntry e = tab.get(key);

      if (ent == null) {
        /* originally the value was not set */

        /* the value was set in popped level */
        if( e != null && e.lev > 0 ) {
          tab.remove( key ); /* throw it away */
          key.restored( e.val );
        }
        else { key.retained(); }

        /* value was overridden in popped level */
      } else if( e == null || ent.lev < e.lev ) {
        tab.put( key, ent ); /* restore it */
        key.restored( e != null ? e.val : null );
      }
      else { key.retained(); }
    }
  }

  /** internal representation of saved externals */
  private static final class KtExtSavEntry extends KtSavEntry {

    /** The value of saved equivalent */
    final Object val;

    /** The level on which it was defined */
    final int lev;

    /** The the external equivalent */
    final KtExtEquiv ext;

    /**
     * Creates |KtExtSavEntry|.
     *
     * @param v the value.
     * @param l the level of definition.
     * @param e the external equivalent.
     * @param s the save level.
     * @param x the entry below in the stack.
     */
    KtExtSavEntry(
      final Object v,
      final int l,
      final KtExtEquiv e,
      final int s,
      final KtSavEntry x) {
      super(s, x);
      val = v;
      lev = l;
      ext = e;
    }

    @Override
    void restore( final Map<KtTabKey, KtTabEntry> tab ) {
      if( lev < ext.getEqLevel() ) {
        ext.restoreEqValue( val );
        ext.setEqLevel( lev );
      }
      else {
        ext.retainEqValue();
      }
    }
  }

  /** table of equivalents for current level */
  private final Map<KtTabKey, KtTabEntry> table = new HashMap<>( 1024 );

  /** stack of saved equivalents for pushed levels */
  private KtSavEntry saves;

  /** current level */
  private int level;

  public KtLevelEqTable() { }

  /**
   * Gives the current level of equivalents.
   *
   * @return current level
   */
  public final int getLevel() {
    return level;
  }

  /**
   * Pushes new level of equivalents.
   */
  public void pushLevel() {
    ++level;
  }

  /**
   * Pops one level of equivalents and restores the saved equivalents from this level.
   */
  public void popLevel() {
    if( level == 0 ) {
      return;
    }

    /* restoring saved equivalents */
    KtSavEntry s;

    while( (s = saves) != null && s.sav == level ) {
      s.restore( table );
      saves = s.next;
    }

    /* pop the level */
    --level;
  }

  /**
   * Gets equivalent object for internal key.
   *
   * @param k internal key.
   * @return equivalent object.
   */
  private Object get( final KtTabKey k ) {
    final KtTabEntry e = table.get( k );
    return e == null ? null : e.val;
  }

  /**
   * Puts the equivalent object for internal key.
   *
   * @param k internal key.
   * @param v equivalent object.
   */
  private void put( final KtTabKey k, final Serializable v ) {
    if( level > 0 ) {
      /* change is not global */
      final KtTabEntry e = table.get( k );

      /* save in each level only once */
      if( e == null || e.lev != level ) {
        saves = new KtIntSavEntry( k, e, level, saves );
      }
    }

    table.put( k, new KtTabEntry( v, level ) );
  }

  private void nastyReplace(KtTabKey k, Serializable v) {
    final KtTabEntry e = table.get( k);
    if (e != null) {
      e.val = v;
    } else {
      if( level > 0 ) { saves = new KtIntSavEntry( k, e, level, saves ); }
      table.put(k, new KtTabEntry(v, level));
    }
  }

  /**
   * Puts the equivalent object for internal key globally.
   *
   * @param k internal key.
   * @param v equivalent object.
   */
  private void gput(KtTabKey k, Serializable v) {
    table.put(k, new KtTabEntry(v, 0));
  }

  /**
   * Gets the equivalent for numeric key of specific kind.
   *
   * @param kind the kind of equivalnce.
   * @param key numeric key.
   * @return equivalent object.
   */
  public final Object get(KtNumKind kind, int key) {
    return get(new KtNumKey(kind, key));
  }

  /**
   * Puts the equivalent for numeric key of specific kind.
   *
   * @param kind the kind of equivalnce.
   * @param key numeric key.
   * @param val equivalent object.
   */
  public final void put(KtNumKind kind, int key, Serializable val) {
    put(new KtNumKey(kind, key), val);
  }

  public final void nastyReplace(KtNumKind kind, int key, Serializable val) {
    nastyReplace(new KtNumKey(kind, key), val);
  }

  /**
   * Puts the equivalent for numeric key of specific kind globally.
   *
   * @param kind the kind of equivalnce.
   * @param key numeric key.
   * @param val equivalent object.
   */
  public final void gput(KtNumKind kind, int key, Serializable val) {
    gput(new KtNumKey(kind, key), val);
  }

  /**
   * Gets the equivalent for object key of specific kind.
   *
   * @param kind the kind of equivalnce.
   * @param key object key.
   * @return equivalent object.
   */
  public final Object get(KtObjKind kind, Serializable key) {
    return get(new KtObjKey(kind, key));
  }

  /**
   * Puts the equivalent for object key of specific kind.
   *
   * @param kind the kind of equivalnce.
   * @param key object key.
   * @param val equivalent object.
   */
  public final void put(KtObjKind kind, Serializable key, Serializable val) {
    put(new KtObjKey(kind, key), val);
  }

  public final void nastyReplace(KtObjKind kind, Serializable key, Serializable val) {
    nastyReplace(new KtObjKey(kind, key), val);
  }

  /**
   * Puts the equivalent for object key of specific kind globally.
   *
   * @param kind the kind of equivalence.
   * @param key object key.
   * @param val equivalent object.
   */
  public final void gput(KtObjKind kind, Serializable key, Serializable val) {
    gput(new KtObjKey(kind, key), val);
  }

  /**
   * Saves the value of external equivalent if necessary.
   *
   * @param ext the external equivalent.
   */
  public void save(KtExtEquiv ext) {
    if (level > 0) {
      /* change is not global */
      int l = ext.getEqLevel();
      if (l != level) saves = new KtExtSavEntry(ext.getEqValue(), l, ext, level, saves);
    }
    ext.setEqLevel(level);
  }
}

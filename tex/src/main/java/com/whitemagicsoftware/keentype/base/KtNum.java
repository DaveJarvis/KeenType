// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.base.Num
// $Id: KtNum.java,v 1.1.1.1 2001/05/17 23:29:55 ksk Exp $
package com.whitemagicsoftware.keentype.base;

import java.io.Serializable;

/**
 * The whole number for representing the \TeX\ values of counters. It is introduced for symetry with
 * other kinds of registers which have their own object types for representation of values (|KtDimen|,
 * |KtGlue|). The instances of class |KtNum| are immutable. It means that each instance represents the
 * same value during its whole life-time and all its methods are free of side-effects. If you need
 * to represent a new value create a new instance. This is done if necessary by all arithmetic
 * operation methods.
 *
 * @author Karel Skoupy
 * @version ${VERSION}
 * @since NTS1.0
 */
public class KtNum implements Serializable, KtIntProvider {

  /** Null constant */
  public static final KtNum NULL = null;

  /** |KtNum| constant representing zero */
  public static final KtNum ZERO = KtNum.valueOf(0);

  /** maximum |int| value which is allowed to be converted to |KtNum| */
  public static final int MAX_INT_VALUE = 0x7fffffff;

  /** KtProvider of |KtNum| value. */
  public interface KtProvider {
    /**
     * Provides the value of type |KtNum|
     *
     * @return the provided |KtNum| value
     */
    KtNum getNumValue();
  }

  /** Internal representation of the whole number */
  private final int value;

  /**
   * Creates |KtNum| representing the given integer.
   *
   * @param num the integer value.
   */
  private KtNum(int num) {
    value = num;
  }

  public static KtNum valueOf(int num) {
    return new KtNum(num);
  }

  public final int intVal() {
    return value;
  }

  public int sign() {
    return Integer.compare( value, 0 );
  }

  public boolean equals(int i) {
    return value == i;
  }

  public boolean equals(KtNum n) {
    return value == n.intVal();
  }

  public boolean lessThan(int i) {
    return value < i;
  }

  public boolean lessThan(KtNum n) {
    return value < n.intVal();
  }

  public boolean moreThan(int i) {
    return value > i;
  }

  public boolean moreThan(KtNum n) {
    return value > n.intVal();
  }

  public KtNum negative() {
    return new KtNum(-value);
  }

  public KtNum plus(int i) {
    return new KtNum(value + i);
  }

  public KtNum plus(KtNum n) {
    return new KtNum(value + n.intVal());
  }

  public KtNum minus(int i) {
    return new KtNum(value - i);
  }

  public KtNum minus(KtNum n) {
    return new KtNum(value - n.intVal());
  }

  public KtNum times(int i) {
    return new KtNum(value * i);
  }

  public KtNum times(KtNum n) {
    return new KtNum(value * n.intVal());
  }

  public KtNum over(int i) {
    return new KtNum(value / i);
  }

  public KtNum over(KtNum n) {
    return new KtNum(value / n.intVal());
  }

  /**
   * Gives the decimal representation of the |KtNum| as a character string.
   *
   * @return the string representation of the |KtNum|.
   */
  public String toString() {
    return Integer.toString(value);
  }

  public String toOctString() {
    return Integer.toOctalString(value);
  }

  public String toHexString() {
    return Integer.toHexString(value);
  }

  /**
   * Gives a hash code for a |KtNum|.
   *
   * @return the hash code for this object.
   */
  public int hashCode() {
    return value;
  }

  /**
   * Compares this |KtNum| to the specified object. The result is |true| if and only if the the
   * argument is not |null| and is the |KtNum| object which represents the same number.
   *
   * @param o the object to compare this |KtNum| against.
   * @return |true| if the argument is equal, |false| otherwise.
   */
  public boolean equals(final Object o) {
    return o instanceof KtNum n && n.value == value;
  }

  private static final class KtRomanDigit {
    char digit;
    int value;
    boolean precedable;

    KtRomanDigit(char d, int v, boolean p) {
      digit = d;
      value = v;
      precedable = p;
    }
  }

  private static final KtRomanDigit[] romanDigits = {
    new KtRomanDigit('m', 1000, false),
    new KtRomanDigit('d', 500, false),
    new KtRomanDigit('c', 100, true),
    new KtRomanDigit('l', 50, false),
    new KtRomanDigit('x', 10, true),
    new KtRomanDigit('v', 5, false),
    new KtRomanDigit('i', 1, true)
  };

  public static String romanString(int n) {
    StringBuilder buf = new StringBuilder();
    for (int i = 0; ; i++) {
      KtRomanDigit dig = romanDigits[i];
      while (n >= dig.value) {
        buf.append(dig.digit);
        n -= dig.value;
      }
      if (n <= 0) break;
      int j = i;
      while (!romanDigits[++j].precedable)
        ;
      if (n + romanDigits[j].value >= dig.value) {
        buf.append(romanDigits[j].digit).append(dig.digit);
        n -= dig.value - romanDigits[j].value;
      }
    }
    return buf.toString();
  }
}

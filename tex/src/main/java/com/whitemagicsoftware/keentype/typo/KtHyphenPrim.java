// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.HyphenPrim
// $Id: KtHyphenPrim.java,v 1.1.1.1 2000/04/30 19:14:12 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtDiscretionaryNode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public class KtHyphenPrim extends KtBuilderPrim {

  public KtHyphenPrim(String name) {
    super(name);
  }

  /* TeXtp[1117] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(final KtBuilder bld, KtToken src) {
          KtNodeList pre = KtNodeList.EMPTY;
          KtFontMetric metric = getCurrFontMetric();
          KtNum num = metric.getNumParam(KtFontMetric.NUM_PARAM_HYPHEN_CHAR);
          if (num != KtNum.NULL) {
            KtCharCode code = KtToken.makeCharCode(num.intVal());
            if (code != KtCharCode.NULL) {
              KtNode node = metric.getCharNode(code);
              if (node != KtNode.NULL) pre = new KtNodeList(node);
              else charWarning(metric, code);
            }
          }
          bld.addNode(new KtDiscretionaryNode(pre, KtNodeList.EMPTY, KtNodeList.EMPTY));
        }
      };
}

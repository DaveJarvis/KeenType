// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.NoBoundaryPrim
// $Id: KtNoBoundaryPrim.java,v 1.1.1.1 2000/11/13 02:00:21 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtWordBuilder;

public class KtNoBoundaryPrim extends KtBuilderPrim {

  public KtNoBoundaryPrim(String name) {
    super(name);
  }

  /* TeXtp[1030] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          KtToken tok = nextExpToken();
          KtCommand cmd = meaningOf(tok);
          KtCharCode code = cmd.charCodeToAdd();
          if (code != KtCharCode.NULL) {
            if (getConfig().getBoolParam(BOOLP_TRACING_COMMANDS)) traceCommand(cmd);
            KtWordBuilder word =
                getCurrFontMetric().getWordBuilder(APPENDER, false, bld.willBeBroken());
            bld.adjustSpaceFactor(code.spaceFactor());
            fixLanguage(bld);
            if (word.add(code)) appendCharsTo(bld, word);
            else charWarning(getCurrFontMetric(), code);
          } else cmd.execute(tok);
        }
      };

  public boolean isNoBoundary() {
    return true;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.KernPrim
// $Id: KtKernPrim.java,v 1.1.1.1 1999/06/10 07:54:50 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtKernPrim extends KtBuilderPrim {

  private final KtDimen kern;

  public KtKernPrim(String name) {
    super(name);
    kern = KtDimen.NULL;
  }

  public KtKernPrim(String name, KtDimen kern) {
    super(name);
    this.kern = kern;
  }

  public KtDimen getKern() {
    return kern != KtDimen.NULL ? kern : scanDimen();
  }

  public void exec(KtBuilder bld, KtToken src) {
    bld.addKern(getKern());
  }
}

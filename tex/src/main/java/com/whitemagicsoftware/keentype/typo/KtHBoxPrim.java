// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.HBoxPrim
// $Id: KtHBoxPrim.java,v 1.1.1.1 1999/07/30 17:44:08 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtGroup;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.node.KtTreatBox;

public class KtHBoxPrim extends KtAnyBoxPrim {

  public KtHBoxPrim(String name, KtTokenList.KtInserter every) {
    super(name, every);
  }

  protected KtGroup makeGroup(KtDimen size, boolean exactly, KtTreatBox proc) {
    return new KtHBoxGroup(size, exactly, proc);
  }
}

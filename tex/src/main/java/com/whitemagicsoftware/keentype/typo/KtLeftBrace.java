// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.LeftBrace
// $Id: KtLeftBrace.java,v 1.1.1.1 2000/02/16 10:03:34 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtLeftBrace extends KtBuilderCommand {

  /* TeXtp[1063] */
  public void exec(KtBuilder bld, KtToken src) {
    // XXX incr(align_state)	[347] [357]
    pushLevel(new KtSimpleGroup());
  }

  public void addOn(KtLog log) {
    log.add("internal begin group");
  }
}

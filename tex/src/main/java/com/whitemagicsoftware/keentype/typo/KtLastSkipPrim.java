// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.LastSkipPrim
// $Id: KtLastSkipPrim.java,v 1.1.1.1 2000/01/10 17:23:57 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtLastSkipPrim extends KtBuilderPrim implements KtGlue.KtProvider {

  public KtLastSkipPrim(String name) {
    super(name);
  }

  public boolean hasGlueValue() {
    return !hasMuGlueValue();
  }

  public boolean hasMuGlueValue() {
    KtNode node = getBld().lastSpecialNode();
    return node != KtNode.NULL && node.isMuSkip();
  }

  /* TeXtp[424] */
  public KtGlue getGlueValue() {
    KtNode node = getBld().lastSpecialNode();
    return node != KtNode.NULL && node.isSkip() ? node.getSkip() : KtGlue.ZERO;
  }

  /* TeXtp[424] */
  public KtGlue getMuGlueValue() {
    KtNode node = getBld().lastSpecialNode();
    return node != KtNode.NULL && node.isMuSkip() ? node.getMuSkip() : KtGlue.ZERO;
  }
}

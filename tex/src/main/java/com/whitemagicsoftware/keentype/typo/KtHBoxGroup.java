// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.HBoxGroup
// $Id: KtHBoxGroup.java,v 1.1.1.1 2001/04/27 16:01:50 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtTreatBox;

public class KtHBoxGroup extends KtHorizGroup {

  protected KtDimen size;
  protected boolean exactly;
  protected KtTreatBox proc;

  public KtHBoxGroup(KtDimen size, boolean exactly, KtTreatBox proc) {
    this.size = size;
    this.exactly = exactly;
    this.proc = proc;
  }

  public void close() {
    super.close();
    KtNodeList list = builder.getList();
    KtNodeEnum mig = proc.wantsMig() ? list.extractedMigrations().nodes() : KtNodeList.EMPTY_ENUM;
    proc.execute(makeBox(list), mig);
  }

  protected KtBox makeBox(KtNodeList list) {
    return KtTypoCommand.packHBox(list, size, exactly);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.TypoPrim
// $Id: KtTypoPrim.java,v 1.1.1.1 1999/08/23 16:01:12 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtTypoPrim extends KtTypoCommand implements KtPrimitive {

  /** The name of the primitive */
  private final String name;

  protected KtTypoPrim(String name) {
    this.name = name;
  }

  public final String getName() {
    return name;
  }

  public final KtCommand getCommand() {
    return this;
  }

  public final void addOn(KtLog log) {
    log.addEsc(name);
  }

  public final String toString() {
    return "@" + name;
  }
}

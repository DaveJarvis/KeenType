// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.UnKernPrim
// $Id: KtUnKernPrim.java,v 1.1.1.1 2000/01/10 21:54:50 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtUnKernPrim extends KtBuilderPrim {

  public KtUnKernPrim(String name) {
    super(name);
  }

  /* TeXtp[1105,1106] */
  public void exec(KtBuilder bld, KtToken src) {
    if (bld.canTakeLastNode()) {
      KtNode node = bld.lastNode();
      if (node != KtNode.NULL && node.isKern()) bld.removeLastNode();
    } else error("CantDeleteLastKern", this, bld);
    /* STRANGE
     * See KtUnSkipPrim.exec() and see also the difference.
     * Unability of taking a kern is reported even if there
     * was no last kern.
     */
  }
}

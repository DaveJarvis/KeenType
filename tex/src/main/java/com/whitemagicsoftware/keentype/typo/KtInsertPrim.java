// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.InsertPrim
// $Id: KtInsertPrim.java,v 1.1.1.1 2000/06/22 18:38:45 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtInsertNode;
import com.whitemagicsoftware.keentype.node.KtInsertion;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;

public class KtInsertPrim extends KtBuilderPrim {

  public KtInsertPrim(String name) {
    super(name);
  }

  /* TeXtp[1099] */
  public void exec(KtBuilder bld, KtToken src) {
    int num = KtPrim.scanRegisterCode();
    int outBoxNum = getConfig().getIntParam(INTP_OUTPUT_BOX_NUM);
    if (num == outBoxNum) {
      num = 0;
      error("CantInsertOutbox", KtInsertPrim.this, num(outBoxNum), num(num));
    }
    pushLevel(new KtInsertGroup(num));
    scanLeftBrace();
  }

  public static final int INTP_FLOATING_PENALTY = newIntParam();

  /* TeXtp[1100] */
  public static class KtInsertGroup extends KtVertGroup {

    private final int num;

    protected KtInsertGroup(int num) {
      this.num = num;
    }

    private KtGlue topSkip;
    private KtDimen maxDepth;
    private int floatCost;

    public void stop() {
      super.stop();
      topSkip = getConfig().getGlueParam(GLUEP_SPLIT_TOP_SKIP);
      maxDepth = getConfig().getDimParam(DIMP_SPLIT_MAX_DEPTH);
      floatCost = getConfig().getIntParam(INTP_FLOATING_PENALTY);
    }

    public void close() {
      super.close();
      KtNodeList list = builder.getList();
      KtVBoxNode vbox = KtVBoxNode.packedOf(list);
      KtDimen size = vbox.getHeight().plus(vbox.getDepth());
      KtBuilder bld = getBld();
      bld.addNode(
          new KtInsertNode(
              new KtInsertion(num, list, size, topSkip, maxDepth, KtNum.valueOf(floatCost))));
      bld.buildPage();
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.PageDimenPrim
// $Id: KtPageDimenPrim.java,v 1.1.1.1 2000/01/13 16:00:09 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtAssignPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtPageSplit;

public abstract class KtPageDimenPrim extends KtAssignPrim implements KtDimen.KtProvider {

  protected KtPageSplit page;

  public KtPageDimenPrim(String name, KtPageSplit page) {
    super(name);
    this.page = page;
  }

  /* STRANGE
   * \global\pagegoal is allowed but has no effect
   */
  /* TeXtp[1245] */
  protected void assign(KtToken src, boolean glob) {
    skipOptEquals();
    KtDimen dim = scanDimen();
    if (page.canChangeDimens()) set(dim);
  }

  /* TeXtp[421] */
  public boolean hasDimenValue() {
    return true;
  }

  public final KtDimen getDimenValue() {
    return get();
  }

  protected abstract KtDimen get();

  protected abstract void set(KtDimen dim);
}

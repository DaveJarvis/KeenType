// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.FontNumPrim
// $Id: KtFontNumPrim.java,v 1.1.1.1 1999/06/24 10:31:59 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtFontMetric;

public class KtFontNumPrim extends KtTypoAssignPrim implements KtNum.KtProvider {

  private final int index;

  public KtFontNumPrim(String name, int index) {
    super(name);
    this.index = index;
  }

  /* STRANGE
   * \global\fontdimen is allowed but has no effect
   */
  /* TeXtp[1253] */
  protected void assign(KtToken src, boolean glob) {
    KtFontMetric metric = scanFontMetric();
    skipOptEquals();
    metric.setNumParam(index, scanNum());
  }

  public boolean hasNumValue() {
    return true;
  }

  /* TeXtp[426] */
  public KtNum getNumValue() {
    KtFontMetric metric = scanFontMetric();
    return metric.getNumParam(index);
  }
}

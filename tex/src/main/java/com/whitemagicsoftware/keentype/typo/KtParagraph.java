// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.Paragraph
// $Id: KtParagraph.java,v 1.1.1.1 2001/03/20 09:41:50 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtParBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtBreaker;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHyphenNodeEnum;
import com.whitemagicsoftware.keentype.node.KtLanguage;
import com.whitemagicsoftware.keentype.node.KtLinesShape;
import com.whitemagicsoftware.keentype.node.KtNamedHSkipNode;
import com.whitemagicsoftware.keentype.node.KtNetDimen;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtPenaltyNode;
import com.whitemagicsoftware.keentype.node.KtVShiftNode;

public abstract class KtParagraph extends KtTypoCommand {

  public static final int DIMP_PAR_INDENT = newDimParam();
  public static final int TOKSP_EVERY_PAR = newToksParam();
  public static final int INTP_INTER_LINE_PENALTY = newIntParam();
  public static final int INTP_BROKEN_PENALTY = newIntParam();
  public static final int INTP_CLUB_PENALTY = newIntParam();
  public static final int INTP_WIDOW_PENALTY = newIntParam();

  public static void makeIndent(KtBuilder bld) {
    bld.addBox(
        new KtHBoxNode(
            new KtBoxSizes(
                KtDimen.ZERO, getConfig().getDimParam(DIMP_PAR_INDENT), KtDimen.ZERO, KtDimen.ZERO),
            KtGlueSetting.NATURAL,
            KtNodeList.EMPTY));
  }

  public static final int GLUEP_PAR_SKIP = newGlueParam();

  /* TeXtp[1091] */
  public static void start(boolean indent) {
    KtCommand.KtConfig cfg = getConfig();
    KtBuilder old = getBld();
    old.setPrevGraf(0);
    if (old.needsParSkip())
      old.addSkip(cfg.getGlueParam(GLUEP_PAR_SKIP), cfg.getGlueName(GLUEP_PAR_SKIP));
    KtBuilder par = new KtParBuilder(currLineNumber(), getTypoConfig().getLanguage());
    KtBuilder.push(par);
    if (indent) makeIndent(par);
    cfg.getToksInserter(TOKSP_EVERY_PAR).insertToks();
    old.buildPage(); // XXX is this order unavoidable?
  }

  public static void finish() {
    KtBuilder parBld = getBld();
    KtNodeList list = parBld.getParagraph();
    if (list != KtNodeList.NULL) {
      lineBreak(
          list,
          parBld.getStartLine(),
          getConfig().getIntParam(INTP_WIDOW_PENALTY),
          parBld.getInitLang(),
          KtDimen.NULL_PAR);
      getTypoConfig().resetParagraph();
      getIOHandler().resetErrorCount();
    }
  }

  public static final int BOOLP_TRACING_PARAGRAPHS = newBoolParam();
  public static final int BOOLP_UC_HYPH = newBoolParam();
  public static final int INTP_PRETOLERANCE = newIntParam();
  public static final int INTP_TOLERANCE = newIntParam();
  public static final int INTP_LOOSENESS = newIntParam();
  public static final int INTP_LINE_PENALTY = newIntParam();
  public static final int INTP_HYPHEN_PENALTY = newIntParam();
  public static final int INTP_EX_HYPHEN_PENALTY = newIntParam();
  public static final int INTP_ADJ_DEMERITS = newIntParam();
  public static final int INTP_DOUBLE_HYPHEN_DEMERITS = newIntParam();
  public static final int INTP_FINAL_HYPHEN_DEMERITS = newIntParam();
  public static final int DIMP_EMERGENCY_STRETCH = newDimParam();
  public static final int GLUEP_LEFT_SKIP = newGlueParam();
  public static final int GLUEP_RIGHT_SKIP = newGlueParam();
  public static final int GLUEP_PAR_FILL_SKIP = newGlueParam();

  /* TeXtp[815] */
  public static void lineBreak(
      KtNodeList list,
      int startingLine,
      int widowPenalty,
      KtLanguage initLang,
      KtDimen.KtPar lastVisibleWidth) {
    if (list.isEmpty()) KtBuilder.pop();
    else {
      KtCommand.KtConfig cfg = getConfig();
      KtConfig tcfg = getTypoConfig();
      KtNode last = list.lastNode();
      if (last.isSkip()) list.removeLastNode();
      list.append(new KtPenaltyNode(KtNum.valueOf(KtNode.INF_PENALTY)));
      list.append(
          new KtNamedHSkipNode(
              cfg.getGlueParam(GLUEP_PAR_FILL_SKIP), cfg.getGlueName(GLUEP_PAR_FILL_SKIP)));
      KtBuilder.pop();
      boolean marginSkipHadInfiniteShrink = false;
      boolean tracing = cfg.getBoolParam(BOOLP_TRACING_PARAGRAPHS);
      KtGlue left = cfg.getGlueParam(GLUEP_LEFT_SKIP);
      KtGlue right = cfg.getGlueParam(GLUEP_RIGHT_SKIP);
      if (left.getShrOrder() != KtGlue.NORMAL && !left.getShrink().isZero()
          || right.getShrOrder() != KtGlue.NORMAL && !right.getShrink().isZero()) {
        marginSkipHadInfiniteShrink = true;
        tcfg.setMarginSkipsShrinkFinite();
        left = cfg.getGlueParam(GLUEP_LEFT_SKIP);
        right = cfg.getGlueParam(GLUEP_RIGHT_SKIP);
      }
      KtNode leftSkip =
          left.isZero() ? KtNode.NULL : new KtNamedHSkipNode( left, cfg.getGlueName( GLUEP_LEFT_SKIP));
      KtNode rightSkip = new KtNamedHSkipNode(right, cfg.getGlueName(GLUEP_RIGHT_SKIP));
      KtNetDimen background = new KtNetDimen(cfg.getGlueParam(GLUEP_LEFT_SKIP));
      background.add(cfg.getGlueParam(GLUEP_RIGHT_SKIP));
      KtLinesShape shape = tcfg.linesShape();
      KtBuilder bld = getBld();
      int lineNo = bld.getPrevGraf();
      KtParBreaker breaker =
          new KtParBreaker(
              list.nodes(),
              shape,
              lineNo,
              cfg.getIntParam(INTP_LOOSENESS),
              cfg.getIntParam(INTP_LINE_PENALTY),
              cfg.getIntParam(INTP_HYPHEN_PENALTY),
              cfg.getIntParam(INTP_EX_HYPHEN_PENALTY),
              cfg.getIntParam(INTP_ADJ_DEMERITS),
              cfg.getIntParam(INTP_DOUBLE_HYPHEN_DEMERITS),
              cfg.getIntParam(INTP_FINAL_HYPHEN_DEMERITS),
              cfg.getBoolParam(BOOLP_TRACING_PARAGRAPHS));
      if (marginSkipHadInfiniteShrink) breaker.infiniteShrinkageError();
      int threshold = cfg.getIntParam(INTP_PRETOLERANCE);
      if (threshold >= 0) {
        if (tracing) diagLog.startLine().add("@firstpass");
        breaker.breakToLines(background, threshold, false);
        if (!breaker.successfullyBroken() && tracing) diagLog.startLine().add("@secondpass");
      }
      if (!breaker.successfullyBroken()) {
        KtDimen emergStr = cfg.getDimParam(DIMP_EMERGENCY_STRETCH);
        threshold = cfg.getIntParam(INTP_TOLERANCE);
        tcfg.preparePatterns();
        breaker.refeed(new KtHyphNodeEnum(list.nodes(), initLang, cfg.getBoolParam(BOOLP_UC_HYPH)));
        breaker.breakToLines(background, threshold, !emergStr.moreThan(0));
        if (!breaker.successfullyBroken()) {
          if (tracing) diagLog.startLine().add("@emergencypass");
          background.addStretch(KtGlue.NORMAL, emergStr);
          breaker.breakToLines(background, threshold, true);
        }
      }
      if (tracing) diagLog.startLine().endLine();
      if (breaker.hasMoreLines()) {
        KtLinePacker packer = new KtLinePacker(startingLine);
        boolean club = true;
        boolean hyph = breaker.nextLineWasHyphenated();
        KtNodeList line = breaker.getNextLine();
        KtHBoxNode lastHBox;
        KtDimen lastIndent;
        for (; ; ) {
          line.append(rightSkip);
          if (leftSkip != KtNode.NULL) {
            KtNodeList old = line;
            line = new KtNodeList(leftSkip);
            line.append(old); // XXX awful
          }
          KtNodeEnum mig =
              bld.wantsMigrations() ? line.extractedMigrations().nodes() : KtNodeList.EMPTY_ENUM;
          lastHBox = packer.packHBox(line, shape.getWidth(lineNo), true);
          KtNode box = lastHBox;
          lastIndent = shape.getIndent(lineNo);
          box = KtVShiftNode.shiftingRight(box, lastIndent);
          appendBox(bld, box, mig, false);
          lineNo++;
          if (!breaker.hasMoreLines()) break;
          int pen = cfg.getIntParam(INTP_INTER_LINE_PENALTY);
          if (club) pen += cfg.getIntParam(INTP_CLUB_PENALTY);
          if (hyph) pen += cfg.getIntParam(INTP_BROKEN_PENALTY);
          club = false;
          hyph = breaker.nextLineWasHyphenated();
          line = breaker.getNextLine();
          if (!breaker.hasMoreLines()) pen += widowPenalty;
          if (pen != 0) bld.addPenalty(KtNum.valueOf(pen));
        }
        bld.setPrevGraf(lineNo);
        if (lastVisibleWidth != KtDimen.NULL_PAR) {
          KtDimen visible = lastHBox.allegedlyVisibleWidth();
          if (visible != KtDimen.NULL) visible = visible.plus(lastIndent);
          lastVisibleWidth.set(visible);
        }
      }
    }
  }

  private static class KtParBreaker extends KtBreaker {

    private final boolean tracing;

    public KtParBreaker(
        KtNodeEnum nodeEnum,
        KtLinesShape shape,
        int firstLineNo,
        int looseness,
        int linePen,
        int hyphPen,
        int exHyphPen,
        int adjDem,
        int dblHyphDem,
        int finHyphDem,
        boolean tracing) {
      super(
          nodeEnum,
          shape,
          firstLineNo,
          looseness,
          linePen,
          hyphPen,
          exHyphPen,
          adjDem,
          dblHyphDem,
          finHyphDem);
      this.tracing = tracing;
    }

    private int lastPrinted;
    private KtFontMetric lastMetric;

    protected void reset() {
      super.reset();
      lastPrinted = -1;
      lastMetric = KtFontMetric.NULL;
    }

    protected void traceBreak(int idx, int serial, int bad, int pen, int dem, boolean artificial) {
      if (tracing) {
        if (lastPrinted < idx) {
          diagLog.startLine();
          do
            if (stillNodeAt(++lastPrinted))
              lastMetric = nodeAt(lastPrinted).addShortlyOn(diagLog, lastMetric);
          while (lastPrinted < idx);
        }
        diagLog.startLine().add('@');
        if (stillNodeAt(idx)) nodeAt(idx).addBreakDescOn(diagLog);
        else diagLog.addEsc("par");
        diagLog.add(" via @@").add(serial).add(" b=");
        if (bad > KtDimen.INF_BAD) diagLog.add('*');
        else diagLog.add(bad);
        diagLog.add(" p=").add(pen).add(" d=");
        if (artificial) diagLog.add('*');
        else diagLog.add(dem);
        // D*/ diagLog.endLine();
      }
    }

    protected void traceBreak(KtBreak brk) {
      if (tracing) {
        diagLog
            .startLine()
            .add("@@")
            .add(brk.serial)
            .add(": line ")
            .add(brk.lineNo)
            .add('.')
            .add(brk.fitness);
        if (brk.hyphenated) diagLog.add('-');
        diagLog.add(" t=").add(brk.demerits).add(" -> @@").add(brk.prev.serial);
        // D*/ diagLog.endLine();
      }
    }

    private boolean infiniteShrinkageSeen;

    protected void infiniteShrinkageError() {
      if (!infiniteShrinkageSeen) {
        error("InfShringInPar");
        infiniteShrinkageSeen = true;
      }
    }
  }

  private static class KtLinePacker extends KtHBoxPacker {

    protected final int startLine;

    public KtLinePacker(int startLine) {
      this.startLine = startLine;
    }

    protected void reportLocation(KtLog log) {
      log.add("in paragraph at lines ").add(startLine).add("--").add(currLineNumber());
    }
  }

  private static class KtHyphNodeEnum extends KtHyphenNodeEnum {

    public KtHyphNodeEnum(KtNodeEnum in, KtLanguage lang, boolean ucHyph) {
      super(in, lang, ucHyph);
    }

    protected KtCharCode hyphenChar(KtFontMetric metric) {
      KtNum num = metric.getNumParam(KtFontMetric.NUM_PARAM_HYPHEN_CHAR);
      return num != KtNum.NULL ? KtToken.makeCharCode( num.intVal()) : KtCharCode.NULL;
    }

    protected void complain(KtFontMetric metric, KtCharCode code) {
      charWarning(metric, code);
    }
  }
}

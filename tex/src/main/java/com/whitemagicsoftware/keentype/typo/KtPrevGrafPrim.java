// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.PrevGrafPrim
// $Id: KtPrevGrafPrim.java,v 1.1.1.1 2000/02/07 11:56:33 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtPrevGrafPrim extends KtTypoAssignPrim implements KtNum.KtProvider {

  public KtPrevGrafPrim(String name) {
    super(name);
  }

  /* STRANGE
   * \global\prevgraf is allowed but has no effect
   */
  /* TeXtp[1244] */
  protected void assign(KtToken src, boolean glob) {
    skipOptEquals();
    int pg = scanInt();
    if (pg < 0) error("BadPrevGraf", this, num(pg));
    else getBld().setPrevGraf(pg);
  }

  public boolean hasNumValue() {
    return true;
  }

  /* TeXtp[422] */
  public KtNum getNumValue() {
    return KtNum.valueOf(getBld().getPrevGraf());
  }
}

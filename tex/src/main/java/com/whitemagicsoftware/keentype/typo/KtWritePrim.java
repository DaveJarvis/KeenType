// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.WritePrim
// $Id: KtWritePrim.java,v 1.1.1.1 2000/05/27 02:14:18 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtNullBuilder;
import com.whitemagicsoftware.keentype.command.KtFrozenToken;
import com.whitemagicsoftware.keentype.command.KtLeftBraceToken;
import com.whitemagicsoftware.keentype.command.KtMacro;
import com.whitemagicsoftware.keentype.command.KtMacroBody;
import com.whitemagicsoftware.keentype.command.KtPrefixPrim;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtRightBraceToken;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBaseNode;
import com.whitemagicsoftware.keentype.node.KtSettingContext;
import com.whitemagicsoftware.keentype.node.KtTypesetter;

public class KtWritePrim extends KtBuilderPrim {

  private transient HashMap<Integer, KtLog> table;

  private void initTable() {
    table = new HashMap<>( 23 );
  }

  public KtWritePrim(String name) {
    super(name);
    initTable();
  }

  private void readObject(ObjectInputStream input) throws IOException, ClassNotFoundException {
    input.defaultReadObject();
    initTable();
  }

  public KtLog get(int num) {
    return table.get( num );
  }

  private KtLog replace(int num, KtLog output) {
    return table.put( num, output);
    // XXX remove if NULL
  }

  public void set(int num, KtLog output) {
    KtLog old = replace(num, output);
    if (old != output && old != KtLog.NULL) old.close();
  }

  public void exec(KtBuilder bld, KtToken src) {
    final int num = scanInt();
    final KtTokenList list = KtPrim.scanTokenList(src, false);
    bld.addNode(new KtWriteNode(num, list));
  }

  /* TeXtp[1352] */
  public boolean immedExec(KtToken src) {
    int num = scanInt();
    KtTokenList list = KtPrim.scanTokenList(src, false);
    write(num, list);
    return true;
  }

  private static final KtToken FROZEN_END_WRITE =
      new KtFrozenToken("endwrite", new KtMacro(KtMacroBody.EMPTY, KtPrefixPrim.OUTER));

  private static final KtToken[] END_TOKS = {KtRightBraceToken.TOKEN, FROZEN_END_WRITE};

  private static final KtTokenList END_LIST = new KtTokenList(END_TOKS);

  private static final KtNullBuilder NULL_BUILDER = new KtNullBuilder();

  /* TeXtp[1370] */
  protected void write(int num, KtTokenList list) {
    insertList(END_LIST);
    tracedPushXList(list, "write");
    insertTokenWithoutCleaning(KtLeftBraceToken.TOKEN);
    KtBuilder.push(NULL_BUILDER);
    // list = KtPrim.scanTokenList(esc(getName()), true);
    list = KtPrim.scanTokenList(this, true);
    KtToken tok = nextRawToken();
    if (tok != FROZEN_END_WRITE) {
      error("UnbalancedWrite");
      do tok = nextRawToken();
      while (tok != FROZEN_END_WRITE);
    }
    KtBuilder.pop();
    getTokStack().dropFinishedPop();
    KtLog output;
    if (num < 0) output = fileLog.startLine();
    else if ((output = get(num)) == KtLog.NULL) output = normLog.startLine();
    output.add(list).endLine();
  }

  protected abstract static class KtFileNode extends KtBaseNode {
    /* root corresponding to whatsit_node */

    protected int num;

    public KtFileNode(int num) {
      this.num = num;
    }

    public boolean sizeIgnored() {
      return true;
    }

    /* TeXtp[1355] */
    protected KtLog addName(KtLog log, String name) {
      log.addEsc(name);
      return num < 0
          ? log.add('-')
          : num > getConfig().getIntParam(KtPrim.INTP_MAX_FILE_CODE) ? log.add( '*') : log.add( num);
    }

    public byte beforeWord() {
      return SKIP;
    }

    public byte afterWord() {
      return SUCCESS;
    }
  }

  protected class KtWriteNode extends KtFileNode {
    /* corresponding to whatsit_node */

    protected KtTokenList list;

    public KtWriteNode(int num, KtTokenList list) {
      super(num);
      this.list = list;
    }

    /* TeXtp[1356] */
    public void addOn(KtLog log, KtCntxLog cntx) {
      addNodeToks(addName(log, "write"), list);
    }

    /* TeXtp[1366, 1367] */
    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      if (sctx.allowIO) write(num, list);
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.CharPrim
// $Id: KtCharPrim.java,v 1.1.1.1 2000/02/17 17:00:28 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtCharPrim extends KtBuilderPrim {

  public KtCharPrim(String name) {
    super(name);
  }

  /* TeXtp[1031] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          appendChar(bld, charCodeToAdd());
        }
      };

  public KtCharCode charCodeToAdd() {
    KtCharCode code = KtToken.makeCharCode(KtPrim.scanCharacterCode());
    if (code == KtCharCode.NULL) throw new RuntimeException("no char number scanned");
    return code;
  }
}

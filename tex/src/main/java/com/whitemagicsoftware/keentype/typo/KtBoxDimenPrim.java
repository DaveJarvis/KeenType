// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.BoxDimenPrim
// $Id: KtBoxDimenPrim.java,v 1.1.1.1 1999/08/02 12:09:24 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtAssignPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;

public abstract class KtBoxDimenPrim extends KtAssignPrim implements KtDimen.KtProvider {

  protected final KtSetBoxPrim reg;

  public KtBoxDimenPrim(String name, KtSetBoxPrim reg) {
    super(name);
    this.reg = reg;
  }

  /* STRANGE
   * \global\hd is allowed but has no effect
   */
  /* TeXtp[1247] */
  protected void assign(KtToken src, boolean glob) {
    int idx = scanRegisterCode();
    KtBox box = reg.get(idx);
    skipOptEquals();
    KtDimen dimen = scanDimen();
    KtBoxSizes sizes = box.getSizes();
    if (sizes != KtBoxSizes.NULL) reg.foist(idx, box.pretendSizesCopy(changeSizes(sizes, dimen)));
  }

  public boolean hasDimenValue() {
    return true;
  }

  /* TeXtp[420] */
  public KtDimen getDimenValue() {
    KtBoxSizes sizes = reg.get(scanRegisterCode()).getSizes();
    return sizes != KtBoxSizes.NULL ? selectSize( sizes) : KtDimen.ZERO;
  }

  protected abstract KtBoxSizes changeSizes(KtBoxSizes sizes, KtDimen dimen);

  protected abstract KtDimen selectSize(KtBoxSizes sizes);
}

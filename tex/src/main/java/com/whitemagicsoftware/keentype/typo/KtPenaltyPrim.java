// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.PenaltyPrim
// $Id: KtPenaltyPrim.java,v 1.1.1.1 2000/04/29 13:09:59 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtPenaltyPrim extends KtBuilderPrim {

  private final KtNum pen;

  public KtPenaltyPrim(String name) {
    super(name);
    pen = KtNum.NULL;
  }

  public KtPenaltyPrim(String name, KtNum pen) {
    super(name);
    this.pen = pen;
  }

  public KtNum getPenalty() {
    return pen != KtNum.NULL ? pen : scanNum();
  }

  /* TeXtp[1103] */
  public void exec(KtBuilder bld, KtToken src) {
    bld.addPenalty(getPenalty());
    bld.buildPage();
  }
}

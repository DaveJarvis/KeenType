// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.DiscretionaryPrim
// $Id: KtDiscretionaryPrim.java,v 1.1.1.1 2000/06/07 04:58:39 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtHBoxBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtDiscretionaryNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;

public class KtDiscretionaryPrim extends KtBuilderPrim {

  public KtDiscretionaryPrim(String name) {
    super(name);
  }

  /* TeXtp[1117,1119] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(final KtBuilder bld, KtToken src) {
          bld.addNode(KtDiscretionaryNode.EMPTY);
          pushLevel(new KtDiscGroup());
        }
      };

  public class KtDiscGroup extends KtSimpleGroup {

    private final KtNodeList[] lists = new KtNodeList[3];
    private KtHBoxBuilder builder;
    private int index = 0;

    {
      lists[0] = lists[1] = lists[2] = KtNodeList.EMPTY;
    }

    public void start() {
      builder = new KtHBoxBuilder(currLineNumber());
      KtBuilder.push(builder);
      scanLeftBrace();
    }

    /* TeXtp[1119-1121] */
    public void close() {
      KtBuilder.pop();
      KtNodeList list = builder.getList();
      for (int i = 0; i < list.length(); i++)
        if (!list.nodeAt(i).canBePartOfDiscretionary()) {
          error("ImproperDisc");
          addItemsOnDiagLog("The following discretionary sublist has been deleted:", list.nodes(i));
          list = new KtNodeList(list.nodes(0, i));
          break;
        }
      lists[index] = list;
      KtBuilder bld = getBld();
      if (++index < lists.length) pushLevel(this);
      else {
        if (bld.forbidsThirdPartOfDiscretionary() && !list.isEmpty()) {
          error("IllegalMathDisc", KtDiscretionaryPrim.this);
          lists[index - 1] = KtNodeList.EMPTY;
        } else if (list.length() > KtDiscretionaryNode.MAX_LIST_LENGTH) {
          error("TooLongDisc");
          bld.removeLastNode();
          bld.addNode(new KtDiscretionaryNode(lists[0], lists[1], KtNodeList.EMPTY));
          bld.addNodes(list.nodes());
          return;
        }
      }
      bld.removeLastNode();
      bld.addNode(new KtDiscretionaryNode(lists[0], lists[1], lists[2]));
    }
  }
}

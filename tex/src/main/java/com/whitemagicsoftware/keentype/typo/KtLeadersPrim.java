// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.LeadersPrim
// $Id: KtLeadersPrim.java,v 1.1.1.1 1999/09/06 08:06:12 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.node.KtAdjustLeaders;
import com.whitemagicsoftware.keentype.node.KtBoxLeaders;
import com.whitemagicsoftware.keentype.node.KtLeaders;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtLeadersPrim extends KtAnyLeadersPrim {

  public KtLeadersPrim(String name) {
    super(name);
  }

  protected String getDesc() {
    return KtAdjustLeaders.DESCRIPTOR;
  }

  protected KtLeaders makeLeaders(KtNode node, KtBoxLeaders.KtMover mover) {
    return new KtAdjustLeaders(node, mover);
  }
}

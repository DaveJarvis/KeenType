// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.AnySkipPrim
// $Id: KtAnySkipPrim.java,v 1.1.1.1 2000/02/16 01:01:49 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtAnySkipPrim extends KtBuilderPrim {

  private final KtGlue skip;

  public KtAnySkipPrim(String name, KtGlue skip) {
    super(name);
    this.skip = skip;
  }

  public KtAnySkipPrim(String name) {
    super(name);
    this.skip = KtGlue.NULL;
  }

  public KtGlue getSkip() {
    return skip != KtGlue.NULL ? skip : scanGlue();
  }

  /* TeXtp[1057,1060] */
  public final KtAction NORMAL =
      new KtAction() {

        public void exec(KtBuilder bld, KtToken src) {
          bld.addSkip(getSkip());
        }

        public KtGlue getSkipForLeaders(KtBuilder bld) {
          return getSkip();
        }
      };
}

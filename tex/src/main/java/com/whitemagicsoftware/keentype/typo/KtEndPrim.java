// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.EndPrim
// $Id: KtEndPrim.java,v 1.1.1.1 2000/01/28 15:57:33 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtPenaltyNode;
import com.whitemagicsoftware.keentype.node.KtVSkipNode;

public class KtEndPrim extends KtBuilderPrim {

  private final boolean dumping;

  public KtEndPrim( String name, boolean dumping ) {
    super( name );
    this.dumping = dumping;
  }

  public static final int DIMP_HSIZE = newDimParam();

  /* TeXtp[1054] */
  public final KtAction NORMAL =
    new KtAction() {
      public void exec( KtBuilder bld, KtToken src ) {
        if( bld.isEmpty() && !getTypoConfig().pendingOutput() ) {
          endMainLoop( dumping );
        }
        else {
          backToken( src );
          bld.addBox(
            new KtHBoxNode(
              new KtBoxSizes(
                KtDimen.ZERO,
                getConfig().getDimParam( DIMP_HSIZE ),
                KtDimen.ZERO,
                KtDimen.ZERO ),
              KtGlueSetting.NATURAL,
              KtNodeList.EMPTY ) );
          bld.addNode(
            new KtVSkipNode(
              KtGlue.valueOf( KtDimen.ZERO,
                            KtDimen.UNITY,
                            KtGlue.FILL,
                            KtDimen.ZERO,
                            KtGlue.NORMAL ) ) );
          bld.addNode( new KtPenaltyNode( KtNum.valueOf( -0x40000000 ) ) );
          bld.buildPage();
        }
      }
    };
}

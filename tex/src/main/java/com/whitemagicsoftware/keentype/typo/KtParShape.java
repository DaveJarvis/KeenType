// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.ParShape
// $Id: KtParShape.java,v 1.1.1.1 2000/04/19 00:18:53 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtLinesShape;

public class KtParShape implements Serializable, KtLinesShape {

  public static final KtParShape NULL = null;

  private static final KtDimen[] EMPTY_DIMENS = new KtDimen[0];

  public static final KtParShape EMPTY = new KtParShape(EMPTY_DIMENS, EMPTY_DIMENS);

  public interface KtProvider {
    KtParShape getParShapeValue();
  }

  private final KtDimen[] indents;
  private final KtDimen[] widths;

  public KtParShape(KtDimen[] indents, KtDimen[] widths) {
    if (indents.length != widths.length) throw new RuntimeException("bad arguments");
    this.indents = indents;
    this.widths = widths;
  }

  public boolean isEmpty() {
    return widths.length == 0;
  }

  public int getLength() {
    return widths.length;
  }

  public boolean isFinal(int idx) {
    return idx >= widths.length - 1;
  }

  private int adjusted(int idx) {
    return idx < widths.length ? idx : widths.length - 1;
  }

  public KtDimen getIndent(int idx) {
    return indents[adjusted(idx)];
  }

  public KtDimen getWidth(int idx) {
    return widths[adjusted(idx)];
  }
}

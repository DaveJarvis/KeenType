// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.VAdjustPrim
// $Id: KtVAdjustPrim.java,v 1.1.1.1 2000/04/29 18:14:33 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtAdjustNode;

public class KtVAdjustPrim extends KtBuilderPrim {

  public KtVAdjustPrim(String name) {
    super(name);
  }

  /* TeXtp[1099] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(final KtBuilder bld, KtToken src) {
          pushLevel(new KtVAdjustGroup());
          scanLeftBrace();
        }
      };

  /* TeXtp[1100] */
  public static class KtVAdjustGroup extends KtVertGroup {

    protected KtVAdjustGroup() {
      super();
    }

    public void close() {
      super.close();
      KtBuilder bld = getBld();
      bld.addNode(new KtAdjustNode(builder.getList()));
      // bld.buildPage(); // impossible
    }
  }
}

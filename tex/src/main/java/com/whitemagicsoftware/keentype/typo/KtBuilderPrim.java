// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.BuilderPrim
// $Id: KtBuilderPrim.java,v 1.1.1.1 2000/04/18 21:38:46 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtCtrlSeqToken;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtLog;

public abstract class KtBuilderPrim extends KtBuilderCommand implements KtPrimitive {

  /** The name of the primitive */
  private final String name;

  protected KtBuilderPrim(String name) {
    this.name = name;
  }

  public final String getName() {
    return name;
  }

  public final KtCommand getCommand() {
    return this;
  }

  public final void addOn(KtLog log) {
    log.addEsc(name);
  }

  public final String toString() {
    return "@" + name;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *
   */

  public static final int INTP_MAX_MARK_WIDTH = newIntParam();

  public static void addNodeToks(KtLog log, KtTokenList list) {
    log.add('{');
    list.addOn(log, getConfig().getIntParam(INTP_MAX_MARK_WIDTH));
    log.add('}');
  }

  private static final KtToken PAR_TOKEN = new KtCtrlSeqToken("par");

  /* TeXtp[1090] */
  public static final KtAction START_PAR =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          backToken(src);
          KtParagraph.start(true);
        }
      };

  /* TeXtp[1094,1095] */
  public static final KtAction FINISH_PAR =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          backToken(src);
          insertToken(PAR_TOKEN);
        }
      };

  /* TeXtp[1094,1095] */
  public static final KtAction REJECT =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          getGrp().reject(src);
        }
      };

  public static final KtAction EMPTY =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {}
      };
}

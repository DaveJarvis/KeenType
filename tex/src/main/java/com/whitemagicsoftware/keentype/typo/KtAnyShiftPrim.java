// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.AnyShiftPrim
// $Id: KtAnyShiftPrim.java,v 1.1.1.1 2000/04/10 17:55:02 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtTreatBox;

public abstract class KtAnyShiftPrim extends KtBuilderPrim {

  public KtAnyShiftPrim(String name) {
    super(name);
  }

  /* TeXtp[1073,1076] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(final KtBuilder bld, KtToken src) {
          final KtDimen shift = scanDimen();
          scanBox(
              new KtTreatBox() {
                public boolean wantsMig() {
                  return bld.wantsMigrations();
                }

                public void execute(KtBox box, KtNodeEnum mig) {
                  appendBox(bld, makeNode(box, shift), mig);
                }
              });
        }
      };

  protected abstract KtNode makeNode(KtNode node, KtDimen shift);
}

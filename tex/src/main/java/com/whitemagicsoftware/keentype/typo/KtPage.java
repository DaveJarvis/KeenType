// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.Page
// $Id: KtPage.java,v 1.1.1.1 2001/02/01 13:34:14 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtOutputBuilder;
import com.whitemagicsoftware.keentype.command.KtCountPrim;
import com.whitemagicsoftware.keentype.command.KtDimenPrim;
import com.whitemagicsoftware.keentype.command.KtSkipPrim;
// XXX KtVSplitPrim dependency
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtPageSplit;
import com.whitemagicsoftware.keentype.node.KtPenaltyNode;
import com.whitemagicsoftware.keentype.node.KtSizesEvaluator;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVoidBoxNode;

public abstract class KtPage extends KtTypoCommand {

  public static final int BOOLP_TRACING_PAGES = newBoolParam();
  public static final int DIMP_VSIZE = newDimParam();
  public static final int DIMP_MAX_DEPTH = newDimParam();
  public static final int GLUEP_TOP_SKIP = newGlueParam();
  public static final int TOKSP_OUTPUT = newToksParam();
  public static final int INTP_MAX_DEAD_CYCLES = newIntParam();
  public static final int BOOLP_HOLDING_INSERTS = newBoolParam();

  private static boolean tracing() {
    return getConfig().getBoolParam(BOOLP_TRACING_PAGES);
  }

  private static String badToString(int b) {
    return b >= KtDimen.AWFUL_BAD ? "*" : Integer.toString( b);
  }

  private static List pageList;

  public static KtPageSplit getPageSplit() {
    return pageList;
  }

  public static List getPageList() {
    return pageList;
  }

  public static void makeStaticData(
      KtCountPrim countReg,
      KtDimenPrim dimenReg,
      KtSkipPrim skipReg,
      KtSetBoxPrim boxReg,
      KtTokenList.KtMaintainer topMark,
      KtTokenList.KtMaintainer firstMark,
      KtTokenList.KtMaintainer botMark) {
    pageList = new List(countReg, dimenReg, skipReg, boxReg, topMark, firstMark, botMark);
  }

  public static void writeStaticData(ObjectOutputStream output) throws IOException {
    output.writeObject(pageList);
  }

  public static void readStaticData(ObjectInputStream input)
      throws IOException, ClassNotFoundException {
    pageList = (List) input.readObject();
  }

  public static class List extends KtPageSplit {

    private final KtCountPrim countReg;
    private final KtDimenPrim dimenReg;
    private final KtSkipPrim skipReg;
    private final KtSetBoxPrim boxReg;
    private final KtTokenList.KtMaintainer topMark;
    private final KtTokenList.KtMaintainer firstMark;
    private final KtTokenList.KtMaintainer botMark;

    public List(
        KtCountPrim countReg,
        KtDimenPrim dimenReg,
        KtSkipPrim skipReg,
        KtSetBoxPrim boxReg,
        KtTokenList.KtMaintainer topMark,
        KtTokenList.KtMaintainer firstMark,
        KtTokenList.KtMaintainer botMark) {
      this.countReg = countReg;
      this.dimenReg = dimenReg;
      this.skipReg = skipReg;
      this.boxReg = boxReg;
      this.topMark = topMark;
      this.firstMark = firstMark;
      this.botMark = botMark;
    }

    /*
     * we suppose that the following holds in TeXtp:
     *
     * output_active -> page_contents = empty
     * fire_up called -> page_contents = box_there
     */

    protected boolean active = false;
    public int deadCycles = 0;

    public boolean outputActive() {
      return active;
    }

    public boolean canChangeNums() {
      return true;
    }

    public void build() {
      if (!active) super.build();
    }

    public boolean canChangeDimens() {
      return super.canChangeDimens() && !active;
    }

    public void show(KtLog log, int depth, int breadth) {
      show(log, depth, breadth, active);
    }

    public int getDeadCycles() {
      return deadCycles;
    }

    public void resetDeadCycles() {
      deadCycles = 0;
    }

    /* TeXtp[987] */
    protected void initSpecs() {
      goal = getConfig().getDimParam(DIMP_VSIZE);
      maxDepth = getConfig().getDimParam(DIMP_MAX_DEPTH);
      if (tracing())
        diagLog
            .startLine()
            .add("%% goal height=")
            .add(goal.toString())
            .add(", max depth=")
            .add(maxDepth.toString())
            .startLine();
    }

    protected void setInsVBox(int num, KtBox box) {
      boxReg.foist(num, box);
    }

    /* TeXtp[993] */
    protected KtBox getInsVBox(int num) {
      KtBox box = boxReg.get(num);
      if (!box.isVoid() && !box.isVBox()) {
        error("MisplacedInsert");
        boxReg.showAndPurge(num);
        box = boxReg.get(num);
      }
      return box;
    }

    /* TeXtp[1009] */
    protected KtGlue getInsSkip(int num) {
      KtGlue skip = skipReg.get(num);
      if (skip.getShrOrder() != KtGlue.NORMAL && !skip.getShrink().isZero())
        error("InfShrinkInsert", skipReg, num(num));
      return skip;
    }

    protected KtDimen getInsSize(int num) {
      return dimenReg.get(num);
    }

    protected int getInsFactor(int num) {
      return countReg.get(num).intVal();
    }

    protected void foistOutputBox(KtBox box) {
      boxReg.foist(getConfig().getIntParam(INTP_OUTPUT_BOX_NUM), box);
    }

    protected void checkOutputBox(String ident) {
      int num = getConfig().getIntParam(INTP_OUTPUT_BOX_NUM);
      if (!boxReg.get(num).isVoid()) {
        error(ident, esc(boxReg.getDesc()), num(num));
        boxReg.showAndPurge(num);
      }
    }

    /* TeXtp[1011] */
    protected void traceSplitCost(int num, KtDimen space, KtDimen best, int cost) {
      if (tracing())
        diagLog
            .startLine()
            .add("% split")
            .add(num)
            .add(" to ")
            .add(space.toString())
            .add(',')
            .add(best.toString())
            .add(" p=")
            .add(cost)
            .startLine();
    }

    protected boolean insertsWanted() {
      return !getConfig().getBoolParam(BOOLP_HOLDING_INSERTS);
    }

    protected KtNode splitTopAdjustment(KtDimen height, KtGlue topSkip) {
      return KtVSplitPrim.makeTopAdjustment(height, GLUEP_SPLIT_TOP_SKIP, topSkip);
    }

    protected KtNode topAdjustment(KtDimen height) {
      return KtVSplitPrim.makeTopAdjustment(height, GLUEP_TOP_SKIP);
    }

    /* TeXtp[1006] */
    protected void traceCost(int pen, int bad, int cost, boolean best) {
      if (tracing()) {
        diagLog
            .startLine()
            .add("% t=")
            .add(soFar.toString())
            .add(" g=")
            .add(goal.toString())
            .add(" b=")
            .add(badToString(bad))
            .add(" p=")
            .add(pen)
            .add(" c=")
            .add(badToString(cost));
        if (best) diagLog.add('#');
        diagLog.startLine();
      }
    }

    private static final KtVBoxPacker pagePacker =
        new KtVBoxPacker() {
          public boolean check(KtSizesEvaluator pack) {
            return false;
          }
        };

    /* TeXtp[1012,1024,1025] */
    protected boolean performOutput(KtNodeList list, KtDimen height) {
      setOutputPenalty();
      setMarks(list);
      checkOutputBox("NonEmptyOutBox");
      KtVBoxNode box = pagePacker.packVBox(list, height, true, maxDepth);
      KtTokenList.KtInserter output = getConfig().getToksInserter(TOKSP_OUTPUT);
      if (!output.isEmpty()) {
        foistOutputBox(box);
        if (deadCycles < getConfig().getIntParam(INTP_MAX_DEAD_CYCLES)) {
          active = true;
          deadCycles++;
          pushLevel(new KtOutputGroup());
          output.insertToks();
          scanLeftBrace();
          /* STRANGE
           * Why is depth zeroed before
           * performing the output routine?
           */
          depth = KtDimen.ZERO;
          return true;
        } else error("TooMuchDead", num(deadCycles));
      }
      foistOutputBox(KtVoidBoxNode.BOX);
      shipOut(box);
      startNextPage();
      return false;
    }

    /* TeXtp[1013] */
    private void setOutputPenalty() {
      int pen = KtNode.EJECT_PENALTY;
      if (!isEmpty()) {
        pen = KtNode.INF_PENALTY;
        KtNode node = nodeAt(0);
        if (node.isPenalty()) {
          data.set(0, new KtPenaltyNode(KtNum.valueOf(pen)));
          pen = node.getPenalty().intVal();
        }
      }
      getTypoConfig().setOutputPenalty(pen);
    }

    private void setMarks(KtNodeList list) {
      topMark.setToksValue(botMark.getToksValue());
      if (!KtVSplitPrim.setMarks(list.nodes(), firstMark, botMark))
        firstMark.setToksValue(topMark.getToksValue());
    }

    public class KtOutputGroup extends KtVertGroup {

      protected KtOutputGroup() {
        super(new KtOutputBuilder(currLineNumber()));
      }

      public void stop() {
        getTokStack().dropFinishedPop();
        // XXX[1026] unbalanced output routine
        super.stop();
      }

      public void close() {
        /* STRANGE
         * insertPenalties are not zeroed after default
         * output routine; maybe it is even a bug
         */
        active = false;
        insertPenalties = 0;
        checkOutputBox("NonEmptyOutBoxAfter");
        startNextPage(builder.getList().nodes());
        super.close();
        build();
      }
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.PatternsPrim
// $Id: KtPatternsPrim.java,v 1.1.1.1 2000/06/13 23:52:24 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.util.Vector;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtLanguage;

public class KtPatternsPrim extends KtTypoAssignPrim {

  public KtPatternsPrim(String name) {
    super(name);
  }

  private static final char WORD_BOUNDARY = KtLanguage.WORD_BOUNDARY;

  /* STRANGE
   * \global\patterns is allowed but has no effect
   * STRANGE
   * note the asymetry of the error handling for late patterns.
   * The method for loaded format is not robust.
   */
  /* TeXtp[960,1252] */
  protected void assign(final KtToken src, final boolean glob) {
    final KtConfig cfg = getTypoConfig();

    if (!cfg.patternsAllowed()) {
      if (getConfig().formatLoaded()) {
        error("CantLoadPatterns");
        while (!nextRawToken().matchRightBrace())
          ;
      } else {
        error("LatePatterns", this);
        KtPrim.scanTokenList(src, false);
      }

      return;
    }

    scanLeftBrace();

    final StringBuilder buf = new StringBuilder();
    final Vector<Integer> valBuf = new Vector<>();

    boolean digitExpected = true;

    for (; ; ) {
      final KtToken tok = nextExpToken();
      final KtCommand cmd = meaningOf(tok);
      final KtCharCode code = cmd.charCode();

      if (code != KtCharCode.NULL) {
        final KtCharCode toAdd = cmd.charCodeToAdd();

        if (toAdd != KtCharCode.NULL && code.match(toAdd)) {
          final char chr = code.toChar();

          if (digitExpected && chr >= '0' && chr <= '9') {
            final int index = buf.length();

            if( index >= valBuf.size() ) { valBuf.setSize( index + 1 ); }

            valBuf.set( index, chr - '0' );
            digitExpected = false;
          } else {
            char letter;

            if( code.match( '.' ) ) {
              letter = WORD_BOUNDARY;
            }
            else {
              letter = code.toCanonicalLetter();

              if( letter == KtCharCode.NO_CHAR ) {
                error( "NonLetter" );
                letter = WORD_BOUNDARY;
              }
            }

            buf.append(letter);
            digitExpected = true;
          }

          continue;
        }
      }

      if( cmd.isSpacer() || cmd.isRightBrace() ) {
        final int size = buf.length();

        if( size > 0 ) {
          final int[] values = new int[ valBuf.size() ];

          for( int i = 0; i < values.length; i++ ) {
            final Integer val = valBuf.get( i );
            values[ i ] = val != null ? val : 0;
          }

          if( values.length > 0 ) {
            if( buf.charAt( 0 ) == WORD_BOUNDARY ) { values[ 0 ] = 0; }
            if( buf.charAt( size - 1 ) == WORD_BOUNDARY && values.length > size ) {
              values[ size ] = 0;
            }
          }

          if( !cfg.getLanguage().setHyphPattern( buf.toString(), values ) ) {
            error( "DupPattern" );
          }

          buf.setLength( 0 );
          valBuf.clear();
        }

        if( cmd.isRightBrace() ) { break; }
        digitExpected = true;
      }
      else { error( "BadPatterns", this ); }
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.FontNamePrim
// $Id: KtFontNamePrim.java,v 1.1.1.1 1999/06/12 14:46:52 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.command.KtExpandablePrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.command.KtTokenListOutput;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtFontMetric;

/* TeXtp[470] */
public class KtFontNamePrim extends KtExpandablePrim {

  public KtFontNamePrim(String name) {
    super(name);
  }

  public void expand(KtToken src) {
    KtFontMetric metric = KtTypoCommand.scanFontMetric();
    KtTokenList.KtBuffer buf = new KtTokenList.KtBuffer();
    KtLog log = makeLog(new KtTokenListOutput(buf));
    metric.addDescOn(log);
    insertList(buf.toTokenList());
  }
}

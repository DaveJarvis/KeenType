// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.SpecialPrim
// $Id: KtSpecialPrim.java,v 1.1.1.1 2000/05/27 02:14:03 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBaseNode;
import com.whitemagicsoftware.keentype.node.KtSettingContext;
import com.whitemagicsoftware.keentype.node.KtTypesetter;

public class KtSpecialPrim extends KtBuilderPrim {

  public KtSpecialPrim(String name) {
    super(name);
  }

  /* TeXtp[1354] */
  public void exec(KtBuilder bld, KtToken src) {
    bld.addNode(new KtSpecialNode(KtPrim.scanTokenList(src, true)));
  }

  protected class KtSpecialNode extends KtBaseNode {
    /* root corresponding to whatsit_node */

    protected KtTokenList list;

    public KtSpecialNode(KtTokenList list) {
      this.list = list;
    }

    public boolean sizeIgnored() {
      return true;
    }

    /* TeXtp[1356] */
    public void addOn(KtLog log, KtCntxLog cntx) {
      addNodeToks(log.addEsc("special"), list);
    }

    /* TeXtp[1368] */
    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      KtLog log = getIOHandler().makeStringLog();
      log.add(list);
      setter.setSpecial(log.toString().getBytes());
      // XXX getBytes() is not proper
    }

    public byte beforeWord() {
      return SKIP;
    }

    public byte afterWord() {
      return SUCCESS;
    }
  }
}

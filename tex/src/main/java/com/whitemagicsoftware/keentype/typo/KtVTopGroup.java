// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.VTopGroup
// $Id: KtVTopGroup.java,v 1.1.1.1 2001/04/27 16:04:14 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.*;

public class KtVTopGroup extends KtVBoxGroup {

  public KtVTopGroup(KtDimen size, boolean exactly, KtTreatBox proc) {
    super(size, exactly, proc);
  }

  public static final KtVTopPacker packer = new KtVTopPacker();

  protected KtBox makeBox(KtNodeList list) {
    return packer.packVTop(list, size, exactly, maxDepth);
  }

  public static class KtVTopPacker extends KtTypoCommand.KtVBoxPacker {

    /* STRANGE
     * two vboxes are created: vtop as a final result and vbox solely for
     * diagnostic messages because TeX displays vbox when reporting bad
     * vbox and only after that moves the reference point to make vtop
     * from the vbox.
     */
    /* TeXtp[1087] */
    public KtVBoxNode packVTop(KtNodeList list, KtDimen desired, boolean exactly, KtDimen maxDepth) {
      KtSizesEvaluator pack = new KtSizesEvaluator();
      KtVertIterator.summarize(list.nodes(), pack);
      if (maxDepth != KtDimen.NULL) pack.restrictDepth(maxDepth);
      KtDimen size = pack.getBody().plus(pack.getHeight());
      boolean empty = list.isEmpty();
      if (exactly) {
        pack.evaluate(desired.minus(size), empty);
        size = desired;
      } else {
        pack.evaluate(desired, empty);
        size = size.plus(desired);
      }
      KtBoxSizes sizes = new KtBoxSizes(size, pack.getWidth(), pack.getDepth(), pack.getLeftX());
      KtVBoxNode vbox = new KtVBoxNode(sizes, pack.getSetting(), list);
      if (check(pack)) reportBox(vbox);
      KtDimen height = pack.getHeight();
      // XXX misusing of instanceof and not correct (discretionary)
      if( !(list.isEmpty() || list.nodeAt( 0 ) instanceof KtAnyBoxedNode) ) {
        height = KtDimen.ZERO;
      }
      sizes = new KtBoxSizes(
        height,
        pack.getWidth(),
        size.minus( height ).plus( pack.getDepth() ),
        pack.getLeftX() );
      return new KtVBoxNode( sizes, pack.getSetting(), list);
    }
  }
}

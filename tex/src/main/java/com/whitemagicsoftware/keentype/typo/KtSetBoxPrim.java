// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.SetBoxPrim
// $Id: KtSetBoxPrim.java,v 1.1.1.1 2001/04/18 06:16:27 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.command.KtRegisterPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtTreatBox;
import com.whitemagicsoftware.keentype.node.KtVoidBoxNode;

public class KtSetBoxPrim extends KtRegisterPrim {

  private final String desc;

  /**
   * Creates a new |KtSetBoxPrim| with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the |KtSetBoxPrim|
   */
  public KtSetBoxPrim(String name, String desc) {
    super(name);
    this.desc = desc;
  }

  public String getDesc() {
    return desc;
  }

  public String getEqDesc() {
    return desc;
  }

  public final void set(int idx, KtBox val, boolean glob) {
    if (glob) getEqt().gput(tabKind, idx, val);
    else getEqt().put(tabKind, idx, val);
  }

  public final KtBox get(int idx) {
    KtBox val = (KtBox) getEqt().get(tabKind, idx);
    return val != KtBox.NULL ? val : KtVoidBoxNode.BOX;
  }

  public final KtBox steal(int idx) {
    KtBox val = (KtBox) getEqt().get(tabKind, idx);
    if (val != KtBox.NULL) {
      getEqt().nastyReplace(tabKind, idx, KtVoidBoxNode.BOX);
      return val;
    }
    return KtVoidBoxNode.BOX;
  }

  public final void foist(int idx, KtBox val) {
    /* if (getEqt().get(tabKind, idx) != KtBox.NULL) */
    getEqt().nastyReplace(tabKind, idx, val);
  }

  public void addEqValueOn(int idx, KtLog log) {
    get(idx).addOn(log, 0, 1);
  }

  /**
   * Performs the assignment.
   *
   * @param src source token for diagnostic output.
   * @param glob indication that the assignment is global.
   */
  protected final void assign(KtToken src, final boolean glob) {
    final int idx = scanRegisterCode();
    skipOptEquals();
    KtTypoCommand.scanBox(
        new KtTreatBox() {
          public void execute(KtBox box, KtNodeEnum mig) {
            set(idx, box, glob);
          }
        });
  }

  /* STRANGE
   * Why it scans code and equals when \setbox is not allowed
   */
  public void doAssignment(KtToken src, int prefixes) {
    beforeAssignment(this, prefixes);
    scanRegisterCode();
    skipOptEquals();
    error("ImproperSetbox", this);
    afterAssignment();
  }

  public void showAndPurge(int idx) {
    // XXX maybe addBoxOnDiagLog(String, KtBox) could by used but only
    // XXX if it is certain that error() ends the previous line in diagLog
    diagLog.add("The following box has been deleted:");
    KtTypoCommand.addBoxOnDiagLog(get(idx));
    foist(idx, KtVoidBoxNode.BOX);
  }
}

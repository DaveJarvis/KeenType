// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.TypoCommand
// $Id: KtTypoCommand.java,v 1.1.1.1 2001/03/20 09:56:21 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtVertBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtCntxLoggableEnum;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.node.KtAnyBoxNode;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHorizIterator;
import com.whitemagicsoftware.keentype.node.KtLanguage;
import com.whitemagicsoftware.keentype.node.KtLinesShape;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtRuleNode;
import com.whitemagicsoftware.keentype.node.KtSizesEvaluator;
import com.whitemagicsoftware.keentype.node.KtTreatBox;
import com.whitemagicsoftware.keentype.node.KtTreatNode;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVertIterator;
import com.whitemagicsoftware.keentype.node.KtWordBuilder;

public abstract class KtTypoCommand extends KtCommand {

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static KtBuilder getBld() {
    return KtBuilder.top();
  }

  public static void illegalCase(KtCommand cmd, KtBuilder bld) {
    error("CantUseIn", cmd, bld);
  }

  public void illegalCase(KtBuilder bld) {
    illegalCase(this, bld);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public interface KtFontDimen {
    KtDimen get();

    void set(KtDimen dim);
  }

  public interface KtTypoHandler {
    KtFontMetric getMetric(KtFileName name, KtDimen size, KtNum scale, KtName ident, KtLoggable tok);

    KtFontDimen getFontDimen(KtFontMetric metric, int num);

    KtTypesetter getTypesetter();

    void setTypesetter(KtTypesetter typeSetter);
  }

  private static KtTypoHandler typoHandler;

  public static KtTypoHandler getTypoHandler() {
    return typoHandler;
  }

  public static void setTypoHandler(KtTypoHandler hand) {
    typoHandler = hand;
  }

  public static void setTypesetter( final KtTypesetter typeSetter ) {
    typoHandler.setTypesetter( typeSetter );
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  private static KtFontMetricEquiv currFontMetric;

  public static KtFontMetric getCurrFontMetric() {
    return currFontMetric.get();
  }

  public static void setCurrFontMetric(KtFontMetric val, boolean glob) {
    currFontMetric.set(val, glob);
  }

  public static void makeStaticData() {
    currFontMetric = new KtFontMetricEquiv();
  }

  public static void writeStaticData(ObjectOutputStream output) throws IOException {
    output.writeObject(currFontMetric);
  }

  public static void readStaticData(ObjectInputStream input)
      throws IOException, ClassNotFoundException {
    currFontMetric = (KtFontMetricEquiv) input.readObject();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public interface KtConfig {
    void setLastBadness(int badness);

    void setOutputPenalty(int penalty);

    int[] currPageNumbers();

    void checkParagraph(KtToken src);

    void resetParagraph();

    void setMarginSkipsShrinkFinite();

    boolean activeOutput();

    boolean pendingOutput();

    void resetOutput();

    KtLinesShape linesShape();

    KtLanguage getLanguage();

    KtLanguage getLanguage(int langNum);

    boolean languageDiffers(KtLanguage lang);

    boolean patternsAllowed();

    void preparePatterns();
  }

  private static KtConfig config;

  public static void setTypoConfig(KtConfig conf) {
    config = conf;
  }

  public static KtConfig getTypoConfig() {
    return config;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static final int BOOLP_TRACING_LOST_CHARS = newBoolParam();

  /* TeXtp[581] */
  public static void charWarning(KtFontMetric metric, KtCharCode code) {
    if (getConfig().getBoolParam(BOOLP_TRACING_LOST_CHARS))
      diagLog
          .startLine()
          .add("Missing character: There is no ")
          .add(code)
          .add(" in font ")
          .add(metric.getName())
          .add('!')
          .startLine();
  }

  public static final KtTreatNode APPENDER =
      new KtTreatNode() {
        public void execute(KtNode node) {
          getBld().addNode(node);
        }
      };

  /* TeXtp[1038] */
  public static void appendCharsTo(KtBuilder bld, KtWordBuilder word) {
    for (; ; ) {
      KtToken tok = nextExpToken();
      KtCommand cmd = meaningOf(tok);
      KtCharCode code = cmd.charCodeToAdd();
      if (code == KtCharCode.NULL) {
        word.close(!cmd.isNoBoundary());
        cmd.execute(tok);
        break;
      }
      bld.adjustSpaceFactor(code.spaceFactor());
      if (!word.add(code)) {
        charWarning(getCurrFontMetric(), code);
        break;
      }
    }
  }

  /* TeXtp[1034,1376] */
  public static void appendChar(KtBuilder bld, KtCharCode code) {
    KtWordBuilder word = getCurrFontMetric().getWordBuilder(APPENDER, true, bld.willBeBroken());
    bld.adjustSpaceFactor(code.spaceFactor());
    fixLanguage(bld);
    if (word.add(code)) appendCharsTo(bld, word);
    else charWarning(getCurrFontMetric(), code);
  }

  /* TeXtp[1376] */
  public static void fixLanguage(KtBuilder bld) {
    if (bld.willBeBroken()) {
      KtLanguage lang = bld.getCurrLang();
      if (lang != KtLanguage.NULL && getTypoConfig().languageDiffers(lang))
        bld.setCurrLang(getTypoConfig().getLanguage());
    }
  }

  public static final int GLUEP_SPACE = newGlueParam();
  public static final int GLUEP_XSPACE = newGlueParam();

  /* TeXtp[1041] */
  public static void appendNormalSpace(KtBuilder bld) {
    KtGlue skip = getConfig().getGlueParam(GLUEP_SPACE);
    if (skip.isZero()) bld.addSkip(getCurrFontMetric().getNormalSpace());
    else bld.addSkip(skip, getConfig().getGlueName(GLUEP_SPACE));
  }

  public static class KtHorizCharHandler implements KtCharHandler {

    /* TeXtp[1034] */
    public void handle(KtBuilder bld, KtCharCode code, KtToken src) {
      appendChar(bld, code);
    }

    /* TeXtp[1030,1043] */
    public void handleSpace(KtBuilder bld, KtToken src) {
      int sf = bld.getSpaceFactor();
      if (sf == 1000) appendNormalSpace(bld);
      else if (sf > 0) {
        if (sf >= 2000) {
          KtGlue skip = getConfig().getGlueParam(GLUEP_XSPACE);
          if (!skip.isZero()) {
            bld.addSkip(skip, getConfig().getGlueName(GLUEP_XSPACE));
            return;
          }
        }
        KtGlue skip = getConfig().getGlueParam(GLUEP_SPACE);
        if (skip.isZero()) skip = getCurrFontMetric().getNormalSpace();
        KtDimen dim = skip.getDimen();
        if (sf >= 2000) {
          KtDimen extra = getCurrFontMetric().getDimenParam(KtFontMetric.DIMEN_PARAM_EXTRA_SPACE);
          if (extra != KtDimen.NULL) dim = dim.plus(extra);
        }
        skip =
            KtGlue.valueOf(
                dim,
                skip.getStretch().times(sf, 1000),
                skip.getStrOrder(),
                skip.getShrink().times(1000, sf),
                skip.getShrOrder());
        bld.addSkip(skip);
      }
    }
  }

  public static class KtVertCharHandler implements KtCharHandler {

    /* TeXtp[1090] */
    public void handle(KtBuilder bld, KtCharCode code, KtToken src) {
      backToken(src);
      KtParagraph.start(true);
    }

    public void handleSpace(KtBuilder bld, KtToken src) {}
  }

  public static final int DIMP_LINE_SKIP_LIMIT = newDimParam();
  public static final int GLUEP_LINE_SKIP = newGlueParam();
  public static final int GLUEP_BASELINE_SKIP = newGlueParam();

  /* TeXtp[679] */
  private static void addBoxToBuilder(KtBuilder bld, KtNode node) {
    KtDimen dim = bld.getPrevDepth();
    if (dim != KtDimen.NULL && dim.moreThan(KtVertBuilder.IGNORE_DEPTH)) {
      KtCommand.KtConfig cfg = getConfig();
      KtGlue bls = cfg.getGlueParam(GLUEP_BASELINE_SKIP);
      dim = bls.getDimen().minus(dim).minus(node.getHeight());
      if (dim.lessThan(cfg.getDimParam(DIMP_LINE_SKIP_LIMIT)))
        bld.addSkip(cfg.getGlueParam(GLUEP_LINE_SKIP), cfg.getGlueName(GLUEP_LINE_SKIP));
      else bld.addSkip(bls.resizedCopy(dim), cfg.getGlueName(GLUEP_BASELINE_SKIP));
    }
    bld.addBox(node);
  }

  public static void appendBox(KtBuilder bld, KtNode box) {
    addBoxToBuilder(bld, box);
    bld.buildPage();
  }

  public static void appendBox(KtBuilder bld, KtNode box, boolean page) {
    addBoxToBuilder(bld, box);
    if (page) bld.buildPage();
  }

  public static void appendBox(KtBuilder bld, KtNode box, KtNodeEnum mig) {
    addBoxToBuilder(bld, box);
    bld.addNodes(mig);
    bld.buildPage();
  }

  public static void appendBox(KtBuilder bld, KtNode box, KtNodeEnum mig, boolean page) {
    addBoxToBuilder(bld, box);
    bld.addNodes(mig);
    if (page) bld.buildPage();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static KtFontMetric scanFontMetric() {
    KtToken tok = nextExpNonSpacer();
    KtCommand cmd = meaningOf(tok);
    if (cmd.hasFontMetricValue()) return cmd.getFontMetricValue();
    backToken(tok);
    error("MissingFontIdent");
    return KtNullFontMetric.METRIC;
  }

  public static void scanBox(KtTreatBox proc) {
    KtToken tok = nextNonRelax();
    KtCommand cmd = meaningOf(tok);
    if (cmd.hasBoxValue()) proc.execute(cmd.getBoxValue(), KtNodeList.EMPTY_ENUM);
    else if (cmd.canMakeBoxValue()) cmd.makeBoxValue(proc);
    else {
      backToken(tok);
      error("BoxExpected");
    }
  }

  public static final int INTP_SHOW_BOX_DEPTH = newIntParam();
  public static final int INTP_SHOW_BOX_BREADTH = newIntParam();

  /* TeXtp[198] */
  public static void addBoxOn(KtLog log, KtBox box) {
    int depth = getConfig().getIntParam(INTP_SHOW_BOX_DEPTH);
    int breadth = getConfig().getIntParam(INTP_SHOW_BOX_BREADTH);
    box.addOn(log, depth, breadth);
    log.endLine();
  }

  public static void addItemsOn(KtLog log, KtCntxLoggableEnum items) {
    int depth = getConfig().getIntParam(INTP_SHOW_BOX_DEPTH);
    int breadth = getConfig().getIntParam(INTP_SHOW_BOX_BREADTH);
    KtCntxLog.addItems(log, items, depth, breadth);
    log.endLine();
  }

  public static void addBoxOnDiagLog(String desc, KtBox box) {
    diagLog.startLine().add(desc);
    addBoxOn(diagLog, box);
    diagLog.startLine().endLine();
  }

  public static void addBoxOnDiagLog(KtBox box) {
    addBoxOn(diagLog, box);
    diagLog.startLine().endLine();
  }

  public static void addItemsOnDiagLog(String desc, KtCntxLoggableEnum items) {
    diagLog.startLine().add(desc);
    addItemsOn(diagLog, items);
    diagLog.startLine().endLine();
  }

  public static void addItemsOnDiagLog(KtCntxLoggableEnum items) {
    addItemsOn(diagLog, items);
    diagLog.startLine().endLine();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  /* TeXtp[666] */
  public abstract static class KtAnyBoxPacker {

    // XXX better name
    public boolean check(KtSizesEvaluator pack) {
      int badness = pack.getBadness();
      getTypoConfig().setLastBadness(badness);
      return switch( pack.getReport() ) {
        case KtSizesEvaluator.UNDERFULL -> underfull( badness );
        case KtSizesEvaluator.TIGHT -> tight( badness );
        case KtSizesEvaluator.OVERFULL -> overfull( pack.getOverfull() );
        default -> false;
      };
    }

    public void reportBox(KtAnyBoxNode box) {
      if (!takingOverLocationReported(normLog)) {
        reportLocation(normLog);
        normLog.endLine();
      }
      addBoxOnDiagLog(box);
    }

    /* TeXtp[660,674] */
    protected boolean underfull(int badness) {
      if (badness > getConfig().getIntParam(getBadnessParam())) {
        normLog
            .endLine()
            .add( badness > KtDimen.UNI_BAD ? "Underfull" : "Loose")
            .add(' ')
            .addEsc(getName())
            .add(" (badness ")
            .add(badness)
            .add(") ");
        return true;
      }
      return false;
    }

    /* TeXtp[667,678] */
    protected boolean tight(int badness) {
      if (badness > getConfig().getIntParam(getBadnessParam())) {
        normLog.endLine().add("Tight ").addEsc(getName()).add(" (badness ").add(badness).add(") ");
        return true;
      }
      return false;
    }

    /* TeXtp[666,677] */
    protected boolean overfull(KtDimen excess) {
      if (getConfig().getIntParam(getBadnessParam()) < KtDimen.UNI_BAD
          || getConfig().getDimParam(getFuzzParam()).lessThan(excess)) {
        normLog
            .endLine()
            .add("Overfull ")
            .addEsc(getName())
            .add(" (")
            .add(excess.toString("pt"))
            .add(" too ")
            .add(getDim())
            .add(") ");
        return true;
      }
      return false;
    }

    protected static boolean takingOverLocationReported(KtLog log) {
      if (getTypoConfig().activeOutput()) {
        log.add("has occurred while \\output is active");
        return true;
      }
      return false;
    }

    protected void reportLocation(KtLog log) {
      log.add("detected at line ").add(currLineNumber());
    }

    protected abstract String getName();

    protected abstract String getDim();

    protected abstract int getBadnessParam();

    protected abstract int getFuzzParam();
  }

  public static final int INTP_HBADNESS = newIntParam();
  public static final int DIMP_HFUZZ = newDimParam();
  public static final int DIMP_OVERFULL_RULE = newDimParam();

  public static class KtHBoxPacker extends KtAnyBoxPacker {

    /* TeXtp[666] */
    public KtHBoxNode packHBox(KtNodeList list, KtDimen desired, boolean exactly) {
      KtSizesEvaluator pack = new KtSizesEvaluator();
      KtHorizIterator.summarize(list.nodes(), pack);
      KtDimen size = pack.getBody().plus(pack.getDepth());
      boolean empty = list.isEmpty();
      if (exactly) {
        pack.evaluate(desired.minus(size), empty);
        size = desired;
      } else {
        pack.evaluate(desired, empty);
        size = size.plus(desired);
      }
      KtBoxSizes sizes = new KtBoxSizes(pack.getWidth(), size, pack.getLeftX(), pack.getHeight());
      KtHBoxNode hbox = new KtHBoxNode(sizes, pack.getSetting(), list);
      if (pack.getReport() == KtSizesEvaluator.OVERFULL) addOverfullRule(list, pack.getOverfull());
      if (check(pack)) reportBox(hbox);
      return hbox;
    }

    protected void addOverfullRule(KtNodeList list, KtDimen excess) {
      if (getConfig().getDimParam(DIMP_OVERFULL_RULE).moreThan(0)
          && getConfig().getDimParam(getFuzzParam()).lessThan(excess)) {
        KtBoxSizes sizes =
            new KtBoxSizes(
                KtDimen.NULL, getConfig().getDimParam(DIMP_OVERFULL_RULE), KtDimen.NULL, KtDimen.ZERO);
        list.append(new KtRuleNode(sizes));
      }
    }

    public void reportBox(KtAnyBoxNode box) {
      if (!takingOverLocationReported(normLog)) reportLocation(normLog);
      normLog.endLine();
      box.addListShortlyOn(normLog);
      normLog.endLine();
      addBoxOnDiagLog(box);
      /* STRANGE: note the asymetry of KtVBoxPacker & KtHBoxPacker */
    }

    protected String getName() {
      return "hbox";
    }

    protected String getDim() {
      return "wide";
    }

    protected int getBadnessParam() {
      return INTP_HBADNESS;
    }

    protected int getFuzzParam() {
      return DIMP_HFUZZ;
    }
  }

  public static final int INTP_VBADNESS = newIntParam();
  public static final int DIMP_VFUZZ = newDimParam();

  public static class KtVBoxPacker extends KtAnyBoxPacker {

    public KtVBoxNode packVBox(KtNodeList list, KtDimen desired, boolean exactly, KtDimen maxDepth) {
      KtSizesEvaluator pack = new KtSizesEvaluator();
      KtVertIterator.summarize(list.nodes(), pack);
      if (maxDepth != KtDimen.NULL) pack.restrictDepth(maxDepth);
      KtDimen size = pack.getBody().plus(pack.getHeight());
      boolean empty = list.isEmpty();
      if (exactly) {
        pack.evaluate(desired.minus(size), empty);
        size = desired;
      } else {
        pack.evaluate(desired, empty);
        size = size.plus(desired);
      }
      KtBoxSizes sizes = new KtBoxSizes(size, pack.getWidth(), pack.getDepth(), pack.getLeftX());
      KtVBoxNode vbox = new KtVBoxNode(sizes, pack.getSetting(), list);
      if (check(pack)) reportBox(vbox);
      return vbox;
    }

    protected String getName() {
      return "vbox";
    }

    protected String getDim() {
      return "high";
    }

    protected int getBadnessParam() {
      return INTP_VBADNESS;
    }

    protected int getFuzzParam() {
      return DIMP_VFUZZ;
    }
  }

  private static final KtHBoxPacker hPacker = new KtHBoxPacker();
  private static final KtVBoxPacker vPacker = new KtVBoxPacker();

  public static KtHBoxNode packHBox(KtNodeList list, KtDimen desired) {
    return packHBox(list, desired, true);
  }

  public static KtHBoxNode packHBox(KtNodeList list, KtDimen desired, boolean exactly) {
    return hPacker.packHBox(list, desired, exactly);
  }

  public static KtVBoxNode packVBox(KtNodeList list, KtDimen desired) {
    return packVBox(list, desired, true);
  }

  public static KtVBoxNode packVBox(KtNodeList list, KtDimen desired, KtDimen maxDepth) {
    return packVBox(list, desired, true, maxDepth);
  }

  public static KtVBoxNode packVBox(KtNodeList list, KtDimen desired, boolean exactly) {
    return packVBox(list, desired, exactly, KtDimen.NULL);
  }

  public static KtVBoxNode packVBox(KtNodeList list, KtDimen desired, boolean exactly, KtDimen maxDepth) {
    return vPacker.packVBox(list, desired, exactly, maxDepth);
  }

  public static final int DIMP_BOX_MAX_DEPTH = newDimParam();
  public static final int DIMP_SPLIT_MAX_DEPTH = newDimParam();
  public static final int GLUEP_SPLIT_TOP_SKIP = newGlueParam();
  public static final int INTP_OUTPUT_BOX_NUM = newIntParam();

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  public static final int BOOLP_TRACING_OUTPUT = newBoolParam();
  public static final int DIMP_H_OFFSET = newDimParam();
  public static final int DIMP_V_OFFSET = newDimParam();

  /* STRANGE
   * Why is height of box checked standalone and width is not?
   */
  /* TeXtp[638-641] */
  public static void shipOut(KtBox box) {
    boolean tracing = getConfig().getBoolParam(BOOLP_TRACING_OUTPUT);
    int[] nums = getTypoConfig().currPageNumbers();
    if (tracing) normLog.startLine().endLine().add("Completed box being shipped out");
    normLog.sepRoom(9).add('[');
    if (nums.length > 0)
      for (int i = 0; ; ) {
        normLog.add(nums[i++]);
        if (i >= nums.length) break;
        else normLog.add('.');
      }
    normLog.flush(); // XXX check all update_terminal
    if (tracing) {
      normLog.add(']');
      addBoxOnDiagLog(box);
    }
    KtDimen hOffset = getConfig().getDimParam(DIMP_H_OFFSET);
    KtDimen vOffset = getConfig().getDimParam(DIMP_V_OFFSET);
    KtDimen height = box.getHeight().plus(box.getDepth());
    KtDimen width = box.getWidth().plus(box.getLeftX());
    if (box.getHeight().moreThan(KtDimen.MAX_VALUE)
        || box.getDepth().moreThan(KtDimen.MAX_VALUE)
        || vOffset.plus(height).moreThan(KtDimen.MAX_VALUE)
        || hOffset.plus(width).moreThan(KtDimen.MAX_VALUE)) {
      error("PageTooLarge");
      if (!tracing) addBoxOnDiagLog("The following box has been deleted:", box);
    } else {
      final KtTypesetter setter = KtTypoCommand.getTypoHandler().getTypesetter();
      setter.startPage(vOffset, hOffset, height, width, nums);
      setter.moveDown(box.getHeight());
      setter.moveRight(box.getLeftX());
      box.typeSet(setter);
      setter.endPage();
    }
    if (!tracing) normLog.add(']');
    normLog.flush();
    getTypoConfig().resetOutput();
  }
}

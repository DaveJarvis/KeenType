// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.CharDefPrim
// $Id: KtCharDefPrim.java,v 1.1.1.1 2000/03/17 14:03:13 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtAssignPrim;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.command.KtRelax;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;

public class KtCharDefPrim extends KtAssignPrim {

  private final KtPrimitive prim;

  public KtCharDefPrim(String name, KtPrimitive prim) {
    super(name);
    this.prim = prim;
  }

  protected void assign(KtToken src, boolean glob) {
    KtToken tok = definableToken();
    tok.define(KtRelax.getRelax(), glob);
    skipOptEquals();
    KtCharCode code = KtToken.makeCharCode(scanCharacterCode());
    if (code != KtCharCode.NULL) tok.define(new KtCharGiven(code, prim.getName()), glob);
    else throw new RuntimeException("no char number scanned");
  }
}

class KtCharGiven extends KtCommand implements KtNum.KtProvider {

  private final KtCharCode code;
  private final String name;

  public KtCharGiven(KtCharCode code, String name) {
    this.code = code;
    this.name = name;
  }

  public void exec(KtToken src) {
    KtBuilderCommand.handleChar(code, src);
  }

  public KtCharCode charCodeToAdd() {
    return code;
  }

  public boolean sameAs(KtCommand cmd) {
    return cmd instanceof KtCharGiven && code.match( ((KtCharGiven) cmd).code);
  }

  public void addOn(KtLog log) {
    log.addEsc(name).add('"').add(Integer.toHexString(code.numValue()).toUpperCase());
  }

  public boolean hasNumValue() {
    return true;
  }

  public KtNum getNumValue() {
    return KtNum.valueOf(code.numValue());
  }

  public final String toString() {
    return "[character given: " + code + ']';
  }
}

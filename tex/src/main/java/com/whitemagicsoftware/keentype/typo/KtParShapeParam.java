// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.ParShapeParam
// $Id: KtParShapeParam.java,v 1.1.1.1 2000/04/19 00:20:04 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtParamPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtLog;

/** Setting paragraph shape parameter primitive. */
public class KtParShapeParam extends KtParamPrim implements KtParShape.KtProvider, KtNum.KtProvider {

  private KtParShape value;

  /**
   * Creates a new KtParShapeParam with given name and value and stores it in language interpreter
   * |KtEqTable|.
   *
   * @param name the name of the KtParShapeParam
   * @param val the value of the KtParShapeParam
   */
  public KtParShapeParam(String name, KtParShape val) {
    super(name);
    value = val;
  }

  /**
   * Creates a new KtParShapeParam with given name and stores it in language interpreter |KtEqTable|.
   *
   * @param name the name of the KtParShapeParam
   */
  public KtParShapeParam(String name) {
    this(name, KtParShape.EMPTY);
  }

  public final Object getEqValue() {
    return value;
  }

  public final void setEqValue(Object val) {
    value = (KtParShape) val;
  }

  public final void addEqValueOn(KtLog log) {
    log.add(value.getLength());
  }

  public final KtParShape get() {
    return value;
  }

  public void set(KtParShape val, boolean glob) {
    beforeSetting(glob);
    value = val;
  }

  protected void scanValue(KtToken src, boolean glob) {
    set(scanParShapeValue(src), glob);
  }

  /* TeXtp[1248] */
  protected KtParShape scanParShapeValue(KtToken src) {
    int len = scanInt();
    if (len <= 0) return KtParShape.EMPTY;
    KtDimen[] indents = new KtDimen[len];
    KtDimen[] widths = new KtDimen[len];
    for (int i = 0; i < len; i++) {
      indents[i] = scanDimen();
      widths[i] = scanDimen();
    }
    return new KtParShape(indents, widths);
  }

  public KtParShape getParShapeValue() {
    return get();
  }

  public boolean hasNumValue() {
    return true;
  }

  public KtNum getNumValue() {
    return KtNum.valueOf(get().getLength());
  }
}

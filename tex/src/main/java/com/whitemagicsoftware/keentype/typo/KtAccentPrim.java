// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.AccentPrim
// $Id: KtAccentPrim.java,v 1.1.1.1 2001/09/09 22:17:59 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtAccKernNode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtHShiftNode;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtAccentPrim extends KtBuilderPrim {

  public KtAccentPrim(String name) {
    super(name);
  }

  /* TeXtp[1122-1125] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(final KtBuilder bld, KtToken src) {
          KtCharCode code = KtToken.makeCharCode(KtPrim.scanCharacterCode());
          if (code == KtCharCode.NULL) throw new RuntimeException("no char number scanned");
          KtFontMetric metric = getCurrFontMetric();
          KtNode node = metric.getCharNode(code);
          if (node == KtNode.NULL) charWarning(metric, code);
          else {
            /* Bernd Raichle:
               The function nextNonAssignment() can execute assignments
               as in \accent <accent_char> [<assignments>] <base_char>
               and these assignments can change parameters like
               the current x height (\fontdimen5\font) and slant
               (\fontdimen1\font).  Thus we have to get the x height and
               slant _before_ calling nextNonAssignment()!
            */
            KtDimen x = metric.getDimenParam(KtFontMetric.DIMEN_PARAM_X_HEIGHT);
            KtDimen s = metric.getDimenParam(KtFontMetric.DIMEN_PARAM_SLANT);
            KtToken tok = nextNonAssignment();
            KtCommand cmd = meaningOf(tok);
            code = cmd.charCodeToAdd();
            if (code == KtCharCode.NULL) backToken(tok);
            else {
              KtFontMetric nextMetric = getCurrFontMetric();
              KtNode nextNode = nextMetric.getCharNode(code);
              if (nextNode == KtNode.NULL) charWarning(nextMetric, code);
              else {
                KtDimen h = nextNode.getHeight();
                if (!h.equals(x)) {
                  node = KtHBoxNode.packedOf(node);
                  node = KtHShiftNode.shiftingDown(node, x.minus(h));
                }
                KtDimen a = node.getWidth();
                KtDimen delta =
                    makeDelta(
                        a,
                        x,
                        s,
                        nextNode.getWidth(),
                        h,
                        nextMetric.getDimenParam(KtFontMetric.DIMEN_PARAM_SLANT));
                bld.addNode(new KtAccKernNode(delta));
                bld.addNode(node);
                node = nextNode;
                bld.addNode(new KtAccKernNode(delta.plus(a).negative()));
              }
            }
            bld.addNode(node);
            bld.resetSpaceFactor();
          }
        }
      };

  private static KtDimen makeDelta(KtDimen a, KtDimen x, KtDimen s, KtDimen w, KtDimen h, KtDimen t) {
    return KtDimen.valueOf(
        w.minus(a).toDouble() / 2 + h.toDouble() * t.toDouble() - x.toDouble() * s.toDouble());
  }

  /*
      private static KtDimen	makeDelta(KtDimen a, KtDimen x, KtDimen s,
      					  KtDimen w, KtDimen h, KtDimen t)
  	{ return w.minus(a).over(2).plus(h.times(t)).minus(x.times(s)); }
  */

  private static int spoints(KtDimen d) {
    return d.toInt(KtDimen.REPR_UNITY);
  }
}

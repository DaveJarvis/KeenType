// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.PageNumPrim
// $Id: KtPageNumPrim.java,v 1.1.1.1 2000/01/03 14:03:43 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtIntProvider;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtAssignPrim;
import com.whitemagicsoftware.keentype.command.KtToken;

public abstract class KtPageNumPrim extends KtAssignPrim implements KtIntProvider, KtNum.KtProvider {

  protected KtPage.List page;

  public KtPageNumPrim(String name, KtPage.List page) {
    super(name);
    this.page = page;
  }

  /* STRANGE
   * \global\pagegoal is allowed but has no effect
   */
  /* TeXtp[1246] */
  protected void assign(KtToken src, boolean glob) {
    skipOptEquals();
    int num = scanInt();
    if (page.canChangeNums()) set(num);
  }

  /* TeXtp[421] */
  public boolean hasNumValue() {
    return true;
  }

  public final KtNum getNumValue() {
    return KtNum.valueOf(get());
  }

  public final int intVal() {
    return get();
  }

  protected abstract int get();

  protected abstract void set(int num);
}

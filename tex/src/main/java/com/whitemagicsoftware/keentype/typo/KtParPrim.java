// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.ParPrim
// $Id: KtParPrim.java,v 1.1.1.1 2001/03/20 09:56:24 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtParPrim extends KtBuilderPrim {

  public KtParPrim(String name) {
    super(name);
  }

  /* TeXtp[1094] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          getTypoConfig().checkParagraph(src);
          KtParagraph.finish();
          getBld().buildPage();
        }
      };

  /* TeXtp[1094] */
  public final KtAction RESET =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          getTypoConfig().resetParagraph();
          bld.buildPage();
        }
      };
}

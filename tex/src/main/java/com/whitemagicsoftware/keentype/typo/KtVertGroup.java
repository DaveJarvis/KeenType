// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.VertGroup
// $Id: KtVertGroup.java,v 1.1.1.1 2001/03/20 09:42:05 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtVBoxBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;

public class KtVertGroup extends KtSimpleGroup {

  protected final KtVBoxBuilder builder;

  public KtVertGroup(KtVBoxBuilder builder) {
    this.builder = builder;
  }

  public KtVertGroup() {
    this(new KtVBoxBuilder(currLineNumber()));
  }

  public void start() {
    KtTypoCommand.getTypoConfig().resetParagraph();
    KtBuilder.push(builder);
  }

  public void stop() {
    KtParagraph.finish();
  }

  public void close() {
    KtBuilder.pop();
  }
}

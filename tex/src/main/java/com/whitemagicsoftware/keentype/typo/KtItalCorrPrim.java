// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.ItalCorrPrim
// $Id: KtItalCorrPrim.java,v 1.1.1.1 1999/08/18 07:27:58 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtNode;

public class KtItalCorrPrim extends KtBuilderPrim {

  public KtItalCorrPrim(String name) {
    super(name);
  }

  /* TeXtp[1113] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          KtNode node = bld.lastNode();
          if (node != KtNode.NULL) {
            KtDimen corr = node.getItalCorr();
            if (corr != KtDimen.NULL) bld.addKern(corr);
          }
        }
      };
}

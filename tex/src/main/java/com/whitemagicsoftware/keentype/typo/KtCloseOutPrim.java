// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.CloseOutPrim
// $Id: KtCloseOutPrim.java,v 1.1.1.1 2000/05/26 21:14:44 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtSettingContext;
import com.whitemagicsoftware.keentype.node.KtTypesetter;

public class KtCloseOutPrim extends KtBuilderPrim {

  private final KtWritePrim write;

  public KtCloseOutPrim(String name, KtWritePrim write) {
    super(name);
    this.write = write;
  }

  private void close(int num) {
    write.set(num, KtLog.NULL);
  }

  public void exec(KtBuilder bld, KtToken src) {
    bld.addNode(new KtCloseOutNode(scanInt()));
  }

  /* TeXtp[1353] */
  public boolean immedExec(KtToken src) {
    close(scanInt());
    return true;
  }

  /* STRANGE
   * \closeout alows any number as a parameter (in spite of \closein and
   * \openout which alow only 0-15). The reason is, that the close_node has
   * the same size as write_node - see TeXtp[1350].
   */

  protected class KtCloseOutNode extends KtWritePrim.KtFileNode {
    /* corresponding to whatsit_node */

    public KtCloseOutNode(int num) {
      super(num);
    }

    /* TeXtp[1356] */
    public void addOn(KtLog log, KtCntxLog cntx) {
      addName(log, "closeout");
    }

    /* TeXtp[1366, 1367] */
    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      if (sctx.allowIO) close(num);
    }
  }
}

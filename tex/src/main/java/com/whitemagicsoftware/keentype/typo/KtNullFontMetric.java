// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.NullFontMetric
// $Id: KtNullFontMetric.java,v 1.1.1.1 2001/01/28 17:57:21 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.node.KtBaseFontMetric;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtMathWordBuilder;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtTreatNode;
import com.whitemagicsoftware.keentype.node.KtWordBuilder;
import com.whitemagicsoftware.keentype.node.KtWordRebuilder;

public class KtNullFontMetric extends KtBaseFontMetric {

  public static final KtNullFontMetric METRIC = new KtNullFontMetric();
  public static final KtSetFont COMMAND = new KtSetFont(METRIC);
  public static final KtName NAME = KtToken.makeName("nullfont");

  public KtNullFontMetric() {
    setNumParam(NUM_PARAM_HYPHEN_CHAR, KtNum.valueOf('-'));
    setNumParam(NUM_PARAM_SKEW_CHAR, KtNum.valueOf(-1));
  }

  public KtName getIdent() {
    return NAME;
  }

  public KtName getName() {
    return NAME;
  }

  public void addDescOn(KtLog log) {
    NAME.addOn(log);
  }

  public boolean isNull() {
    return true;
  }

  public KtNode getCharNode(KtCharCode code) {
    return KtNode.NULL;
  }

  public KtNode getLargerNode(KtCharCode code) {
    return KtNode.NULL;
  }

  public KtNode getSufficientNode(KtCharCode code, KtDimen desired) {
    return KtNode.NULL;
  }

  public KtBox getFittingWidthBox(KtCharCode code, KtDimen desired) {
    return KtBox.NULL;
  }

  public KtDimen getKernBetween(KtCharCode left, KtCharCode right) {
    return KtDimen.NULL;
  }

  // CCC jikes rejects boundary
  public KtWordBuilder getWordBuilder(KtTreatNode proc, boolean bound, boolean discretionaries) {
    return new KtNullWordBuilder();
  }

  public KtWordRebuilder getWordRebuilder(KtTreatNode proc, boolean bound) {
    return new KtNullWordBuilder();
  }

  public KtMathWordBuilder getMathWordBuilder(KtTreatNode proc) {
    return new KtNullWordBuilder();
  }

  private class KtNullWordBuilder implements KtWordRebuilder, KtMathWordBuilder {

    public boolean add(KtCharCode code) {
      return false;
    }

    public byte addIfBelongsToCut(KtCharCode code) {
      return INDEPENDENT;
    }

    public boolean prolongsCut(KtCharCode code) {
      return false;
    }

    public void close(KtCharCode code) {}

    public void close(boolean boundary) {}

    public void close() {}

    public boolean lastHasCollapsed() {
      return false;
    }

    public KtNode takeLastNode() {
      return KtNode.NULL;
    }

    public KtNode takeLastLargerNode() {
      return KtNode.NULL;
    }
  }
}

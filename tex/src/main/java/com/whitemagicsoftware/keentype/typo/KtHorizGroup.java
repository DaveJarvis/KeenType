// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.HorizGroup
// $Id: KtHorizGroup.java,v 1.1.1.1 2000/08/01 06:25:57 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtHBoxBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;

public class KtHorizGroup extends KtSimpleGroup {

  protected final KtHBoxBuilder builder;

  public KtHorizGroup(KtHBoxBuilder builder) {
    this.builder = builder;
  }

  public KtHorizGroup() {
    this(new KtHBoxBuilder(currLineNumber()));
  }

  public void start() {
    KtBuilder.push(builder);
  }

  public void close() {
    KtBuilder.pop();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.VSplitPrim
// $Id: KtVSplitPrim.java,v 1.1.1.1 2000/03/20 13:04:52 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNamedVSkipNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVertSplit;
import com.whitemagicsoftware.keentype.node.KtVoidBoxNode;

public class KtVSplitPrim extends KtFetchBoxPrim {

  private final KtSetBoxPrim reg;
  private final KtTokenList.KtMaintainer firstMark;
  private final KtTokenList.KtMaintainer lastMark;

  public KtVSplitPrim(
      String name, KtSetBoxPrim reg, KtTokenList.KtMaintainer firstMark, KtTokenList.KtMaintainer lastMark) {
    super(name);
    this.reg = reg;
    this.firstMark = firstMark;
    this.lastMark = lastMark;
  }

  /* TeXtp[1082,977] */
  public KtBox getBoxValue() {
    int idx = KtPrim.scanRegisterCode();
    if (!scanKeyword("to")) error("MissingToForVsplit");
    KtDimen size = scanDimen();
    KtBox box = reg.get(idx);
    firstMark.setToksValue(KtTokenList.EMPTY);
    lastMark.setToksValue(KtTokenList.EMPTY);
    if (box.isVoid()) return box;
    KtNodeEnum nodes = box.getVertList();
    if (nodes == KtNodeEnum.NULL) {
      error("SplittingNonVbox", this, esc("vbox"));
      return KtVoidBoxNode.BOX;
    }
    KtDimen maxDepth = getConfig().getDimParam(DIMP_SPLIT_MAX_DEPTH);
    KtVSplitSplit splitter = new KtVSplitSplit(nodes);
    KtNodeList head = splitter.makeSplitting(size, maxDepth);
    setMarks(head.nodes(), firstMark, lastMark);
    splitter.pruneTop();
    KtNodeList tail = new KtNodeList(splitter.nodes());
    if (tail.isEmpty()) box = KtVoidBoxNode.BOX;
    else box = KtVBoxNode.packedOf(tail);
    reg.foist(idx, box);
    return packVBox(head, size, maxDepth);
  }

  protected static class KtVSplitSplit extends KtVertSplit {

    public KtVSplitSplit(KtNodeEnum nodes) {
      super(nodes);
    }

    protected KtNode topAdjustment(KtDimen height) {
      return makeTopAdjustment(height, GLUEP_SPLIT_TOP_SKIP);
    }
  }

  public static boolean setMarks(
      KtNodeEnum nodes, KtTokenList.KtMaintainer first, KtTokenList.KtMaintainer last) {
    boolean noMark = true;
    while (nodes.hasMoreNodes()) {
      KtNode node = nodes.nextNode();
      if (node.isMark()) {
        if (noMark) {
          noMark = false;
          first.setToksValue(node.getMark());
        }
        last.setToksValue(node.getMark());
      }
    }
    return !noMark;
  }

  /* TeXtp[969,1001] */
  public static KtNode makeTopAdjustment(KtDimen height, int param, KtGlue skip) {
    KtDimen dim = skip.getDimen();
    dim = dim.moreThan(height) ? dim.minus( height) : KtDimen.ZERO;
    skip = skip.resizedCopy(dim);
    return new KtNamedVSkipNode(skip, getConfig().getGlueName(param));
  }

  public static KtNode makeTopAdjustment(KtDimen height, int param) {
    return makeTopAdjustment(height, param, getConfig().getGlueParam(param));
  }
}

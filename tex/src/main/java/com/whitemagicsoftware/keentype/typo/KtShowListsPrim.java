// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.ShowListsPrim
// $Id: KtShowListsPrim.java,v 1.1.1.1 2000/02/08 06:42:39 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;

public class KtShowListsPrim extends KtTypoShowingPrim {

  public KtShowListsPrim(String name) {
    super(name);
  }

  /* TeXtp[218] */
  protected void performShow() {
    int depth = getConfig().getIntParam(INTP_SHOW_BOX_DEPTH);
    int breadth = getConfig().getIntParam(INTP_SHOW_BOX_BREADTH);
    diagLog.startLine().endLine();
    KtBuilder.showStack(diagLog, depth, breadth);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.FontMetricEquiv
// $Id: KtFontMetricEquiv.java,v 1.1.1.1 2000/02/01 10:53:23 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtFontMetric;

public class KtFontMetricEquiv extends KtCommand.KtExtEquiv implements Serializable {

  private KtFontMetric value = KtNullFontMetric.METRIC;

  public KtFontMetric get() {
    return value;
  }

  public void set(KtFontMetric val, boolean glob) {
    beforeSetting(glob);
    value = val;
  }

  public Object getEqValue() {
    return value;
  }

  public void setEqValue(Object val) {
    value = (KtFontMetric) val;
  }

  public final void addEqDescOn(KtLog log) {
    log.add("current font");
  }

  public void addEqValueOn(KtLog log) {
    value.getIdent().addEscapedOn(log);
  }
}

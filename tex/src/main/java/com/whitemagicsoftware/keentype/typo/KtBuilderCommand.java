// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.BuilderCommand
// $Id: KtBuilderCommand.java,v 1.1.1.1 2001/03/22 15:55:21 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtClassAssoc;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public abstract class KtBuilderCommand extends KtTypoCommand {

  private static KtClassAssoc assoc;
  private static Object charHandlerMark;

  public static void makeStaticData() {
    assoc = new KtClassAssoc(KtBuilder.class);
    charHandlerMark =
        new Serializable() {
          public int hashCode() {
            return 53479;
          }
        };
  }

  public static void writeStaticData(ObjectOutputStream output) throws IOException {
    output.writeObject(assoc);
    output.writeObject(charHandlerMark);
  }

  public static void readStaticData(ObjectInputStream input)
      throws IOException, ClassNotFoundException {
    assoc = (KtClassAssoc) input.readObject();
    charHandlerMark = input.readObject();
  }

  public static void registerBuilder(Class<?> bld) {
    assoc.record(bld);
  }

  public final void defineAction(Class<?> bld, KtAction act) {
    assoc.put(bld, this, act);
  }

  public final KtAction getAction(Class<?> bld) {
    return (KtAction) assoc.get(bld, this);
  }

  public final void exec(KtToken src) {
    KtBuilder bld = getBld();
    KtAction act = getAction(bld.getClass());
    if (act == KtAction.NULL) exec(bld, src);
    else act.exec(bld, src);
  }

  public final KtGlue getSkipForLeaders() {
    KtBuilder bld = getBld();
    KtAction act = getAction(bld.getClass());
    return act == KtAction.NULL ? KtGlue.NULL : act.getSkipForLeaders( bld);
  }

  public void exec(KtBuilder bld, KtToken src) {
    illegalCase(bld);
  }

  /* ****************************************************************** */

  public static void defineCharHandler(Class<?> bld, KtCharHandler hnd) {
    assoc.put(bld, charHandlerMark, hnd);
  }

  public static KtCharHandler getCharHandler(Class<?> bld) {
    return (KtCharHandler) assoc.get(bld, charHandlerMark);
  }

  public static void handleChar(KtCharCode code, KtToken src) {
    KtBuilder bld = getBld();
    KtCharHandler hnd = getCharHandler(bld.getClass());
    if (hnd == KtCharHandler.NULL) error("CantUseIn", str("character"), bld);
    else hnd.handle(bld, code, src);
  }

  public static void handleSpace(KtToken src) {
    KtBuilder bld = getBld();
    KtCharHandler hnd = getCharHandler(bld.getClass());
    if (hnd == KtCharHandler.NULL) error("CantUseIn", str("space"), bld);
    else hnd.handleSpace(bld, src);
  }
}

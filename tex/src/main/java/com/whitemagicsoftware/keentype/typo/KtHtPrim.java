// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.HtPrim
// $Id: KtHtPrim.java,v 1.1.1.1 1999/08/02 12:20:49 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;

public class KtHtPrim extends KtBoxDimenPrim {

  public KtHtPrim(String name, KtSetBoxPrim reg) {
    super(name, reg);
  }

  protected KtBoxSizes changeSizes(KtBoxSizes sizes, KtDimen dimen) {
    return new KtBoxSizes(dimen, sizes.getWidth(), sizes.getDepth(), sizes.getLeftX());
  }

  protected KtDimen selectSize(KtBoxSizes sizes) {
    return sizes.getHeight();
  }
}

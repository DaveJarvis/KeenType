// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.FontPrim
// $Id: KtFontPrim.java,v 1.1.1.1 2001/02/26 00:01:31 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.command.KtCtrlSeqToken;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.command.KtPrefixPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.node.KtFontMetric;

public class KtFontPrim extends KtTypoAssignPrim implements KtTokenList.KtProvider {

  public KtFontPrim(String name) {
    super(name);
  }

  /* STRANGE
   * ensureOpenLog() is in TeX to prevent the font file
   * name from becoming a job name. Is it possible?
   * First line of the form \font\tenrm=cmr10 \input document.tex
   * assigns texput to logname now, but would it assign cmr10 instead of
   * document otherwise?
   */
  /* TeXtp[1257] */
  protected void assign(KtToken src, boolean glob) {
    ensureOpenLog();
    KtToken tok = KtPrefixPrim.definableToken();
    KtName ident = makeIdent(tok);
    tok.define(KtNullFontMetric.COMMAND, glob);
    skipOptEquals();
    boolean inpEnbl = getConfig().enableInput(false);
    KtFileName name = scanFileName();
    KtDimen size = scanSizeSpec();
    KtNum scale = size != KtDimen.NULL ? KtNum.NULL : scanScaleSpec();
    getConfig().enableInput(inpEnbl);
    KtFontMetric metric = getTypoHandler().getMetric(name, size, scale, ident, tok);
    if (metric != KtFontMetric.NULL) tok.define(new KtSetFont(metric), glob);
  }

  private static final KtName NAME_FONT = KtToken.makeName("FONT");

  private KtName makeIdent(KtToken tok) {
    KtName ident = tok.controlName();
    if (ident == KtName.NULL) {
      KtCharCode code = tok.charCode();
      if (code != KtCharCode.NULL) {
        KtName.KtBuffer buf = new KtName.KtBuffer(NAME_FONT.length() + 1);
        buf.append(NAME_FONT).append(code);
        ident = buf.toName();
      }
    } else if (ident.length() == 0) ident = NAME_FONT;
    return ident;
  }

  private KtDimen scanSizeSpec() {
    KtDimen size = KtDimen.NULL;
    if (scanKeyword("at")) {
      size = scanDimen();
      if (!size.moreThan(0) || !size.lessThan(2048)) {
        error("ImproperAt", str(size));
        size = KtDimen.valueOf(10); // XXX literals, err strings
      }
    }
    return size;
  }

  private KtNum scanScaleSpec() {
    KtNum scale = KtNum.NULL;
    if (scanKeyword("scaled")) {
      scale = scanNum();
      if (!scale.moreThan(0) || scale.moreThan(32768)) {
        error("IllegalMag", str(scale));
        scale = KtNum.valueOf(1000); // XXX literals, err strings
      }
    }
    return scale;
  }

  public boolean hasFontTokenValue() {
    return true;
  }

  public boolean hasFontMetricValue() {
    return true;
  }

  /* TeXtp[415,465] */
  public KtToken getFontTokenValue() {
    return new KtCtrlSeqToken(getCurrFontMetric().getIdent());
  }

  /* TeXtp[577] */
  public KtFontMetric getFontMetricValue() {
    return getCurrFontMetric();
  }
}

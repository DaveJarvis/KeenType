// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.MarkPrim
// $Id: KtMarkPrim.java,v 1.1.1.1 2000/05/26 21:25:01 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtMigratingNode;

public class KtMarkPrim extends KtBuilderPrim {

  public KtMarkPrim(String name) {
    super(name);
  }

  /* TeXtp[1101] */
  public void exec(KtBuilder bld, KtToken src) {
    bld.addNode(new KtMarkNode(KtPrim.scanTokenList(src, true)));
  }

  protected static class KtMarkNode extends KtMigratingNode {
    /* root corresponding to mark_node */

    protected KtTokenList list;

    public KtMarkNode(KtTokenList list) {
      this.list = list;
    }

    public boolean isMark() {
      return true;
    }

    public KtTokenList getMark() {
      return list;
    }

    /* TeXtp[196] */
    public void addOn(KtLog log, KtCntxLog cntx) {
      addNodeToks(log.addEsc("mark"), list);
    }
  }
}

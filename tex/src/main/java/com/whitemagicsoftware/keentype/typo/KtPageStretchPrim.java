// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.PageStretchPrim
// $Id: KtPageStretchPrim.java,v 1.1.1.1 2000/01/13 16:00:09 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtPageSplit;

public class KtPageStretchPrim extends KtPageDimenPrim {

  private final byte ord;

  public KtPageStretchPrim(String name, KtPageSplit page, byte ord) {
    super(name, page);
    this.ord = ord;
  }

  protected KtDimen get() {
    return page.soFar.getStretch(ord);
  }

  protected void set(KtDimen dim) {
    page.soFar.setStretch(dim, ord);
  }
}

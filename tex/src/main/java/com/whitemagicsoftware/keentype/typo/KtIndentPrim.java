// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.IndentPrim
// $Id: KtIndentPrim.java,v 1.1.1.1 1999/11/27 22:27:57 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtIndentPrim extends KtBuilderPrim {

  private final boolean indenting;

  public KtIndentPrim(String name, boolean indenting) {
    super(name);
    this.indenting = indenting;
  }

  public boolean isIndenting() {
    return indenting;
  }

  /* TeXtp[1090] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          KtParagraph.start(indenting);
        }
      };

  /* TeXtp[1093] */
  public final KtAction INSERT =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          KtParagraph.makeIndent(bld);
        }
      };
}

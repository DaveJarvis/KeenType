// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.CharHandler
// $Id: KtCharHandler.java,v 1.1.1.1 2000/02/17 15:38:59 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.io.Serializable;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public interface KtCharHandler extends Serializable {
  KtCharHandler NULL = null;

  void handle(KtBuilder bld, KtCharCode code, KtToken src);

  void handleSpace(KtBuilder bld, KtToken src);
}

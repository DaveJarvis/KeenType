// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.HyphenationPrim
// $Id: KtHyphenationPrim.java,v 1.1.1.1 2000/06/12 21:39:52 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import java.util.Vector;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCharCode;

public class KtHyphenationPrim extends KtTypoAssignPrim {

  public KtHyphenationPrim(String name) {
    super(name);
  }

  /* STRANGE
   * \global\hyphenation is allowed but has no effect
   * STRANGE
   * in one error message \hyphenation is passed in other it is literal
   */
  /* TeXtp[934] */
  protected void assign(KtToken src, boolean glob) {
    scanLeftBrace();
    StringBuilder buf = new StringBuilder();
    Vector<Integer> hyphBuf = new Vector<>();
    int index = 0;
    for (; ; ) {
      KtToken tok = nextExpToken();
      KtCommand cmd = meaningOf(tok);
      KtCharCode code = cmd.charCodeToAdd();
      if (code != KtCharCode.NULL) {
        if (code.match('-')) hyphBuf.add( index );
        else {
          char letter = code.toCanonicalLetter();
          if (letter != KtCharCode.NO_CHAR) {
            buf.append(letter);
            index++;
          } else error("NonLetterInHyph");
        }
      } else if (cmd.isSpacer() || cmd.isRightBrace()) {
        if (index > 1) {
          int[] positions = new int[hyphBuf.size()];
          for (int i = 0; i < positions.length; i++)
            positions[i] = hyphBuf.get( i );
          getTypoConfig().getLanguage().setHyphException(buf.toString(), positions);
          index = 0;
          buf.setLength(0);
          hyphBuf.clear();
        }
        if (cmd.isRightBrace()) break;
      } else error("ImproperHyphen", this);
    }
  }
}

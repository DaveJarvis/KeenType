// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.AnyLeadersPrim
// $Id: KtAnyLeadersPrim.java,v 1.1.1.1 2000/02/15 18:47:21 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtBoxLeaders;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtLeaders;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtTreatBox;

public abstract class KtAnyLeadersPrim extends KtBuilderPrim {

  public KtAnyLeadersPrim(String name) {
    super(name);
  }

  public void exec(final KtBuilder bld, KtToken src) {
    KtToken tok = nextNonRelax();
    KtCommand cmd = meaningOf(tok);
    if (cmd.hasBoxValue()) addBoxLeaders(bld, cmd.getBoxValue());
    else if (cmd.canMakeBoxValue())
      cmd.makeBoxValue(
          new KtTreatBox() {
            public void execute(KtBox box, KtNodeEnum mig) {
              addBoxLeaders(bld, box);
            }
          });
    else if (cmd.hasRuleValue()) {
      KtBoxSizes sizes = cmd.getRuleValue();
      KtGlue skip = getSkip();
      if (skip != KtGlue.NULL) bld.addLeadRule(skip, sizes, getDesc());
    } else {
      backToken(tok);
      error("BoxExpected");
    }
  }

  protected void addBoxLeaders(KtBuilder bld, KtBox box) {
    if (!box.isVoid()) {
      KtGlue skip = getSkip();
      if (skip != KtGlue.NULL) {
        KtBoxLeaders.KtMover mover = bld.getBoxLeadMover();
        if (mover != KtBoxLeaders.NULL_MOVER) bld.addLeaders(skip, makeLeaders(box, mover));
      }
    }
  }

  private KtGlue getSkip() {
    KtToken tok = nextNonRelax();
    KtCommand cmd = meaningOf(tok);
    KtGlue skip = cmd.getSkipForLeaders();
    if (skip == KtGlue.NULL) {
      backToken(tok);
      error("BadGlueAfterLeaders");
    }
    return skip;
  }

  protected abstract String getDesc();

  protected abstract KtLeaders makeLeaders(KtNode node, KtBoxLeaders.KtMover mover);
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.RulePrim
// $Id: KtRulePrim.java,v 1.1.1.1 1999/09/06 05:43:08 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;

public class KtRulePrim extends KtBuilderPrim {

  private final KtDimen defaultHeight;
  private final KtDimen defaultWidth;
  private final KtDimen defaultDepth;
  private final KtDimen defaultLeftX;

  public KtRulePrim(String name) {
    super(name);
    defaultHeight = KtDimen.NULL;
    defaultWidth = KtDimen.NULL;
    defaultDepth = KtDimen.NULL;
    defaultLeftX = KtDimen.NULL;
  }

  public KtRulePrim(String name, KtDimen h, KtDimen w, KtDimen d, KtDimen l) {
    super(name);
    defaultHeight = h;
    defaultWidth = w;
    defaultDepth = d;
    defaultLeftX = l;
  }

  /* TeXtp[1056] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          bld.addRule(getRule());
        }
      };

  /* TeXtp[1094,1095] */
  public final KtAction BAD_HRULE =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          error("CantUseHrule", KtRulePrim.this);
        }
      };

  public boolean hasRuleValue() {
    return true;
  }

  public KtBoxSizes getRuleValue() {
    return getRule();
  }

  public KtBoxSizes getRule() {
    return scanRule(defaultHeight, defaultWidth, defaultDepth, defaultLeftX);
  }

  /* TeXtp[463] */
  public static KtBoxSizes scanRule(KtDimen h, KtDimen w, KtDimen d, KtDimen l) {
    for (; ; )
      if (scanKeyword("width")) w = scanDimen();
      else if (scanKeyword("height")) h = scanDimen();
      else if (scanKeyword("depth")) d = scanDimen();
      else break;
    return new KtBoxSizes(h, w, d, l);
  }
}

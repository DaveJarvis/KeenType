// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.LastBoxPrim
// $Id: KtLastBoxPrim.java,v 1.1.1.1 2000/02/15 09:54:43 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtVoidBoxNode;

public class KtLastBoxPrim extends KtFetchBoxPrim {

  public KtLastBoxPrim(String name) {
    super(name);
  }

  /* TeXtp[1080] */
  public KtBox getBoxValue() {
    KtBuilder bld = getBld();
    if (bld.canTakeLastBox()) {
      KtNode node = bld.lastNode();
      if (node != KtNode.NULL && node.isBox()) {
        bld.removeLastNode();
        return node.getBox();
      }
    } else if (bld.canTakeLastNode()) error("LastBoxIn", this, bld);
    else error("CantTakeFromPage", this, bld);
    return KtVoidBoxNode.BOX;
  }
}

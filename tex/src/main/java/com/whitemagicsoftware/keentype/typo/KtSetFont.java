// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.SetFont
// $Id: KtSetFont.java,v 1.1.1.1 2001/03/22 13:35:51 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtCtrlSeqToken;
import com.whitemagicsoftware.keentype.command.KtPrefixPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtFontMetric;

// XXX maybe create PrefixedCommand and derive KtPrefixPrim of it
// XXX maybe create AssignCommand and derive KtAssignPrim of it

public class KtSetFont extends KtTypoCommand implements KtTokenList.KtProvider {

  private final KtFontMetric metric;

  public KtSetFont(KtFontMetric metric) {
    this.metric = metric;
  }

  /** Non prefixed version of exec */
  public final void exec(KtToken src) {
    exec(src, 0);
  }

  public final boolean assignable() {
    return true;
  }

  public final void doAssignment(KtToken src, int prefixes) {
    exec(src, prefixes);
  }

  /**
   * Performs itself in the process of interpretation of the macro language after sequence of prefix
   * commands.
   *
   * @param src source token for diagnostic output.
   * @param prefixes accumulated code of prefixes.
   */
  /* TeXtp[1217] */
  public final void exec(KtToken src, int prefixes) {
    KtPrefixPrim.beforeAssignment(this, prefixes);
    setCurrFontMetric(metric, KtPrefixPrim.globalAssignment(prefixes));
    KtPrefixPrim.afterAssignment();
  }

  /* TeXtp[1261] */
  public final void addOn(KtLog log) {
    log.add("select font ");
    metric.addDescOn(log);
  }

  public boolean sameAs(KtCommand cmd) {
    return cmd instanceof KtSetFont && metric.equals( ((KtSetFont) cmd).metric);
  }

  public boolean hasFontTokenValue() {
    return true;
  }

  public boolean hasFontMetricValue() {
    return true;
  }

  /* STRANGE
   * What is \FONT~ good for?
   */
  /* TeXtp[415,465] */
  public KtToken getFontTokenValue() {
    return new KtCtrlSeqToken(metric.getIdent());
  }

  /* TeXtp[577] */
  public KtFontMetric getFontMetricValue() {
    return metric;
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.VBoxGroup
// $Id: KtVBoxGroup.java,v 1.1.1.1 2001/04/27 16:02:16 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.node.KtBox;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtTreatBox;

public class KtVBoxGroup extends KtVertGroup {

  protected KtDimen size;
  protected boolean exactly;
  protected KtTreatBox proc;

  public KtVBoxGroup(KtDimen size, boolean exactly, KtTreatBox proc) {
    this.size = size;
    this.exactly = exactly;
    this.proc = proc;
  }

  protected KtDimen maxDepth;

  public void stop() {
    super.stop();
    maxDepth = getConfig().getDimParam(KtTypoCommand.DIMP_BOX_MAX_DEPTH);
  }

  public void close() {
    super.close();
    proc.execute(makeBox(builder.getList()), KtNodeList.EMPTY_ENUM);
  }

  protected KtBox makeBox(KtNodeList list) {
    return KtTypoCommand.packVBox(list, size, exactly, maxDepth);
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.OpenOutPrim
// $Id: KtOpenOutPrim.java,v 1.1.1.1 2000/05/26 21:14:44 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtSettingContext;
import com.whitemagicsoftware.keentype.node.KtTypesetter;

public class KtOpenOutPrim extends KtBuilderPrim {

  private final KtWritePrim write;

  public KtOpenOutPrim(String name, KtWritePrim write) {
    super(name);
    this.write = write;
  }

  private void open(int num, KtFileName name) {
    write.set(num, getIOHandler().openWrite(name, num));
  }

  public void exec(KtBuilder bld, KtToken src) {
    int num = KtPrim.scanFileCode();
    skipOptEquals();
    KtFileName name = scanFileName();
    bld.addNode(new KtOpenOutNode(num, name));
  }

  /* TeXtp[1351] */
  public boolean immedExec(KtToken src) {
    int num = KtPrim.scanFileCode();
    skipOptEquals();
    KtFileName name = scanFileName();
    open(num, name);
    return true;
  }

  protected class KtOpenOutNode extends KtWritePrim.KtFileNode {
    /* corresponding to whatsit_node */

    protected KtFileName name;

    public KtOpenOutNode(int num, KtFileName name) {
      super(num);
      this.name = name;
    }

    /* TeXtp[1356] */
    public void addOn(KtLog log, KtCntxLog cntx) {
      addName(log, "openout").add('=').add(name);
    }

    /* TeXtp[1366, 1367] */
    public void typeSet(KtTypesetter setter, KtSettingContext sctx) {
      if (sctx.allowIO) open(num, name);
    }
  }
}

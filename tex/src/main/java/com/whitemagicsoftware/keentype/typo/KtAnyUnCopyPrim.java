// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.typo.AnyUnCopyPrim
// $Id: KtAnyUnCopyPrim.java,v 1.1.1.1 1999/08/02 17:29:36 ksk Exp $
package com.whitemagicsoftware.keentype.typo;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.node.KtBox;

public class KtAnyUnCopyPrim extends KtBuilderPrim {

  protected final KtSetBoxPrim reg;

  public KtAnyUnCopyPrim(String name, KtSetBoxPrim reg) {
    super(name);
    this.reg = reg;
  }

  protected KtBox getBox(int idx) {
    return reg.get(idx);
  }

  protected void finishBox(int idx) {}

  /* TeXtp[1109,1110] */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          int idx = KtPrim.scanRegisterCode();
          KtBox box = getBox(idx);
          if (!box.isVoid()) {
            if (bld.unBox(box)) finishBox(idx);
            else error("IncompatibleUnbox");
          }
        }
      };
}

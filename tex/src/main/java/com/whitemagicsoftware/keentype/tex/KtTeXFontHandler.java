// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeXFontHandler
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.base.KtPairKey;
import com.whitemagicsoftware.keentype.command.KtCommandBase;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.tfm.KtBadTeXFmException;
import com.whitemagicsoftware.keentype.tfm.KtTeXFm;
import com.whitemagicsoftware.keentype.tfm.KtTeXFontMetric;
import com.whitemagicsoftware.keentype.typo.KtNullFontMetric;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

public class KtTeXFontHandler extends KtCommandBase
    implements KtTypoCommand.KtTypoHandler, KtFontInformator {

  public interface KtConfig {
    KtNum defaultHyphenChar();

    KtNum defaultSkewChar();
  }

  private static final class KtSequencer implements Serializable {
    public int nextIdNum = 0;
    public KtFontMetric lastLoaded = KtNullFontMetric.METRIC;
    /* STRANGE
     * The only purpose of this field is to artificially restrict creating
     * of new \fontdimen parameters only to the last loaded metric.
     */
  }

  protected static final KtTeXFmGroup NULL_GROUP = null;

  protected static class KtTeXFmGroup implements Serializable {

    private final KtName name;
    private final KtTeXFm tfm;
    private final byte[] dirName;
    private final byte[] fileName;
    private final Vector<KtTeXFontMetric> members = new Vector<>();

    public KtTeXFmGroup(KtName name, KtTeXFm tfm, String path) {
      this.name = name;
      this.tfm = tfm;
      String fname = new File(path).getName();
      int i = path.lastIndexOf(fname);
      dirName = i > 0 ? path.substring( 0, i).getBytes() : new byte[0];
      for (i = fname.length(); --i > 0 && fname.charAt(i) != '.'; )
        ;
      fileName = (i > 0 ? fname.substring( 0, i) : fname).getBytes();
    }

    public KtName getName() {
      return name;
    }

    public KtTeXFm getTfm() {
      return tfm;
    }

    /* TeXtp[1260] */
    public KtFontMetric get(KtDimen size, KtNum scale, KtName ident, KtConfig config, KtSequencer seq) {
      if (size == KtDimen.NULL) size = KtDimen.valueOf(tfm.getDesignSize());
      if (scale != KtNum.NULL) size = size.times(scale.intVal(), 1000);
      KtTeXFontMetric metric;
      for (int i = 0; i < members.size(); i++) {
        metric = members.elementAt(i);
        if (size.equals(metric.getAtSize())) {
          metric.setIdent(ident);
          return metric;
        }
      }
      metric = new KtInfoTeXFontMetric( name, tfm, size, ident, seq.nextIdNum++, dirName, fileName);
      metric.setNumParam(KtFontMetric.NUM_PARAM_HYPHEN_CHAR, config.defaultHyphenChar());
      metric.setNumParam(KtFontMetric.NUM_PARAM_SKEW_CHAR, config.defaultSkewChar());
      members.addElement(metric);
      seq.lastLoaded = metric;
      return metric;
    }
  }

  private static class KtSeed implements Serializable {
    public final HashMap<String, KtTeXFmGroup> groupTab;
    public final KtSequencer sequencer;

    public KtSeed(HashMap<String, KtTeXFmGroup> tab, KtSequencer seq) {
      groupTab = tab;
      sequencer = seq;
    }
  }

  private KtTypesetter mTypesetter = KtTypesetter.NULL;

  private final KtConfig config;
  private final HashMap<String, KtTeXFmGroup> groupTab;
  private final KtSequencer sequencer;

  public KtTeXFontHandler(
    final KtConfig config,
    final Object seed) {
    assert config != null;

    this.config = config;

    if( seed instanceof KtSeed s ) {
      groupTab = s.groupTab;
      sequencer = s.sequencer;
    }
    else {
      groupTab = new HashMap<>();
      sequencer = new KtSequencer();
    }
  }

  public KtTeXFontHandler( final KtConfig config ) {
    this( config, null );
  }

  public Object getSeed() {
    return new KtSeed(groupTab, sequencer);
  }

  public KtFontMetric getMetric(KtFileName name, KtDimen size, KtNum scale, KtName ident, KtLoggable tok) {
    String path = name.getPath();
    KtTeXFmGroup group = groupTab.get(path);
    if (group == NULL_GROUP) {
      KtName groupName = name.baseName();
      KtTeXFm tfm;
      try {
        tfm = KtTeXFm.readFrom( KtTeXIOHandler.openTeXFm( name));
      } catch (FileNotFoundException e) {
        fontError("TFMnotFound", path, size, scale, tok);
        return KtFontMetric.NULL;
      } catch (KtBadTeXFmException e) {
        fontError("TFMisBad", path, size, scale, tok);
        return KtFontMetric.NULL;
      } catch (IOException e) {
        System.err.println(e + "when reading " + name);
        return KtFontMetric.NULL;
      }
      group = new KtTeXFmGroup(groupName, tfm, path);
      groupTab.put(path, group);
    }
    return group.get(size, scale, ident, config, sequencer);
  }

  private void fontError(
      String err, final String path, final KtDimen size, final KtNum scale, final KtLoggable tok) {
    error(
        err,
        log -> {
          log.add( tok ).add( '=' ).add( path );
          if( size != KtDimen.NULL ) { log.add( " at " + size + "pt" ); }
          if( scale != KtNum.NULL ) { log.add( " scaled " + scale ); }
        } );
  }

  /*
   *		Indexed Font Dimension Parameters
   */
  // XXX treat the font numeric parameters in the same way

  private final HashMap<KtFontMetric, KtNum> paramTab1 = new HashMap<>();
  private final HashMap<KtPairKey, KtDimen> paramTab2 = new HashMap<>();

  public KtTypoCommand.KtFontDimen getFontDimen(KtFontMetric metric, int num) {
    int idx = num - 1;
    int maxDefined = -1;
    if (validRawDimIdx(idx)) {
      if (getRawDimPar(metric, idx) != KtDimen.NULL) return makeFontDimen(metric, idx);
      else if (metric == sequencer.lastLoaded) {
        defineRawDimParsUpTo(metric, idx);
        return makeFontDimen(metric, idx);
      }
    } else {
      /*
       * If there already are some parameters in paramTab associated with
       * the metric, the upper bound is simply associated to metric.
       */
      KtNum max = paramTab1.get( metric);
      if (max != KtNum.NULL) maxDefined = max.intVal();
      /*
       * We can increase the upper bound only if the metric is the last
       * loaded. It is artificial constraint for compatibility with TeX.
       */
      if (idx > maxDefined && metric == sequencer.lastLoaded) { // SSS
        defineRawDimParsUpTo(metric, idx);
        maxDefined = idx;
        paramTab1.put(metric, KtNum.valueOf(maxDefined));
      }
      if (0 <= idx && idx <= maxDefined)
        return makeFontDimen(new KtPairKey(metric, KtNum.valueOf(idx)));
    }
    /*
     * If all above fails the error message is isued and fake KtDimenParam
     * is returned.
     */
    if (maxDefined < 0) maxDefined = maxDefinedRawDimPar(metric);
    error("TooBigFontdimenNum", esc(metric.getIdent()), num(maxDefined + 1));
    return new KtTypoCommand.KtFontDimen() {
      public KtDimen get() {
        return KtDimen.ZERO;
      }

      public void set(KtDimen dim) {}
    };
  }

  private KtTypoCommand.KtFontDimen makeFontDimen(final KtFontMetric metric, final int idx) {
    return new KtTypoCommand.KtFontDimen() {
      public KtDimen get() {
        return getRawDimPar(metric, idx);
      }

      public void set(KtDimen dim) {
        setRawDimPar(metric, idx, dim);
      }
    };
  }

  private KtTypoCommand.KtFontDimen makeFontDimen(final KtPairKey key) {
    return new KtTypoCommand.KtFontDimen() {
      public KtDimen get() {
        KtDimen dim = paramTab2.get(key);
        return dim != KtDimen.NULL ? dim : KtDimen.ZERO;
      }

      public void set(KtDimen dim) {
        paramTab2.put(key, dim);
      }
    };
  }

  /*
   *		reverse mapping from high to raw dimen param indexes
   */

  private static final int[] rTab;

  static {
    int lTab = KtTeXFontMetric.numberOfRawDimenPars();
    int mTab = -1;
    for (int i = 0; i < lTab; i++)
      if (mTab < KtTeXFontMetric.rawDimenParNumber(i)) mTab = KtTeXFontMetric.rawDimenParNumber(i);

    rTab = new int[mTab + 1];
    {
      int i = 0;
      while( i < rTab.length ) { rTab[ i++ ] = -1; }
    }
    for (int i = 0; i < lTab; i++) {
      int j = KtTeXFontMetric.rawDimenParNumber(i);
      if (j >= 0) rTab[j] = i;
    }
  }

  private static boolean validRawDimIdx(int idx) {
    return 0 <= idx && idx < rTab.length && rTab[idx] >= 0;
  }

  private KtDimen getRawDimPar(KtFontMetric metric, int idx) {
    return metric.getDimenParam(rTab[idx]);
  }

  private void setRawDimPar(KtFontMetric metric, int idx, KtDimen val) {
    int lTab = KtTeXFontMetric.numberOfRawDimenPars();
    for (int i = 0; i < lTab; i++)
      if (KtTeXFontMetric.rawDimenParNumber(i) == idx) metric.setDimenParam(i, val);
  }

  private int maxDefinedRawDimPar(KtFontMetric metric) {
    for (int i = rTab.length; --i >= 0; )
      if (rTab[i] >= 0 && metric.getDimenParam(rTab[i]) != KtDimen.NULL) return i;
    return -1;
  }

  private void defineRawDimParsUpTo(KtFontMetric metric, int idx) {
    int lTab = KtTeXFontMetric.numberOfRawDimenPars();
    for (int i = 0; i < lTab; i++)
      if (KtTeXFontMetric.rawDimenParNumber(i) <= idx && metric.getDimenParam(i) == KtDimen.NULL)
        metric.setDimenParam(i, KtDimen.ZERO);
  }

  public KtTypesetter getTypesetter() {
    return mTypesetter;
  }

  public void setTypesetter(final KtTypesetter typeSetter) {
    mTypesetter = typeSetter;
  }

  public KtFontInfo getInfo( KtFontMetric metric) {
    if (metric instanceof KtInfoTeXFontMetric ) return (KtInfoTeXFontMetric) metric;
    else throw new RuntimeException("No info about a font metric");
  }
}

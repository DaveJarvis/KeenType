// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.Primitives
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.align.*;
import com.whitemagicsoftware.keentype.base.*;
import com.whitemagicsoftware.keentype.builder.*;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.hyph.*;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.math.*;
import com.whitemagicsoftware.keentype.noad.*;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtHangIndent;
import com.whitemagicsoftware.keentype.node.KtLanguage;
import com.whitemagicsoftware.keentype.node.KtLinesShape;
import com.whitemagicsoftware.keentype.typo.*;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class KtPrimitives implements
  Serializable,
  KtCommand.KtConfig,
  KtTypoCommand.KtConfig,
  KtMathPrim.KtConfig,
  KtTeXCharMapper.KtConfig,
  KtConfig,
  KtTeXFontHandler.KtConfig {

  public static final int NUMBER_OF_LANGUAGES = 256;
  // XXX also other constants from TeXConfig

  private final KtLevelEqTable eqTab = new KtLevelEqTable();
  private final KtEqTable.KtObjKind nameKind =
      new KtCommand.KtTokKind() {
        protected KtToken getToken(Object key) {
          return new KtCtrlSeqToken((KtName) key);
        }
      };
  private final KtEqTable.KtObjKind codeKind =
      new KtCommand.KtTokKind() {
        protected KtToken getToken(Object key) {
          return new KtActiveCharToken((KtCharCode) key);
        }
      };

  protected transient Calendar now;

  private void init() {
    now = Calendar.getInstance();
    KtCommand.setEqt(eqTab);
    KtNormalCharToken.setHandler( KtBuilderCommand::handleChar );
    KtSpaceToken.setHandler( ( code, src ) -> KtBuilderCommand.handleSpace( src ) );
    KtCtrlSeqToken.setMeaninger(
        new KtCtrlSeqToken.KtMeaninger() {

          public KtCommand get(KtName name) {
            return (KtCommand) eqTab.get(nameKind, name);
          }

          public void set(KtName name, KtCommand cmd, boolean glob) {
            if (glob) eqTab.gput(nameKind, name, cmd);
            else eqTab.put(nameKind, name, cmd);
          }
        });
    KtActiveCharToken.setMeaninger(
        new KtActiveCharToken.KtMeaninger() {

          public KtCommand get(KtCharCode code) {
            return (KtCommand) eqTab.get(codeKind, code);
          }

          public void set(KtCharCode code, KtCommand cmd, boolean glob) {
            if (glob) eqTab.gput(codeKind, code, cmd);
            else eqTab.put(codeKind, code, cmd);
          }
        });
  }

  {
    init();
  }

  @Serial
  private void writeObject( ObjectOutputStream output) throws IOException {
    output.writeInt(KtInteractionPrim.get());
    KtRelax.writeStaticData(output);
    KtExpandable.writeStaticData(output);
    KtUndefined.writeStaticData(output);
    KtTypoCommand.writeStaticData(output);
    KtBuilderCommand.writeStaticData(output);
    KtPage.writeStaticData( output);
    output.defaultWriteObject();
  }

  private transient boolean loaded = false;

  @Serial
  private void readObject( ObjectInputStream input) throws IOException, ClassNotFoundException {
    Class<?> align_class = KtAlignment.class;
    // XXX maybe it would not be necessary if KtColumnEnding is not inner
    KtInteractionPrim.set(input.readInt());
    KtRelax.readStaticData(input);
    KtExpandable.readStaticData(input);
    KtUndefined.readStaticData(input);
    KtTypoCommand.readStaticData(input);
    KtBuilderCommand.readStaticData(input);
    KtPage.readStaticData(input);
    input.defaultReadObject();
    init();
    initTime();
    initMath();
    initAlign();
    initGroups();
    initParams();
    initHyphenation();
    initMathSpacing();
    loaded = true;
  }

  protected void def(String name, KtCommand cmd) {
    eqTab.gput(nameKind, KtToken.makeName(name), cmd);
  }

  protected void def(KtPrimitive prim) {
    def(prim.getName(), prim.getCommand());
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Mode independent primitives
   */

  private final KtExpandablePrim prim_fi = new KtEndCondPrim( "fi", KtEndCondPrim.FI);

  private final KtInputPrim input = new KtInputPrim("input");
  private final KtCountPrim count = new KtCountPrim("count");
  private final KtDimenPrim dimen = new KtDimenPrim("dimen");
  private final KtSkipPrim skip = new KtSkipPrim("skip");
  private final KtMuSkipPrim muskip = new KtMuSkipPrim("muskip");
  private final KtToksPrim toks = new KtToksPrim("toks");

  {
    KtRelax.makeStaticData();
    KtExpandable.makeStaticData();
    KtUndefined.makeStaticData();

    def(KtRelax.NAME, KtRelax.getRelax());

    def(new KtExpandAfterPrim("expandafter"));
    def(new KtNoExpandPrim("noexpand"));
    def(new KtCsNamePrim("csname"));
    def(new KtEndCsNamePrim("endcsname"));
    def(new KtNumberPrim("number"));
    def(new KtRomanNumeralPrim("romannumeral"));
    def(new KtStringPrim("string"));
    def(new KtMeaningPrim("meaning"));
    def(new KtJobNamePrim("jobname"));
    def(new KtThePrim("the"));
    def(input);
    def(new KtEndInputPrim("endinput"));

    def(new KtIfNumPrim("ifnum"));
    def(new KtIfDimPrim("ifdim"));
    def(new KtIfOddPrim("ifodd"));
    def(new KtIfPrim( "if"));
    def(new KtIfCatPrim("ifcat"));
    def(new KtIfXPrim("ifx"));
    def(new KtIfBoolPrim("iftrue", true));
    def(new KtIfBoolPrim("iffalse", false));
    def(new KtIfCasePrim("ifcase"));
    def(new KtEndCondPrim("else", KtEndCondPrim.ELSE));
    def(new KtEndCondPrim("or", KtEndCondPrim.OR));
    def(prim_fi);

    def(new KtIgnoreSpacesPrim("ignorespaces"));
    def(new KtLowerCasePrim("lowercase"));
    def(new KtUpperCasePrim("uppercase"));
    def(new KtMessagePrim("message"));
    def(new KtErrMessagePrim("errmessage"));
    def(new KtShowPrim("show"));
    def(new KtShowThePrim("showthe"));
    def(new KtInputLineNoPrim("inputlineno"));
    def(new KtImmediatePrim("immediate"));

    def(new KtPrefix("long", KtPrefix.LONG));
    def(new KtPrefix("outer", KtPrefix.OUTER));
    def(new KtPrefix("global", KtPrefix.GLOBAL));

    def(new KtLetPrim("let"));
    def(new KtFutureLetPrim("futurelet"));
    def(new KtDefPrim("def", false, 0));
    def(new KtDefPrim("edef", true, 0));
    def(new KtDefPrim("gdef", false, KtPrefixPrim.GLOBAL));
    def(new KtDefPrim("xdef", true, KtPrefixPrim.GLOBAL));

    def(count);
    def(dimen);
    def(skip);
    def(muskip);
    def(toks);

    def(new KtCountDefPrim( "countdef", count));
    def(new KtDimenDefPrim("dimendef", dimen));
    def(new KtSkipDefPrim("skipdef", skip));
    def(new KtMuSkipDefPrim("muskipdef", muskip));
    def(new KtToksDefPrim("toksdef", toks));

    def(new KtOperatorPrim("advance", KtOperatorPrim.ADVANCE));
    def(new KtOperatorPrim("multiply", KtOperatorPrim.MULTIPLY));
    def(new KtOperatorPrim("divide", KtOperatorPrim.DIVIDE));

    for (int i = 0; i < KtInteractionPrim.names.length; i++)
      def(new KtInteractionPrim(KtInteractionPrim.names[i], i));

    KtReadPrim read = new KtReadPrim("read");

    def(read);
    def(new KtOpenInPrim("openin", read));
    def(new KtCloseInPrim("closein", read));
    def(new KtIfEofPrim("ifeof", read));
  }

  private final KtNumParam pretolerance = new KtNumParam("pretolerance"),
      tolerance = new KtNumParam("tolerance", 10000),
      line_penalty = new KtNumParam("linepenalty"),
      hyphen_penalty = new KtNumParam("hyphenpenalty"),
      ex_hyphen_penalty = new KtNumParam("exhyphenpenalty"),
      club_penalty = new KtNumParam("clubpenalty"),
      widow_penalty = new KtNumParam("widowpenalty"),
      display_widow_penalty = new KtNumParam("displaywidowpenalty"),
      broken_penalty = new KtNumParam("brokenpenalty"),
      bin_op_penalty = new KtNumParam("binoppenalty"),
      rel_penalty = new KtNumParam("relpenalty"),
      pre_display_penalty = new KtNumParam("predisplaypenalty"),
      post_display_penalty = new KtNumParam("postdisplaypenalty"),
      inter_line_penalty = new KtNumParam("interlinepenalty"),
      double_hyphen_demerits = new KtNumParam("doublehyphendemerits"),
      final_hyphen_demerits = new KtNumParam("finalhyphendemerits"),
      adj_demerits = new KtNumParam("adjdemerits"),
      mag = new KtMagParam("mag", 1000),
      delimiter_factor = new KtNumParam("delimiterfactor"),
      looseness = new KtNumParam("looseness"),
      show_box_breadth = new KtNumParam("showboxbreadth"),
      show_box_depth = new KtNumParam("showboxdepth"),
      hbadness = new KtNumParam("hbadness"),
      vbadness = new KtNumParam("vbadness"),
      pausing = new KtNumParam("pausing"),
      tracing_online = new KtTracingOnlineParam( "tracingonline"),
      tracing_macros = new KtNumParam("tracingmacros"),
      tracing_stats = new KtNumParam("tracingstats"),
      tracing_paragraphs = new KtNumParam("tracingparagraphs"),
      tracing_pages = new KtNumParam("tracingpages"),
      tracing_output = new KtNumParam("tracingoutput"),
      tracing_lost_chars = new KtNumParam("tracinglostchars"),
      tracing_commands = new KtNumParam("tracingcommands"),
      tracing_restores = new KtNumParam("tracingrestores"),
      uc_hyph = new KtNumParam("uchyph"),
      output_penalty = new KtNumParam("outputpenalty"),
      max_dead_cycles = new KtNumParam("maxdeadcycles", 25),
      hang_after = new KtNumParam("hangafter", 1),
      floating_penalty = new KtNumParam("floatingpenalty"),
      global_defs = new KtNumParam("globaldefs"),
      cur_fam = new KtNumParam("fam"),
      escape_char = new KtNumParam("escapechar", '\\'),
      default_hyphen_char = new KtNumParam("defaulthyphenchar"),
      default_skew_char = new KtNumParam("defaultskewchar"),
      end_line_char = new KtNumParam("endlinechar", '\r'),
      new_line_char = new KtNumParam("newlinechar"),
      language = new KtNumParam("language"),
      left_hyphen_min = new KtNumParam("lefthyphenmin"),
      right_hyphen_min = new KtNumParam("righthyphenmin"),
      holding_inserts = new KtNumParam("holdinginserts"),
      error_context_lines = new KtNumParam("errorcontextlines");
  private final KtDimenParam par_indent = new KtDimenParam("parindent"),
      math_surround = new KtDimenParam("mathsurround"),
      line_skip_limit = new KtDimenParam("lineskiplimit"),
      hsize = new KtDimenParam("hsize"),
      vsize = new KtDimenParam("vsize"),
      max_depth = new KtDimenParam("maxdepth"),
      split_max_depth = new KtDimenParam("splitmaxdepth"),
      box_max_depth = new KtDimenParam("boxmaxdepth"),
      hfuzz = new KtDimenParam("hfuzz"),
      vfuzz = new KtDimenParam("vfuzz"),
      delimiter_shortfall = new KtDimenParam("delimitershortfall"),
      null_delimiter_space = new KtDimenParam("nulldelimiterspace"),
      script_space = new KtDimenParam("scriptspace"),
      pre_display_size = new KtDimenParam("predisplaysize"),
      display_width = new KtDimenParam("displaywidth"),
      display_indent = new KtDimenParam("displayindent"),
      overfull_rule = new KtDimenParam("overfullrule"),
      hang_indent = new KtDimenParam("hangindent"),
      h_offset = new KtDimenParam("hoffset"),
      v_offset = new KtDimenParam("voffset"),
      emergency_stretch = new KtDimenParam("emergencystretch");
  private final KtGlueParam line_skip = new KtGlueParam("lineskip"),
      baseline_skip = new KtGlueParam("baselineskip"),
      par_skip = new KtGlueParam("parskip"),
      above_display_skip = new KtGlueParam("abovedisplayskip"),
      below_display_skip = new KtGlueParam("belowdisplayskip"),
      above_display_short_skip = new KtGlueParam("abovedisplayshortskip"),
      below_display_short_skip = new KtGlueParam("belowdisplayshortskip"),
      left_skip = new KtGlueParam("leftskip"),
      right_skip = new KtGlueParam("rightskip"),
      top_skip = new KtGlueParam("topskip"),
      split_top_skip = new KtGlueParam("splittopskip"),
      tab_skip = new KtTabSkipParam( "tabskip"),
      space_skip = new KtGlueParam("spaceskip"),
      xspace_skip = new KtGlueParam("xspaceskip"),
      par_fill_skip = new KtGlueParam("parfillskip");
  private final KtMuGlueParam thin_mu_skip = new KtMuGlueParam("thinmuskip"),
      med_mu_skip = new KtMuGlueParam("medmuskip"),
      thick_mu_skip = new KtMuGlueParam("thickmuskip");
  private final KtToksParam output_routine = new KtOutputParam("output"),
      every_par = new KtToksParam("everypar"),
      every_math = new KtToksParam("everymath"),
      every_display = new KtToksParam("everydisplay"),
      every_hbox = new KtToksParam("everyhbox"),
      every_vbox = new KtToksParam("everyvbox"),
      every_job = new KtToksParam("everyjob"),
      every_cr = new KtToksParam("everycr"),
      err_help = new KtToksParam("errhelp");
  private transient KtNumParam time, day, month, year;

  private void initTime() {
    time = new KtNumParam("time", now.get(Calendar.MINUTE) + 60 * now.get(Calendar.HOUR_OF_DAY));
    day = new KtNumParam("day", now.get(Calendar.DAY_OF_MONTH));
    month = new KtNumParam("month", now.get(Calendar.MONTH) + 1);
    year = new KtNumParam("year", now.get(Calendar.YEAR));
    def(time);
    def(day);
    def(month);
    def(year);
  }

  {
    initTime();
  }

  private final KtParShapeParam par_shape = new KtParShapeParam("parshape");

  private final KtAfterAssignment after_assignment = new KtAfterAssignment("afterassignment");

  {
    def(pretolerance);
    def(tolerance);
    def(line_penalty);
    def(hyphen_penalty);
    def(ex_hyphen_penalty);
    def(club_penalty);
    def(widow_penalty);
    def(display_widow_penalty);
    def(broken_penalty);
    def(bin_op_penalty);
    def(rel_penalty);
    def(pre_display_penalty);
    def(post_display_penalty);
    def(inter_line_penalty);
    def(double_hyphen_demerits);
    def(final_hyphen_demerits);
    def(adj_demerits);
    def(mag);
    def(delimiter_factor);
    def(looseness);
    def(show_box_breadth);
    def(show_box_depth);
    def(hbadness);
    def(vbadness);
    def(pausing);
    def(tracing_online);
    def(tracing_macros);
    def(tracing_stats);
    def(tracing_paragraphs);
    def(tracing_pages);
    def(tracing_output);
    def(tracing_lost_chars);
    def(tracing_commands);
    def(tracing_restores);
    def(uc_hyph);
    def(output_penalty);
    def(max_dead_cycles);
    def(hang_after);
    def(floating_penalty);
    def(global_defs);
    def(cur_fam);
    def(escape_char);
    def(default_hyphen_char);
    def(default_skew_char);
    def(end_line_char);
    def(new_line_char);
    def(language);
    def(left_hyphen_min);
    def(right_hyphen_min);
    def(holding_inserts);
    def(error_context_lines);

    def(par_indent);
    def(math_surround);
    def(line_skip_limit);
    def(hsize);
    def(vsize);
    def(max_depth);
    def(split_max_depth);
    def(box_max_depth);
    def(hfuzz);
    def(vfuzz);
    def(delimiter_shortfall);
    def(null_delimiter_space);
    def(script_space);
    def(pre_display_size);
    def(display_width);
    def(display_indent);
    def(overfull_rule);
    def(hang_indent);
    def(h_offset);
    def(v_offset);
    def(emergency_stretch);

    def(line_skip);
    def(baseline_skip);
    def(par_skip);
    def(above_display_skip);
    def(below_display_skip);
    def(above_display_short_skip);
    def(below_display_short_skip);
    def(left_skip);
    def(right_skip);
    def(top_skip);
    def(split_top_skip);
    def(tab_skip);
    def(space_skip);
    def(xspace_skip);
    def(par_fill_skip);

    def(thin_mu_skip);
    def(med_mu_skip);
    def(thick_mu_skip);

    def(output_routine);
    def(every_par);
    def(every_math);
    def(every_display);
    def(every_hbox);
    def(every_vbox);
    def(every_job);
    def(every_cr);
    def(err_help);

    def(par_shape);

    def(after_assignment);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Typesetting  primitives
   */

  private final KtSimpleNumPrim badness = new KtSimpleNumPrim("badness");
  private final KtSetBoxPrim setbox = new KtSetBoxPrim( "setbox", "box");

  {
    KtTypoCommand.makeStaticData();
    KtBuilderCommand.makeStaticData();

    // Mode independent

    def(badness);
    def("nullfont", KtNullFontMetric.COMMAND);
    def(new KtFontPrim("font"));
    def(new KtFontDimenPrim("fontdimen"));
    def(new KtFontNumPrim("hyphenchar", KtFontMetric.NUM_PARAM_HYPHEN_CHAR));
    def(new KtFontNumPrim("skewchar", KtFontMetric.NUM_PARAM_SKEW_CHAR));
    def(new KtFontNamePrim("fontname"));
    def(new KtShipOutPrim("shipout"));
    def(new KtPatternsPrim("patterns"));
    def(new KtHyphenationPrim("hyphenation"));

    KtWritePrim write = new KtWritePrim("write");

    def(write);
    def(new KtOpenOutPrim("openout", write));
    def(new KtCloseOutPrim("closeout", write));
    def(new KtSpecialPrim("special"));

    def(setbox);
    def(new KtIfVoidPrim("ifvoid", setbox));
    def(new KtIfHBoxPrim("ifhbox", setbox));
    def(new KtIfVBoxPrim("ifvbox", setbox));
    def(new KtHtPrim("ht", setbox));
    def(new KtWdPrim("wd", setbox));
    def(new KtDpPrim("dp", setbox));
    def(new KtShowBoxPrim("showbox", setbox));

    // typesetting

    KtBuilderCommand.defineCharHandler( KtVertBuilder.class, new KtTypoCommand.KtVertCharHandler());
    KtBuilderCommand.defineCharHandler( KtHorizBuilder.class, new KtTypoCommand.KtHorizCharHandler());
    KtBuilderCommand.defineCharHandler(KtMathBuilder.class, new KtMathPrim.KtMathCharHandler());

    KtDimen default_rule = KtCommand.makeDimen(26214, "sp");

    KtRulePrim hrule = new KtRulePrim("hrule", default_rule, KtDimen.NULL, KtDimen.ZERO, KtDimen.ZERO);

    KtRulePrim vrule = new KtRulePrim("vrule", KtDimen.NULL, default_rule, KtDimen.NULL, KtDimen.ZERO);

    def(hrule);
    hrule.defineAction(KtVertBuilder.class, hrule.NORMAL);
    hrule.defineAction( KtParBuilder.class, KtBuilderPrim.FINISH_PAR );
    hrule.defineAction( KtHBoxBuilder.class, hrule.BAD_HRULE);
    hrule.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);

    def(vrule);
    vrule.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    vrule.defineAction(KtHorizBuilder.class, vrule.NORMAL);
    vrule.defineAction(KtMathBuilder.class, vrule.NORMAL);
  }

  private void defVSkip(KtAnySkipPrim skip) {
    def(skip);
    skip.defineAction(KtVertBuilder.class, skip.NORMAL);
    skip.defineAction( KtParBuilder.class, KtBuilderPrim.FINISH_PAR );
    skip.defineAction( KtHBoxBuilder.class, KtBuilderPrim.REJECT );
    skip.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);
  }

  private void defHSkip(KtAnySkipPrim skip) {
    def(skip);
    skip.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    skip.defineAction(KtHorizBuilder.class, skip.NORMAL);
    skip.defineAction(KtMathBuilder.class, skip.NORMAL);
  }

  private void defUnVCopy(KtAnyUnCopyPrim uncopy) {
    def(uncopy);
    uncopy.defineAction(KtVertBuilder.class, uncopy.NORMAL);
    uncopy.defineAction( KtParBuilder.class, KtBuilderPrim.FINISH_PAR );
    uncopy.defineAction( KtHBoxBuilder.class, KtBuilderPrim.REJECT );
    uncopy.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);
  }

  private void defUnHCopy(KtAnyUnCopyPrim uncopy) {
    def(uncopy);
    uncopy.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    uncopy.defineAction(KtHorizBuilder.class, uncopy.NORMAL);
    uncopy.defineAction(KtMathBuilder.class, uncopy.NORMAL);
  }

  private void defIndent(KtIndentPrim indent) {
    def(indent);
    KtAction act = indent.isIndenting() ? indent.INSERT : KtBuilderPrim.EMPTY;
    indent.defineAction(KtVertBuilder.class, indent.NORMAL);
    indent.defineAction(KtHorizBuilder.class, act);
    indent.defineAction(KtMathBuilder.class, act);
  }

  private void defEnd(KtEndPrim end) {
    def(end);
    end.defineAction( KtPageBuilder.class, end.NORMAL);
    end.defineAction( KtParBuilder.class, KtBuilderPrim.FINISH_PAR );
    end.defineAction( KtHBoxBuilder.class, KtBuilderPrim.REJECT );
    end.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);
  }

  {
    KtGlue fil_glue = KtGlue.valueOf( KtDimen.ZERO, KtDimen.UNITY, KtGlue.FIL, KtDimen.ZERO, KtGlue.NORMAL),
        fil_neg_glue =
            KtGlue.valueOf(KtDimen.ZERO, KtDimen.UNITY.negative(), KtGlue.FIL, KtDimen.ZERO, KtGlue.NORMAL),
        fill_glue = KtGlue.valueOf(KtDimen.ZERO, KtDimen.UNITY, KtGlue.FILL, KtDimen.ZERO, KtGlue.NORMAL),
        ss_glue = KtGlue.valueOf(KtDimen.ZERO, KtDimen.UNITY, KtGlue.FIL, KtDimen.UNITY, KtGlue.FIL);

    defVSkip(new KtAnySkipPrim("vskip"));
    defVSkip(new KtAnySkipPrim("vfil", fil_glue));
    defVSkip(new KtAnySkipPrim("vfilneg", fil_neg_glue));
    defVSkip(new KtAnySkipPrim("vfill", fill_glue));
    defVSkip(new KtAnySkipPrim("vss", ss_glue));

    defHSkip(new KtAnySkipPrim("hskip"));
    defHSkip(new KtAnySkipPrim("hfil", fil_glue));
    defHSkip(new KtAnySkipPrim("hfilneg", fil_neg_glue));
    defHSkip(new KtAnySkipPrim("hfill", fill_glue));
    defHSkip(new KtAnySkipPrim("hss", ss_glue));

    def(new KtKernPrim("kern"));
    def(new KtPenaltyPrim("penalty"));
    def(new KtVBoxPrim("vbox", every_vbox));
    def(new KtVTopPrim("vtop", every_vbox));
    def(new KtHBoxPrim("hbox", every_hbox));
    def(new KtBoxPrim("box", setbox));
    def(new KtCopyPrim("copy", setbox));

    def(new KtLeadersPrim("leaders"));
    def(new KtCLeadersPrim("cleaders"));
    def(new KtXLeadersPrim("xleaders"));

    KtRaisePrim raise = new KtRaisePrim("raise");
    KtLowerPrim lower = new KtLowerPrim("lower");
    KtMoveLeftPrim moveleft = new KtMoveLeftPrim("moveleft");
    KtMoveRightPrim moveright = new KtMoveRightPrim("moveright");

    def(raise);
    def(lower);
    def(moveleft);
    def(moveright);
    moveleft.defineAction(KtVertBuilder.class, moveleft.NORMAL);
    moveright.defineAction(KtVertBuilder.class, moveright.NORMAL);
    raise.defineAction(KtHorizBuilder.class, raise.NORMAL);
    lower.defineAction(KtHorizBuilder.class, lower.NORMAL);
    raise.defineAction(KtMathBuilder.class, raise.NORMAL);
    lower.defineAction(KtMathBuilder.class, lower.NORMAL);

    defUnVCopy(new KtAnyUnCopyPrim("unvcopy", setbox));
    defUnVCopy(new KtAnyUnBoxPrim("unvbox", setbox));
    defUnHCopy(new KtAnyUnCopyPrim("unhcopy", setbox));
    defUnHCopy(new KtAnyUnBoxPrim("unhbox", setbox));

    KtCharPrim char_prim = new KtCharPrim("char");
    KtExSpacePrim ex_space = new KtExSpacePrim(" ");
    KtNoBoundaryPrim no_boundary = new KtNoBoundaryPrim("noboundary");
    KtItalCorrPrim ital_corr = new KtItalCorrPrim("/");
    KtVAdjustPrim vadjust = new KtVAdjustPrim("vadjust");
    KtParPrim par = new KtParPrim("par");

    def(char_prim);
    def(ex_space);
    def(no_boundary);
    def(ital_corr);
    def(vadjust);
    def(par);

    def(new KtCharDefPrim("chardef", char_prim));

    char_prim.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    ex_space.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    no_boundary.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    par.defineAction(KtVertBuilder.class, par.RESET);

    char_prim.defineAction(KtHorizBuilder.class, char_prim.NORMAL);
    ex_space.defineAction(KtHorizBuilder.class, ex_space.NORMAL);
    no_boundary.defineAction(KtHorizBuilder.class, no_boundary.NORMAL);
    ital_corr.defineAction(KtHorizBuilder.class, ital_corr.NORMAL);
    vadjust.defineAction(KtHorizBuilder.class, vadjust.NORMAL);
    par.defineAction(KtHorizBuilder.class, par.NORMAL);

    char_prim.defineAction(KtMathBuilder.class, new KtCharMathAction(char_prim));
    ex_space.defineAction(KtMathBuilder.class, ex_space.NORMAL);
    no_boundary.defineAction( KtMathBuilder.class, KtBuilderPrim.EMPTY );
    ital_corr.defineAction(KtMathBuilder.class, KtMathPrim.ZERO_KERN);
    vadjust.defineAction(KtMathBuilder.class, vadjust.NORMAL);
    par.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);

    KtHyphenPrim hyph = new KtHyphenPrim( "-");
    KtDiscretionaryPrim disc = new KtDiscretionaryPrim("discretionary");
    KtSetLanguagePrim set_lang = new KtSetLanguagePrim("setlanguage");

    def(hyph);
    def(disc);
    def(set_lang);
    hyph.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    disc.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    hyph.defineAction(KtHorizBuilder.class, hyph.NORMAL);
    disc.defineAction(KtHorizBuilder.class, disc.NORMAL);
    set_lang.defineAction(KtHorizBuilder.class, set_lang.NORMAL);
    hyph.defineAction(KtMathBuilder.class, hyph.NORMAL);
    disc.defineAction(KtMathBuilder.class, disc.NORMAL);

    defIndent(new KtIndentPrim("indent", true));
    defIndent(new KtIndentPrim("noindent", false));

    defEnd(new KtEndPrim("end", false));
    defEnd(new KtEndPrim("dump", true));
  }

  private KtPage.List pageList() {
    return KtPage.getPageList();
  }

  {
    KtAnyMarkPrim top_mark = new KtAnyMarkPrim("topmark"),
        first_mark = new KtAnyMarkPrim("firstmark"),
        bot_mark = new KtAnyMarkPrim("botmark"),
        split_first_mark = new KtAnyMarkPrim("splitfirstmark"),
        split_bot_mark = new KtAnyMarkPrim("splitbotmark");

    def(top_mark);
    def(first_mark);
    def(bot_mark);
    def(split_first_mark);
    def(split_bot_mark);

    KtPage.makeStaticData(count, dimen, skip, setbox, top_mark, first_mark, bot_mark);
    def(new KtVSplitPrim("vsplit", setbox, split_first_mark, split_bot_mark));

    def(new KtPageGoalPrim("pagegoal", pageList()));
    def(new KtPageTotalPrim("pagetotal", pageList()));
    def(new KtPageStretchPrim("pagestretch", pageList(), KtGlue.NORMAL));
    def(new KtPageStretchPrim("pagefilstretch", pageList(), KtGlue.FIL));
    def(new KtPageStretchPrim("pagefillstretch", pageList(), KtGlue.FILL));
    def(new KtPageStretchPrim("pagefilllstretch", pageList(), KtGlue.FILLL));
    def(new KtPageShrinkPrim("pageshrink", pageList()));
    def(new KtPageDepthPrim("pagedepth", pageList()));
    def(new KtDeadCyclesPrim("deadcycles", pageList()));
    def(new KtInsertPenaltiesPrim("insertpenalties", pageList()));

    def(new KtShowListsPrim("showlists"));
    def(new KtInsertPrim("insert"));
    def(new KtMarkPrim("mark"));

    def(new KtUnSkipPrim("unskip"));
    def(new KtUnKernPrim("unkern"));
    def(new KtUnPenaltyPrim("unpenalty"));

    def(new KtLastBoxPrim("lastbox"));
    def(new KtLastSkipPrim("lastskip"));
    def(new KtLastKernPrim("lastkern"));
    def(new KtLastPenaltyPrim("lastpenalty"));

    def(new KtSpaceFactorPrim("spacefactor"));
    def(new KtPrevDepthPrim("prevdepth"));
    def(new KtPrevGrafPrim( "prevgraf"));

    def(new KtIfVModePrim("ifvmode"));
    def(new KtIfHModePrim("ifhmode"));
    def(new KtIfMModePrim( "ifmmode"));
    def(new KtIfInnerPrim("ifinner"));
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Grouping-related primitives
   */

  private final KtLeftBrace left_brace = new KtLeftBrace();
  private final KtRightBrace right_brace = new KtRightBrace();

  private final KtEndGroupPrim end_group = new KtEndGroupPrim("endgroup");
  private final KtBeginGroupPrim begin_group = new KtBeginGroupPrim("begingroup", end_group);

  {
    def(new KtAfterGroupPrim("aftergroup"));
    def(begin_group);
    def(end_group);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Math  primitives
   */

  private void mActDef(KtMathPrim prim) {
    prim.defineAction( KtBuilder.class, KtMathPrim.DOLLAR );
    prim.defineAction(KtMathBuilder.class, prim.mathAction());
  }

  private void mdef(KtMathPrim prim) {
    def(prim);
    mActDef(prim);
  }

  private final KtMathCompPrim math_ord = new KtMathOrdPrim( "mathord");
  private final KtMathCompPrim math_op = new KtMathOpPrim( "mathop");
  private final KtMathCompPrim math_bin = new KtMathBinPrim( "mathbin");
  private final KtMathCompPrim math_rel = new KtMathRelPrim( "mathrel");
  private final KtMathCompPrim math_open = new KtMathOpenPrim( "mathopen");
  private final KtMathCompPrim math_close = new KtMathClosePrim( "mathclose");
  private final KtMathCompPrim math_punct = new KtMathPunctPrim( "mathpunct");
  private final KtMathCompPrim math_inner = new KtMathInnerPrim( "mathinner");
  private final KtScriptPrim super_script = new KtSuperScriptPrim( "superscript");
  private final KtScriptPrim sub_script = new KtSubScriptPrim( "subscript");
  private final KtFamilyPrim text_font = new KtFamilyPrim( "textfont");
  private final KtFamilyPrim script_font = new KtFamilyPrim( "scriptfont");
  private final KtFamilyPrim script_script_font = new KtFamilyPrim( "scriptscriptfont");

  private final KtOpenMath open_math = new KtOpenMath(every_math, every_display);

  // XXX maybe that all the KtEndGroupCommand-s can be constructed inside
  // XXX their owners
  private final KtEndGroupCommand close_math = new KtEndGroupCommand("internal close math"),
      close_left = new KtEndGroupCommand("internal close left"),
      start_eqno = new KtEndGroupCommand("internal start eqno"),
      start_leqno = new KtEndGroupCommand("internal start leqno");
  private final KtEqNoPrim eqno = new KtEqNoPrim( "eqno", start_eqno, every_math, false);
  private final KtEqNoPrim leqno = new KtEqNoPrim( "leqno", start_leqno, every_math, true);

  {
    open_math.defineAction(KtVertBuilder.class, KtBuilderPrim.START_PAR);
    open_math.defineAction(KtParBuilder.class, open_math.OPEN_MATH);
    open_math.defineAction(KtHBoxBuilder.class, open_math.OPEN_FORMULA);
    open_math.defineAction(KtMathBuilder.class, new KtAdapterAction(close_math));
    left_brace.defineAction(KtMathBuilder.class, KtMathPrim.LEFT_BRACE);

    KtAdaptMathPrim right = new KtAdaptMathPrim("right", close_left);
    KtLeftPrim left = new KtLeftPrim("left", right);

    mdef(left);
    mdef(right);
    mdef(math_ord);
    mdef(math_op);
    mdef(math_bin);
    mdef(math_rel);
    mdef(math_open);
    mdef(math_close);
    mdef(math_punct);
    mdef(math_inner);
    mActDef(super_script);
    mActDef(sub_script);

    def(eqno);
    def(leqno);
    eqno.defineAction(KtDisplayBuilder.class, eqno.NORMAL);
    leqno.defineAction(KtDisplayBuilder.class, leqno.NORMAL);

    KtAccentPrim accent = new KtAccentPrim("accent");
    KtMathCharPrim math_char = new KtMathCharPrim("mathchar");
    KtMathAccentPrim math_accent = new KtMathAccentPrim( "mathaccent");

    def(accent);
    mdef(math_char);
    mdef(math_accent);
    accent.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    accent.defineAction(KtHorizBuilder.class, accent.NORMAL);
    accent.defineAction(KtMathBuilder.class, math_accent.FORCE_MATH_ACCENT);

    def(new KtMathCharDefPrim("mathchardef", math_char));
    def(text_font);
    def(script_font);
    def(script_script_font);

    mdef(new KtOverlinePrim("overline"));
    mdef(new KtUnderlinePrim("underline"));
    mdef(new KtDelimiterPrim("delimiter"));
    mdef(new KtRadicalPrim("radical"));
    mdef(new KtVCenterPrim("vcenter", every_vbox));
    mdef(new KtMathChoicePrim("mathchoice"));
    mdef(new KtFractionPrim("above", false, KtFractionPrim.EXPLICIT_THICKNESS));
    mdef(new KtFractionPrim("over", false, KtFractionPrim.DEFAULT_THICKNESS));
    mdef(new KtFractionPrim("atop", false, KtFractionPrim.ZERO_THICKNESS));
    mdef(new KtFractionPrim("abovewithdelims", true, KtFractionPrim.EXPLICIT_THICKNESS));
    mdef(new KtFractionPrim("overwithdelims", true, KtFractionPrim.DEFAULT_THICKNESS));
    mdef(new KtFractionPrim("atopwithdelims", true, KtFractionPrim.ZERO_THICKNESS));

    mdef(new KtLimitsPrim( "nolimits", KtOpNoad.SIDE_LIMITS));
    mdef(new KtLimitsPrim("limits", KtOpNoad.USUAL_LIMITS));
    mdef(new KtLimitsPrim("displaylimits", KtOpNoad.DEFAULT_LIMITS));

    mdef(new KtStylePrim( "displaystyle", KtStyleNoad.DISPLAY_STYLE));
    mdef(new KtStylePrim("textstyle", KtStyleNoad.TEXT_STYLE));
    mdef(new KtStylePrim("scriptstyle", KtStyleNoad.SCRIPT_STYLE));
    mdef(new KtStylePrim("scriptscriptstyle", KtStyleNoad.SCRIPT_SCRIPT_STYLE));

    mdef(new KtMSkipPrim("mskip"));
    mdef(new KtMKernPrim( "mkern"));
    mdef(new KtNonScriptPrim("nonscript"));
  }

  private transient KtFamilyPrim[] families;

  private void initMath() {
    KtMathShiftToken.setCommand(open_math);
    KtSuperMarkToken.setCommand(super_script);
    KtSubMarkToken.setCommand(sub_script);

    families = new KtFamilyPrim[KtFormulaGroup.NUMBER_OF_FONT_SIZES];
    families[KtFormulaGroup.TEXT_SIZE] = text_font;
    families[KtFormulaGroup.SCRIPT_SIZE] = script_font;
    families[KtFormulaGroup.SCRIPT_SCRIPT_SIZE] = script_script_font;
  }

  {
    initMath();
  }

  private final KtEndv endv = new KtEndv();
  private final KtTabMarkPrim tab_mark = new KtTabMarkPrim( "tabmark");
  private final KtCrPrim cr = new KtCrPrim( "cr");
  private final KtVAlignPrim valign = new KtVAlignPrim( "valign", every_cr, cr, endv);
  private final KtHAlignPrim halign = new KtHAlignPrim("halign", every_cr, cr, endv);

  private final KtEndGroupCommand close_column = new KtEndGroupCommand("internal close column");
  private final KtEndGroupCommand disp_halign = new KtEndGroupCommand("internal displayed halign");

  {
    def(cr);
    def(valign);
    def(halign);
    halign.defineAction(KtVertBuilder.class, halign.NORMAL);
    halign.defineAction( KtParBuilder.class, KtBuilderPrim.FINISH_PAR );
    halign.defineAction( KtHBoxBuilder.class, KtBuilderPrim.REJECT );
    halign.defineAction(KtDisplayBuilder.class, new KtAdapterAction(disp_halign));
    valign.defineAction( KtVertBuilder.class, KtBuilderPrim.START_PAR );
    valign.defineAction(KtHorizBuilder.class, valign.NORMAL);
    valign.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);

    def(new KtCrCrPrim("crcr"));
    def(new KtSpanPrim("span"));
    def(new KtOmitPrim("omit"));
    def(new KtNoAlignPrim("noalign"));

    KtAction normalEndv = new KtAdapterAction(close_column);
    endv.defineAction(KtVertBuilder.class, normalEndv);
    endv.defineAction(KtHorizBuilder.class, normalEndv);
    endv.defineAction(KtMathBuilder.class, KtMathPrim.DOLLAR);
  }

  private void initAlign() {
    KtTabMarkToken.setCommand(tab_mark);
  }

  {
    initAlign();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Groups initialization
   */

  private void initGroups() {
    KtLeftBraceToken.setCommand(left_brace);
    KtRightBraceToken.setCommand(right_brace);

    end_group.defineClosing( KtSemiSimpleGroup.class, KtEndGroupPrim.NORMAL );
    close_math.defineClosing( KtFormulaGroup.class, KtEndGroupCommand.NORMAL );
    close_left.defineClosing( KtLeftGroup.class, KtEndGroupCommand.NORMAL );
    close_column.defineClosing(KtAlignment.KtSpanGroup.class, KtAlignment.CLOSE_COLUMN);
    start_eqno.defineClosing( KtDisplayGroup.class, eqno.makeDisplayedClosing());
    start_leqno.defineClosing(KtDisplayGroup.class, leqno.makeDisplayedClosing());
    disp_halign.defineClosing(KtDisplayGroup.class, halign.makeDisplayedClosing());

    right_brace.defineClosing( KtSemiSimpleGroup.class, KtRightBrace.EXTRA );
    right_brace.defineClosing( KtFormulaGroup.class, KtRightBrace.EXTRA );
    right_brace.defineClosing( KtLeftGroup.class, KtRightBrace.EXTRA );
    close_left.defineClosing(KtFormulaGroup.class, KtLeftPrim.EXTRA_RIGHT);
    right_brace.defineClosing(KtAlignment.KtSpanGroup.class, KtAlignment.MISSING_CR);

    KtGroupCommand.registerGroup(KtGroup.class);
    KtGroupCommand.registerGroup( KtBottomGroup.class);
    KtGroupCommand.registerGroup(KtSimpleGroup.class);
    KtGroupCommand.registerGroup(KtSemiSimpleGroup.class);
    KtGroupCommand.registerGroup(KtHorizGroup.class);
    KtGroupCommand.registerGroup(KtHBoxGroup.class);
    KtGroupCommand.registerGroup(KtVertGroup.class);
    KtGroupCommand.registerGroup(KtVBoxGroup.class);
    KtGroupCommand.registerGroup(KtVTopGroup.class);
    KtGroupCommand.registerGroup(KtPage.List.KtOutputGroup.class);
    KtGroupCommand.registerGroup(KtInsertPrim.KtInsertGroup.class);
    KtGroupCommand.registerGroup(KtVAdjustPrim.KtVAdjustGroup.class);
    KtGroupCommand.registerGroup(KtVCenterGroup.class);
    KtGroupCommand.registerGroup(KtFormulaGroup.class);
    KtGroupCommand.registerGroup(KtDisplayGroup.class);
    KtGroupCommand.registerGroup(KtEqNoGroup.class);
    KtGroupCommand.registerGroup( KtMathGroup.class);
    KtGroupCommand.registerGroup(KtLeftGroup.class);
    KtGroupCommand.registerGroup(KtMathChoiceGroup.class);
    KtGroupCommand.registerGroup(KtAlignGroup.class);
    KtGroupCommand.registerGroup(KtDispAlignGroup.class);
    KtGroupCommand.registerGroup(KtAlignment.KtSpanGroup.class);
    KtGroupCommand.registerGroup(KtAlignment.KtNoAlignGroup.class);
  }

  {
    initGroups();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Builders initialization
   */

  {
    KtBuilderCommand.registerBuilder(KtBuilder.class);
    KtBuilderCommand.registerBuilder(KtListBuilder.class);
    KtBuilderCommand.registerBuilder(KtVertBuilder.class);
    KtBuilderCommand.registerBuilder(KtPageBuilder.class);
    KtBuilderCommand.registerBuilder(KtVBoxBuilder.class);
    KtBuilderCommand.registerBuilder(KtOutputBuilder.class);
    KtBuilderCommand.registerBuilder(KtHorizBuilder.class);
    KtBuilderCommand.registerBuilder(KtParBuilder.class);
    KtBuilderCommand.registerBuilder(KtHBoxBuilder.class);
    KtBuilderCommand.registerBuilder(KtMathBuilder.class);
    KtBuilderCommand.registerBuilder(KtFormulaBuilder.class);
    KtBuilderCommand.registerBuilder(KtDisplayBuilder.class);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   */

  private final KtDefCodePrim
      cat_code = new KtDefCodePrim( "catcode", KtTeXConfig.CAT_OTHER_CHAR, KtTeXConfig.MAX_CATEGORY),
      lc_code = new KtDefCodePrim( "lccode", 0, KtTeXConfig.MAX_TEX_CHAR),
      uc_code = new KtDefCodePrim( "uccode", 0, KtTeXConfig.MAX_TEX_CHAR),
      sf_code =
          new KtDefCodePrim( "sfcode", KtTeXConfig.NORMAL_SPACE_FACTOR, KtTeXConfig.MAX_SPACE_FACTOR),
      math_code = new KtMathCodePrim("mathcode", 0x8000),
      del_code = new KtDelCodePrim("delcode", -1, 0xffffff);

  {
    def( cat_code );
    def( lc_code );
    def( uc_code );
    def( sf_code );
    def( math_code );
    def( del_code );

    for( char uc = 'A', lc = 'a'; uc <= 'Z'; uc++, lc++ ) {
      cat_code.init( uc, KtTeXConfig.CAT_LETTER );
      cat_code.init( lc, KtTeXConfig.CAT_LETTER );
      lc_code.init( uc, lc );
      uc_code.init( uc, uc );
      lc_code.init( lc, lc );
      uc_code.init( lc, uc );
      sf_code.init( uc, KtTeXConfig.UPCASE_SPACE_FACTOR );
      math_code.init( uc, uc + 0x7100 ); // XXX
      math_code.init( lc, lc + 0x7100 ); // XXX
    }

    for( char digit = '0'; digit <= '9'; digit++ ) {
      math_code.init( digit, digit + 0x7000 ); // XXX
    }

    cat_code.init( '\r', KtTeXConfig.CAT_CAR_RET );
    cat_code.init( ' ', KtTeXConfig.CAT_SPACER );
    cat_code.init( '\\', KtTeXConfig.CAT_ESCAPE );
    cat_code.init( '%', KtTeXConfig.CAT_COMMENT );
    cat_code.init( 0, KtTeXConfig.CAT_IGNORE );
    cat_code.init( 127, KtTeXConfig.CAT_INVALID_CHAR );
    del_code.init( '.', 0 );
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Implementation of |KtCommand.KtConfig|.
   */

  private static final class KtConstIntProvider implements KtIntProvider {
    private final int value;

    public KtConstIntProvider(int val) {
      value = val;
    }

    public int intVal() {
      return value;
    }
  }

  private static final class KtAtLeastIntProvider implements KtIntProvider {
    private final KtIntProvider prov;
    private final int threshold;
    private final int value;

    public KtAtLeastIntProvider(KtIntProvider p, int t, int val) {
      prov = p;
      threshold = t;
      value = val;
    }

    public int intVal() {
      int i = prov.intVal();
      return i > threshold ? i : value;
    }
  }

  private static final class KtFontDimenProvider implements KtDimen.KtProvider {
    private final int param;

    public KtFontDimenProvider(int par) {
      param = par;
    }

    public KtDimen getDimenValue() {
      return KtTypoCommand.getCurrFontMetric().getDimenParam(param);
    }
  }

  private abstract static class KtRelation {
    protected KtIntProvider provider;
    protected int value;

    public KtRelation(KtIntProvider prov, int val) {
      provider = prov;
      value = val;
    }

    public abstract boolean execute();
  }

  private static class KtLessThan extends KtRelation {
    public KtLessThan(KtIntProvider prov, int val) {
      super(prov, val);
    }

    public boolean execute() {
      return provider.intVal() < value;
    }
  }

  private static class KtMoreThan extends KtRelation {
    public KtMoreThan(KtIntProvider prov, int val) {
      super(prov, val);
    }

    public boolean execute() {
      return provider.intVal() > value;
    }
  }

  private transient KtIntProvider[] intParams;
  private transient KtDimen.KtProvider[] dimParams;
  private transient KtGlueParam[] glueParams;
  private transient KtToksParam[] toksParams;
  private transient KtRelation[] boolParams;

  public int getIntParam(int param) {
    if (fits(param, intParams)) return intParams[param].intVal();
    paramError("Int", param);
    return 0;
  }

  public KtDimen getDimParam(int param) {
    if (fits(param, dimParams)) return dimParams[param].getDimenValue();
    paramError("Dim", param);
    return KtDimen.ZERO;
  }

  public KtGlue getGlueParam(int param) {
    if (fits(param, glueParams)) return glueParams[param].getGlueValue();
    paramError("KtGlue", param);
    return KtGlue.ZERO;
  }

  public String getGlueName(int param) {
    if (fits(param, glueParams)) return glueParams[param].getName();
    return null;
  }

  public KtTokenList getToksParam(int param) {
    if (fits(param, toksParams)) return toksParams[param].getToksValue();
    paramError("Toks", param);
    return KtTokenList.EMPTY;
  }

  public KtTokenList.KtInserter getToksInserter(int param) {
    if (fits(param, toksParams)) return toksParams[param];
    paramError("Toks", param);
    return null;
  }

  public boolean getBoolParam(int param) {
    if (fits(param, boolParams)) return boolParams[param].execute();
    paramError("Bool", param);
    return false;
  }

  private boolean fits(int idx, Object array) {
    return 0 <= idx && idx < Array.getLength( array) && Array.get( array, idx) != null;
  }

  private void paramError(String which, int param) {
    throw new RuntimeException(which + "Param(" + param + ") not supported");
  }

  private void initParams() {
    /* KtParagraph must be loaded before
     * KtCommandBase.max*Param is used
     */
    Class<?> dummy = KtParagraph.class;

    KtConstIntProvider CONST_15 = new KtConstIntProvider(15);
    KtConstIntProvider CONST_255 = new KtConstIntProvider(255);
    KtConstIntProvider CONST_2_15_1 = new KtConstIntProvider(0x7fff);
    KtConstIntProvider CONST_2_27_1 = new KtConstIntProvider(0x7ffffff);

    /* ** Int Parameters ** */

    // Note: 33 was hardcoded by James Fennell and stops NTS from crashing on startup
    intParams = new KtIntProvider[33];
    intParams[KtCommandBase.INTP_MAGNIFICATION] = mag;
    intParams[KtCommandBase.INTP_MAX_RUNAWAY_WIDTH] = new KtConstIntProvider(
      KtTeXConfig.MAX_RUNAWAY_WD);
    intParams[KtCommandBase.INTP_MAX_TLRES_TRACE] = new KtConstIntProvider(
      KtTeXConfig.MAX_TOK_LIST_RT);
    intParams[KtPrim.INTP_MAX_REG_CODE] = CONST_255;
    intParams[KtPrim.INTP_MAX_FILE_CODE] = CONST_15;
    intParams[KtPrim.INTP_MAX_CHAR_CODE] = CONST_255;
    intParams[KtMathPrim.INTP_MAX_MATH_CODE] = CONST_2_15_1;
    intParams[KtMathPrim.INTP_MAX_DEL_CODE] = CONST_2_27_1;
    intParams[KtTypoCommand.INTP_SHOW_BOX_DEPTH] = show_box_depth;
    intParams[KtTypoCommand.INTP_SHOW_BOX_BREADTH] = new KtAtLeastIntProvider(show_box_breadth, 0, 5);
    intParams[KtTypoCommand.INTP_HBADNESS] = hbadness;
    intParams[KtTypoCommand.INTP_VBADNESS] = vbadness;
    intParams[KtTypoCommand.INTP_OUTPUT_BOX_NUM] = CONST_255;
    intParams[KtParagraph.INTP_PRETOLERANCE] = pretolerance;
    intParams[KtParagraph.INTP_TOLERANCE] = tolerance;
    intParams[KtParagraph.INTP_LOOSENESS] = looseness;
    intParams[KtParagraph.INTP_LINE_PENALTY] = line_penalty;
    intParams[KtParagraph.INTP_HYPHEN_PENALTY] = hyphen_penalty;
    intParams[KtParagraph.INTP_EX_HYPHEN_PENALTY] = ex_hyphen_penalty;
    intParams[KtParagraph.INTP_ADJ_DEMERITS] = adj_demerits;
    intParams[KtParagraph.INTP_DOUBLE_HYPHEN_DEMERITS] = double_hyphen_demerits;
    intParams[KtParagraph.INTP_FINAL_HYPHEN_DEMERITS] = final_hyphen_demerits;
    intParams[KtParagraph.INTP_INTER_LINE_PENALTY] = inter_line_penalty;
    intParams[KtParagraph.INTP_BROKEN_PENALTY] = broken_penalty;
    intParams[KtParagraph.INTP_CLUB_PENALTY] = club_penalty;
    intParams[KtParagraph.INTP_WIDOW_PENALTY] = widow_penalty;
    intParams[KtPage.INTP_MAX_DEAD_CYCLES] = max_dead_cycles;
    intParams[KtBuilderPrim.INTP_MAX_MARK_WIDTH] = new KtConstIntProvider(
      KtTeXConfig.MAX_MARK_WD);
    intParams[KtInsertPrim.INTP_FLOATING_PENALTY] = floating_penalty;
    intParams[KtOpenMath.INTP_DISPLAY_WIDOW_PENALTY] = display_widow_penalty;
    intParams[KtFormulaGroup.INTP_DELIMITER_FACTOR] = delimiter_factor;
    intParams[KtDisplayGroup.INTP_PRE_DISPLAY_PENALTY] = pre_display_penalty;
    intParams[KtDisplayGroup.INTP_POST_DISPLAY_PENALTY] = post_display_penalty;

    /* ** Dim Parameters ** */

    dimParams = new KtDimen.KtProvider[KtCommandBase.maxDimParam()];
    dimParams[KtCommandBase.DIMP_EM] = new KtFontDimenProvider(KtFontMetric.DIMEN_PARAM_QUAD);
    dimParams[KtCommandBase.DIMP_EX] = new KtFontDimenProvider(KtFontMetric.DIMEN_PARAM_X_HEIGHT);
    dimParams[KtTypoCommand.DIMP_HFUZZ] = hfuzz;
    dimParams[KtTypoCommand.DIMP_VFUZZ] = vfuzz;
    dimParams[KtTypoCommand.DIMP_OVERFULL_RULE] = overfull_rule;
    dimParams[KtTypoCommand.DIMP_LINE_SKIP_LIMIT] = line_skip_limit;
    dimParams[KtTypoCommand.DIMP_H_OFFSET] = h_offset;
    dimParams[KtTypoCommand.DIMP_V_OFFSET] = v_offset;
    dimParams[KtTypoCommand.DIMP_BOX_MAX_DEPTH] = box_max_depth;
    dimParams[KtTypoCommand.DIMP_SPLIT_MAX_DEPTH] = split_max_depth;
    dimParams[KtParagraph.DIMP_EMERGENCY_STRETCH] = emergency_stretch;
    dimParams[KtParagraph.DIMP_PAR_INDENT] = par_indent;
    dimParams[KtPage.DIMP_VSIZE] = vsize;
    dimParams[KtPage.DIMP_MAX_DEPTH] = max_depth;
    dimParams[KtEndPrim.DIMP_HSIZE] = hsize;
    dimParams[KtFormulaGroup.DIMP_MATH_SURROUND] = math_surround;
    dimParams[KtFormulaGroup.DIMP_NULL_DELIMITER_SPACE] = null_delimiter_space;
    dimParams[KtFormulaGroup.DIMP_DELIMITER_SHORTFALL] = delimiter_shortfall;
    dimParams[KtFormulaGroup.DIMP_SCRIPT_SPACE] = script_space;
    dimParams[KtDisplayGroup.DIMP_PRE_DISPLAY_SIZE] = pre_display_size;
    dimParams[KtDisplayGroup.DIMP_DISPLAY_WIDTH] = display_width;
    dimParams[KtDisplayGroup.DIMP_DISPLAY_INDENT] = display_indent;

    /* ** KtGlue Parameters ** */

    glueParams = new KtGlueParam[KtCommandBase.maxGlueParam()];
    glueParams[KtTypoCommand.GLUEP_SPACE] = space_skip;
    glueParams[KtTypoCommand.GLUEP_XSPACE] = xspace_skip;
    glueParams[KtTypoCommand.GLUEP_LINE_SKIP] = line_skip;
    glueParams[KtTypoCommand.GLUEP_BASELINE_SKIP] = baseline_skip;
    glueParams[KtTypoCommand.GLUEP_SPLIT_TOP_SKIP] = split_top_skip;
    glueParams[KtParagraph.GLUEP_PAR_SKIP] = par_skip;
    glueParams[KtParagraph.GLUEP_PAR_FILL_SKIP] = par_fill_skip;
    glueParams[KtParagraph.GLUEP_LEFT_SKIP] = left_skip;
    glueParams[KtParagraph.GLUEP_RIGHT_SKIP] = right_skip;
    glueParams[KtPage.GLUEP_TOP_SKIP] = top_skip;
    glueParams[KtDisplayGroup.GLUEP_ABOVE_DISPLAY_SKIP] = above_display_skip;
    glueParams[KtDisplayGroup.GLUEP_BELOW_DISPLAY_SKIP] = below_display_skip;
    glueParams[KtDisplayGroup.GLUEP_ABOVE_DISPLAY_SHORT_SKIP] = above_display_short_skip;
    glueParams[KtDisplayGroup.GLUEP_BELOW_DISPLAY_SHORT_SKIP] = below_display_short_skip;
    glueParams[KtAlignment.GLUEP_TAB_SKIP] = tab_skip;

    /* ** Toks Parameters ** */

    toksParams = new KtToksParam[KtCommandBase.maxToksParam()];
    toksParams[KtCommand.TOKSP_EVERY_JOB] = every_job;
    toksParams[KtParagraph.TOKSP_EVERY_PAR] = every_par;
    toksParams[KtPage.TOKSP_OUTPUT] = output_routine;

    /* ** Bool Parameters ** */

    boolParams = new KtRelation[KtCommandBase.maxBoolParam()];
    boolParams[KtCommandBase.BOOLP_TRACING_TOKEN_LISTS] = new KtMoreThan(tracing_macros, 1);
    boolParams[KtCommandBase.BOOLP_TRACING_RESTORES] = new KtMoreThan(tracing_restores, 0);
    boolParams[KtPrefixPrim.BOOLP_ALWAYS_GLOBAL] = new KtMoreThan(global_defs, 0);
    boolParams[KtPrefixPrim.BOOLP_NEVER_GLOBAL] = new KtLessThan(global_defs, 0);
    boolParams[KtMacro.BOOLP_TRACING_MACROS] = new KtMoreThan(tracing_macros, 0);
    boolParams[KtCommand.BOOLP_TRACING_COMMANDS] = new KtMoreThan(tracing_commands, 0);
    boolParams[KtExpandable.BOOLP_TRACING_ALL_COMMANDS] = new KtMoreThan(tracing_commands, 1);
    boolParams[KtTypoCommand.BOOLP_TRACING_LOST_CHARS] = new KtMoreThan(tracing_lost_chars, 0);
    boolParams[KtTypoCommand.BOOLP_TRACING_OUTPUT] = new KtMoreThan(tracing_output, 0);
    boolParams[KtParagraph.BOOLP_TRACING_PARAGRAPHS] = new KtMoreThan(tracing_paragraphs, 0);
    boolParams[KtParagraph.BOOLP_UC_HYPH] = new KtMoreThan(uc_hyph, 0);
    boolParams[KtPage.BOOLP_TRACING_PAGES] = new KtMoreThan(tracing_pages, 0);
    boolParams[KtPage.BOOLP_HOLDING_INSERTS] = new KtMoreThan(holding_inserts, 0);
  }

  {
    initParams();
  }

  private boolean afterAssignmentEnabled = true;

  public boolean enableAfterAssignment(boolean val) {
    boolean old = afterAssignmentEnabled;
    afterAssignmentEnabled = val;
    return old;
  }

  public void afterAssignment() {
    if (afterAssignmentEnabled) after_assignment.apply();
  }

  private final KtToken FROZEN_FI = new KtFrozenToken(prim_fi);

  public boolean enableInput(boolean val) {
    return input.enable(val);
  }

  public boolean formatLoaded() {
    return loaded;
  }

  public KtToken frozenFi() {
    return FROZEN_FI;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Implementation of |KtTypoCommand.KtConfig|.
   */

  public void setLastBadness(int val) {
    badness.set(val);
  }

  public void setOutputPenalty(int val) {
    output_penalty.set( KtNum.valueOf( val), true);
  }

  public int[] currPageNumbers() {
    int i = 9;
    while (i > 0 && count.intVal(i) == 0) i--;
    int[] nums = new int[++i];
    while (--i >= 0) nums[i] = count.intVal(i);
    return nums;
  }

  /* TeXtp[1094] */
  public void checkParagraph(KtToken src) {
    int disbalance = KtAlignment.activeColumnDisbalance();
    if (disbalance < 0 && disbalance != KtAlignment.NOALIGN_DISBALANCE) KtCommand.getGrp().reject(src);
  }

  /* TeXtp[1070] */
  public void resetParagraph() {
    if (looseness.intVal() != 0) looseness.set(KtNum.ZERO, false);
    if (!hang_indent.get().isZero()) hang_indent.set(KtDimen.ZERO, false);
    if (hang_after.intVal() != 1) hang_after.set(KtNum.valueOf(1), false);
    if (!par_shape.get().isEmpty()) par_shape.set(KtParShape.EMPTY, false);
  }

  public void setMarginSkipsShrinkFinite() {
    left_skip.makeShrinkFinite();
    right_skip.makeShrinkFinite();
  }

  public boolean activeOutput() {
    return pageList().outputActive();
  }

  public void resetOutput() {
    pageList().resetDeadCycles();
  }

  public boolean pendingOutput() {
    return pageList().getDeadCycles() != 0;
  }

  public KtLinesShape linesShape() {
    KtParShape parShape = par_shape.get();
    return !parShape.isEmpty()
        ? parShape
        : KtHangIndent.makeShape(hang_after.intVal(), hang_indent.get(), hsize.get());
  }

  private final KtHashHyphenation[] hyphenations = new KtHashHyphenation[NUMBER_OF_LANGUAGES];

  private final KtWordMap[] patterns = new KtWordMap[NUMBER_OF_LANGUAGES];

  private boolean packed = false;

  private transient HashMap<KtHyphNode, KtHyphNode> hyphNodeMap;
  private transient int[] hyphStats;

  public boolean patternsAllowed() {
    return !packed;
  }

  public void preparePatterns() {
    if (!packed) {
      /*
          if (hyphStats[0] > 0) {
      	System.err.println("hyphen nodes: " + hyphStats[0]);
      	System.err.println("shared nodes: " + hyphStats[1]
      	    + " (" + (100 * hyphStats[1] / hyphStats[0]) + "%)");
          }
      */
      for (int i = 0; i < patterns.length; i++)
        if (patterns[i] != KtWordMap.NULL) {
          // patterns[i] = patterns[i].packed();
          /*
              try {
          	System.err.println("patterns(" + i + "):");
          	java.io.PrintWriter		perr
          	    = new java.io.PrintWriter(System.err);
          	KtHyphLanguage.dumpPatterns(perr, patterns[i]);
          	perr.flush();
              } catch (IOException e) { }
          */
        }
      packed = true;
    }
  }

  private int normalizedLangNum(int ln) {
    return ln < 0 || ln >= hyphenations.length ? 0 : ln;
  }

  public KtLanguage getLanguage(int langNum) {
    langNum = normalizedLangNum(langNum);
    if (hyphenations[langNum] == KtHashHyphenation.NULL) {
      hyphenations[langNum] = new KtHashHyphenation();
      if (packed) patterns[langNum] = KtWordTrie.EMPTY;
      else patterns[langNum] = new KtWordTree();
    }
    return new KtHyphLanguage(
        langNum,
        normalHyphenMin(left_hyphen_min.intVal()),
        normalHyphenMin(right_hyphen_min.intVal()),
        hyphenations[langNum],
        patterns[langNum],
        hyphNodeMap,
        hyphStats);
  }

  public KtLanguage getLanguage() {
    return getLanguage(language.intVal());
  }

  /* STRANGE
   * Why is only the language number compared and not also hyphen_(min|max)?
   */
  public boolean languageDiffers(KtLanguage lang) {
    return !lang.sameNumberAs(normalizedLangNum(language.intVal()));
  }

  /* TeXtp[1091] */
  private int normalHyphenMin(int h) {
    return h <= 0 ? 1 : h >= 64 ? 63 : h;
  } // XXX

  private void initHyphenation() {
    hyphNodeMap = new HashMap<>();
    hyphStats = new int[2];
  }

  {
    initHyphenation();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Implementation of |KtMathPrim.KtConfig|.
   */

  public void initFormula() {
    cur_fam.set(KtNum.valueOf(-1), false);
  }

  public void initDisplay(KtDimen preSize, KtDimen width, KtDimen indent) {
    cur_fam.set(KtNum.valueOf(-1), false);
    pre_display_size.set(preSize, false);
    display_width.set(width, false);
    display_indent.set(indent, false);
  }

  private final KtMathCompPrim[] mathComps = {
    math_ord, math_op, math_bin, math_rel, math_open, math_close, math_punct
  };

  /* TeXtp[1155] */
  public KtNoad noadForCode( int code) {
    if (code < 0 || code >= 0x8000) return KtNoad.NULL;
    int mathClass = code >>> 12;
    byte mathFam = (byte) (code >>> 8 & 15);
    if (mathClass >= mathComps.length) {
      int fam = cur_fam.intVal();
      if (fam >= 0 && fam < 16) mathFam = (byte) fam;
      mathClass = 0;
    }
    return mathComps[mathClass].makeNoad(new KtCharField( mathFam, KtToken.makeCharCode( code & 0xff)));
  }

  /* TeXtp[1151] */
  public KtCharField fieldForCode(int code) {
    if (code < 0 || code >= 0x8000) return KtCharField.NULL;
    int mathClass = code >>> 12;
    byte mathFam = (byte) (code >>> 8 & 15);
    if (mathClass >= mathComps.length) {
      int fam = cur_fam.intVal();
      if (fam >= 0 && fam < 16) mathFam = (byte) fam;
    }
    return new KtCharField(mathFam, KtToken.makeCharCode(code & 0xff));
  }

  public KtNoad noadForDelCode(int code) {
    return noadForCode(code >>> 12);
  }

  public KtCharField fieldForDelCode(int code) {
    return fieldForCode(code >>> 12);
  }

  /* TeXtp[1160] */
  public KtDelimiter delimiterForDelCode( int code) {
    if (code < 0) return KtDelimiter.NULL;
    int small = code >>> 12 & 0xfff;
    int large = code & 0xfff;
    byte smallFam = (byte) (small >>> 8 & 15);
    byte largeFam = (byte) (large >>> 8 & 15);
    return new KtDelimiter(smallFam, delCharCode(small), largeFam, delCharCode(large));
  }

  private static KtCharCode delCharCode(int code) {
    return code != 0 ? KtToken.makeCharCode( code & 0xff) : KtCharCode.NULL;
  }

  public KtFontMetric familyFont(byte size, byte fam) {
    return families[size].get(fam);
  }

  public String familyName(byte size, byte fam) {
    return families[size].getName();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *  Math Spacing
   */

  private static class KtZeroSpacer implements KtMathPrim.KtMathSpacer {
    public KtGlue getGlue(byte fontSize) {
      return KtGlue.ZERO;
    }

    public String getName() {
      return "";
    }
  }

  private static class KtBaseSpacer implements KtMathPrim.KtMathSpacer {
    protected final KtMuGlueParam param;

    public KtBaseSpacer(KtMuGlueParam param) {
      this.param = param;
    }

    public KtGlue getGlue(byte fontSize) {
      return param.getMuGlueValue();
    }

    public String getName() {
      return param.getName();
    }
  }

  private static class KtTextSpacer extends KtBaseSpacer {
    public KtTextSpacer(KtMuGlueParam param) {
      super(param);
    }

    public KtGlue getGlue(byte fontSize) {
      return fontSize == KtFormulaGroup.TEXT_SIZE ? param.getMuGlueValue() : KtGlue.ZERO;
    }
  }

  private transient KtMathPrim.KtMathSpacer[] mathSpacers;

  private transient KtMathPrim.KtMathSpacer ZERO_SPACER,
      THIN_SPACER,
      XTHIN_SPACER,
      MED_SPACER,
      THICK_SPACER;

  private int mathSpacingIndex(byte left, byte right) {
    return left * KtNoad.NUMBER_OF_SPACING_TYPES + right;
  }

  private void initMathSpacing(byte left, byte right, KtMathPrim.KtMathSpacer spacer) {
    mathSpacers[mathSpacingIndex(left, right)] = spacer;
  }

  private void msInval(byte left, byte right) {
    initMathSpacing(left, right, null);
  }

  private void msZero(byte left, byte right) {
    initMathSpacing(left, right, ZERO_SPACER);
  }

  private void msThin(byte left, byte right) {
    initMathSpacing(left, right, THIN_SPACER);
  }

  private void msTHIN(byte left, byte right) {
    initMathSpacing(left, right, XTHIN_SPACER);
  }

  private void msMed(byte left, byte right) {
    initMathSpacing(left, right, MED_SPACER);
  }

  private void msThick(byte left, byte right) {
    initMathSpacing(left, right, THICK_SPACER);
  }

  public KtMathPrim.KtMathSpacer mathSpacing(byte left, byte right) {
    KtMathPrim.KtMathSpacer spacer = mathSpacers[mathSpacingIndex(left, right)];
    if (spacer == null) throw new RuntimeException("invalid combination of spacing types");
    return spacer;
  }

  public KtNum mathPenalty(byte left, byte right) {
    if (right != KtNoad.SPACING_TYPE_REL)
      switch( left ) {
        case KtNoad.SPACING_TYPE_BIN -> {
          return bin_op_penalty.getNumValue();
        }
        case KtNoad.SPACING_TYPE_REL -> {
          return rel_penalty.getNumValue();
        }
      }
    return KtNum.NULL;
  }

  private void initMathSpacing() {
    int size = KtNoad.NUMBER_OF_SPACING_TYPES;
    mathSpacers = new KtMathPrim.KtMathSpacer[size * size];

    ZERO_SPACER = new KtZeroSpacer();
    THIN_SPACER = new KtTextSpacer(thin_mu_skip);
    XTHIN_SPACER = new KtBaseSpacer(thin_mu_skip);
    MED_SPACER = new KtTextSpacer(med_mu_skip);
    THICK_SPACER = new KtTextSpacer(thick_mu_skip);

    msZero(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_ORD); // 0
    msTHIN(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_OP); // 2
    msMed(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_BIN); // 3
    msThick(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_REL); // 4
    msZero(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_OPEN); // 0
    msZero(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_CLOSE); // 0
    msZero(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_PUNCT); // 0
    msThin(KtNoad.SPACING_TYPE_ORD, KtNoad.SPACING_TYPE_INNER); // 1

    msTHIN(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_ORD); // 2
    msTHIN(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_OP); // 2
    msInval(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_BIN); // *
    msThick(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_REL); // 4
    msZero(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_OPEN); // 0
    msZero(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_CLOSE); // 0
    msZero(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_PUNCT); // 0
    msThin(KtNoad.SPACING_TYPE_OP, KtNoad.SPACING_TYPE_INNER); // 1

    msMed(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_ORD); // 3
    msMed(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_OP); // 3
    msInval(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_BIN); // *
    msInval(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_REL); // *
    msMed(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_OPEN); // 3
    msInval(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_CLOSE); // *
    msInval(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_PUNCT); // *
    msMed(KtNoad.SPACING_TYPE_BIN, KtNoad.SPACING_TYPE_INNER); // 3

    msThick(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_ORD); // 4
    msThick(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_OP); // 4
    msInval(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_BIN); // *
    msZero(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_REL); // 0
    msThick(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_OPEN); // 4
    msZero(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_CLOSE); // 0
    msZero(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_PUNCT); // 0
    msThick(KtNoad.SPACING_TYPE_REL, KtNoad.SPACING_TYPE_INNER); // 4

    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_ORD); // 0
    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_OP); // 0
    msInval(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_BIN); // *
    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_REL); // 0
    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_OPEN); // 0
    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_CLOSE); // 0
    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_PUNCT); // 0
    msZero(KtNoad.SPACING_TYPE_OPEN, KtNoad.SPACING_TYPE_INNER); // 0

    msZero(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_ORD); // 0
    msTHIN(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_OP); // 2
    msMed(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_BIN); // 3
    msThick(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_REL); // 4
    msZero(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_OPEN); // 0
    msZero(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_CLOSE); // 0
    msZero(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_PUNCT); // 0
    msThin(KtNoad.SPACING_TYPE_CLOSE, KtNoad.SPACING_TYPE_INNER); // 1

    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_ORD); // 1
    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_OP); // 1
    msInval(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_BIN); // *
    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_REL); // 1
    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_OPEN); // 1
    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_CLOSE); // 1
    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_PUNCT); // 1
    msThin(KtNoad.SPACING_TYPE_PUNCT, KtNoad.SPACING_TYPE_INNER); // 1

    msThin(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_ORD); // 1
    msTHIN(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_OP); // 2
    msMed(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_BIN); // 3
    msThick(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_REL); // 4
    msThin(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_OPEN); // 1
    msZero(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_CLOSE); // 0
    msThin(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_PUNCT); // 1
    msThin(KtNoad.SPACING_TYPE_INNER, KtNoad.SPACING_TYPE_INNER); // 1
  }

  {
    initMathSpacing();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * Implementation of ?
   */

  // TeXCharMapper

  public int catCode(int c) {
    return cat_code.intVal(c);
  }

  public int lcNumCode(int c) {
    return lc_code.intVal(c);
  }

  public int ucNumCode(int c) {
    return uc_code.intVal(c);
  }

  public int spaceFactor(int c) {
    return sf_code.intVal(c);
  }

  public int mathCode(int c) {
    return math_code.intVal(c);
  }

  public int delCode(int c) {
    return del_code.intVal(c);
  }

  public int escapeNumCode() {
    return escape_char.intVal();
  }

  public int newLineNumCode() {
    return new_line_char.intVal();
  }

  public int endLineNumCode() {
    return end_line_char.intVal();
  }

  // TeXIOHandler

  public Calendar date() {
    return new GregorianCalendar(
        year.intVal(), month.intVal() - 1, day.intVal(), time.intVal() / 60, time.intVal() % 60);
  }

  public int errContextLines() {
    return error_context_lines.intVal() + 1;
  }

  public KtTokenList errHelp() {
    return err_help.get();
  }

  public boolean confirmingLines() {
    return pausing.intVal() > 0;
  }

  public int magnification() {
    return mag.intVal();
  }

  // TeXFontHandler

  public KtNum defaultHyphenChar() {
    return default_hyphen_char.get();
  }

  public KtNum defaultSkewChar() {
    return default_skew_char.get();
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  public KtCommand.KtConfig getCommandConfig() {
    return this;
  }

  public KtTypoCommand.KtConfig getTypoConfig() {
    return this;
  }

  public KtMathPrim.KtConfig getMathConfig() {
    return this;
  }

  public KtTeXCharMapper.KtConfig getCharMapConfig() {
    return this;
  }

  public KtConfig getIOHandConfig() {
    return this;
  }

  public KtTeXFontHandler.KtConfig getFontHandConfig() {
    return this;
  }
}

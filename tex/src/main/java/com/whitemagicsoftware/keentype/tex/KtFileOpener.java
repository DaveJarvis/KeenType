// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.FileOpener
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.io.KtClassPathResources;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import static java.io.InputStream.nullInputStream;

/**
 * Responsible for opening a file of a particular file format either from a
 * resource bundled with the application (inside or outside a JAR) or provided
 * from the file system. Callers are responsible for closing the streams when
 * finished processing.
 * <p>
 * If no handlers are registered
 * </p>
 */
public final class KtFileOpener {

  /**
   * Default handler closes the stream.
   */
  private static final KtFileHandler FILE_HANDLER = InputStream::close;

  private final Map<KtFileFormat, KtFileHandler> mHandlers = new HashMap<>(8);

  public KtFileOpener() { }

  /**
   * Adds a new handler to the collection of known handlers. When reading
   * a file, this will call the handler with the {@link KtFileFormat} and the
   * {@link InputStream}. This allows using a factory pattern, of sorts, to
   * delegate different processors for different types of file formats.
   *
   * @param format  The type of data to be processed.
   * @param handler The processor capable of reading an {@link InputStream}
   *                that conforms to the associated {@link KtFileFormat}.
   */
  public void attachHandler(
    final KtFileFormat format, final KtFileHandler handler ) {
    assert format != null;
    assert handler != null;

    mHandlers.put( format, handler );
  }

  /**
   * Removes the given handler from the collection of known handlers. This
   * method is
   * idempotent.
   *
   * @param format The {@link KtFileFormat} having a handler to deregister.
   */
  public void detachHandler( final KtFileFormat format ) {
    assert format != null;

    mHandlers.remove( format );
  }

  public void read( final KtFileName name, final KtFileFormat... formats )
    throws IOException, FontFormatException {
    final var path = name.getPath();
    final var uri = findFile( path, formats );

    final var url = uri.isPresent()
      ? uri.get().toURL()
      : new File( path ).toURI().toURL();

    for( final var format : formats ) {
      if( format.matches( url ) ) {
        final var handler = mHandlers.getOrDefault( format, FILE_HANDLER );
        final var stream = url.openStream();
        handler.handle( stream );
      }
    }
  }

  /**
   * Convenience method to read a file of a singularly known format and
   * return it as a stream.
   *
   * @param name   The file name to find and open.
   * @param format The type of file to find.
   * @return An open {@link InputStream} for reading the file's contents.
   * @throws UncheckedIOException Could not open the file for reading.
   */
  public static InputStream readFile(
    final KtFileName name, final KtFileFormat format ) {
    assert name != null;
    assert format != null;

    final var opener = new KtFileOpener();
    final var stream = new AtomicReference<>( nullInputStream() );

    try {
      opener.attachHandler( format, stream::set );
      opener.read( name, format );
    } catch( final FontFormatException e ) {
      throw new UncheckedIOException( new IOException( e ) );
    } catch( final IOException e ) {
      throw new UncheckedIOException( e );
    } finally {
      opener.detachHandler( format );
    }

    return stream.get();
  }

  public static OutputStream write(
    final KtFileName name, final KtFileFormat format )
    throws IOException {
    assert name != null;
    assert format != null;

    name.addDefaultExt( format.toString() );

    final var path = name.getPath();
    return new FileOutputStream( path );
  }

  /**
   * Builds a regular expression to match file names ending with the given
   * name having any of the given formats. This will find files in the local
   * file system as well as resources within JAR files.
   *
   * @param name    The ending of the file name to match, can include paths.
   * @param formats File name extensions to match.
   * @return The fully qualified {@link URI} to the file, if found.
   */
  private static Optional<URI> findFile(
    final String name, final KtFileFormat... formats ) {
    assert name != null;
    assert !name.isBlank();
    assert formats != null;
    assert formats.length > 0;

    final var regex = new StringBuilder( 128 );

    regex.append( ".*" );
    regex.append( name );
    regex.append( "(\\.(" );

    var sep = "";

    for( final var format : formats ) {
      regex.append( sep );
      regex.append( format );
      sep = "|";
    }

    regex.append( "))$" );

    final var pattern = Pattern.compile( regex.toString() );
    return KtClassPathResources.getResource( pattern );
  }
}

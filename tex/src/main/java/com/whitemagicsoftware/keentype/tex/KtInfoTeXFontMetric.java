// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeXFontHandler.InfoTeXFontMetric
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.tfm.KtTeXFm;
import com.whitemagicsoftware.keentype.tfm.KtTeXFontMetric;

class KtInfoTeXFontMetric extends KtTeXFontMetric implements KtFontInfo {

  private final int mId;
  private final byte[] mDirName;
  private final byte[] mFileName;

  public KtInfoTeXFontMetric(
    final KtName name,
    final KtTeXFm tfm,
    final KtDimen atSize,
    final KtName ident,
    final int id,
    final byte[] dirName,
    final byte[] fileName
  ) {
    super( name, tfm, atSize, ident );
    this.mId = id;
    this.mDirName = dirName;
    this.mFileName = fileName;
  }

  @Override
  public int getIdNumber() {
    return mId;
  }

  @Override
  public byte[] getDirName() {
    return mDirName;
  }

  @Override
  public byte[] getFileName() {
    return mFileName;
  }

  @Override
  public String toString() {
    return "<KtTeXFontMetric "
      + getName()
      + '('
      + (mDirName.length != 0 ? new String( mDirName ) + '/' : "")
      + new String( mFileName )
      + ")>";
  }
}

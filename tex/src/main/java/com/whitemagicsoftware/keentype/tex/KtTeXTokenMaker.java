// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeXTokenMaker
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.align.KtTabMarkToken;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.events.KtErrorEvent;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLoggable;
import com.whitemagicsoftware.keentype.io.KtName;
import com.whitemagicsoftware.keentype.math.KtMathShiftToken;
import com.whitemagicsoftware.keentype.math.KtSubMarkToken;
import com.whitemagicsoftware.keentype.math.KtSuperMarkToken;

public class KtTeXTokenMaker implements KtInputLineTokenizer.KtTokenMaker {

  public interface KtCategorizer {
    int catCode(int c);
  }

  public interface KtErrHandler {
    void error(String ident, KtLoggable[] params, boolean delAllowed);

    void fatalError(String ident);
  }

  private final KtCategorizer categorizer;

  private final byte[] scanCats = new byte[ KtTeXConfig.MAX_CATEGORY + 1];

  private final KtCharToken.KtMaker[] charTokMakers = new KtCharToken.KtMaker[ KtTeXConfig.MAX_CATEGORY + 1];

  public KtTeXTokenMaker( final KtCategorizer categ ) {
    categorizer = categ;

    for ( int i = 0; i <= KtTeXConfig.MAX_CATEGORY; i++) {
      scanCats[i] = KtInputLineTokenizer.OTHER;
      charTokMakers[i] = KtOtherToken.MAKER;
    }

    initScanCat( KtTeXConfig.CAT_ESCAPE, KtInputLineTokenizer.ESCAPE);
    initScanCat( KtTeXConfig.CAT_CAR_RET, KtInputLineTokenizer.ENDLINE);
    initScanCat( KtTeXConfig.CAT_IGNORE, KtInputLineTokenizer.IGNORE);
    initScanCat( KtTeXConfig.CAT_SPACER, KtInputLineTokenizer.SPACER);
    initScanCat( KtTeXConfig.CAT_COMMENT, KtInputLineTokenizer.COMMENT);

    scanCats[ KtTeXConfig.CAT_LETTER] = KtInputLineTokenizer.LETTER;

    charTokMakers[ KtTeXConfig.CAT_LEFT_BRACE] = KtLeftBraceToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_RIGHT_BRACE] = KtRightBraceToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_MATH_SHIFT] = KtMathShiftToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_TAB_MARK] = KtTabMarkToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_MAC_PARAM] = KtMacroParamToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_SUP_MARK] = KtSuperMarkToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_SUB_MARK] = KtSubMarkToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_LETTER] = KtLetterToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_OTHER_CHAR] = KtOtherToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_ACTIVE_CHAR] = KtActiveCharToken.MAKER;
    charTokMakers[ KtTeXConfig.CAT_INVALID_CHAR] = code -> {
      KtErrorEvent.publish( KtTeXErrorPool.get( "InvalidChar" ) );
      return KtToken.NULL;
    };
  }

  private void initScanCat(int idx, byte cat) {
    scanCats[idx] = cat;
    charTokMakers[idx] = null;
  }

  /**
   * Gives the scanning category of given internal character code. The return value must be one of:
   * |ESCAPE|, |LETTER|, |SPACER|, |ENDLINE|, |COMMENT|, |IGNORE|, |OTHER|.
   *
   * @param code internal character code.
   * @return scanning category of |code|.
   */
  public byte scanCat(KtCharCode code) {
    return scanCats[categorizer.catCode(code.toChar())];
  }

  /**
   * Makes a control sequence |KtToken| with given name.
   *
   * @param name the name of the control sequence.
   * @return the control sequence |KtToken|.
   */
  public KtToken make(KtName name) {
    return new KtCtrlSeqToken(name);
  }

  /**
   * Makes a character |KtToken| for given internal character code.
   *
   * @param code the internal character code.
   * @return the character |KtToken|.
   */
  public KtToken make(KtCharCode code) {
    KtCharToken.KtMaker maker = charTokMakers[categorizer.catCode(code.toChar())];
    if (maker != null) return maker.make(code);
    throw new RuntimeException("Invalid category for making a KtToken");
  }

  /**
   * Makes a |KtToken| corresponding to space.
   *
   * @return the space |KtToken|.
   */
  public KtToken makeSpace() {
    return KtSpaceToken.TOKEN;
  }

  private static final KtToken PAR_TOKEN = new KtCtrlSeqToken("par");

  /**
   * makes a |KtToken| corresponding to blank line (paragraph end).
   *
   * @return the paragraph |KtToken|.
   */
  public KtToken makePar() {
    return PAR_TOKEN;
  }
}

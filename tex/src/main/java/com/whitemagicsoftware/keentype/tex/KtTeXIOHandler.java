// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeXIOHandler
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.events.*;
import com.whitemagicsoftware.keentype.io.*;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

import java.io.*;

import static com.whitemagicsoftware.keentype.tex.KtFileFormat.*;
import static com.whitemagicsoftware.keentype.tex.KtFileOpener.readFile;
import static com.whitemagicsoftware.keentype.tex.KtFileOpener.write;
import static java.nio.charset.StandardCharsets.UTF_8;

public class KtTeXIOHandler implements
  KtCommand.KtIOHandler,
  KtLineInputTokenizer.KtInputHandler,
  KtLineInputReadInput.KtInputHandler,
  KtTeXTokenMaker.KtErrHandler {

  private final KtConfig mConfig;
  private final KtTeXCharMapper mMapper;
  private final KtTeXTokenMaker mMaker;
  private final KtContextDisplay mContextDisplay;
  private final KtTypesetter mTypesetter;

  public KtTeXIOHandler(
    final KtConfig config,
    final KtTeXCharMapper mapper,
    final KtTeXTokenMaker maker,
    final KtTeXContextDisplay display,
    final KtTypesetter typeSetter ) {
    assert config != null;
    assert mapper != null;
    assert display != null;
    assert typeSetter != null;

    mConfig = config;
    mMapper = mapper;
    mTypesetter = typeSetter;
    mMaker = maker;
    mContextDisplay = display;
  }

  public KtTeXTokenMaker getTokenMaker() {
    return mMaker;
  }

  private KtName jobName = KtName.NULL;

  public KtName getJobName() {
    if( jobName == KtName.NULL ) {
      jobName = KtToken.makeName( "texput" );
      openLogFile( jobName );
    }
    return jobName;
  }

  public void ensureOpenLog() {
    getJobName();
  }

  /* TeXtp[537] */
  public void openInput( KtFileName name ) {
    Reader in;
    KtFileName fullName;
    for( ; ; ) {
      fullName = name.copy();
      try {
        in = openReader( fullName, INPUT_EXT );
        break;
      } catch( final IOException e ) {
        // XXX distinguish IO and FileNotFound
        cantOpen( name, true );
        name = promptFileName( "input file name" );
      }
    }

    if( jobName == KtName.NULL ) {
      jobName = name.baseName();
      openLogFile( jobName );
    }

    final int len = KtCommand.normLog.voidCounter().add( fullName ).getCount();
    KtCommand.normLog.sepRoom( len + 2 ).add( '(' ).add( fullName ).flush();
    KtCommand.getTokStack()
           .push(
             new KtLineInputTokenizer(
               new KtLineInput( in, mMapper ), this, name
             )
           );
  }

  private boolean afterEnd;

  public void setAfterEnd() {
    afterEnd = true;
  }

  /* STRANGE
   * After finishing main loop, the space is printed before ')'
   */
  public void closeInput() {
    if( afterEnd ) { KtCommand.normLog.add( ' ' ); }
    KtCommand.normLog.add( ')' ).flush();
  }

  /* TeXtp[1275] */
  public KtReadInput openRead( KtFileName name, int num ) {
    Reader in;
    try {
      in = openReader( name, READ_EXT );
    } catch( IOException e ) {
      return KtReadInput.NULL;
    }
    return new KtLineInputReadInput( new KtLineInput( in, mMapper ), this, num );
  }

  public KtReadInput defaultRead( int num ) {
    return new KtStdinReadInput( this, num );
  }

  public KtLog openWrite( KtFileName name, int num ) {
    Writer wr;

    for( ; ; ) {
      try {
        wr = openWriter( name, WRITE_EXT );
        break;
      } catch( IOException e ) {
        cantOpen( name, false );
        name = promptFileName( "output file name" );
      }
    }

    if( jobName != KtName.NULL ) {
      KtCommand.diagLog
        .startLine()
        .addEsc( "openout" )
        .add( num )
        .add( " = `" )
        .add( name )
        .add( "'." )
        .startLine()
        .endLine();
    }
    return new KtStandardLog( new KtWriterLineOutput( wr, mMapper ), mMapper );
  }

  public KtLog makeLog( KtLineOutput out ) {
    return new KtStandardLog( out, mMapper );
  }

  public KtLog makeStringLog() {
    return new KtStringLog( mMapper );
  }

  public KtFileName makeFileName() {
    return new KtTeXCharMapper.KtTeXFileName();
  }

  public void error( String ident, KtLoggable[] params, boolean delAllowed ) {
    error( KtTeXErrorPool.get( ident ), params, true, delAllowed );
  }

  public void fatalError( String ident ) {
    ensureOpenLog();
    KtInteractionPrim.setScroll();
    KtTeXError err = KtTeXErrorPool.get( ident );

    if( KtCommand.isFileLogActive() ) {
      error( err, null, true, false );
    }
    else {
      err.addText( KtCommand.normLog.startLine().add( "! " ), null );
    }

    throw new KtFatalError( ident );
  }

  public void errMessage( KtTokenList message ) {
    error( KtTeXErrorPool.get( message, mConfig.errHelp() ), null, true, true );
  }

  private transient String shownBuilderName = null;

  public void logMode() {
    final var name = KtTypoCommand.getBld().modeName();

    if( !name.equals( shownBuilderName ) ) {
      KtCommand.diagLog.add( KtTypoCommand.getBld() ).add( ": " );
      shownBuilderName = name;
    }
  }

  public void completeShow() {
    final var ident = !KtInteractionPrim.isErrStopping()
      ? "Void"
      : KtCommand.termDiagActive()
      ? "ShortShow"
      : "LongShow";

    error( KtTeXErrorPool.get( ident ), null, false, true );
  }

  public void illegalCommand( KtCommand cmd ) {
    KtTypoCommand.illegalCase( cmd, KtTypoCommand.getBld() );
  }

  public void resetErrorCount() {
    KtResetErrorCountEvent.publish();
  }

  private void error(
    final KtTeXError err,
    final KtLoggable[] params,
    final boolean counting,
    final boolean delAllowed ) {
    KtErrorEvent.publish( err, params, counting, delAllowed );
  }

  private KtFileName getFileName() {
    return new KtTeXCharMapper.KtTeXFileName( getJobName() );
  }

  public void setScale( final double scale ) {
    mTypesetter.setScale( scale );
  }

  public void startDocument() {
    KtTypesettingBeganEvent.publish();
    mTypesetter.startDocument( getFileName() );
  }

  public void startDocument( final OutputStream out ) {
    KtTypesettingBeganEvent.publish();
    mTypesetter.startDocument( out );
  }

  /* TeXtp[642] */
  public void stopDocument() {
    mTypesetter.close();
    KtTypesettingEndedEvent.publish( mTypesetter );
  }

  private ObjectOutputStream dumper = null;

  public ObjectOutputStream getDumper() throws IOException {
    if( dumper == null ) {
      final var name = new KtTeXCharMapper.KtTeXFileName( getJobName() );
      final var out = new BufferedOutputStream( write( name, FMT_EXT ) );
      dumper = new ObjectOutputStream( out );
    }
    return dumper;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * KtLog File
   */

  public void closeLogFile() {
    KtCloseLogEvent.publish();
  }

  private void openLogFile( final KtName jobName ) {
    KtOpenLogEvent.publish( jobName );
  }

  private KtFileName promptFileName( String prompt ) {
    KtCommand.normLog.startLine().add( "Please type another " ).add( prompt );
    if( !KtInteractionPrim.isInteractive() ) { fatalError( "MissingFile" ); }
    KtInputLine line = KtCommand.promptInput( ": " );
    KtFileName name = new KtTeXCharMapper.KtTeXFileName();
    KtCharCode code;
    line.skipSpaces();
    do { code = line.getNextRawCode(); }
    while( code != KtInputLine.EOL && name.accept( code ) > 0 );
    return name;
  }

  private void cantOpen( final KtFileName name, final boolean inp ) {
    KtCommand.normLog.startLine()
                   .add( inp
                           ? "! I can't find file `"
                           : "! I can't write on file `" )
                   .add( name )
                   .add( "'." );

    KtCommand.getTokStack().show( mContextDisplay );
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   * implementation of |LineInputTokenizer.KtInputHandler|
   */

  public KtInputLine emptyLine() {
    return new KtInputLine( mMapper );
  }

  public KtInputLine confirmLine( KtInputLine line ) {
    if( mConfig.confirmingLines() && KtInteractionPrim.isInteractive() ) {
      KtCommand.normLog.endLine().add( line );
      KtInputLine instead = KtCommand.promptInput( "=>" );
      if( instead != KtInputLine.NULL && !instead.empty() ) { return instead; }
    }
    return line;
  }

  public KtTokenizer makeTokenizer( KtInputLine line, String desc,
                                  boolean addEolc ) {
    if( addEolc ) { line = line.addEndOfLineChar(); }
    return new KtInputLineTokenizer( line, mMaker, desc );
  }

  public static Reader makeReader( final InputStream in ) {
    return new InputStreamReader( in, UTF_8 );
  }

  private Reader openReader( final KtFileName name, final KtFileFormat format )
    throws IOException {
    return makeReader( new BufferedInputStream( readFile( name, format ) ) );
  }

  public static Writer makeWriter( final OutputStream out ) {
    return new OutputStreamWriter( out, UTF_8 );
  }

  public static Writer openWriter(
    final KtFileName name, final KtFileFormat format )
    throws IOException {
    return makeWriter( new BufferedOutputStream( write( name, format ) ) );
  }

  public static InputStream openTeXFm( final KtFileName name )
    throws IOException {
    return new BufferedInputStream( readFile( name, TFM_EXT ) );
  }

  static class KtStdinReadInput implements KtReadInput {

    private final KtTeXIOHandler handler;
    private final int num;
    private final String desc = "<read *> ";

    public KtStdinReadInput( KtTeXIOHandler handler, int num ) {
      this.handler = handler;
      this.num = num;
    }

    public KtTokenizer nextTokenizer( KtToken def, int ln ) {
      if( !KtInteractionPrim.isInteractive() ) {
        KtCommand.fatalError( "NoTermRead" );
      }
      KtInputLine line;
      if( num > 0 && ln == 0 ) {
        KtCommand.normLog.endLine().add( def );
        line = KtCommand.promptInput( "=" );
      }
      else { line = KtCommand.promptInput( "" ); }
      return line == KtLineInput.EOF ? KtTokenizer.NULL : handler.makeTokenizer(
        line,
        desc,
        true );
    }

    public KtTokenizer emptyLineTokenizer() {
      return handler.makeTokenizer( handler.emptyLine(), desc, true );
    }

    public void close() { }
  }

}

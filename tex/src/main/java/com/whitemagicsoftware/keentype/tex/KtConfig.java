// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeXIOHandler.Config
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.command.KtTokenList;

import java.util.Calendar;

public interface KtConfig extends KtTeXTokenMaker.KtCategorizer {
  Calendar date();

  KtTokenList errHelp();

  int errContextLines();

  boolean confirmingLines();

  int magnification();
}

// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeXCharMapper
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.io.*;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.io.*;

import java.io.File;
import java.io.StringWriter;

public final class KtTeXCharMapper
  implements KtCharCode.KtMaker, KtInputLine.KtMapper, KtStandardLog.KtEscape {

  public interface KtConfig {
    int catCode( int c );

    int lcNumCode( int c );

    int ucNumCode( int c );

    int spaceFactor( int c );

    int mathCode( int c );

    int delCode( int c );

    int escapeNumCode();

    int newLineNumCode();

    int endLineNumCode();
  }

  private static KtConfig config;

  public static void setConfig( KtConfig conf ) {
    config = conf;
  }

  private static KtCharCode forNum( int n ) {
    return 0 <= n && n <= KtTeXConfig.MAX_TEX_CHAR
      ? new KtChar( (char) n )
      : KtCharCode.NULL;
  }

  private static KtCharCode forNum( KtNum num ) {
    return forNum( num.intVal() );
  }

  private static boolean matchNum( char code, int n ) {
    return n <= KtTeXConfig.MAX_TEX_CHAR && n == code;
  }

  private static boolean matchNum( char code, KtNum num ) {
    return matchNum( code, num.intVal() );
  }

  private static void putExpCodes( char chr, KtCharCode.KtCodeWriter out ) {
    writeCode( '^', out );
    writeCode( '^', out );
    if( chr < '\100' ) { writeCode( (char) (chr + '\100'), out ); }
    else if( chr < '\200' ) { writeCode( (char) (chr - '\100'), out ); }
    else {
      writeCode( lcHexDig( chr >> 4 & 15 ), out );
      writeCode( lcHexDig( chr & 15 ), out );
    }
  }

  private static void putExpChars( char chr, KtCharCode.KtCharWriter out ) {
    out.writeChar( '^' );
    out.writeChar( '^' );
    if( chr < '\100' ) { out.writeChar( (char) (chr + '\100') ); }
    else if( chr < '\200' ) { out.writeChar( (char) (chr - '\100') ); }
    else {
      out.writeChar( lcHexDig( chr >> 4 & 15 ) );
      out.writeChar( lcHexDig( chr & 15 ) );
    }
  }

  private static boolean isPrintable( char chr ) {
    return chr >= ' ' && chr < '\177' || chr > '\240';
  }

  private static void writeCode( char chr, KtCharCode.KtCodeWriter out ) {
    out.writeCode( new KtChar( chr ) );
  }

  private static char lcHexDig( int d ) {
    return (char) (d < 10 ? '0' + d : 'a' + d - 10);
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  public KtTeXCharMapper() {
    KtToken.setCharCodeMaker( this );
  }

  public KtCharCode make( char chr ) {
    return new KtChar( chr );
  }

  public KtCharCode make( int num ) {
    return forNum( num );
  }

  public KtCharCode getEscape() {
    return forNum( config.escapeNumCode() );
  }

  public boolean isNewLine( char chr ) {
    return matchNum( chr, config.newLineNumCode() );
  }

  public void writeExpCodes( char chr, KtCharCode.KtCodeWriter out ) {
    if( isPrintable( chr ) ) { writeCode( chr, out ); }
    else { putExpCodes( chr, out ); }
  }

  public void writeExpChars( char chr, KtCharCode.KtCharWriter out ) {
    if( isPrintable( chr ) ) { out.writeChar( chr ); }
    else { putExpChars( chr, out ); }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  public KtCharCode map( char chr ) {
    return new KtChar( chr );
  }

  public KtCharCode map( int num ) {
    return forNum( num );
  }

  public KtCharCode endLine() {
    return forNum( config.endLineNumCode() );
  }

  /* '\t' is ignored in web2c too */
  public boolean ignoreTrailing( char chr ) {
    return chr == ' ' || chr == '\t';
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  private static final class KtChar implements KtCharCode {

    private final char code;

    public KtChar( char chr ) {
      code = chr;
    }

    public char toChar() {
      return code <= KtTeXConfig.MAX_TEX_CHAR ? code : NO_CHAR;
    }

    public char toCanonicalLetter() {
      int n = config.lcNumCode( code );
      return 0 < n && n <= KtTeXConfig.MAX_TEX_CHAR ? (char) n : NO_CHAR;
    }

    public int numValue() {
      return code <= KtTeXConfig.MAX_TEX_CHAR ? code : -1;
    }

    public boolean match( KtCharCode x ) {
      return x.match( code );
    }

    public boolean match( char c ) {
      return c == code;
    }

    public boolean match( int n ) {
      return matchNum( code, n );
    }

    public boolean match( KtNum num ) {
      return matchNum( code, num );
    }

    public KtCharCode toLowerCase() {
      int n = config.lcNumCode( code );
      return 0 < n && n <= KtTeXConfig.MAX_TEX_CHAR ? new KtChar( (char) n ) :
        this;
    }

    public KtCharCode toUpperCase() {
      int n = config.ucNumCode( code );
      return 0 < n && n <= KtTeXConfig.MAX_TEX_CHAR ? new KtChar( (char) n ) :
        this;
    }

    public int spaceFactor() {
      return config.spaceFactor( code );
    }

    public int mathCode() {
      return config.mathCode( code );
    }

    public int delCode() {
      return config.delCode( code );
    }

    public boolean isEscape() {
      return config.catCode( code ) == KtTeXConfig.CAT_ESCAPE;
    }

    public boolean startsExpand() {
      return config.catCode( code ) == KtTeXConfig.CAT_SUP_MARK;
    }

    public boolean isLetter() {
      return config.catCode( code ) == KtTeXConfig.CAT_LETTER;
    }

    public boolean isEndLine() {
      return match( config.endLineNumCode() );
    }

    public boolean isNewLine() {
      return match( config.newLineNumCode() );
    }

    public boolean startsFileExt() {
      return code == '.';
    }

    public void writeExpCodes( KtCharCode.KtCodeWriter out ) {
      if( isPrintable( code ) ) { out.writeCode( this ); }
      else { putExpCodes( code, out ); }
    }

    public void writeExpChars( KtCharCode.KtCharWriter out ) {
      if( isPrintable( code ) ) { out.writeChar( code ); }
      else { putExpChars( code, out ); }
    }

    public void writeRawChars( KtCharCode.KtCharWriter out ) {
      out.writeChar( code );
    }

    public void addOn( KtLog log ) {
      log.add( this );
    }

    /**
     * Gives a hash code for an internal character code.
     *
     * @return the hash code for this object.
     */
    public int hashCode() {
      return code;
    }

    /**
     * Compares this internal character code to the specified object. The
     * result is |true| if and
     * only if the the argument is not |null| and is the |KtCharCode| object
     * representing the same
     * internal character code.
     *
     * @param o the object to compare this internal character code against.
     * @return |true| if the argument is equal, |false| otherwise.
     */
    public boolean equals( Object o ) {
      return o instanceof KtCharCode && ((KtCharCode) o).match( code );
    }

    public String toString() {
      return String.valueOf( code );
    }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  public static class KtTeXFileName implements KtFileName {

    private final KtName.KtBuffer data = new KtName.KtBuffer();

    public KtTeXFileName() { }

    public KtTeXFileName( final String name ) {
      setPath( name );
    }

    public KtTeXFileName( KtName name ) {
      data.append( name );
    }

    public int accept( KtCharCode code ) {
      if( code.toChar() == KtCharCode.NO_CHAR ) { return -1; }
      if( code.match( ' ' ) ) { return 0; }
      data.append( code );
      return 1;
    }

    public String getPath() {
      StringWriter out = new StringWriter();
      for( int i = 0; i < data.length(); i++ ) {
        KtCharCode code = data.codeAt( i );
        if( code instanceof KtChar ) { out.write( ((KtChar) code).code ); }
        // XXX else code.writeExpanded(out);
      }
      return out.toString();
    }

    public void setPath( String path ) {
      data.clear();
      append( path );
    }

    public KtName baseName() {
      int beg = indexOfName();
      int end = indexOfExt( beg );
      KtCharCode[] codes = new KtCharCode[ end - beg ];
      data.getCodes( beg, end, codes, 0 );
      return new KtName( codes );
    }

    @Override
    public void addDefaultExt( final String ext ) {
      final int i = indexOfExt( indexOfName() );
      if( i < data.length() ) { return; }
      data.append( new KtChar( '.' ) );
      append( ext );
    }

    public void append( char chr ) {
      data.append( new KtChar( chr ) );
    }

    public void append( String str ) {
      int len = str.length();
      for( int i = 0; i < len; i++ ) { append( str.charAt( i ) ); }
    }

    private int indexOfName() {
      final var path = getPath();
      final var name = new File( path ).getName();
      final var i = path.lastIndexOf( name );
      return Math.max( i, 0 );
    }

    private int indexOfExt( int start ) {
      int i = data.length();
      while( --i > start ) {
        if( data.codeAt( i ).startsFileExt() ) { return i; }
      }
      return data.length();
    }

    public KtFileName copy() {
      return new KtTeXFileName( data.toName() );
    }

    public void addOn( KtLog log ) {
      log.add( data.toName() );
    }

    public int hashCode() {
      return data.hashCode();
    }

    public boolean equals( Object o ) {
      return o instanceof KtTeXFileName && data.equals( ((KtTeXFileName) o).data );
    }

    public String toString() {
      return getPath();
    }
  }
}

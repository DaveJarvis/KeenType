// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeX.StdinTokenizer
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtInputLineTokenizer;
import com.whitemagicsoftware.keentype.command.KtInteractionPrim;
import com.whitemagicsoftware.keentype.command.KtSequenceTokenizer;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.io.KtInputLine;
import com.whitemagicsoftware.keentype.io.KtLineInput;

public final class KtStdinTokenizer extends KtSequenceTokenizer {

  private final KtInputLineTokenizer.KtTokenMaker maker;
  private KtInputLine line;

  public KtStdinTokenizer(
    final KtInputLineTokenizer.KtTokenMaker maker,
    final KtInputLine first ) {
    super(
      first == KtInputLine.NULL
        ? KtTokenizer.NULL
        : new KtInputLineTokenizer( first, maker, "<*> " )
    );
    this.maker = maker;
    line = first;
  }

  @Override
  public KtTokenizer nextTokenizer() {
    KtCommand.ensureOpenLog();
    if( !KtInteractionPrim.isInteractive() ) {
      KtCommand.fatalError( "NoEnd" );
    }

    if( line != KtInputLine.NULL && line.wasEmpty( true ) ) {
      KtCommand.normLog.startLine()
                     .add( "(Please type a command or say `\\end')" );
    }

    KtCommand.normLog.endLine();
    line = KtCommand.promptInput( "*" );

    if( line != KtLineInput.EOF ) {
      line = line.addEndOfLineChar();
      return new KtInputLineTokenizer( line, maker, "<*> " );
    }

    return KtTokenizer.NULL;
  }
}

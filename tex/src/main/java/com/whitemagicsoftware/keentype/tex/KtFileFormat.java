/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.tex;

import java.net.URL;

/**
 * Represents filename extensions for various file formats.
 */
public enum KtFileFormat {
  LOG_EXT( "log" ),
  INPUT_EXT( "tex" ),
  READ_EXT( "tex" ),
  WRITE_EXT( "tex" ),
  // Font metrics
  TFM_EXT( "tfm" ),
  // Open Type Font
  OTF_EXT( "otf" ),
  // True Type Font
  TTF_EXT( "ttf" ),
  // Printer Font Binary
  PFB_EXT( "pfb" ),
  // DeVice Independent
  DVI_EXT( "dvi" ),
  // Scalable Vector Graphics
  SVG_EXT( "svg" ),
  FMT_EXT( "nfmt" );

  private final String mExt;

  KtFileFormat( final String ext ) {
    mExt = ext;
  }

  /**
   * Answers whether the given path has a filename extension that matches this
   * {@link KtFileFormat} instance.
   *
   * @param url The path to a file name, with optional leading directories.
   * @return {@code true} if the given path is of the type represented by this
   * {@link KtFileFormat}.
   */
  public boolean matches( final URL url ) {
    assert url != null;

    final var path = url.getPath();

    return path.toLowerCase().endsWith( '.' + mExt );
  }

  /**
   * Returns the filename extension for this format.
   *
   * @return A short identifier suitable for appending to file names;
   * a separator period must be added separately.
   */
  @Override
  public String toString() {
    return mExt;
  }
}

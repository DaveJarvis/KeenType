/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.tex;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Implementations are responsible for handling an {@link InputStream} that
 * contains information corresponding to a particular {@link KtFileFormat}.
 */
@FunctionalInterface
public interface KtFileHandler {

  /**
   * Used by {@link KtFileOpener} to process data of a particular format.
   *
   * @param stream The {@link InputStream} to process.
   * @throws IOException Could not process the corresponding file contents.
   */
  void handle( final InputStream stream )
    throws IOException, FontFormatException;
}

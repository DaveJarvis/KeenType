// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
package com.whitemagicsoftware.keentype.tex;

import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtContextDisplay;
import com.whitemagicsoftware.keentype.io.KtBufferLineOutput;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.io.KtStandardLog;

public class KtTeXContextDisplay implements KtContextDisplay {

  private final KtConfig config;
  private final KtBufferLineOutput leftOut;
  private final KtBufferLineOutput rightOut;
  private final KtLog leftLog;
  private final KtLog rightLog;
  private int origin = 0;

  public KtTeXContextDisplay(
    KtConfig config, KtCharCode.KtMaker maker, KtStandardLog.KtEscape esc ) {
    this.config = config;
    leftOut = new KtBufferLineOutput(
      KtTeXConfig.HALF_ERROR_LINE, 0, maker );
    rightOut = new KtBufferLineOutput(
      KtTeXConfig.ERROR_LINE, KtTeXConfig.ERROR_LINE, maker );
    leftLog = new KtStandardLog( leftOut, esc );
    rightLog = new KtStandardLog( rightOut, esc );
    if( normal() != KtLog.NULL ) { origin = normal().getCount(); }
  }

  public void reset() {
    leftOut.reset();
    rightOut.reset();
    origin = normal().getCount();
  }

  public int lines() {
    return config.errContextLines();
  }

  public KtLog normal() {
    return KtCommand.normLog;
  }

  public KtLog left() {
    return leftLog;
  }

  public KtLog right() {
    return rightLog;
  }

  public void show() {
    final int intro = normal().getCount() - origin;
    int half = intro + leftOut.size();

    if( half <= KtTeXConfig.HALF_ERROR_LINE ) {
      leftOut.addOn( normal() );
    }
    else {
      normal().add( "..." );
      half = KtTeXConfig.HALF_ERROR_LINE;
      final int n = half - intro - 3;
      if( n > 0 ) {
        leftOut.addOn( normal(), leftOut.size() - n, leftOut.size() );
      }
    }
    normal().endLine().add( ' ', half );
    if( half + rightOut.size() <= KtTeXConfig.ERROR_LINE ) {
      rightOut.addOn( normal() );
    }
    else {
      final int n = KtTeXConfig.ERROR_LINE - half - 3;
      if( n > 0 ) { rightOut.addOn( normal(), 0, n ); }
      normal().add( "..." );
    }
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.VAlignPrim
// $Id: KtVAlignPrim.java,v 1.1.1.1 2001/03/21 08:18:23 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtHBoxBuilder;
import com.whitemagicsoftware.keentype.builder.KtListBuilder;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;

public class KtVAlignPrim extends KtAlignPrim {

  public KtVAlignPrim(String name, KtTokenList.KtInserter everyCr, KtPrimitive carRet, KtCommand endv) {
    super(name, everyCr, carRet, endv);
  }

  protected KtListBuilder makeOuterBuilder(int lineNo) {
    KtListBuilder builder = new KtHBoxBuilder(lineNo);
    builder.setSpaceFactor(KtBuilder.top().nearestValidSpaceFactor());
    return builder;
  }

  protected KtAlignment makeAlignment(
      KtDimen size,
      boolean exactly,
      KtTokenList.KtInserter everyCr,
      KtToken frzCr,
      KtToken frzEndt,
      KtListBuilder builder) {
    return new KtVertAlignment(size, exactly, everyCr, frzCr, frzEndt, builder);
  }
}

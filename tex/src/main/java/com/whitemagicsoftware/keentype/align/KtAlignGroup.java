// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.AlignGroup
// $Id: KtAlignGroup.java,v 1.1.1.1 2001/03/12 03:53:03 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;

public class KtAlignGroup extends KtSimpleGroup {

  protected final KtAlignment align;

  public KtAlignGroup(KtAlignment align) {
    this.align = align;
  }

  /* TeXtp[812] */
  public void stop() {
    KtNodeEnum nodes = align.finish(KtDimen.ZERO);
    KtBuilder bld = KtBuilder.top();
    bld.addNodes(nodes);
    align.copyPrevParameters(bld);
    bld.buildPage();
  }
}

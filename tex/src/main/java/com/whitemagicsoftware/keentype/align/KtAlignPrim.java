// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.AlignPrim
// $Id: KtAlignPrim.java,v 1.1.1.1 2001/03/21 08:24:12 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtListBuilder;
import com.whitemagicsoftware.keentype.command.KtClosing;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtFrozenToken;
import com.whitemagicsoftware.keentype.command.KtGroup;
import com.whitemagicsoftware.keentype.command.KtPrimitive;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.math.KtDisplayBuilder;
import com.whitemagicsoftware.keentype.typo.KtAction;
import com.whitemagicsoftware.keentype.typo.KtBuilderPrim;

public abstract class KtAlignPrim extends KtBuilderPrim {

  private final KtTokenList.KtInserter everyCr;
  private final KtToken frozenCr;
  private final KtToken frozenEndTemplate;

  public KtAlignPrim(String name, KtTokenList.KtInserter everyCr, KtPrimitive carRet, KtCommand endv) {
    super(name);
    this.everyCr = everyCr;
    frozenCr = new KtFrozenToken(carRet);
    frozenEndTemplate =
        new KtFrozenToken("endtemplate", new KtEndTemplate(new KtFrozenToken("endtemplate", endv)));
  }

  /* STRANGE
   * Why is the outer builder pushed before scanning the alignment spec?
   * It is inconsistent with \hbox, etc. and makes the code more complicated.
   */
  public final KtAction NORMAL =
      new KtAction() {
        public void exec(KtBuilder bld, KtToken src) {
          // XXX[774] first align_group (scan_spec(align_group, false);)
          // XXX[774] pushLevel(new KtSimpleGroup()); ???
          KtListBuilder builder = makeOuterBuilder(currLineNumber());
          KtBuilder.push(builder);
          KtAlignment align = scanAlignSpec(builder);
          pushLevel(new KtAlignGroup(align));
          align.start(src);
        }
      };

  protected class KtDisplayedClosing extends KtClosing {

    /* TeXtp[774,776] */
    public void exec(KtGroup grp, KtToken src) {
      if (!(getBld() instanceof KtDisplayBuilder))
        throw new RuntimeException("non-display builder for displayed halign");
      KtBuilder bld = getBld();
      if (!bld.isEmpty()) {
        error("ImproperAlignInFormula", KtAlignPrim.this);
        KtBuilder.pop();
        KtBuilder.push(new KtDisplayBuilder(bld.getStartLine()));
      }
      KtListBuilder builder = makeOuterBuilder(currLineNumber());
      KtBuilder.push(builder);
      KtAlignment align = scanAlignSpec(builder);
      pushLevel(new KtDispAlignGroup(align));
      align.start(src);
    }
  }

  public KtClosing makeDisplayedClosing() {
    return new KtDisplayedClosing();
  }

  /* STRANGE
   * Why is the starting line number (for outer builder) before
   * size specification and left brace?
   */
  /* TeXtp[774] */
  protected KtAlignment scanAlignSpec(KtListBuilder builder) {
    KtDimen size = KtDimen.ZERO;
    boolean exactly = false;
    if (scanKeyword("to")) {
      size = scanDimen();
      exactly = true;
    } else if (scanKeyword("spread")) size = scanDimen();
    scanLeftBrace();
    return makeAlignment(size, exactly, everyCr, frozenCr, frozenEndTemplate, builder);
  }

  protected abstract KtListBuilder makeOuterBuilder(int lineNo);

  protected abstract KtAlignment makeAlignment(
      KtDimen size,
      boolean exactly,
      KtTokenList.KtInserter everyCr,
      KtToken frzCr,
      KtToken frzEndt,
      KtListBuilder builder);
}

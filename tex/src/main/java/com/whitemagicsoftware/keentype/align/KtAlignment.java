// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.Alignment
// $Id: KtAlignment.java,v 1.1.1.1 2001/04/20 05:18:15 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import java.io.ObjectStreamException;
import java.io.Serializable;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtBraceNesting;
import com.whitemagicsoftware.keentype.command.KtClosing;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtCommandBase;
import com.whitemagicsoftware.keentype.command.KtGroup;
import com.whitemagicsoftware.keentype.command.KtInpTokChecker;
import com.whitemagicsoftware.keentype.command.KtInsertedTokenList;
import com.whitemagicsoftware.keentype.command.KtSimpleGroup;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.node.KtAnyBoxNode;
import com.whitemagicsoftware.keentype.node.KtAnySkipNode;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtHBoxNode;
import com.whitemagicsoftware.keentype.node.KtNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtRuleNode;
import com.whitemagicsoftware.keentype.node.KtSizesEvaluator;
import com.whitemagicsoftware.keentype.node.KtVShiftNode;
import com.whitemagicsoftware.keentype.typo.KtParagraph;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

public abstract class KtAlignment extends KtCommandBase {

  public static final KtAlignment NULL = null;

  protected final KtDimen desired;
  protected final boolean exactly;
  protected final KtTokenList.KtInserter everyCr;
  protected final KtToken frozenCr;
  protected final KtToken frozenEndTemplate;

  public KtAlignment(
      KtDimen desired, boolean exactly, KtTokenList.KtInserter everyCr, KtToken frzCr, KtToken frzEndt) {
    this.desired = desired;
    this.exactly = exactly;
    this.everyCr = everyCr;
    this.frozenCr = frzCr;
    this.frozenEndTemplate = frzEndt;
  }

  protected KtPreamble preamble;
  protected KtAlignSizesMatrix maxSizes;
  protected int currColumn;
  protected int currSpan;

  /* TeXtp[774] */
  public void start(KtToken src) {
    push();
    preamble = scanPreamble(src);
    maxSizes = new KtAlignSizesMatrix(preamble.length());
    everyCr.insertToks();
    nextRow();
  }

  public static final int GLUEP_TAB_SKIP = newGlueParam();

  /* TeXtp[777] */
  protected KtPreamble scanPreamble(KtToken src) {
    KtCommand.KtConfig cfg = getConfig();
    KtPreambleScanning scan = new KtPreambleScanning(src, frozenCr);
    KtInpTokChecker savedChk = setTokenChecker(scan);
    KtBraceNesting savedBrn = setBraceNesting(scan);
    boolean afterAssign = cfg.enableAfterAssignment(false);
    KtGlue firstSkip = cfg.getGlueParam(GLUEP_TAB_SKIP);
    String skipName = cfg.getGlueName(GLUEP_TAB_SKIP);
    while (scanRecord(scan))
      ;
    setBraceNesting(savedBrn);
    setTokenChecker(savedChk);
    cfg.enableAfterAssignment(afterAssign);
    return scan.toPreamble(firstSkip, skipName, frozenEndTemplate);
  }

  /* TeXtp[779,783,784] */
  protected boolean scanRecord(KtPreambleScanning scan) {
    KtToken tok;
    KtCommand cmd;
    for (boolean fresh = true; ; ) {
      tok = getPreambleToken();
      cmd = meaningOf(tok);
      if (cmd.isMacroParam()) break;
      else if (scan.balanced() && (cmd.isTabMark() || cmd.isCarRet())) {
        if (!(fresh && cmd.isTabMark() && scan.setLoopIndex())) {
          backToken(tok);
          error("MissingSharp");
          break;
        }
      } else if (!(fresh && cmd.isSpacer())) {
        scan.append(tok);
        fresh = false;
      }
    }
    scan.finishHalf();
    for (; ; ) {
      tok = getPreambleToken();
      cmd = meaningOf(tok);
      if (scan.balanced() && (cmd.isTabMark() || cmd.isCarRet())) break;
      else if (cmd.isMacroParam()) error("SecondSharpInTab");
      else scan.append(tok);
    }
    scan.append(frozenEndTemplate);
    scan.finishRecord(getConfig().getGlueParam(GLUEP_TAB_SKIP));
    return !cmd.isCarRet();
  }

  /* TeXtp[782] */
  protected KtToken getPreambleToken() {
    for (; ; ) {
      KtToken tok = nextRawToken();
      while (meaningOf(tok).isSpan()) tok = nextExpToken();
      // XXX[782] curr_cmd = endv
      KtCommand cmd = meaningOf(tok);
      if (cmd.isTabSkip()) cmd.exec(tok);
      else return tok;
    }
  }

  /* TeXtp[785,786] */
  protected void nextRow() {
    for (; ; ) {
      KtToken tok = nextExpNonSpacer();
      KtCommand cmd = meaningOf(tok);
      if (cmd.isCrCr()) continue;
      else if (cmd.isNoAlign()) startNonAligned();
      else if (cmd.isRightBrace()) stop();
      else {
        pushNewRowBuilder();
        addNamedSkipToRow(preamble.firstSkip, preamble.skipName);
        startSpan(0);
        currColumn = 0;
        startColumn(tok, cmd);
      }
      break;
    }
  }

  protected void startNonAligned() {
    scanLeftBrace();
    pushLevel(new KtNoAlignGroup());
  }

  /* TeXtp[787] */
  protected void startSpan(int index) {
    // System.err.println("=== startSpan(" + index + ')');
    pushLevel(new KtSpanGroup());
    pushNewSpanBuilder();
    currSpan = index;
  }

  /* TeXtp[788] */
  protected void startColumn(KtToken tok, KtCommand cmd) {
    // System.err.println("=== startColumn(" + tok + ") [" + currColumn + ')');
    if (cmd.isOmit()) startColumnBody(false);
    else {
      backToken(tok);
      getTokStack().push(new KtTemplateTokenizer(preamble.getUPart(currColumn)));
    }
  }

  public static final class KtColumnEnding implements Serializable {

    private static int nextOrdinal = 0;
    private final int ordinal = nextOrdinal++;
    private final String name;
    private static final KtColumnEnding[] VALS = new KtColumnEnding[3];

    private KtColumnEnding(String name) {
      this.name = name;
      VALS[ordinal] = this;
    }

    public String toString() {
      return name;
    }

    private Object readResolve() throws ObjectStreamException {
      return VALS[ordinal];
    }
  }

  public static final KtColumnEnding SPAN_ENDING = new KtColumnEnding("SPAN_ENDING"),
      TAB_ENDING = new KtColumnEnding("TAB_ENDING"),
      CR_ENDING = new KtColumnEnding("CR_ENDING"),
      NULL_ENDING = null;

  protected KtColumnBraceNesting columnBalance = NULL_COLUMN_NESTING;
  protected KtBraceNesting savedColumnNesting;
  protected boolean insertTemplate;
  protected KtColumnEnding columnEnding = NULL_ENDING;

  protected boolean balancedColumn() {
    return columnBalance != NULL_COLUMN_NESTING && columnBalance.balanced();
  }

  public static final int NOALIGN_DISBALANCE = 1000000;

  protected int columnDisbalance() {
    return columnBalance != NULL_COLUMN_NESTING ? columnBalance.disbalance() : NOALIGN_DISBALANCE;
  }

  /* TeXtp[788] */
  protected void startColumnBody(boolean insert) {
    // System.err.println("=== startColumnBody()");
    columnBalance = new KtColumnBraceNesting();
    savedColumnNesting = setBraceNesting(columnBalance);
    insertTemplate = insert;
  }

  /* TeXtp[789] */
  protected void finishColumnBody(KtColumnEnding ending) {
    // System.err.println("=== finishColumnBody()");
    if (columnBalance == NULL_COLUMN_NESTING) throw new RuntimeException("no column scanned");
    // XXX[789] if (scanner_status=aligning)
    setBraceNesting(savedColumnNesting);
    savedColumnNesting = KtBraceNesting.NULL;
    columnBalance = NULL_COLUMN_NESTING;
    if (insertTemplate) pushList(preamble.getVPart(currColumn), "template");
    else pushToken(preamble.endTemplate, "template");
    columnEnding = ending;
  }

  /* TeXtp[791,799] */
  protected void finishColumn() {
    // System.err.println("=== finishColumn() [" + currColumn + ')');
    // XXX[791] if (align_state < 500000)
    if (columnEnding == NULL_ENDING)
      throw new RuntimeException("column body not started and finished");
    if (columnEnding != CR_ENDING && !preamble.hasRecord(currColumn + 1)) {
      error("ExtraAlignTab", frozenCr);
      columnEnding = CR_ENDING;
    }
    if (columnEnding != SPAN_ENDING) {
      popLevel();
      // System.err.println("=== finishSpan(" + currSpan + ')');
      maxSizes.setMax(currSpan, currColumn, packedSpanSize(currColumn - currSpan));
      addNamedSkipToRow(preamble.getSkip(currColumn), preamble.skipName);
      if (columnEnding == CR_ENDING) {
        packRow();
        everyCr.insertToks();
        nextRow();
        return;
      }
      startSpan(currColumn + 1);
    }
    KtToken tok = nextExpNonSpacer();
    KtCommand cmd = meaningOf(tok);
    ++currColumn;
    startColumn(tok, cmd);
    columnEnding = NULL_ENDING;
  }

  protected void stop() {
    KtBuilder.pop();
    popLevel();
  }

  /* TeXtp[800] */
  public KtNodeEnum finish(KtDimen indent) {
    pop();
    calculateWidths();
    return setAndTransform(indent).nodes();
  }

  /* TeXtp[801,803] */
  protected void calculateWidths() {
    for (int k = 0, i = 1; i < maxSizes.size(); k = i++) {
      KtDimen tw = maxSizes.get(k);
      tw = tw == KtDimen.NULL ? KtDimen.ZERO : tw.plus( preamble.getSkip( k).getDimen());
      for (int j = i; j < maxSizes.size(); j++) {
        KtDimen w = maxSizes.get(k, j);
        if (w != KtDimen.NULL) maxSizes.setMax(i, j, w.minus(tw));
      }
    }
  }

  /* TeXtp[804] */
  protected KtNodeList setAndTransform(KtDimen indent) {
    KtSizesEvaluator pack = new KtSizesEvaluator();
    pack.add(preamble.firstSkip);
    for (int i = 0; i < maxSizes.size(); i++) {
      KtDimen w = maxSizes.get(i);
      if (w != KtDimen.NULL) {
        pack.add(w);
        pack.add(preamble.getSkip(i));
      }
    }
    KtDimen size = pack.getBody();
    if (exactly) {
      pack.evaluate(desired.minus(size), false);
      size = desired;
    } else {
      pack.evaluate(desired, false);
      size = size.plus(desired);
    }
    check(pack, size);
    return transform(size, pack.getSetting(), indent);
  }

  /* TeXtp[805,806] */
  protected KtNodeList transform(KtDimen size, KtGlueSetting setting, KtDimen indent) {
    KtBoxSizes around = transformSizes(KtBoxSizes.ZERO, size);
    KtNodeList list = new KtNodeList();
    KtNodeEnum nodes = getUnsetNodes();
    while (nodes.hasMoreNodes()) {
      KtNode node = nodes.nextNode();
      if (node instanceof KtAnyUnsetNode)
        node = transform((KtAnyUnsetNode) node, size, setting, indent);
      else if (node instanceof KtRuleNode) {
        // XXX[806] not general, fix it!!!
        KtBoxSizes sizes = ((KtRuleNode) node).getSizes();
        KtBoxSizes full = sizes.replenished(around);
        if (!sizes.equals(full)) node = new KtRuleNode(full);
        if (!indent.isZero()) {
          node = KtHBoxNode.packedOf(node);
          node = KtVShiftNode.shiftingRight(node, indent);
        }
      }
      list.append(node);
    }
    return list;
  }

  /* TeXtp[807] */
  protected KtNode transform(KtAnyUnsetNode row, KtDimen size, KtGlueSetting setting, KtDimen indent) {
    KtNodeList list = new KtNodeList();
    KtNodeEnum nodes = row.getList().nodes();
    list.append(nodes.nextNode());
    for (int i = 0; nodes.hasMoreNodes(); i++) {
      i = transform((KtAnyUnsetNode) nodes.nextNode(), list, i, row.getSizes(), setting);
      list.append(nodes.nextNode());
    }
    return KtVShiftNode.shiftingRight(
        makeBox(transformSizes(row.getSizes(), size), setting, list), indent);
  }

  /* TeXtp[808,809] */
  protected int transform(
      KtAnyUnsetNode col, KtNodeList list, int i, KtBoxSizes rowSizes, KtGlueSetting setting) {
    int n = i + col.getSpanCount();
    KtDimen size = maxSizes.get(i);
    boolean empty = size == KtDimen.NULL;
    if (empty) size = KtDimen.ZERO;
    KtNodeList filler = new KtNodeList();
    KtDimen total = size;
    if (i < n) {
      KtGlue tab = empty ? KtGlue.ZERO : preamble.getSkip( i);
      for (int j = i; ; ) {
        total = total.plus(setting.set(tab, false));
        filler.append(makeSkip(tab, preamble.skipName));
        KtDimen w = maxSizes.get(++j);
        if (w != KtDimen.NULL) {
          tab = preamble.getSkip(j);
          total = total.plus(w);
        } else {
          w = KtDimen.ZERO;
          tab = KtGlue.ZERO;
        }
        filler.append(
            makeBox(transformSizes(KtBoxSizes.ZERO, w), KtGlueSetting.NATURAL, KtNodeList.EMPTY));
        if (j == n) break;
      }
    }
    list.append(
            makeBox( // XXX beware of depth and leftX
                transformSizes(rowSizes, size),
                col.getSetting(total.minus(getRelevantSize(col.getSizes()))),
                col.getList()))
        .append(filler);
    return n;
  }

  /* STRANGE
   * Isn't this Named?SkipNode vs. ?SkipNode crazy?
   * Although the whole complex construction of the list is completely
   * artificial for TeX compatibility only.
   */
  /* TeXtp[804] */
  protected void check(KtSizesEvaluator pack, KtDimen size) {
    KtTypoCommand.KtAnyBoxPacker packer = makeBoxPacker();
    if (packer.check(pack)) {
      KtNodeList list = new KtNodeList();
      list.append(makeSkip(preamble.firstSkip, preamble.skipName));
      int i = 0;
      for (; i < maxSizes.size(); i++) {
        KtDimen w = maxSizes.get(i);
        if (w != KtDimen.NULL)
          list.append(new KtAnyUnsetNode(transformSizes(KtBoxSizes.ZERO, w), KtNodeList.EMPTY))
              .append(
                  i < preamble.length()
                      ? makeSkip(preamble.getSkip(i), preamble.skipName)
                      : makeSkip(preamble.getSkip(i)));
        else list.append(new KtAnyUnsetNode()).append(makeSkip(KtGlue.ZERO, preamble.skipName));
      }
      for (; i < preamble.length(); i++)
        list.append(new KtAnyUnsetNode()).append(makeSkip(KtGlue.ZERO, preamble.skipName));
      packer.reportBox(makeBox(transformSizes(KtBoxSizes.ZERO, size), pack.getSetting(), list));
    }
  }

  protected abstract void pushNewRowBuilder();

  protected abstract void pushNewSpanBuilder();

  protected abstract void addNamedSkipToRow(KtGlue skip, String name);

  protected abstract KtDimen packedSpanSize(int spanCount);

  protected abstract void packRow();

  protected abstract KtNodeEnum getUnsetNodes();

  protected abstract KtDimen getRelevantSize(KtBoxSizes sizes);

  protected abstract KtBoxSizes transformSizes(KtBoxSizes sizes, KtDimen dim);

  protected abstract KtAnyBoxNode makeBox(KtBoxSizes sizes, KtGlueSetting setting, KtNodeList list);

  protected abstract KtAnySkipNode makeSkip(KtGlue skip);

  protected abstract KtAnySkipNode makeSkip(KtGlue skip, String name);

  protected abstract KtTypoCommand.KtAnyBoxPacker makeBoxPacker();

  public abstract void copyPrevParameters(KtBuilder bld);

  protected static final KtColumnBraceNesting NULL_COLUMN_NESTING = null;

  protected static class KtColumnBraceNesting implements KtBraceNesting {
    private int braceNesting = 0;

    public void adjust(int count) {
      braceNesting += count;
    }

    public int disbalance() {
      return braceNesting;
    }

    public boolean balanced() {
      return braceNesting == 0;
    }
  }

  protected class KtTemplateTokenizer extends KtInsertedTokenList {

    public KtTemplateTokenizer(KtTokenList list) {
      super(list, "<template> ");
    }

    /* TeXtp[324] */
    public boolean close() {
      // XXX[324] if (align_state <= 500000)
      startColumnBody(true);
      return false;
    }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  private static KtAlignment top = NULL;
  private KtAlignment next = NULL;

  private void push() {
    next = top;
    top = this;
  }

  private static void pop() {
    top = top.next;
  }

  private static void checkTop() {
    if (top == KtAlignment.NULL) throw new RuntimeException("no alignment active");
  }

  public static boolean columnBodyIsActiveAndBalanced() {
    return top != KtAlignment.NULL && top.balancedColumn();
  }

  public static void finishActiveColumnBody(KtColumnEnding ending) {
    checkTop();
    top.finishColumnBody(ending);
  }

  public static int activeColumnDisbalance() {
    return top != KtAlignment.NULL ? top.columnDisbalance() : NOALIGN_DISBALANCE;
  }

  public static class KtSpanGroup extends KtSimpleGroup {}

  public static /*final*/ KtClosing CLOSE_COLUMN =
      new KtClosing() {
        /* TeXtp[1131] */
        public void exec(KtGroup grp, KtToken src) {
          KtParagraph.finish();
          checkTop();
          top.finishColumn();
        }
      };

  public static /*final*/ KtClosing MISSING_CR =
      new KtClosing() {
        /* TeXtp[1132] */
        public void exec(KtGroup grp, KtToken src) {
          backToken(src);
          checkTop();
          insertToken(top.frozenCr);
          error("MissingCr", top.frozenCr);
        }
      };

  public static class KtNoAlignGroup extends KtSimpleGroup {
    public void stop() {
      KtParagraph.finish();
    }

    public void close() {
      checkTop();
      top.nextRow();
    }
  }
}

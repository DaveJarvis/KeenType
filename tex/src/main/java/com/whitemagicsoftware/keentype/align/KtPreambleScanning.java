// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.PreambleScanning
// $Id: KtPreambleScanning.java,v 1.1.1.1 2001/03/20 03:02:17 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import java.util.Vector;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.command.KtBaseToksChecker;
import com.whitemagicsoftware.keentype.command.KtBraceNesting;
import com.whitemagicsoftware.keentype.command.KtCommand;
import com.whitemagicsoftware.keentype.command.KtRightBraceToken;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;

public class KtPreambleScanning extends KtBaseToksChecker implements KtBraceNesting {

  private final Vector<KtPreamble.Entry> entries = new Vector<>();
  private KtTokenList.KtBuffer buf = new KtTokenList.KtBuffer();
  private KtTokenList uPart = KtTokenList.NULL;
  private int loopIndex = -1;
  private int braceNesting = 0;
  private final KtTokenList crAndBrace;

  public KtPreambleScanning(KtToken source, KtToken frozenCr) {
    super("OuterInPreamble", "EOFinPreamble", source);
    KtToken[] toks = {frozenCr, KtRightBraceToken.TOKEN};
    crAndBrace = new KtTokenList(toks);
  }

  public boolean balanced() {
    return braceNesting == 0;
  }

  public void append(KtToken tok) {
    buf.append(tok);
  }

  public void finishHalf() {
    uPart = buf.toTokenList();
    buf = new KtTokenList.KtBuffer();
  }

  public void finishRecord(KtGlue skip) {
    entries.add(new KtPreamble.Entry(uPart, buf.toTokenList(), skip));
    uPart = KtTokenList.NULL;
    buf = new KtTokenList.KtBuffer();
  }

  public boolean setLoopIndex() {
    if (loopIndex >= 0) return false;
    loopIndex = entries.size();
    return true;
  }

  public KtPreamble toPreamble(KtGlue firstSkip, String skipName, KtToken endTemplate) {
    KtPreamble.Entry[] records = new KtPreamble.Entry[entries.size()];
    entries.copyInto(records);
    return new KtPreamble(firstSkip, skipName, endTemplate, records, loopIndex);
  }

  public void adjust(int count) {
    braceNesting += count;
  }

  /* TeXtp[339] */
  // XXX align_state [339]
  protected void tryToFix() {
    KtCommand.insertList(crAndBrace);
  }

  protected void reportRunAway() {
    KtCommand.runAway("preamble", buf);
  }
}

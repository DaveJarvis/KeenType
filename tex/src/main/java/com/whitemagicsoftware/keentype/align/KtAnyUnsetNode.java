// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.AnyUnsetNode
// $Id: KtAnyUnsetNode.java,v 1.1.1.1 2001/03/22 05:38:30 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.io.KtCntxLog;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtAnyBoxedNode;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtSizesEvaluator;

public class KtAnyUnsetNode extends KtAnyBoxedNode {
  /* corresponding to unset_node */

  protected final KtNodeList list;
  protected final int spanCount;
  protected final KtDimen stretch;
  protected final byte strOrder;
  protected final KtDimen shrink;
  protected final byte shrOrder;

  public KtAnyUnsetNode(
      KtBoxSizes sizes,
      KtNodeList list,
      int spanCount,
      KtDimen stretch,
      byte strOrder,
      KtDimen shrink,
      byte shrOrder) {
    super(sizes);
    this.list = list;
    this.spanCount = spanCount;
    this.stretch = stretch;
    this.strOrder = strOrder;
    this.shrink = shrink;
    this.shrOrder = shrOrder;
  }

  public KtAnyUnsetNode(KtBoxSizes sizes, KtNodeList list) {
    this(sizes, list, 0, KtDimen.ZERO, KtGlue.NORMAL, KtDimen.ZERO, KtGlue.NORMAL);
  }

  public KtAnyUnsetNode() {
    this(KtBoxSizes.ZERO, KtNodeList.EMPTY);
  }

  public KtNodeList getList() {
    return list;
  }

  public int getSpanCount() {
    return spanCount;
  }

  /* TeXtp[184,185] */
  public void addOn(KtLog log, KtCntxLog cntx) {
    log.addEsc(getDesc()).add(sizes);
    StringBuffer buf = new StringBuffer(80);
    if (spanCount != 0) buf.append(" (").append(spanCount + 1).append(" columns)");
    if (!stretch.isZero()) KtGlue.append(buf.append(", stretch "), stretch, strOrder, null);
    if (!shrink.isZero()) KtGlue.append(buf.append(", shrink "), shrink, shrOrder, null);
    log.add(buf.toString());
    cntx.addOn(log, list.nodes());
  }

  public String getDesc() {
    return "unsetbox";
  }

  /* TeXtp[810,811] */
  public KtGlueSetting getSetting(KtDimen excess) {
    KtSizesEvaluator pack = new KtSizesEvaluator();
    pack.addStretch(stretch, strOrder);
    pack.addShrink(shrink, shrOrder);
    pack.evaluate(excess, false);
    return pack.getSetting();
  }
}

// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.HAlignBuilder
// $Id: KtHAlignBuilder.java,v 1.1.1.1 2000/08/08 04:36:35 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.builder.KtHBoxBuilder;

public class KtHAlignBuilder extends KtHBoxBuilder {
  public KtHAlignBuilder(int line) {
    super(line);
  }

  public int getSpaceFactor() {
    return 0;
  }

  public void setSpaceFactor(int sf) {}

  public void resetSpaceFactor() {}

  public void adjustSpaceFactor(int sf) {
    throw new RuntimeException("char not allowed");
  }
}

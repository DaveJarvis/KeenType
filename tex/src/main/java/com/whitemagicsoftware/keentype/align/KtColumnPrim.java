// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.ColumnPrim
// $Id: KtColumnPrim.java,v 1.1.1.1 2001/03/20 02:42:10 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.command.KtLeftBraceToken;
import com.whitemagicsoftware.keentype.command.KtPrim;
import com.whitemagicsoftware.keentype.command.KtRightBraceToken;
import com.whitemagicsoftware.keentype.command.KtToken;

public class KtColumnPrim extends KtPrim {

  private final KtAlignment.KtColumnEnding ending;

  KtColumnPrim(String name, KtAlignment.KtColumnEnding ending) {
    super(name);
    this.ending = ending;
  }

  /* TeXtp[342] */
  public final boolean explosive() {
    return KtAlignment.columnBodyIsActiveAndBalanced();
  }

  /* TeXtp[342] */
  public void detonate(KtToken src) {
    KtAlignment.finishActiveColumnBody(ending);
  }

  /* TeXtp[1127,1128] */
  public void exec(KtToken src) {
    int disbalance = KtAlignment.activeColumnDisbalance();
    if (Math.abs(disbalance) > 2)
      error(
        src.match(KtTabMarkToken.TOKEN) ? "MisplacedTabMark" : "MisplacedCrSpan", meaningOf( src));
    else {
      backToken(src);
      if (disbalance < 0) {
        insertToken(KtLeftBraceToken.TOKEN);
        error("AllignLeftError");
      } else {
        insertToken(KtRightBraceToken.TOKEN);
        error("AllignRightError");
      }
    }
  }
}

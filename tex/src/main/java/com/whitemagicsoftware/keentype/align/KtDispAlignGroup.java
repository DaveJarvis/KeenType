// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.DispAlignGroup
// $Id: KtDispAlignGroup.java,v 1.1.1.1 2001/03/22 13:47:54 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtNum;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.math.KtDisplayGroup;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;

public class KtDispAlignGroup extends KtAlignGroup {

  public KtDispAlignGroup(KtAlignment align) {
    super(align);
  }

  public static final int INTP_PRE_DISPLAY_PENALTY = KtDisplayGroup.INTP_PRE_DISPLAY_PENALTY;
  public static final int INTP_POST_DISPLAY_PENALTY = KtDisplayGroup.INTP_POST_DISPLAY_PENALTY;
  public static final int DIMP_DISPLAY_INDENT = KtDisplayGroup.DIMP_DISPLAY_INDENT;
  public static final int GLUEP_ABOVE_DISPLAY_SKIP = KtDisplayGroup.GLUEP_ABOVE_DISPLAY_SKIP;
  public static final int GLUEP_BELOW_DISPLAY_SKIP = KtDisplayGroup.GLUEP_BELOW_DISPLAY_SKIP;

  /* TeXtp[812,1206] */
  public void stop() {
    KtConfig cfg = getConfig();
    KtDimen indent = cfg.getDimParam(DIMP_DISPLAY_INDENT);
    KtNodeEnum nodes = align.finish(indent);
    if (expectMathShift()) KtDisplayGroup.expectAnotherMathShift();
    KtBuilder.pop();
    KtBuilder bld = KtBuilder.top();
    bld.addPenalty(KtNum.valueOf(cfg.getIntParam(INTP_PRE_DISPLAY_PENALTY)));
    bld.addSkip(
        cfg.getGlueParam(GLUEP_ABOVE_DISPLAY_SKIP), cfg.getGlueName(GLUEP_ABOVE_DISPLAY_SKIP));
    bld.addNodes(nodes);
    bld.addPenalty(KtNum.valueOf(cfg.getIntParam(INTP_POST_DISPLAY_PENALTY)));
    bld.addSkip(
        cfg.getGlueParam(GLUEP_BELOW_DISPLAY_SKIP), cfg.getGlueName(GLUEP_BELOW_DISPLAY_SKIP));
    align.copyPrevParameters(bld);
  }

  public void close() {
    killLevel();
    KtDisplayGroup.resumeAfterDisplay();
  }

  /* TeXtp[1207] */
  protected static boolean expectMathShift() {
    KtToken tok = nextNonAssignment();
    if (!meaningOf(tok).isMathShift()) {
      backToken(tok);
      error("MissingFormulaEnd");
      return false;
    }
    return true;
  }
}

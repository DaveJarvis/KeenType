// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.VertAlignment
// $Id: KtVertAlignment.java,v 1.1.1.1 2001/03/21 08:16:51 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtGlue;
import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtListBuilder;
import com.whitemagicsoftware.keentype.builder.KtVBoxBuilder;
import com.whitemagicsoftware.keentype.command.KtToken;
import com.whitemagicsoftware.keentype.command.KtTokenList;
import com.whitemagicsoftware.keentype.io.KtLog;
import com.whitemagicsoftware.keentype.node.KtAnyBoxNode;
import com.whitemagicsoftware.keentype.node.KtAnySkipNode;
import com.whitemagicsoftware.keentype.node.KtBoxSizes;
import com.whitemagicsoftware.keentype.node.KtGlueSetting;
import com.whitemagicsoftware.keentype.node.KtNamedVSkipNode;
import com.whitemagicsoftware.keentype.node.KtNodeEnum;
import com.whitemagicsoftware.keentype.node.KtNodeList;
import com.whitemagicsoftware.keentype.node.KtSizesSummarizer;
import com.whitemagicsoftware.keentype.node.KtVBoxNode;
import com.whitemagicsoftware.keentype.node.KtVSkipNode;
import com.whitemagicsoftware.keentype.node.KtVertIterator;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

public class KtVertAlignment extends KtAlignment {

  protected final KtListBuilder builder;

  public KtVertAlignment(
      KtDimen size,
      boolean exactly,
      KtTokenList.KtInserter everyCr,
      KtToken frzCr,
      KtToken frzEndt,
      KtListBuilder builder) {
    super(size, exactly, everyCr, frzCr, frzEndt);
    this.builder = builder;
  }

  /* TeXtp[785] */
  protected void startNonAligned() {
    super.startNonAligned();
    KtTypoCommand.getTypoConfig().resetParagraph();
  }

  /* TeXtp[787] */
  protected void startSpan(int index) {
    super.startSpan(index);
    KtTypoCommand.getTypoConfig().resetParagraph();
  }

  protected KtVBoxBuilder rowBuilder;
  protected KtVBoxBuilder spanBuilder;

  /* TeXtp[786] */
  protected void pushNewRowBuilder() {
    // XXX[786] prev_depth = 0
    rowBuilder = new KtVAlignBuilder(currLineNumber());
    KtBuilder.push(rowBuilder);
  }

  /* TeXtp[787] */
  protected void pushNewSpanBuilder() {
    spanBuilder = new KtVBoxBuilder(currLineNumber());
    KtBuilder.push(spanBuilder);
  }

  protected void addNamedSkipToRow(KtGlue skip, String name) {
    rowBuilder.addNamedSkip(skip, name);
  }

  /* TeXtp[796] */
  protected KtDimen packedSpanSize(int spanCount) {
    KtNodeList list = spanBuilder.getList();
    KtBuilder.pop();
    spanBuilder = null;
    KtSizesSummarizer pack = new KtSizesSummarizer();
    KtVertIterator.summarize(list.nodes(), pack);
    KtDimen size = pack.getBody().plus(pack.getHeight()).plus(pack.getDepth());
    KtBoxSizes sizes = new KtBoxSizes(size, pack.getWidth(), KtDimen.ZERO, pack.getLeftX());
    byte strOrder = pack.maxTotalStr();
    byte shrOrder = pack.maxTotalShr();
    rowBuilder.addNode(
        new KtAnyUnsetNode(
            sizes,
            list,
            spanCount,
            pack.getTotalStr(strOrder),
            strOrder,
            pack.getTotalShr(shrOrder),
            shrOrder));
    return size;
  }

  /* TeXtp[799] */
  protected void packRow() {
    KtNodeList list = rowBuilder.getList();
    KtBuilder.pop();
    rowBuilder = null;
    KtTypoCommand.appendBox(
        builder, new KtAnyUnsetNode(KtVertIterator.naturalSizes(list.nodes(), KtDimen.NULL), list));
  }

  protected KtNodeEnum getUnsetNodes() {
    return builder.getList().nodes();
  }

  protected KtDimen getRelevantSize(KtBoxSizes sizes) {
    return sizes.getHeight();
  }

  protected KtBoxSizes transformSizes(KtBoxSizes sizes, KtDimen dim) {
    return sizes.withHeight(dim);
  }

  protected KtAnyBoxNode makeBox(KtBoxSizes sizes, KtGlueSetting setting, KtNodeList list) {
    return new KtVBoxNode(sizes, setting, list);
  }

  protected KtAnySkipNode makeSkip(KtGlue skip) {
    return new KtVSkipNode(skip);
  }

  protected KtAnySkipNode makeSkip(KtGlue skip, String name) {
    return new KtNamedVSkipNode(skip, name);
  }

  protected KtTypoCommand.KtAnyBoxPacker makeBoxPacker() {
    return new KtTypoCommand.KtVBoxPacker() {
      /* TeXtp[675] */
      protected void reportLocation(KtLog log) {
        log.add("in alignment at lines ")
            .add(builder.getStartLine())
            .add("--")
            .add(currLineNumber());
      }
    };
  }

  public void copyPrevParameters(KtBuilder bld) {
    bld.setSpaceFactor(builder.getSpaceFactor());
    // XXX[812] language
  }
}

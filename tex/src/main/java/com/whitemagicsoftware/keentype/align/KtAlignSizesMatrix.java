// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.align.AlignSizesMatrix
// $Id: KtAlignSizesMatrix.java,v 1.1.1.1 2000/08/10 03:28:29 ksk Exp $
package com.whitemagicsoftware.keentype.align;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.base.KtIntPairKey;

public class KtAlignSizesMatrix {

  private final Vector<KtDimen> diagonal;
  private final Map<KtIntPairKey, KtDimen> matrix = new HashMap<>(64);

  public KtAlignSizesMatrix(int initialSize) {
    diagonal = new Vector<>( initialSize );
  }

  public int size() {
    return diagonal.size();
  }

  public KtDimen get(int i) {
    return i < diagonal.size() ? diagonal.get( i) : KtDimen.NULL;
  }

  public KtDimen get(int i, int j) {
    return i != j ? matrix.get( new KtIntPairKey( i, j)) : get( i);
  }

  public KtDimen set(int i, int j, KtDimen value) {
    if (i != j) return matrix.put( new KtIntPairKey( i, j), value);
    if (i >= diagonal.size()) diagonal.setSize(i + 1);
    return diagonal.set( i, value);
  }

  public void setMax(int i, int j, KtDimen value) {
    if (i != j) {
      KtIntPairKey key = new KtIntPairKey(i, j);
      KtDimen old = matrix.get( key);
      if (old == KtDimen.NULL || value.moreThan(old)) matrix.put(key, value);
    } else if (i >= diagonal.size()) {
      diagonal.setSize(i + 1);
      diagonal.set(i, value);
    } else {
      KtDimen old = diagonal.get( i);
      if (old == KtDimen.NULL || value.moreThan(old)) diagonal.set(i, value);
    }
  }
}

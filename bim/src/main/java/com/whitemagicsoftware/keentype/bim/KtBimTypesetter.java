/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.bim;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.render.KtCoordinateStack;
import com.whitemagicsoftware.keentype.render.KtPendingRule;
import com.whitemagicsoftware.keentype.render.KtTurtle;
import com.whitemagicsoftware.keentype.tex.KtFileFormat;
import com.whitemagicsoftware.keentype.tex.KtFontInfo;
import com.whitemagicsoftware.keentype.tex.KtFontInformator;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.whitemagicsoftware.keentype.base.KtDimen.ZERO;
import static com.whitemagicsoftware.keentype.tex.KtFileFormat.SVG_EXT;

/**
 * Responsible for typesetting characters into an SVG diagram.
 */
public final class KtBimTypesetter implements KtTypesetter {
  private final OutputStream mOutput;
  private final KtFontInformator mFontInf;

  private KtBimFormatWriter mWriter;
  private KtTurtle mTurtle;
  private KtCoordinateStack mStack;

  private final Map<KtFontMetric, KtFontInfo> mFontMetrics =
    new HashMap<>( 64 );
  private KtFontMetric mCurrentFontMetric = KtFontMetric.NULL;
  private KtFontInfo mCurrentFontInfo = KtFontInfo.NULL;
  private KtPendingRule mPendingRule = KtPendingRule.NULL;

  private KtDimen mPageWidth;
  private KtDimen mPageHeight;
  private final boolean mFullPage;

  /**
   * @param os       Where to write the final document.
   * @param fontInf  Initial font information to use for drawing primitives.
   * @param fullPage Set to {@code true} to use the page dimensions provided
   *                 when
   *
   *
   *               {@link #startPage(KtDimen, KtDimen, KtDimen, KtDimen, int[])}
   *                 is called; use {@code false} to use the natural dimensions
   *                 derived from the extents of the drawing primitive boundary.
   */
  public KtBimTypesetter(
    final Function<KtFileFormat, OutputStream> os,
    final KtFontInformator fontInf,
    final boolean fullPage ) {
    assert os != null;
    assert fontInf != null;

    mOutput = os.apply( SVG_EXT );
    mFontInf = fontInf;
    mFullPage = fullPage;
  }

  @Override
  public void set( final char ch, final KtFontMetric metric ) {
    mTurtle.sync();

    if( !metric.equals( mCurrentFontMetric ) ) {
      final var info = mFontMetrics.computeIfAbsent(
        metric, k -> mFontInf.getInfo( metric )
      );

      mTurtle.font( info );

      mCurrentFontMetric = metric;
      mCurrentFontInfo = info;
    }

    final var w = mCurrentFontInfo.getCharWidth( ch );

    if( w != KtDimen.NULL ) {
      mTurtle.draw( ch );
      mTurtle.advance( w );
    }
  }

  public void set( final KtCharCode code, final KtFontMetric metric ) {
    set( code.toChar(), metric );
  }

  @Override
  public void setRule( final KtDimen h, final KtDimen w ) {
    if( !h.lessThan( 0 ) && !w.lessThan( 0 ) ) {
      mTurtle.sync();
      mPendingRule = new KtPendingRule( mTurtle, w, h );
    }
  }

  @Override
  public void setSpecial( final byte[] spec ) {
    mTurtle.sync();
  }

  @Override
  public void moveLeft( final KtDimen x ) {
    mTurtle.left( x );
    tryToSetPendRule();
  }

  @Override
  public void moveRight( final KtDimen x ) {
    mTurtle.right( x );
    tryToSetPendRule();
  }

  @Override
  public void moveUp( final KtDimen y ) {
    mTurtle.up( y );
  }

  @Override
  public void moveDown( final KtDimen y ) {
    mTurtle.down( y );
  }

  @Override
  public KtMark mark() {
    return mTurtle.mark();
  }

  @Override
  public void syncHoriz() {
    putPendRule();
    mTurtle.syncHorizontal();
  }

  @Override
  public void syncVert() {
    putPendRule();
    mTurtle.syncVertical();
  }

  @Override
  public void push() {
    putPendRule();
    mStack = mTurtle.stack( mStack );
  }

  @Override
  public void pop() {
    putPendRule();

    if( mStack != KtCoordinateStack.NULL ) {
      mTurtle.moveSvgTo( mStack.x(), mStack.y() );
      mStack = mStack.next();
    }
  }

  /**
   * @param fileName KtName of the file to write to.
   */
  @Override
  public void setFileName( final KtFileName fileName ) {
  }

  @Override
  public Optional<KtFileName> getFileName() {
    return Optional.empty();
  }

  @Override
  public void startDocument( final OutputStream os ) {
  }

  @Override
  public void startPage(
    final KtDimen yOffset,
    final KtDimen xOffset,
    final KtDimen height,
    final KtDimen width,
    final int[] paragraphs ) {

    mPageWidth = width.plus( xOffset );
    mPageHeight = height.plus( yOffset );

    mCurrentFontMetric = KtFontMetric.NULL;
    mCurrentFontInfo = KtFontInfo.NULL;

    mWriter = new KtBimFormatWriter();
    mTurtle = new KtTurtle( ZERO, ZERO, xOffset, yOffset, mWriter );
    mStack = KtCoordinateStack.NULL;
  }

  @Override
  public int pageCount() {
    return 1;
  }

  @Override
  public void endPage() {
  }

  @Override
  public void close() {
    final var doc = mFullPage
      ? mWriter.export( mPageWidth.toDouble(), mPageHeight.toDouble() )
      : mWriter.export();

    try {
      ImageIO.write( doc, "png", mOutput );
    } catch( final IOException e ) {
      throw new UncheckedIOException( e );
    } finally {
      try {
        mOutput.close();
      } catch( final IOException ignored ) { }
    }
  }

  @Override
  public KtFileFormat getExtension() {
    return null;
  }

  private void putPendRule() {
    if( mPendingRule != KtPendingRule.NULL ) {
      mPendingRule.put();
      mPendingRule = KtPendingRule.NULL;
    }
  }

  private void tryToSetPendRule() {
    if( mPendingRule != KtPendingRule.NULL && mPendingRule.canBeSet() ) {
      mPendingRule.set();
      mPendingRule = KtPendingRule.NULL;
    }
  }
}

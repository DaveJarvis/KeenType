/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.bim;

import com.whitemagicsoftware.keentype.render.KtCanvas;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import static com.whitemagicsoftware.keentype.render.KtFontReader.DEFAULT_FONT;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

/**
 * Responsible for building a {@link BufferedImage} version of a TeX formula.
 * <p>
 * This class is not thread-safe.
 * </p>
 */
public final class KtBimFormatWriter implements KtCanvas<BufferedImage> {

  private static final FontRenderContext RENDER_CONTEXT =
    new FontRenderContext( null, true, true );

  private BufferedImage mImage;

  /**
   * Font used when drawing glyph paths.
   */
  private Font mFont = DEFAULT_FONT;

  /**
   * Maximum canvas width, updated while drawing.
   */
  private double mW;

  /**
   * Maximum canvas height, updated while drawing.
   */
  private double mH;

  /**
   * Creates a new instance with a default buffer size.
   */
  public KtBimFormatWriter() { }

  /**
   * Changes the {@link Font} used when drawing glyphs.
   *
   * @param font The new text {@link Font}.
   */
  @Override
  public void setFont( final Font font ) {
    assert font != null;
    mFont = font;
  }

  /**
   * Adds a string to the vector graphic.
   *
   * @param s The string to place on the graphics.
   * @param x The upper-left X coordinate.
   * @param y The upper-left Y coordinate.
   */
  @Override
  public void drawString( final String s, final double x, final double y ) {
    assert s != null;

    final var glyphs = mFont.createGlyphVector( RENDER_CONTEXT, s );
    final var path = (Path2D) glyphs.getOutline( (float) x, (float) y );
    final var bounds = path.getBounds();

    mImage = updateExtents(
      bounds.getX() + bounds.getWidth(),
      bounds.getY() + bounds.getHeight()
    );

    // TODO: Draw the character.
  }

  public void drawLine(
    final double x1, final double y1,
    final double x2, final double y2,
    final double strokeWidth ) {

    mImage = updateExtents( x2, y2 );

    // TODO: Draw the line.
  }

  @Override
  public BufferedImage export() {
    // TODO: Crop the image to its true max width and height.

    return mImage;
  }

  public BufferedImage export( final double scale ) {
    return mImage;
  }

  @Override
  public BufferedImage export(
    final double w, final double h, final double scale ) {
    return mImage;
  }

  /**
   * When drawing primitives are called, track the maximum width and height
   * so that a minimal page boundary can be formed around the SVG document.
   * If either the given width or height exceed the current maximum extents,
   * then extents are embiggened to enclose the whole diagram.
   *
   * @param w The width boundary to compare against the running maximum.
   * @param h The height boundary to compare against the running maximum.
   */
  private BufferedImage updateExtents( final double w, final double h ) {
    final var image = mImage;

    if( w > mW ) {
      mW = w;
    }

    if( h > mH ) {
      mH = h;
    }

    // TODO: Bump the image size by a larger amount than needed to prevent
    //       superfluous resizes. Later, crop the image to (mW, mH).

    return w > image.getWidth() || h > image.getHeight()
      ? resize( image, w, h )
      : image;
  }

  private static BufferedImage resize(
    final BufferedImage source, final double w, final double h ) {
    final var result = new BufferedImage( (int) w, (int) h, TYPE_INT_ARGB );
    final var src = getData( source );
    final var dst = getData( result );
    System.arraycopy( src, 0, dst, 0, src.length );
    return result;
  }

  private static byte[] getData( final BufferedImage image ) {
    return image == null
      ? new byte[ 0 ]
      : ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
  }
}

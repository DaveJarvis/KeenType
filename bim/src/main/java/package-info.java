/**
 * This package contains instructions for transforming TeX drawing primitives
 * into a {@link java.awt.image.BufferedImage} (BIM).
 */
package com.whitemagicsoftware.keentype.bim;

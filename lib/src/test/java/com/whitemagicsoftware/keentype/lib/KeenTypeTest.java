/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.lib;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;

class KeenTypeTest {
  private static final Set<String> EQUATIONS = Set.of(
    "$\\root n \\of{\\pi}$",
    "$\\vec{u}^2 \\tilde{\\nu}$",
    "$\\rightleftharpoons\\angle\\hbar$",
    "$E=mc^2$",
    "$e^{\\pi i} + 1 = 0$",
    "$(a+b)^2=a^2 + 2ab + b^2$",
    "$\\lim\\limits_{x \\to \\infty} f(x)$",
    "$\\int_{a}^{b} x^2 dx$",
    "$\\oint_V f(s) \\,ds$",
    "$\\prod\\limits_{i=a}^{b} f(i)$",
    "$G_{\\mu \\nu} = \\frac{8 \\pi G}{c^4} T_{{\\mu \\nu}}$",
    "$S_x = \\sqrt{{SS_x}/{N-1}}$",
    "$\\sigma=\\sqrt{\\sum\\limits_{i=1}^{k} p_i(x_i-\\mu)^2}$",
    "$-\\frac{{\\hbar ^2 }}{{2m}}\\frac{{\\partial ^2 \\psi (x,t)}}{{\\partial x^2 }} + U(x)\\psi (x,t) = i\\hbar \\frac{{\\partial\\psi (x,t)}}{{\\partial t}}$",
    "$u(n) \\Leftrightarrow \\frac{1}{1-e^{-jw}} + \\sum\\limits_{k=-\\infty}^{\\infty} \\pi \\delta (\\omega + 2\\pi k)$",
    "$\\sum\\limits_{i=1}^n i = \\biggl(\\sum\\limits_{i=1}^{n-1} i\\biggr) + n =\\frac{(n-1)(n)}{2} + n = \\frac{n(n+1)}{2}$",
    "$\\root n \\of{|z| . e^{i \\theta}} = \\root n \\of{|z| . e^{i (\\frac{\\theta + 2 k \\pi}{n})}}, k \\in \\lbrace 0, ..., n-1 \\rbrace, n \\in NN$"
  );

  @Test
  void test_Typesetting_Equation_Publishable() {
    final var typesetter = new KeenType();

    EQUATIONS.forEach( e -> {
      final var svg = typesetter.toSvg( e );
      assertFalse( svg.isBlank() );
    } );
  }
}

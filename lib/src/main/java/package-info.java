/**
 * This package contains a library that converts TeX instructions into
 * publishable documents in various formats. The library is intended to be
 * integrated into applications. As such, any errors will not result in
 * prompting the user for further input (such as missing files).
 */

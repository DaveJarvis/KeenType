/* Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */
package com.whitemagicsoftware.keentype.lib;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtPageBuilder;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.io.KtLineInput;
import com.whitemagicsoftware.keentype.math.KtMathPrim;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.svg.KtSvgTypesetter;
import com.whitemagicsoftware.keentype.tex.*;
import com.whitemagicsoftware.keentype.typo.KtPage;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;

import static com.whitemagicsoftware.keentype.tex.KtFileFormat.INPUT_EXT;
import static com.whitemagicsoftware.keentype.tex.KtTeXCharMapper.KtTeXFileName;
import static com.whitemagicsoftware.keentype.tex.KtTeXCharMapper.setConfig;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Responsible for typesetting math into various formats, such as SVG. This
 * is the main entry point for using the library to transform math expressions
 * in TeX format into formats suitable for rendering.
 */
public final class KeenType {

  /**
   * Stream where SVG document are written.
   */
  private final ByteArrayOutputStream mOut = new ByteArrayOutputStream();
  private final KtFileName mFileName = new KtTeXFileName( "ext/math" );
  private final KtTeXIOHandler mHandler;
  private final KtTeXCharMapper mMapper;
  private final KtPrimitives mConfig;

  {
    // TODO: Instantiation order matters; could use a builder pattern.
    mMapper = new KtTeXCharMapper();
    mConfig = new KtPrimitives();
  }

  /**
   * Creates a new {@link KeenType} object for typesetting mathematical
   * expressions using TeX-based commands. This initializes the TeX
   * processing engine and reads plain TeX into memory.
   */
  public KeenType() {
    this( true );
  }

  /**
   * Creates a new {@link KeenType} object for typesetting mathematical
   * expressions using TeX-based commands. This initializes the TeX
   * processing engine and reads plain TeX into memory.
   *
   * @param fullPage Set to {@code false} to typeset the primitives within
   *                 their minimal extents.
   */
  public KeenType( final boolean fullPage ) {
    KtCommand.setConfig( mConfig.getCommandConfig() );
    KtTypoCommand.setTypoConfig( mConfig.getTypoConfig() );
    KtMathPrim.setMathConfig( mConfig.getMathConfig() );
    setConfig( mConfig.getCharMapConfig() );

    KtBuilder.push( new KtPageBuilder( 0, KtPage.getPageSplit() ) );

    KtTeXTokenMaker maker = new KtTeXTokenMaker( mConfig );
    KtTeXContextDisplay display = new KtTeXContextDisplay(
      mConfig, mMapper, mMapper
    );
    KtConfig ioConfig = mConfig.getIOHandConfig();
    KtTeXFontHandler fontHand = new KtTeXFontHandler(
      mConfig.getFontHandConfig()
    );
    KtTypesetter typeSetter = new KtSvgTypesetter( fontHand, fullPage );
    mHandler = new KtTeXIOHandler(
      ioConfig, mMapper, maker, display, typeSetter
    );
    KtTypoCommand.setTypoHandler( fontHand );
    KtTypoCommand.setTypesetter( typeSetter );

    KtCommand.setIOHandler( mHandler );

    try( final var fileInput = KtFileOpener.readFile( mFileName, INPUT_EXT ) ) {
      final var fileReader = KtTeXIOHandler.makeReader( fileInput );
      KtLineInput input = new KtLineInput( fileReader, mMapper );
      KtTokenizerStack stack = new KtTokenizerStack();
      stack.push( new KtLineInputTokenizer( input, mHandler, mFileName ) );
      KtCommand.setInput( input );
      KtCommand.setTokStack( stack );
      KtCommand.mainLoop();
    } catch( final IOException e ) {
      throw new UncheckedIOException( e );
    }
  }

  /**
   * Typesets a math expression into a scalable vector graphic.
   *
   * @param expression The expression to typeset, which must be surrounded by
   *                   {@code $} or {@code $$} sigils on both sides of the
   *                   string. The sigils must be present for the expression
   *                   be evaluated as math.
   * @return An SVG document.
   */
  public String toSvg( final String expression ) {
    return toSvg( expression, 1.0 );
  }

  public String toSvg( final String expression, final double scale ) {
    final var reader = new StringReader( expression + "\\end" );
    final var input = new KtLineInput( reader, mMapper );
    final var stack = new KtTokenizerStack();
    stack.push( new KtLineInputTokenizer( input, mHandler, mFileName ) );

    KtCommand.setInput( input );
    KtCommand.setTokStack( stack );

    mHandler.startDocument( mOut );
    mHandler.setScale( scale );
    KtCommand.mainLoop();
    mHandler.stopDocument();

    KtCommand.cleanUp();
    KtCondPrim.cleanUp();

    final var result = mOut.toString( UTF_8 );

    mOut.reset();

    return result;
  }

  /*
   * Typesets a math expression into a document object model. This generates
   * the DOM directly, without first generating an SVG string.
   *
   * @param tex The TeX expression to typeset, which must be surrounded by
   *            {@code $} or {@code $$} sigils on both sides of the string.
   *            If the sigils are not present, the expression will not be
   *            evaluated as math.
   * @return A document object model representing an SVG document.
   */
//  public Document toDom( final String tex ) {
//    return null;
//  }
}

/**
 * This package contains an application that can be run from the command-line
 * to convert plain TeX documents into a publishable document file format
 * (e.g., DVI, SVG, or similar format).
 */

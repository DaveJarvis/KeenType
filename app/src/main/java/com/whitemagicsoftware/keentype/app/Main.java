// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.tex.TeX
package com.whitemagicsoftware.keentype.app;

import com.whitemagicsoftware.keentype.builder.KtBuilder;
import com.whitemagicsoftware.keentype.builder.KtPageBuilder;
import com.whitemagicsoftware.keentype.bus.KtBus;
import com.whitemagicsoftware.keentype.command.*;
import com.whitemagicsoftware.keentype.io.KtInputLine;
import com.whitemagicsoftware.keentype.io.KtLineInput;
import com.whitemagicsoftware.keentype.io.KtLineOutput;
import com.whitemagicsoftware.keentype.io.KtWriterLineOutput;
import com.whitemagicsoftware.keentype.log.KtLogEventHandler;
import com.whitemagicsoftware.keentype.math.KtMathPrim;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.svg.KtSvgTypesetter;
import com.whitemagicsoftware.keentype.tex.*;
import com.whitemagicsoftware.keentype.typo.KtPage;
import com.whitemagicsoftware.keentype.typo.KtTypoCommand;

import java.io.*;

import static com.whitemagicsoftware.keentype.tex.KtFileFormat.FMT_EXT;
import static com.whitemagicsoftware.keentype.tex.KtFileOpener.readFile;
import static com.whitemagicsoftware.keentype.tex.KtTeXCharMapper.KtTeXFileName;
import static com.whitemagicsoftware.keentype.tex.KtTeXCharMapper.setConfig;

public final class Main {

  private Main() { }

  /**
   * KtMain entrypoint to demonstrate how to typeset using the API.
   *
   * @param args KtCommand-line arguments.
   */
  public static void main( final String[] args ) {
    OutputStream stdout = new FileOutputStream( FileDescriptor.out );
    Writer stdWr = KtTeXIOHandler.makeWriter( stdout );
    PrintWriter out = new PrintWriter( stdWr, true );

    out.println( KtTeXConfig.BANNER );

    KtTeXCharMapper mapper = new KtTeXCharMapper();

    /* mapper has no config yet */
    KtToken.setCharCodeMaker( mapper);

    Reader stdin = KtTeXIOHandler.makeReader( System.in );
    KtLineInput input = new KtLineInput( stdin, mapper );
    KtInputLine firstLine = KtInputLine.NULL;

    String fmtName = System.getProperty("nts.fmt");

    if (args.length > 0) {
      StringBuilder buf = new StringBuilder( args[ 0 ] );

      for( int i = 1; i < args.length; i++ ) {
        buf.append( ' ' ).append( args[ i ] );
      }

      firstLine = new KtInputLine( buf.toString(), mapper );
    }

    /* TeXtp[37] */
    for( ; ; ) {
      if( firstLine != KtInputLine.NULL ) {
        firstLine.skipSpaces();
        if( !firstLine.empty() ) { break; }
        out.println( "Please type the name of your input file." );
      }

      out.write( "**" );
      out.flush();
      firstLine = input.readLine();

      if( firstLine == KtLineInput.EOF ) {
        out.println();
        out.println( "! End of file on the terminal... why?" );
        System.exit( 1 );
      }
    }

    KtPrimitives config = null;
    Object fontSeed = null;

    if( fmtName != null ) {
      final var name = new KtTeXFileName();

      name.setPath( fmtName );
      name.addDefaultExt( FMT_EXT.toString() );

      try {
        try(
          final var is = readFile( name, FMT_EXT );
          final var ois = new ObjectInputStream( is ) ) {
          config = (KtPrimitives) ois.readObject();
          fontSeed = ois.readObject();
        }
      } catch( final IOException | ClassNotFoundException e ) {
        System.exit( 2 );
      }
    }
    else {
      config = new KtPrimitives();
    }

    KtCommand.setConfig( config.getCommandConfig() );
    KtTypoCommand.setTypoConfig( config.getTypoConfig() );
    KtMathPrim.setMathConfig( config.getMathConfig() );
    setConfig( config.getCharMapConfig() );

    firstLine = firstLine.addEndOfLineChar();
    KtInputLine savedFirstLine = new KtInputLine(firstLine);
    KtLineOutput terminal = new KtWriterLineOutput(
      stdWr, mapper, true, KtTeXConfig.MAX_PRINT_LINE
    );
    KtConfig ioConfig = config.getIOHandConfig();
    KtTeXTokenMaker maker = new KtTeXTokenMaker( config );
    KtTeXFontHandler.KtConfig fontConfig = config.getFontHandConfig();
    KtTeXFontHandler fontHand = new KtTeXFontHandler( fontConfig, fontSeed );
    KtTypesetter typeSetter = new KtSvgTypesetter( fontHand, true );
    KtTypoCommand.setTypoHandler( fontHand );
    KtTypoCommand.setTypesetter( typeSetter );
    KtTeXContextDisplay display = new KtTeXContextDisplay( config, mapper, mapper );
    KtTeXIOHandler ioHand = new KtTeXIOHandler(
      ioConfig, mapper, maker, display, typeSetter
    );
    KtLogEventHandler logEventHandler = new KtLogEventHandler(
      ioConfig, savedFirstLine, mapper
    );
    KtTokenizerStack tokStack = new KtTokenizerStack();

    tokStack.push( new KtStdinTokenizer( ioHand.getTokenMaker(), firstLine ) );

    KtCommand.setIOHandler( ioHand );
    KtCommand.setInput( input );
    KtCommand.setTerminal( terminal );
    KtCommand.setTokStack( tokStack );
    KtBuilder.push( new KtPageBuilder( 0, KtPage.getPageSplit() ) );

    KtEditException edit = null;
    int status = 0;

    try {
      if( ioHand.getTokenMaker()
                .scanCat( firstLine.peekNextRawCode() ) != KtInputLineTokenizer.ESCAPE ) {
        KtCommand.startInput();
      }

      ioHand.startDocument();

      final boolean dumping = KtCommand.mainLoop();
      ioHand.setAfterEnd();
      KtCommand.cleanUp();
      KtCondPrim.cleanUp();

      if (dumping) {
        try {
          config.preparePatterns();
          ObjectOutputStream dumper = ioHand.getDumper();
          long time = System.currentTimeMillis();
          dumper.writeObject(config);
          dumper.writeObject(fontHand.getSeed());
          time = System.currentTimeMillis() - time;
          System.err.println("format file stored in " + time + " milliseconds");
          dumper.close();
        } catch (final IOException e) {
          status = 2;
          e.printStackTrace(System.err);
        }
      }
    } catch (final KtFatalError e) {
      status = 1;
    } catch (final KtEditException e) {
      edit = e;
    } catch (final OutOfMemoryError e) {
      status = 2;
      e.printStackTrace(System.err);
    }

    // XXX TeXtp[1333]
    ioHand.stopDocument();
    KtFileName logName = logEventHandler.getLogName();
    ioHand.closeLogFile();
    terminal.startLine();

    if (logName != KtFileName.NULL && !KtInteractionPrim.isSilent()) {
      terminal.add("Transcript written on " + logName.getPath() + '.');
      // XXX getPath() is wrong here
      terminal.endLine();
    }

    terminal.close();

    if( edit != null ) {
      edit.exec();
    }

    KtBus.unregister( logEventHandler );

    System.exit(status);
  }
}

// Copyright 2023 White Magic Software, Ltd. -- All rights reserved.
//
// Copyright 2001 by
// DANTE e.V. and any individual authors listed elsewhere in this file.
//
// This file is part of the NTS system.
// ------------------------------------
//
// It may be distributed and/or modified under the
// conditions of the NTS Public License (NTSPL), either version 1.0
// of this license or (at your option) any later version.
// The latest version of this license is in
//    http://www.dante.de/projects/nts/ntspl.txt
// and version 1.0 or later is part of all distributions of NTS
// version 1.0-beta or later.
//
// Originally: nts.dvi.DviTypeSetter
package com.whitemagicsoftware.keentype.dvi;

import com.whitemagicsoftware.keentype.base.KtDimen;
import com.whitemagicsoftware.keentype.command.KtFileName;
import com.whitemagicsoftware.keentype.io.KtCharCode;
import com.whitemagicsoftware.keentype.node.KtFontMetric;
import com.whitemagicsoftware.keentype.node.KtTypesetter;
import com.whitemagicsoftware.keentype.tex.KtFileFormat;
import com.whitemagicsoftware.keentype.tex.KtConfig;
import com.whitemagicsoftware.keentype.tex.KtFontInfo;
import com.whitemagicsoftware.keentype.tex.KtFontInformator;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.whitemagicsoftware.keentype.tex.KtFileFormat.DVI_EXT;

public class KtDviTypesetter implements KtTypesetter {

  private static final KtPendRule NULL_PEND_RULE = null;

  protected static final int DVI_NUM = 25400000;
  protected static final int DVI_DEN = 473628672;
  protected static final int DIM_DEN = 0x10000;

  private static int dim( KtDimen d ) {
    return d.toInt( DIM_DEN );
  }

  private final KtFontInformator mFontInf;
  private KtFileName mFileName;
  private final KtDviFormatWriter mWriter;

  public KtDviTypesetter(
    final KtFontInformator fontInf,
    final KtConfig config ) {
    assert fontInf != null;
    assert config != null;

    mFontInf = fontInf;
    mWriter = new KtDviFormatWriter(
      DVI_NUM, DVI_DEN, config
    );
  }

  @Override
  public void moveLeft( final KtDimen x ) {
    currX -= dim( x );
    tryToSetPendRule();
  }

  @Override
  public void moveRight( final KtDimen x ) {
    currX += dim( x );
    tryToSetPendRule();
  }

  @Override
  public void moveUp( final KtDimen y ) {
    currY -= dim( y );
  }

  @Override
  public void moveDown( final KtDimen y ) {
    currY += dim( y );
  }

  private final Map<KtFontMetric, KtFontInfo> fontMap = new HashMap<>();
  private KtFontMetric currMetric = KtFontMetric.NULL;
  private KtFontInfo currInfo = KtFontInfo.NULL;

  @Override
  public void set( char ch, KtFontMetric metric ) {
    syncHoriz();
    syncVert();
    if( !metric.equals( currMetric ) ) {
      KtFontInfo info = fontMap.get( metric );
      if( info == KtFontInfo.NULL ) {
        info = mFontInf.getInfo( metric );
        fontMap.put( metric, info );
        mWriter.defFont(
          info.getIdNumber(),
          info.getCheckSum(),
          dim( info.getAtSize() ),
          dim( info.getDesignSize() ),
          info.getDirName(),
          info.getFileName() );
      }
      mWriter.setFont( info.getIdNumber() );
      currMetric = metric;
      currInfo = info;
    }
    KtDimen w = currInfo.getCharWidth( ch );
    if( w == KtDimen.NULL ) { mWriter.putChar( ch ); }
    else {
      mWriter.setChar( ch );
      dviX += dim( w );
    }
  }

  @Override
  public void set( KtCharCode code, KtFontMetric metric ) {
    set( code.toChar(), metric );
  }

  private class KtPendRule {
    private final int height;
    private final int width;

    KtPendRule( int h, int w ) {
      height = h;
      width = w;
    }

    void put() {
      mWriter.putRule( height, width );
    }

    void set() {
      mWriter.setRule( height, width );
      dviX += width;
    }

    boolean canBeSet() {
      return currX == dviX + width;
    }
  }

  private KtPendRule pendRule = NULL_PEND_RULE;

  private void putPendRule() {
    if( pendRule != NULL_PEND_RULE ) {
      pendRule.put();
      pendRule = NULL_PEND_RULE;
    }
  }

  private void tryToSetPendRule() {
    if( pendRule != NULL_PEND_RULE && pendRule.canBeSet() ) {
      pendRule.set();
      pendRule = NULL_PEND_RULE;
    }
  }

  @Override
  public void setRule( KtDimen h, KtDimen w ) {
    int height = dim( h );
    int width = dim( w );
    if( height > 0 && width > 0 ) {
      syncHoriz();
      syncVert();
      pendRule = new KtPendRule( height, width );
    }
  }

  @Override
  public void setSpecial( byte[] spec ) {
    syncHoriz();
    syncVert();
    mWriter.setSpecial( spec );
  }

  @Override
  public KtTypesetter.KtMark mark() {
    return new KtTypesetter.KtMark() {

      private final int x = currX;
      private final int y = currY;

      public void move() {
        currX = x;
        currY = y;
      }

      public KtDimen xDiff() {
        return KtDimen.valueOf( currX - x, DIM_DEN );
      }

      public KtDimen yDiff() {
        return KtDimen.valueOf( currY - y, DIM_DEN );
      }
    };
  }

  // XXX provide also mark origin
  // XXX (for example for uniform visualisation dashed lines)

  private record KtLevel( int x, int y, KtLevel next ) {
    static final KtLevel NULL = null;
  }

  private int maxH = 0;
  private int maxW = 0;

  private int currX = 0;
  private int currY = 0;

  private int dviX = 0;
  private int dviY = 0;

  private KtLevel stack = KtLevel.NULL;

  @Override
  public void syncHoriz() {
    putPendRule();
    if( dviX != currX ) {
      mWriter.moveX( currX - dviX );
      dviX = currX;
    }
  }

  @Override
  public void syncVert() {
    putPendRule();
    if( dviY != currY ) {
      mWriter.moveY( currY - dviY );
      dviY = currY;
    }
  }

  @Override
  public void push() {
    putPendRule();
    mWriter.push();
    stack = new KtLevel( dviX, dviY, stack );
  }

  @Override
  public void pop() {
    putPendRule();
    if( stack != KtLevel.NULL ) {
      dviX = stack.x;
      dviY = stack.y;
      stack = stack.next;
      mWriter.pop();
    }
    else { throw new RuntimeException( "too many pops in DVI" ); }
  }

  @Override
  public void startDocument( final OutputStream os ) {
    mWriter.startDocument( os );
  }

  @Override
  public void startPage(
    final KtDimen yOffset,
    final KtDimen xOffset,
    final KtDimen height,
    final KtDimen width,
    final int[] paragraphs ) {
    int y = dim( yOffset );
    int x = dim( xOffset );
    int h = dim( height ) + y;
    int w = dim( width ) + x;

    if( maxH < h ) {
      maxH = h;
    }

    if( maxW < w ) {
      maxW = w;
    }

    currMetric = KtFontMetric.NULL;
    currInfo = KtFontInfo.NULL;
    dviX = dviY = 0;
    stack = KtLevel.NULL;
    currX = x;
    currY = y;

    mWriter.startPage( paragraphs );
  }

  @Override
  public int pageCount() {
    return mWriter.pageCount();
  }

  @Override
  public void endPage() {
    putPendRule();
    mWriter.endPage();
  }

  @Override
  public void close() {
    putPendRule();
    mWriter.close( maxH, maxW );
  }

  @Override
  public void setFileName(final KtFileName fileName) {
    assert fileName != null;
    mFileName = addExtension( fileName );
  }

  @Override
  public Optional<KtFileName> getFileName() {
    return Optional.ofNullable( mFileName );
  }

  @Override
  public KtFileFormat getExtension() {
    return DVI_EXT;
  }
}
